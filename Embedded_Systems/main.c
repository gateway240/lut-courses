#include <stdio.h>
#include <stdint.h>

#define NBR_EVENTS 5
#define CHAR_IN 0		// Char pressed
#define DELETE 1		// Delete pressed
#define ENTER 2			// Enter pressed
#define EMPTY_BUFFER 3		// Buffer empty
#define SENT_BUFFER 4		// Buffer sent

#define NBR_STATES 4
#define BUFFER_EMPTY 0		// Wait and rest
#define CHAR_TO_BUFFER 1	//
#define CHAR_FROM_BUFFER 2	//
#define SEND_BUFFER 3		// Send to communication media


const char StateChngTable[NBR_STATES][NBR_EVENTS] =
// event CHAR_IN DELETE ENTER BUFFER_EMPTY BUFFER_SENT
{ 1, 0, 0, 0, 0,		// BUFFER_EMPTY
  1, 2, 3, 1, 1,		// CHAR_TO_BUFFER
  1, 2, 3, 2, 2,		// CHAR_FROM_BUFFER
  3, 3, 3, 3, 0,		// SEND_BUFFER
};

char
ProcessEvent (char Event)
{
  static char CurrentState = BUFFER_EMPTY;
  printf ("Current State: %d \n", CurrentState);
  if (Event <= NBR_EVENTS)
    {
      CurrentState = StateChngTable[CurrentState][Event];
    }
  return CurrentState;		// if (Event> NBR_EVENTS) just return state
}

char
CurrentState (char event)
{
  printf ("Received event: %d\n", event);
  return ProcessEvent (event);	// handle new event
}

int
main ()
{
  char buffer[50];
  unsigned int buf_size = 0;
  char input = 0;
  while (1)
    {
      scanf ("%c", &input);
      // flushes the standard input
      // (clears the input buffer)
      while ((getchar ()) != '\n');
      printf ("Char: %d \n", input);
      char state;
      switch (input)
	{
	case 10:		// enter key
	  state = CurrentState (ENTER);
	  break;
	case 27:		// delete Key
	  state = CurrentState (DELETE);
	case 8:		// backspace Key
	  state = CurrentState (DELETE);
	default:
	  state = CurrentState (CHAR_IN);
	}
      switch (state)
	{
	case BUFFER_EMPTY:
	  break;
	case CHAR_TO_BUFFER:
	  buffer[buf_size++] = input;
	  break;
	case CHAR_FROM_BUFFER:
	  if (buf_size > 0)
	    {
	      buffer[buf_size--] = '\0';
	    }
	  else
	    {
	      state = CurrentState (EMPTY_BUFFER);
	    }
	  break;
	case SEND_BUFFER:
	  printf ("Send Buffer:%.*s\n", buf_size, buffer);
	  buf_size = 0;
	  state = CurrentState (SENT_BUFFER);
	  break;

	}
      printf ("New State: %d \n", state);
    }

  return 0;
}
