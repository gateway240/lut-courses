\section{Results}
\label{ref_results}
The main findings from each section of the report are presented here. At the end the simulation results for asymptotically bounding home energy consumption are presented.

\subsection{Whole Home Battery Technology}

Three major whole home battery technologies are considered in this section: Tesla Powerwall+, LG Chem RESU10H, and Sonnen EcoLinx30. Each of these battery technologies has unique properties that are compared in table \ref{tab:bat_whole_home}. Notably the LG Chem battery is a DC coupled battery while the Tesla and Sonnen batteries are AC coupled batteries. The AC coupled batteries have favorable properties because it is possible to chain AC coupled batteries to increase battery current and capacity. With DC coupled batteries it is possible to increase energy storage capacity by chaining but it is not possible to increase the available current. Additionally the AC coupled batteries include inverters in the units while the DC coupled battery does not so the inverter properties will also be discussed. Because the LG Chem RESU10H does not come with an inverter the SolarEdge Energy Hub SE7600H inverter will be selected for use with the RESU10H for comparison.

% Talk about temperature of batteries 
\input{tables/batteries}

\subsection{Energy Market Prices in Finland}

There are a variety of suppliers of different priced electricity in Finland. A comparison of the average day and night cost of each provider is provided in table \ref{tab:eletric_rates}. A Helsinki based electric producer called Helen sells time-of-day electricity. Additionally Vaasan Sähkö in Vaasa sells a few different energy plans that offer daytime and nighttime pricing but in this case the basic electricity plan is considered. Interestingly Väre Eergiea, a nationwide energy conglomerate produced by the merger of Kuopion Energia, Savon Voima, Lappeenranta Energia, and Jyväskylän Energia, offers an energy stock market based pricing model. In this exchange-traded model the buyer will purchase electricity at the market price every hour. This allows users to tailor their consumption (and in this case charge the batteries at the cheapest times). There are also various other features available such as price capping, green electricity, and local electricity consumption. 

\input{tables/electric_rates}

The prices are logically higher in more populous areas like Helsinki where the prices are nearly 1 cent higher than both Vaasa and Lappeenranta. This analysis will compare the change between day and night prices between each market and average it though so this difference will help provide an accurate picture of the difference across Finland. On average across the markets studied, purchasing night-time energy yields a savings of 1.51 € cents/kWh.


\subsection{Simulation Parameters}

For the sake of this analysis, standard residential loads are considered during normal waking hours (7 A.M. to 11 P.M.). This 14 hour span of time is the target of this study because loads that are required at night are generally much more predictable and thus much easier to schedule and predict. For example if you desire to have your electric vehicle (E.V.) ready before you leave work, or you need to run a load of laundry overnight, or you need to heat your house at night, these are all either periodic, interruptable, or low urgency demands and work has been conducted already to show the possibility to schedule these loads efficiently \cite{virtual_microgrid}. This study will also consider that the home will be using a heat source that does not depend on electricity to generate the heat, for example a geothermal system or air heat pump. Air conditioning will also not be considered because it is rare to see residential air conditioning in Finnish homes. Table \ref{tab:res_loads} shows standard day-time residential loads in Finland and their respective properties. Some of the values from this table like priority, usage, and runtime are reasonable estimates for the provided loads and their usage scenarios. Otherwise the values are obtained from the sources provided in the table. The properties discussed in the table are outlined as follows:

\paragraph{Priority} -
Each appliance or load will be assigned a priority which describes how urgent it is that the request if fulfilled. The priority definitions are stated below: 
\begin{itemize}
    \item P0 - Request fulfillment within 1 minute
    \item P1 - Request fulfillment within 10 minutes
    \item P2 - Request fulfillment within 30 minutes
    \item P3 - Request fulfillment within 1 hour
    \item P4 - Request fulfillment within 4 hours
\end{itemize}

\paragraph{Runtime} - This property represents how long the load must run before it is completed

\paragraph{Power Rating} - The power rating of the appliance or load in kilowatts

\paragraph{Usages} - This property represents the number of times the appliance is expected to be used on average per day. In this case a day is defined as 14 hours because we are only considering daytime or waking hours usage.

\paragraph{ Current } - In this column if two values are listed it indicates a high load appliance as defined below. The first number is always nominal current and if there is a second number it is the locked rotor amps (LRA) required to start the motor.
\begin{itemize}
    \item High - any load that needs a surge current greater than 20 amps to start (any motor based appliance) or a load that requires greater than 20 nominal amps.
    \item Low  - a load that does not require significantly more current to start or requires less than 20 amps to operate. 
\end{itemize}
    

\input{tables/resedential_loads}

\subsection{Simulation Results}
Using the techniques discussed in section \ref{ref_bounding_energy}, a simulation was created with Jupyter Notebooks and Python 3. The source code for the simulation modeling can be examined in appendix \ref{apendix:python}. The simulation presents two distinctly separate but equally important scenarios. In the first scenario represented in figure \ref{fig:asymptotic_bounds}, two separate trials were conducted. In each trial the appliances run for exactly the average number of occurrences specified in table \ref{tab:res_loads}. In the first trial the absolute best energy usage scenario was computed by taking the total consumption across the time period and equally distributing it over the time range to yield the theoretical best case scenario. In the second trial, the worst case when each appliance is used exactly the average number of times. This was computed by having every appliance start at exactly the same time and run continuously until the number of occurrences of the appliance running was completed. When this occurs the maximum current required by the system is 89 amps while the maximum LRA required is 195 amps. In both cases the system uses 24.325 kWh through the span of the day but the best case trial uses 1.74 kWh per hour whereas the worst case trial uses 13.45 kWh in the first hour. The upper bound is dictated by the worst case scenario first hour power consumption of 13.45 kWh and the lower bound is 0 which would not be very useful in this case since it would indicate no energy consumption.

\begin{figure} [H]
    \centering
    \includegraphics[width=0.7\textwidth]{plots/best_worst.png}
    \caption{Asymptotic Bounds of Simulated Energy Consumption}
    \label{fig:asymptotic_bounds}
\end{figure}

Figure \ref{fig:avg_consumption} represent the asymptotically bounded simulated average energy consumption for the loads described in table \ref{tab:res_loads}. This is a distinctly separate scenario from the previously described scenario. In this case, a Poisson distribution was generated for each appliance based on it's average rate of occurrence. This distribution dictates when each appliance is turned on during each minute of the simulation. Since each load is assumed to be non-interruptable the appliance will run to completion. The priorities are not considered in this case so each load is run immediately as it arrives. Since it is probabilistic it will most likely not use every appliance the exact average number of times so there will be overall less energy usage then the previous scenario. Additionally if an appliance is currently running and it is scheduled to run again it will not restart but instead continue running until the end of its current runtime. Essentially if an appliance is triggered again while it is running the second trigger will be ignored. This simulation is again bounded by the best and worst case consumption presented in the scenario and the average is dictated by the actual simulated consumption. In this scenario the maximum and minimum consumption was 1.01 and 0 kWh respectively.
\begin{figure} [H]
    \centering
    \includegraphics[width=0.7\textwidth]{plots/average_consumption.png}
    \caption{Simulated Average Energy Consumption}
    \label{fig:avg_consumption}
\end{figure}
