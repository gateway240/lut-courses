clear; clc

m1 = 2500;
m2 = 320;
k1 = 80000;
k2 = 500000;
b1 = 350;
b2 = 15020;

[A,B,C,D]=linmod('assignment_10_subsystem')
[num,den]=ss2tf(A,B,C,D)

step(num,den);

K = [ 0 2.3e6 5e8 0 8e6 ];