clear all
clc


%% Information 

%PV panel formed of 29 cells TPS-102 2.4W (amorphous silicon)

Iscn = 0.170;            %Nominal short-circuit voltage [A]       (experimental)
Vocn = 21;            %Nominal array open-circuit voltage [V]     (datasheet)
Imp = 0.137;             %Array current @ maximum power point [A] (datasheet)
Vmp = 17.5;             %Array voltage @ maximum power point [V]  (datasheet)
Pmax_e = Vmp*Imp;       %Array maximum output peak power [W]      (datasheet)
Kv = -0.123;            %Voltage/temperature coefficient [V/K]    (unknown)
Ki = 3.18e-3;           %Current/temperature coefficient [A/K]    (unknown)
Ns = 29;                %Nunber of series cells                   (datasheet)

Tn = 25+273.15;
Gn = 1000;

%% Constants

k = 1.3806503e-23;   %Boltzmann [J/K]
q = 1.60217646e-19;  %Electron charge [C]

a = 1.0;

%% Algorithm parameters

%Increment of Rs
Rsinc = 0.01;

%Initial value of "a"
%a = 1.0;                 

%Increment of "a"
%ainc = 0.01;

%Maximum tolerable power error
tol = 0.0001;

%Maximum number of iteractions for each value of "a"
nimax = 5000;

%Voltage points in each iteraction
nv = 50;

%Number of "a" values to try
%namax = 100;




 