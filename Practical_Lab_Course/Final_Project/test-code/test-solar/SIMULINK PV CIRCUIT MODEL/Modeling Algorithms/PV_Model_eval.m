%% PV MODEL EVALUATION

% Author: Marcelo Gradella Villalva

% University of Campinas - UNICAMP - Brazil - 2015
% mvillalva@gmail.com

% If you like my work please cite my papers:

% M. G. Villalva, J. R. Gazoli, E. Ruppert F., 
% "Comprehensive approach to modeling and simulation 
% of photovoltaic arrays", IEEE Transactions on 
% Power Electronics, 2009, % vol. 25, no. 5, 
% pp. 1198--1208, ISSN 0885-8993
 
% M. G. Villalva, J. R. Gazoli, E. Ruppert F.
% "Modeling and circuit-based simulation of photovoltaica arrays"
% Brazilian Journal of Power Electronics, 2009
% vol. 14, no. 1, pp. 35--45, ISSN 1414-8862

% Visit for updates: http://sites.google.com/site/mvillalva/pvmodel

% Instructions:

% This program evaluates the PV model for any T or G.
% You must run the PV_Modeling algorithm first

%% INPUTS
    
% You may change these values as you wish

T = 25 + 273.15;    %Temperature [K]
G = 1000;  %Irradiance [W/m^2]     
         

%% PROGRAM STARTS HERE

% Constants 
k = 1.3806503e-23;   %Boltzmann [J/K]
q = 1.60217646e-19;  %Electron charge [C]

% Thermal voltages 
Vtn = k * Tn / q;    %Thermal junction voltage (nominal) 
Vt = k * T / q;      %Thermal junction voltage (actual temperature)
  
% Method of calculating Io

% Chose 1 to use the original method with (T/Tn)^3
% Chose 2 to use the alternative method with KV and KI

method = 1; % You do not need to change this 
 
% Calculation of Io (method 1) 
   
if method == 1,
        
%Calculation of Eg and Io

Tmax = 75 + 273.15;
dT_ = Tmax - Tn;
Isc_ = (  Iscn + Ki*dT_  );
Voc_ = (  Vocn + Kv*dT_  );
Vt_ = k * Tmax / q;

% I'm no longer using these equations
% Egap  = log(Isc_*Tn^3/Ion/Tmax^3/(exp(Voc_/a/Ns/k/Tmax*q)-1))*a*k*Tn*Tmax/q/(Tmax-Tn);
%or
% Egap = -log((Iscn+Ki*(Tmax-Tn))*Tn^3/Ion/Tmax^3/(exp((Vocn+Kv*(Tmax-Tn))/a/Ns/k/Tmax*q)-1))*a*k*Tn*Tmax/q/(-Tmax+Tn);

Io  = Ion *(T/Tn)^(3) * exp(  q * Egap/a/ k * (1/Tn-1/T) ); 

end

% Calculation of Io (method 2)

% This is the alternative Io equation suggested in my paper. 

if method == 2,
    
dT = T - Tn;
Isc_ = (  Iscn + Ki*dT  );
Voc_ = (  Vocn + Kv*dT  );

%Io = Isc_/(exp(Voc_/a/Ns/Vt)-1); %% OLD %%

Ipv_ = (Rs+Rp)/Rp * Isc_; %% NEW %% UPDATED ON JUNE/2010 %%

Io = (Ipv_ - Voc_/Rp)/(exp(Voc_/Vt/a/Ns)-1) %% NEW %% UPDATED ON JUNE/2010 %%

end

%  Temperature and irradiation effect on the current
dT = T-Tn;
Ipvn = (Rs+Rp)/Rp * Iscn;         % Nominal light-generated current
Ipv = (Ipvn + Ki*dT) *G/Gn;       % Actual light-generated current 
Isc = (Iscn + Ki*dT) *G/Gn;       % Actual short-circuit current

% Solving the I-V equation for several (V,I) pairs 
clear V
clear I

nv = 100;

V = 0:(Vocn+Kv*dT)/nv:(Vocn+Kv*dT);  % Voltage vector
I = zeros(1,size(V,2));    % Current vector

for j = 1 : size(V,2) %Calculates for all voltage values 
    
  % Solves g = I - f(I,V) = 0 with Newton-Raphson
  
  g(j) = Ipv-Io*(exp((V(j)+I(j)*Rs)/Vt/Ns/a)-1)-(V(j)+I(j)*Rs)/Rp-I(j);
  
  while (abs(g(j)) > 0.001)
      
  g(j) = Ipv-Io*(exp((V(j)+I(j)*Rs)/Vt/Ns/a)-1)-(V(j)+I(j)*Rs)/Rp-I(j);
  glin(j) = -Io*Rs/Vt/Ns/a*exp((V(j)+I(j)*Rs)/Vt/Ns/a)-Rs/Rp-1;
  I_(j) = I(j) - g(j)/glin(j);
  I(j) = I_(j);   
  
  end  

end

% PROGRAM ENDS HERE


%% PLOTING 
 
 % I-V curve
 figure(1) 
 grid on
 hold on 
 title('Adjusted I-V curve');
 xlabel('V [V]');
 ylabel('I [A]');
 xlim([0 max(V)*1.1]);
 ylim([0 max(I)*1.1]);
 plot(V,I,'LineWidth',2,'Color','k') %
  
 % P-V curve
 figure(2) 
 grid on
 hold on 
 title('Adjusted P-V curve');
 xlabel('V [V]');
 ylabel('P [W]');
 xlim([0 max(V)*1.1]);
 ylim([0 max(V.*I)*1.1]);
 plot(V,V.*I,'LineWidth',2,'Color','k') %
 

 
 
