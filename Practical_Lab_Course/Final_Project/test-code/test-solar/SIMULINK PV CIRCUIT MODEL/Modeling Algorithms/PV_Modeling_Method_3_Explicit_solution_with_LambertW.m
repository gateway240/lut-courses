%% PV modeling - Method 3 - calculation using the Lambert function

% Author: Marcelo Gradella Villalva

% University of Campinas - UNICAMP - Brazil - 2015
% mvillalva@gmail.com

% If you like my work please cite my papers:

% M. G. Villalva, J. R. Gazoli, E. Ruppert F., 
% "Comprehensive approach to modeling and simulation 
% of photovoltaic arrays", IEEE Transactions on 
% Power Electronics, 2009, % vol. 25, no. 5, 
% pp. 1198--1208, ISSN 0885-8993
 
% M. G. Villalva, J. R. Gazoli, E. Ruppert F.
% "Modeling and circuit-based simulation of photovoltaica arrays"
% Brazilian Journal of Power Electronics, 2009
% vol. 14, no. 1, pp. 35--45, ISSN 1414-8862

% Visit for updates: http://sites.google.com/site/mvillalva/pvmodel


%% Load PV datasheet information

clear all

 %data_KC85T
 %data_KD135SX_UPU
 %data_MSX60
 %data_amorphous
 %data_MSX60_single_cell
 %data_Q6LM
 data_M2453BB
  


%% PROGRAM STARTS HERE

% PV model calculation

Gn = 1000;               %Nominal irradiance [W/m^2] @ 25oC
G =  1000;               %Irradiance for evaluating the model after the parameter extraction
Tn = 25 + 273.15;        %Nominal operating temperature [K]
T =  25 + 273.15;        %Temperature for evaluating the model after the parameter extraction

%Egap = 2.72370016e-19;  % Bandgap do sil�cio amorfo em J (=1.7 eV)
Egap = 1.8e-19;          % Bandgap do sil�cio cristalino em J (=1.124 eV)

% The theory used in this program for modeling the PV device is found 
% in many sources in the litterature and is well explained in Chapter 1 of
% "Power Electronics and Control Techniques" by Nicola Femia, Giovanni 
% Petrone, Giovanni Spagnuolo and Massimo Vitelli. 

% This is straightforward way to calculate the PV model parameters but I 
% have found some convergence problems for some devices. 

Ipvn = Iscn; % We are assuming Ipv = Isc, which is a simplification

Ipv = Ipvn * G/Gn * (1 + Ki * (T-Tn));

k = 1.3806503e-23;   %Boltzmann [J/K]
q = 1.60217646e-19;  %Electron charge [C]

Vt =  k*T /q;
Vtn = k*Tn/q;

a = (Kv - Vocn/Tn) / ( Ns * Vtn * ( Ki/Ipvn - 3/Tn - Egap/(k*Tn^2) )  );

Ion = Ipvn * exp( - Vocn/(a*Ns*Vtn) );

C = Ion /  (Tn^3 * exp (-Egap / (k * Tn)));

Io = C * T^3 * exp(-Egap/k/T);

alfa = (Vmp*(Vmp-2*a*Vtn*Ns)/(a^2*Vtn^2*Ns^2));

beta = 2*Imp-Ipv-Ion;

delta = a * Ion*Vtn*Ns;

gama = 2 * Vmp/(a * Vtn*Ns) - Vmp^2/(a^2*Vtn^2*Ns^2);

y = ( Vmp*(beta)*exp (alfa) ) / (delta); 

x = lambertw(y) + gama; 

Rs = ( x * a * Vtn*Ns - Vmp)/Imp;
Rp = ( x * a * Vtn*Ns )/(Ipv - Imp - Ion * (exp(x)-1));

% PROGRAM ENDS HERE



%% I-V and P-V CURVES of the calculated model at STC

% Solving the I-V equation for several (V,I) pairs 
clear V
clear I

nv = 50; % n�mero de pontos da curva

V = 0:Vocn/nv:Vocn;  % Voltage vector
I = zeros(1,size(V,2));    % Current vector

for j = 1 : size(V,2) %Calculates for all voltage values 
    
  % Solves g = I - f(I,V) = 0 with Newton-Raphson
  
  g(j) = Ipv-Io*(exp((V(j)+I(j)*Rs)/Vt/Ns/a)-1)-(V(j)+I(j)*Rs)/Rp-I(j);
  
  while (abs(g(j)) > 0.001)
      
  g(j) = Ipv-Io*(exp((V(j)+I(j)*Rs)/Vt/Ns/a)-1)-(V(j)+I(j)*Rs)/Rp-I(j);
  glin(j) = -Io*Rs/Vt/Ns/a*exp((V(j)+I(j)*Rs)/Vt/Ns/a)-Rs/Rp-1;
  I_(j) = I(j) - g(j)/glin(j);
  I(j) = I_(j);   
  
  end  

end
 
 % I-V curve
 figure(3) 
 grid on
 hold on 
 title('I-V curve');
 xlabel('V [V]');
 ylabel('I [A]');
 xlim([0 max(V)*1.1]);
 ylim([0 max(I)*1.1]);
 
 plot(V,I,'LineWidth',2,'Color','red') %
 plot([0 Vmp Vocn ],[Iscn Imp 0 ],'o','LineWidth',2,'MarkerSize',5,'Color','red') 
  
 % P-V curve
 figure(4) 
 grid on
 hold on 
 title('P-V curve');
 xlabel('V [V]');
 ylabel('P [W]');
 xlim([0 max(V)*1.1]);
 ylim([0 max(V.*I)*1.1]);
  
 plot(V,V.*I,'LineWidth',2,'Color','red') %
 plot([0 Vmp Vocn ],[0 Pmax_e 0 ],'o','LineWidth',2,'MarkerSize',5,'Color','red') 

 
disp(sprintf('Method 3 - using the Lambert function \n'));
disp(sprintf('     Rp = %f',Rp));
disp(sprintf('     Rs = %f',Rs));
disp(sprintf('      a = %f',a));
disp(sprintf('      T = %f',T-273.15));
disp(sprintf('      G = %f',G));
disp(sprintf(' Pmax,e = %f  (experimental)',Pmax_e));
disp(sprintf('    Ipv = %f',Ipvn));
disp(sprintf('    Isc = %f',Iscn));
disp(sprintf('    Ion = %g',Ion));
disp(sprintf('\n\n'));
