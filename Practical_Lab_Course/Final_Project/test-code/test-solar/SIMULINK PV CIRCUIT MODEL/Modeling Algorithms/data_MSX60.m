clear all
clc


%% Information from the MSX60 solar array datasheet

Iscn = 3.8;             %Nominal short-circuit voltage [A] 
Vocn = 21.1;            %Nominal array open-circuit voltage [V] 
Imp = 3.5;              %Array current @ maximum power point [A] 
Vmp = 17.1;             %Array voltage @ maximum power point [V] 
Pmax_e = Vmp*Imp;       %Array maximum output peak power [W] 
Kv = -80e-3;            %Voltage/temperature coefficient [V/K] 
Ki = .003;              %Current/temperature coefficient [A/K] 
Ns = 36;                %Nunber of series cells 
Gn = 1000;              %Nominal irradiance [W/m^2] @ 25oC
Tn = 25 + 273.15;       %Nominal operating temperature [K]


%% Constants

k = 1.3806503e-23;   %Boltzmann [J/K]
q = 1.60217646e-19;  %Electron charge [C]

a = 1.0;

%% Algorithm parameters

%Increment of Rs
Rsinc = 0.001;

%Initial value of "a"
%a = 1.0; 

%Increment of "a"
%ainc = 0.01;

%Maximum tolerable power error
tol = 0.0001;

%Maximum number of iteractions for each value of "a"
nimax = 10000;

%Voltage points in each iteraction
nv = 200;

%Number of "a" values to try
%namax = 50;




%% Experimental points collected from datasheet

Iexp = [3.81
3.81
3.81
3.81
3.81
3.80
3.80
3.80
3.80
3.80
3.80
3.75
3.70
3.65
3.60
3.55
3.50
3.45
3.40
3.35
3.30
3.25
3.20
3.15
3.10
3.05
3.00
2.95
2.90
2.85
2.80
2.75
2.70
2.65
2.60
2.55
2.50
2.40
2.30
2.20
2.10
2.00
1.90
1.80
1.70
1.60
1.50
1.40
1.30
1.20
1.10
1.00
0.90
0.80
0.70
0.60
0.50
0.40
0.30
0.20
0.10
0.00]';

Vexp=[0.00
2.00
3.00
4.00
5.00
6.00
7.00
8.00
9.00
10.00
11.02
14.69
15.62
16.24
16.63
16.88
17.14
17.35
17.53
17.71
17.82
17.96
18.11
18.22
18.29
18.40
18.50
18.58
18.65
18.76
18.83
18.90
18.94
19.04
19.08
19.15
19.19
19.33
19.44
19.55
19.62
19.73
19.80
19.91
19.98
20.05
20.12
20.23
20.27
20.34
20.41
20.48
20.56
20.63
20.70
20.74
20.81
20.88
20.92
20.99
21.02
21.10]';


