%% Information from the solar array datasheet

Iscn = 8.7;            %Nominal short-circuit voltage [A] 
Vocn = 37.7;            %Nominal array open-circuit voltage [V] 
Imp = 8.2;             %Array current @ maximum power point [A] 
Vmp = 30.1;             %Array voltage @ maximum power point [V] 
Pmax_e = Vmp*Imp;       %Array maximum output peak power [W] 
Kv = -0.32/100*Vocn;            %Voltage/temperature coefficient [V/K] 
Ki = -0.032/100*Iscn;           %Current/temperature coefficient [A/K] 
Ns = 60;                %Nunber of series cells 
