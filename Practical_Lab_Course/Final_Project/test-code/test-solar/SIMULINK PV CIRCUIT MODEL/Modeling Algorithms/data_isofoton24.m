
clear all
clc
close all hidden

%%

Iscn = 8.7;             %Nominal short-circuit voltage [A] 
Vocn = 22.6;           %Nominal array open-circuit voltage [V] 
Imp = 8.12;            %Array current @ maximum power point [A] 
Vmp = 18.5;            %Array voltage @ maximum power point [V] 
Pmax_e = Vmp*Imp;        %Array maximum output peak power [W] 
Kv = -0.454*Vocn        %Voltage/temperature coefficient [V/K] 
Ki = 0.042*Iscn        %Current/temperature coefficient [A/K] 
Ns = 36;   %Nunber of series cells
Npp= 1 ;
Nss= 1;


%% Constants

k = 1.3806503e-23;     %Boltzmann (J/K)
q = 1.60217646e-19;    %Electron charge (C)
a1 = 1;                %Diode Ideality Facotrs constant
a=1;


%% Nominal values 

Gn = 1000;            % Nominal irradiance (W/m^2) @ 25oC
Tn = 25 + 273.15;     % Nominal operating temperature (K)

