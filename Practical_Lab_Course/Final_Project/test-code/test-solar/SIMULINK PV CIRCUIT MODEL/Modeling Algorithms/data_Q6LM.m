clear all
clc


%% Information from the Q6LM solar cell datasheet (typical curve)

Iscn = 7.61;             %Nominal short-circuit voltage [A] 
Vocn = 0.6118;           %Nominal array open-circuit voltage [V] 
Imp = 7.1147;            %Array current @ maximum power point [A] 
Vmp = 0.5119;            %Array voltage @ maximum power point [V] 
Pmax_e = Vmp*Imp;        %Array maximum output peak power [W] 
Kv = -.0037*Vocn;        %Voltage/temperature coefficient [V/K] 
Ki = .0005*Iscn;         %Current/temperature coefficient [A/K] 
Ns = 1;                  %Nunber of series cells 
Gn = 1000;               %Nominal irradiance [W/m^2] @ 25oC
Tn = 25 + 273.15;        %Nominal operating temperature [K]

%% Constants

k = 1.3806503e-23;   %Boltzmann [J/K]
q = 1.60217646e-19;  %Electron charge [C]

a = 1.0;


%% Algorithm parameters

%Increment of Rs
Rsinc = 0.0001;

%Initial value of "a"
a = 1.0; 

%Increment of "a"
ainc = 0.005;

%Maximum tolerable power error
tol = 0.0001;

%Maximum number of iteractions for each value of "a"
nimax = 10000;

%Voltage points in each iteraction
nv = 100;

%Number of "a" values to try
namax = 80;


%% Experimental points 

Vexp = [0.611819908587657;0.607465544191854;0.603608559647686;0.599524541908329;0.593290743676791;0.585340924452329;0.577109547890860;0.569813499724621;0.560026682421154;0.545033408118042;0.522890303461890;0.511871280492613;0.503231464950328;0.494037192092328;0.487999667501846;0.479746228640203;0.469165952127729;0.457920334815528;0.404903560559521;0.373213864032782;0.323186675009409;0.276206028995771;0.233048309858244;0.186289111647344;0.140803916528419;0.0952083964647279;0.0553191468875981;0.00844918487966869;]';

Iexp = [0.0315438065220916;0.758809215240196;1.44587762492717;1.95668636248502;2.82140613611130;3.73284809418081;4.49654632450438;5.04894782120062;5.67661389065429;6.34724203542260;6.92092607318887;7.11444780624644;7.20808338509557;7.28748235031276;7.32917885102664;7.37614106499748;7.42448480003581;7.45600381608158;7.54867657425281;7.55502445453255;7.57889476675374;7.58450798520142;7.59646128154414;7.59688201608227;7.58947944423092;7.59763398262281;7.60169074322864;7.60341163289270;]';