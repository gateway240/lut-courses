clear all
clc
% Author: Saad Motahhir
% SMBA University - ENSA - F�s 
% Saad.motahhir@usmba.ac.ma

%% Specification of the MSX60 solar panel extracted in the paper below:
%Motahhir, S., El Ghzizal, A., Sebti, S., & Derouich, A. (2017).
%MIL and SIL and PIL tests for MPPT algorithm. Cogent Engineering,
%4(1), 1378475.

Npp=1; % number of PV panels in parallel
Nss=1; % number of PV panels in series
Rs=0.38572;
Rp=153.5644;
Ki=0.0024;
Kv=-0.08;
Tn=298.15;
Gn=1000;
Ipvn=3.8128;
Vocn=21.1;
Iscn=3.8;
Ns=36;
A=268.2578758;

k = 1.3806503e-23;   %Boltzmann [J/K]
q = 1.60217646e-19;  %Electron charge [C]

a = 0.97484;