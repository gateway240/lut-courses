
Please cite this work as: 

1. Saad MOTAHHIR, Abdelaziz El Ghzizal, Souad Sebti, Aziz Derouich, Une ressource p�dagogique pour l'enseignement par simulation : cas des panneaux photovolta�ques, 
Proceedings of International Workshop on Pedagogic Approaches & E-Learning (APEL 2015)

2. Motahhir, S., El Ghzizal, A., Sebti, S., & Derouich, A. (2017). MIL and SIL and PIL tests for MPPT algorithm.
   Cogent Engineering, 4(1), 1378475.

3. Motahhir, S.(2018). Contribution to the optimization of energy withdrawn from a PV panel using an Embedded System. 
(doctoral dissertation). University sidi mohammed ben abdellah, Fez, Morocco.

For more papers and works please visit : 
https://www.researchgate.net/profile/Saad_Motahhir



