% Code to plot simulation results from ee_solar_cell
%% Plot Description:
%
% This example shows how to generate the power-voltage curve for a solar
% array. Understanding the power-voltage curve is important for inverter
% design. Ideally the solar array would always be operating at peak power
% given the irradiance level and panel temperature.

% Copyright 2013-2018 The MathWorks, Inc.

clear; close all
%% PV Curve at 25 C
% Need to specify the range of irradiance levels and temperatures over
% which to plot the power curve.
measurment_temp = 25;
irradianceVec = [400 600 800 1000 1200];
% temperatureVec = [15 25 35 45 55];
temperatureVec = [25];
irr_size = numel(irradianceVec);
temp_size = numel(temperatureVec);
results_size = irr_size * temp_size;
%% Load the model and simulate all combinations of irradiance and temperature
open_system('solar_model')
index = 0;
global_results = cell(results_size, 2);
for temperature = temperatureVec
    for irradiance = irradianceVec
       index = index+1;
       sim_results = sim('solar_model');
       logsout = sim_results.logsout;

       local_results = zeros(numel(logsout{1}.Values.Time),4);
       local_results(:,1) = logsout{1}.Values.Time;
       for i = 1:3
            local_results(:,i+1) = logsout{i}.Values.Data;
       end
       if irr_size == 1
           label = ['T=' num2str(temperature) '^{\o}C'];
       elseif temp_size == 1
           label = ['Ir=' num2str(irradiance) 'W/m{^2}'];
       else
           label = ['T=' num2str(temperature) '^{\o}C, Ir=' num2str(irradiance) 'W/m{^2}'];
       end
       global_results{index,1} = label;
       global_results{index,2} = local_results;
    end    
end

% Time, Voltage, Current, Power
disp(global_results)
clear simlog_vCell simlog_icell

%% Plot the results
figure
legend_info = cell(irr_size,1);
hold on
for i = 1:irr_size
    legend_info{i} = global_results{i,1};
    data = global_results{i,2};
    plot(data(:,2),data(:,4),'LineWidth',1);
end

temp_ylim = get(gca,'YLim');
set(gca,'YLim',[0 temp_ylim(2)]); % Only show positive results
grid on
xlabel('Output Voltage (V)')
ylabel('Output Power (W)')
legend(legend_info,'Location','NorthWest')
saveas(gcf,'pv_25C.png')
%% Plot the results
figure
legend_info = cell(irr_size,1);
hold on
for i = 1:irr_size
    legend_info{i} = global_results{i,1};
    data = global_results{i,2};
    plot(data(:,2),data(:,3),'LineWidth',1);
end

temp_ylim = get(gca,'YLim');
set(gca,'YLim',[0 temp_ylim(2)]); % Only show positive results
grid on
xlabel('Output Voltage (V)')
ylabel('Output Current (A)')
legend(legend_info,'Location','NorthWest')
saveas(gcf,'vi_25C.png')

%% VP Curve at 1000W
% Need to specify the range of irradiance levels and temperatures over
% which to plot the power curve.
irradianceVec = [1000];
temperatureVec = [15 25 35 45 55];
irr_size = numel(irradianceVec);
temp_size = numel(temperatureVec);
results_size = temp_size;
%% Load the model and simulate all combinations of irradiance and temperature

index = 0;
global_results = cell(results_size, 2);
for irradiance = irradianceVec
    for temperature = temperatureVec
       index = index+1;
       sim_results = sim('solar_model');
       logsout = sim_results.logsout;

       local_results = zeros(numel(logsout{1}.Values.Time),4);
       local_results(:,1) = logsout{1}.Values.Time;
       for i = 1:3
            local_results(:,i+1) = logsout{i}.Values.Data;
       end
       if irr_size == 1
           label = ['T=' num2str(temperature) '^{\o}C'];
       elseif temp_size == 1
           label = ['Ir=' num2str(irradiance) 'W/m{^2}'];
       else
           label = ['T=' num2str(temperature) '^{\o}C, Ir=' num2str(irradiance) 'W/m{^2}'];
       end
       global_results{index,1} = label;
       global_results{index,2} = local_results;
    end    
end

% Time, Voltage, Current, Power
disp(global_results)
clear simlog_vCell simlog_icell

%% Plot the results
figure
legend_info = cell(temp_size,1);
hold on
for i = 1:temp_size
    legend_info{i} = global_results{i,1};
    data = global_results{i,2};
    plot(data(:,2),data(:,4),'LineWidth',1);
end

temp_ylim = get(gca,'YLim');
set(gca,'YLim',[0 temp_ylim(2)]); % Only show positive results
grid on
xlabel('Output Voltage (V)')
ylabel('Output Power (W)')
legend(legend_info,'Location','NorthWest')
saveas(gcf,'pv_1000W.png')

%% Plot the results
figure
legend_info = cell(temp_size,1);
hold on
for i = 1:temp_size
    legend_info{i} = global_results{i,1};
    data = global_results{i,2};
    plot(data(:,2),data(:,3),'LineWidth',1);
end

temp_ylim = get(gca,'YLim');
set(gca,'YLim',[0 temp_ylim(2)]); % Only show positive results
grid on
xlabel('Output Voltage (V)')
ylabel('Output Current (A)')
legend(legend_info,'Location','NorthWest')
saveas(gcf,'vi_1000W.png')
