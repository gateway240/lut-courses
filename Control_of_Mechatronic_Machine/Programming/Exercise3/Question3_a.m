
numerator = [0.005 1];
coeff_den1 = [0.01 1];
coeff_den2 = [0.05 1];
denominator_sub1 = conv(coeff_den1,coeff_den2);
denominator_final = conv([1 0 0], denominator_sub1);

sys = tf(numerator,denominator_final);

figure
bode(sys)
title('Bode open loop')
figure
pzmap(sys)
title('Pole-Zero open loop')
figure

closed = feedback(sys,1);
bode(closed)
title('Bode closed loop')
figure
pzmap(closed)
title('Pole-Zero closed loop')
figure
impulse(closed)
title('Impulse closed loop')
figure
step(closed)
title('Step response closed loop')
