g_num=[10];
g_den = conv(conv([0.2 1],[0.2 1]),[0.05 1]);
sys_a = tf(g_num,g_den);

g_den_b = conv([0.2 1],[0.05 1]);
sys_b = tf(g_num,g_den_b);

g_den_c = conv([0.02 1],conv([0.2 1],[0.05 1]));
sys_c = tf(g_num,g_den_c);

figure
rlocus(sys_a)
title('Root locus 1a')

figure
rlocus(sys_b)
title('Root locus 1b')

figure
rlocus(sys_c)
title('Root locus 1c')

