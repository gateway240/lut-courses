for k = 18:1:22

    sys_1 = tf([k k], [1 4 5 0]);

    sys_2= tf([1],[0.5 1]);

    sys = feedback(sys_1,sys_2);

    figure
    rlocus(sys)
    title('Root locus sys')
    
    step(sys)
    title('Step Response sys')
end


