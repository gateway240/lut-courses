disp('Convert Fahrenheit to Centigrade');

f = input('Enter the value for degrees Fahrenheit: ');

result = f2c(f);

fprintf('%3.2f degrees fahrenheit is %3.2f degrees Centigrate\n',f,result)

function c = f2c(f)
    c = (5/9)*(f-32);
end
