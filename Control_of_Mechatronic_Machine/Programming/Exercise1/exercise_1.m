disp('Quadratic Equation solver in the form ax^2 + bx + c = 0');

a = input('Enter the value for a: ');
b = input('Enter the value for b: ');
c = input('Enter the value for c: ');

roots = quadratic_equation(a,b,c);

disp('Calculated Roots: ')
disp(roots)



function roots = quadratic_equation(a,b,c)
    roots = zeros(2,1);
    
    % Calculate the protion under the square root
    delta = b^2 - 4*a*c;
    
    disp('Delta = b^2 - 4*a*c = ');
    disp(delta)
    
    square_portion = sqrt(delta);
    
    % Set the negative and positive portion of the equation
    roots(1) = square_portion;
    roots(2) = -square_portion;
    
    % Apply the remainder of the quadratic formulat
    roots = (-b + roots)/(2*a);
end