clc; clear; close all;

K_A = 0.05;
K_v_K_q = 1E-3;
K_h = 10;
A = 35;
omega_h = 100;
delta_h = 0.03;

K_a = 100;
K_v = 1;

K_p = 1;


sys_base = tf((K_A*K_v_K_q*(omega_h^2))/A, [1 2*delta_h*omega_h omega_h^2 0]);

sys_feed_1= tf([K_a 0 0],1);

sys_feed_2 = tf([K_v 0],1);

sys_feed_3 = tf(K_h,1);

sys_1 = feedback(sys_base,sys_feed_1);

sys_2 = feedback(sys_1, sys_feed_2);

sys_3 = feedback(sys_2 * K_p, sys_feed_3);


step(sys_3)
title('Step Response sys')

figure;
step(sys_2)


