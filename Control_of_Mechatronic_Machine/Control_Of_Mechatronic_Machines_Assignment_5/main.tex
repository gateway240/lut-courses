\documentclass{article}

% If you're new to LaTeX, here's some short tutorials:
% https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes
% https://en.wikibooks.org/wiki/LaTeX/Basics

% Formatting
\usepackage[utf8]{inputenc}
\usepackage[margin=1in]{geometry}
\usepackage[titletoc,title]{appendix}

% Math
% https://www.overleaf.com/learn/latex/Mathematical_expressions
% https://en.wikibooks.org/wiki/LaTeX/Mathematics
\usepackage{amsmath,amsfonts,amssymb,mathtools}

% Images
% https://www.overleaf.com/learn/latex/Inserting_Images
% https://en.wikibooks.org/wiki/LaTeX/Floats,_Figures_and_Captions
\usepackage{graphicx,float}

% Tables
% https://www.overleaf.com/learn/latex/Tables
% https://en.wikibooks.org/wiki/LaTeX/Tables

% Algorithms
% https://www.overleaf.com/learn/latex/algorithms
% https://en.wikibooks.org/wiki/LaTeX/Algorithms
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{algorithmic}

% Code syntax highlighting
% https://www.overleaf.com/learn/latex/Code_Highlighting_with_minted
\usepackage{minted}
\usemintedstyle{borland}

% References
% https://www.overleaf.com/learn/latex/Bibliography_management_in_LaTeX
% https://en.wikibooks.org/wiki/LaTeX/Bibliography_Management
\usepackage{biblatex}
\addbibresource{references.bib}

\usepackage{physics}
\usepackage{siunitx}

\usepackage[scr]{rsfso}

\usepackage{pdfpages}

\newcommand{\Laplace}{\mathscr{L}}

% Title content
\title{Control of Mechatronic Machines Assignment 5}
\author{Alex Beattie}
\date{November 15, 2020}

\begin{document}

\maketitle



\section{Coil step response}

The first step of creating the system STEP response is isolating the highest derivative:

\begin{equation}
    \frac{di(t)}{dt} = \frac{1}{L} \left( u(t) - i(t)R \right) 
\end{equation}

Assuming values $L = 0.01$ and $R = 0.01$ and modeling this system in SimuLink yields the following STEP response curve:

\includepdf{question1.PNG}

\section{Electrical servo system}

Air gap flux $\propto$ field current

\begin{equation}
    \phi \propto i_f
\end{equation}

\begin{equation}
    \phi = k_f i_f
\end{equation}

Torque of motor $\propto \phi i_a$ 
\begin{equation}
    T_m \propto \phi i_a = K_p i_f i_a
\end{equation}

\begin{equation}
    T_m = K_i K_f i_f i_a
\end{equation}
$K_t$ is the combination of the two constants $K_i$ and $K_f$ from the above equation
\begin{equation}
    T_m = K_t i_f i_a
\end{equation}

back emf $\propto$ speed

\begin{equation}
    e_b = K_b \omega
\end{equation}

Applying KVL to the armature circuit

\begin{equation}
    -e_a + i_a R_a + L_a \frac{di_a}{d_t} + e_b = 0
\end{equation}

\begin{equation}
    e_a = i_a R_a + L_a \frac{di_a}{d_t} + e_b
\end{equation}

at load:

\begin{equation}
    T_L = J \frac{d \omega}{dt} + B \omega
\end{equation}

Remaining Load $T_L = 0$

Frictional torque $T_d = k_d \omega$

Using the laplace transform on the above equations:

\begin{equation}
    T_d(s) = K_d \omega(s)
\end{equation}

\begin{equation}
    T_m(s) = K_T i_a(s)
\end{equation}

\begin{equation}
    e_b(s) = K_b \omega(s)
\end{equation}

\begin{equation}
    e_a(s) = i_a(s) [R_a + L_a s] + e_b(s)
\end{equation}

\begin{equation}
    e_a(s) - e_b(s) = i_a(R_a + L_a s)
\end{equation}

\includepdf{BlockDiagram.jpg}

\begin{equation}
    \frac{\omega(s)}{E_a(s)} = \frac{\frac{K_T K_d}{R_a + L_a s}}{ 1 + \frac{K_b K_T K_d}{(R_a + L_a s)}} = \frac{K_T K_d}{(R_a + L_a s) + K_b K_t K_d} 
\end{equation}

By substituting the provided values and solving you can obtain the following:

\begin{equation}
    \frac{\omega(s)}{E_a(s)} = \frac{2.4}{(28.80 + \num{18.7e-3}) + (8820 * 2.4) * 0.35} = \frac{2.4}{7437.6+0.01875} = \num{3.226e-4}
\end{equation}


\section{DC motor of an electro-mechanical system }

For an armature-controlled motor the torque can be modeled as follows:

\begin{equation}
    T_m = K_t i_m
\end{equation}

Back emf, $e_m$, is proportional to shaft angular velocity 

\begin{equation}
    e_m = K_e \dot{\theta}
\end{equation}

Since this is in SI units the constants $K_t = K_e$ are equal and can be represented by a common constant $K$.

Using Newton's 2nd law and Kirchoff's voltage law the following equations can be obtained:

\begin{equation}
    J \ddot{\theta} + b \dot{\theta} = K i_m
\end{equation}
\begin{equation}
    L_m \frac{di}{dt} + R_m i_m = e_a - K \dot{\theta} 
\end{equation}

Using the Laplace transform on the above equations the following equations can be obtained:

\begin{equation}
    s(Js +b)\Theta(s) = K E_a(s)
\end{equation}
\begin{equation}
    (L_m s + R_m) I_m(s) = E_a(s) - K_s \Theta(s)
\end{equation}

The following equations can be combined to the following

\begin{equation}
    P(s) = \frac{\dot{\Theta}(s)}{E_a(s)} = \frac{K}{(Js + b)(L_m s + R_m) + K^2} \frac{rad/sec}{Volt}
\end{equation}

This can be expressed in state space form by using the rotational speed and electric current as state variables

\begin{equation}
    \frac{d}{dt} 
    \begin{bmatrix}
        \dot{\theta} \\
        e_a
    \end{bmatrix} =
    \begin{bmatrix}
        - \frac{b}{J} & \frac{K}{J}\\
        - \frac{K}{L_m} & - \frac{R_m}{L_m}
        \end{bmatrix}
    \begin{bmatrix}
        \dot{\theta} \\
        e_a
    \end{bmatrix} +
    \begin{bmatrix}
        0 \\
        \frac{1}{L_m}
    \end{bmatrix} V
\end{equation}

\begin{equation}
    y = 
    \begin{bmatrix}
        1 & 0
    \end{bmatrix}
    \begin{bmatrix}
        \dot{\theta} \\
        e_a
    \end{bmatrix}
\end{equation}

\section{Electrohydraulic position servo}

Using the provided value $H=0.3 m$ the following values for A and B can be calculated

\begin{equation}
    A = \frac{1}{2} H = 0.15 m (at t=0)
\end{equation}

\begin{equation}
    B = \frac{1}{2} H + 0.006 m = 0.156 m (at t = 0.5)
\end{equation}

Using the provided trajectory equation
\begin{equation}
   y(t) = at^3 + bt^2 + ct + d
\end{equation}
and solving for t=0 and t=0.5 to represent each position to calculate the constants

\begin{equation}
    y(0)= 0.150 \rightarrow d = 0.150 m
\end{equation}

\begin{equation}
    y(0.5) = 0.156 \rightarrow a(0.5)^3 + b(0.5)^2 + c(0.5) + 0.150 = 0.156
\end{equation}

\begin{equation}
    \dot{y}(0) = 0 \rightarrow 3a0^2 +2b0 + c = 0 \rightarrow c = 0
\end{equation}

\begin{equation}
    \dot{y}(0.5) = 0 \rightarrow 3a(0.5)^2 +2b(0.5) = 0
\end{equation}

\begin{equation}
    0.125a + 0.25b = 0.006
\end{equation}

\begin{equation}
    0.75a + b = 0 \rightarrow b = -0.75a
\end{equation}

Plugging the b value from equation (35) into equation (34)
\begin{equation}
    0.125 a + 0.25(-0.75a) = 0.006
\end{equation}
\begin{equation}
    -0.0625a = 0.006 \rightarrow a = -0.096
\end{equation}

Now using this value of a to calculate b
\begin{equation}
    b = -0.75a = 0.072
\end{equation}

Now the trajectory equation can be solved with the constants calculated above

\begin{equation}
    y(t) = -0.096t^3 + 0.072t^2 + 0.150
\end{equation}

To determine the cylinder area with a system pressure $P_s = 21 MPa$ then $P_L = 18 MPa$ 

\begin{equation}
    P_L A= m \ddot{y} + b\dot{y} + F
\end{equation}

\begin{equation}
    \ddot{y} = 6at + 2b = -0.576t + 0.144
\end{equation}

\begin{equation}
    \ddot{y}_{max}(0) = 0.144 m/s^2
\end{equation}

\begin{equation}
    A = \frac{m * \ddot{y}_{max} + F}{P_L} = \frac{3000 * 0.144 + \num{20e3}}{\num{18e6}} = 0.001135 m^2
\end{equation}



The maximum valve flow can be computed with t the following:

$V_0$ can be computed as follows:
\begin{equation}
    V_0 = \frac{A H}{2} = \frac{0.001135 * 0.3}{2} = \num{1.7025 e-4} m^3
\end{equation}

\begin{equation}
    P_L = \frac{m\ddot{y}+ F}{A} = \frac{m}{A} (-0.576t +0.144) + \frac{F}{A}
\end{equation}

\begin{equation}
    \dot{P_L} = \frac{m}{A}(-0.576) = -1.522 MPa/s
\end{equation}

\begin{equation}
    \dot{y} = -0.288t^2 + 0.144t = t(0.144-0.288t)
\end{equation}
\begin{equation}
    \ddot{y} = -0.576t + 0.144 \rightarrow t_{max} = 0.25 s
\end{equation}

\begin{equation}
   \dot{y}_{max} = 0.028 m/s
\end{equation}

\begin{equation}
    Q_L = A \dot{y}_{max} + c P_L + \frac{V_0}{2 B_e} \dot{P_L} = 0.001135 * 0.028 + 0 + \frac{\num{1.7025e-4}}{2*\num{1400e6}} * \num{-1.522e6} = \num{3.168e-5}m^3/s
\end{equation}

\section{Simulink model of differential equation}
See the following page

\includepdf{question5.PNG}

\end{document}
