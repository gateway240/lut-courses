\section{Methods}
\label{ref_methods}

The system modeling is performed in multiple stages. Initially, SolidWorks 2020 is used to create CAD models of the parts outlined in the provided 2D schematics. Then an assembly is created and constrained. Then the Simscape MultiBody Link plugin is used to export the model from SolidWorks to Matlab 2020b. Once the model is loaded into Matlab the additional components (cylinder, point mass, hydraulics, etc.) are added to the model. Finally the parameters and model are optimized with tuning and testing.

\subsection{Boom Crane System}

Figure \ref{fig:simscape_model} shows the system modeled in SimScape MultiBody from four standard angles in the starting position. The system consists of a main boom arm with a 50 kg (per the student specific values) mass attached (the blue box). The mass is attached at a distance of 3400 mm down the boom per the student specific values. The hydraulic cylinder (red and green) are responsible for controlling the raising and lowering of the boom. The remainder of the structure is responsible for keeping the boom stable and two types of lugs are utilized to ensure proper boom actuation. The system has one degree of freedom.  


 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/SimScape_Model.PNG}
    \caption{SimScape Model}
    \label{fig:simscape_model}
 \end{figure}
 
The system is modeled from the schematic presented in figure \ref{fig:schematic1}. The schematic specifies the use of fe510 steel. This is not available in SolidWorks but is equivalent to S355J2G3 Structural Steel which is what was selected for SolidWorks modeling. 
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/SystemSchematic.PNG}
    \caption{System Schematics}
    \label{fig:schematic1}
 \end{figure}

The assembled SolidWorks model is shown in figure \ref{fig:solidworks1}. This model was exported using the SimScape MultiBody link plugin to Matlab and then the model shown in figure \ref{fig:simscape_model} was constructed.

  \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/SolidWorksModel.PNG}
    \caption{SolidWorks Model}
    \label{fig:solidworks1}
 \end{figure}


\subsection{Initial Parameters}

The hydraulic system parameters listed in table \ref{tab:provided_parameters} for the simulation are provided.

 \begin{table}[h!]
\centering
\begin{tabular}{||c c c||} 
 \hline
 Parameter & Variable & Value \\ [0.5ex] 
 \hline\hline
 Cylinder stroke & (L) & 300 mm \\
 Cylinder inner diameter & d & 50 mm\\
 Piston rod diameter & d & 30mm \\
 Hose diameter & d & 3/8" \\
 Hose length & l & 1.5 m \\
 Setting Pressure (Student Specific) & p & 140 bar \\
 Bulk modulus of oil & $B_o$ & 1500 MPa \\
 Bulk modulus of cylinder & $B_c$ & 21000 MPa \\
 Volume Hose to DCV & $V_h$ & $\num{3e-3} m^3$ \\
 Fluid density & $\rho$ & 850 $kg/m^3$ \\
 Kinematic viscosity & $K_v$ & $\num{32e-5} m^2/s$ \\
 Input shaft rotation speed & v & 1500 rpm \\
 Cylinder efficiency & $\eta$ & 0.9 \\[1ex] 
 \hline
\end{tabular}
\caption{Provided Simulation Parameters}
\label{tab:provided_parameters}
\end{table}

\subsubsection{Manufacturer's Data Sheet}
\label{sec:man_cat}
For the modeling of the Variable-displacement Pressure-compensated Pump the values are obtained from the Parker PV016 pump data sheet. For the modeling of the 3/4 Directional Control Valve the BOSCH: in Regel-valve Code: 0811404611 data sheet is used. 

\subsubsection{Starting Position}

The boom begins slightly above the horizontal position when the system starts. To achieve this the revolute joint connecting the boom to the world frame was set to an initial position target angle of 2.5 degrees above vertical. Without this parameter the boom does not start in the same position as the laboratory experiment.

\subsubsection{Rigid Transforms}

Rigid transforms from the world frame are utilized to position the various grounded components in the appropriate positions. Additionally rigid transform offsets are utilized to correctly anchor the top beam to the boom in the correct location.

\subsubsection{Control Signal}

As specified in the requirements, the control signal shown in figure \ref{fig:control_signal} is used to control the hydraulic cylinder. This signal is the control signal used with the laboratory experiment.

  \begin{figure}[H]
    \centering
    \begin{tikzpicture}
    \begin{axis}[xlabel=Time (s),
        ylabel=Voltage (V) ]
    \addplot table [x index=0, y=u_in,mark=none, col sep=comma] {data_file/simulation_main.csv};
    \end{axis}
    \end{tikzpicture}
    \caption{Control Signal $U_in$}
    \label{fig:control_signal}
 \end{figure}

\subsection{Calculated Parameters}

\subsubsection{Cylinder, Piston, and Hose Volume}

In order to correctly define the parameters for the hydraulic actuator the areas of the cylinder and piston chambers need to be calculated. 

\begin{equation}
    A_{cylinder} = \pi * r_{cylinder}^2 = \num{7.85e-3} m^2
\end{equation}

\begin{equation}
    A_{piston} = \pi * (r_{cylinder}^2 - r_{piston}^2) = \num{5.02e-3} m^2
\end{equation}

\begin{equation}
\label{eqn:hose}
    V_{hose} = l_{hose} * \frac{\pi}{4} * d_{hose}^2 = \num{1.068e-4}m^3
\end{equation}

The dead volume on the piston side ($V_{dead}$) is equal to the volume of the hose shown in equation \ref{eqn:hose}

\begin{equation}
    V_{dead} = V_{hose} = \num{1.068e-4}m^3
\end{equation}

\subsubsection{Semi-Empirical Flow Rate Constant}

Using the values of $\Delta p$ = 35 bar and $Q_{nom}$ = 24 l/min from the manufacturer's catalog specified in section \ref{sec:man_cat}

\begin{equation}
    C_v = \frac{Q_{nom}}{ U * \sqrt{\Delta p}} = \frac{12/60000}{ 10 * \sqrt{\num{35e5}}} = \num{1.08e-8} \frac{m^3}{sV \sqrt{Pa}}
\end{equation}

\subsection{Initial Simulation Model}

Once the SolidWorks assembly was exported into SimScape, the modeling began by creating the hydraulic cylinder system and attaching it to the required lugs. The cylinder from previous simulation work was used. Additional frames were added to both lugs on the boom and the base pillar to attached the hydraulic cylinder. Then rigid transforms were utilized to translate the cylinder and position to the appropriate positions. A revolute joint and a bearing joint were used as to not overconstrain the system. The system has 1 DOF which is in the X direction of the model. All components are modeled as rigid bodies but the boom will be modeled as a flexible body in a later iteration. 

 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/system_model.PNG}
    \caption{SimScape Initial System Model}
    \label{fig:system_model}
 \end{figure}
 
 The main force applied involved in this model is gravity acting on the point mass on the end of the boom and the boom itself. This force acts downward and is what causes the boom to fall when the hydraulic system is not setup. Since the system is constrained to one degree of freedom the rotational forces are neglected. This means that it is not possible with this model to analyze fatigue or potential failure of components due to torsional or axial forces. For example, the connection point of the boom to lug on top of the pillar is quite thin and if not manufactured well it could possibly fail if enough axial or torsional force is applied. This could be caused by an off-center or swinging object suspended from the boom. This force will not be significant in this model though because the boom is bolted to the floor so it seems unlikely that a mass suspended from it would swing sideways significantly. 

\subsection{Hydraulic Simulation Model}

After constructing the initial model the hydraulics were modeled and added to yield the system model shown in figure \ref{fig:system_model_hydraulic}.

 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/system_model_hydraulic.PNG}
    \caption{SimScape Hydraulic System Model}
    \label{fig:system_model_hydraulic}
 \end{figure}
 
As shown in figure \ref{fig:system_model_rising} the control signal provided is able to raise the boom and hold the position without oscillation. 

  \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/simscape_model_risen.PNG}
    \caption{SimScape Model Raising the Boom}
    \label{fig:system_model_rising}
 \end{figure}
 
\subsubsection{Variable-Displacement Pressure-compensated Pump}

The values for the variable-displacement pressure-compensated pump (VDPC) were obtained from the manufacturer's data sheet shown in figure \ref{fig:pump_data_sheet}. The setting pressure was provided as shown in table \ref{tab:provided_parameters}. The pressure regulation range was calculated as the difference between the Min. and Max. inlet pressure. Additionally, the pump is driven by an ideal angular velocity source with a shaft speed of 1500 rpm as provided in table \ref{tab:provided_parameters}.

  \begin{table}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/pump_data_sheet.PNG}
    \caption{Pump Data Sheet}
    \label{fig:pump_data_sheet}
 \end{table}

\subsubsection{Time Constant}

 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/bode-diagram.PNG}
    \caption{DCV Bode Diagram}
    \label{fig:bode}
 \end{figure}
 
 The time constant can be calculated from the DCV bode diagram shown in figure \ref{fig:bode} using equation \ref{eqn:bode}.
 
 \begin{equation}
 \label{eqn:bode}
    \tau = \frac{1}{2\pi f_{-45\degree}} = \frac{1}{2\pi * 35} = \num{4.5e-3}
\end{equation}
 
\subsubsection{Custom Hydraulic Components}

The 4/3 Directional Control Valve (DCV) and Double Acting Hydraulic Cylinder were modeled using custom simscape components. 

The primary equation governing the DCV is the volume flower rate equation shown by equation \ref{eqn:volume_flow}

\begin{equation}
    Q = C_v U * sgn(\Delta p) \sqrt{\left |\Delta p \right |}
    \label{eqn:volume_flow}
\end{equation}

The Hydraulic Cylinder is governed by a variety of equations.
Firstly the equation determining the piston side and rod-side volume is shown in equations \ref{eqn:volume_piston} \& \ref{eqn:volume_cylinder}

\begin{equation}
    \label{eqn:volume_piston}
    V_A = 
    \begin{cases}
        V_0,& \text{if } A_1x \leq V_0\\
        A_1x,              & \text{otherwise}
    \end{cases}
\end{equation}

\begin{equation}
    \label{eqn:volume_cylinder}
    V_B = 
    \begin{cases}
        V_0,& \text{if } A_2(l-x) \leq V_0\\
        A_2(l-x),              & \text{otherwise}
    \end{cases}
\end{equation}

The effective bulk modulus in each sub-system is modeled by equation \ref{eqn:bulk_mod} where $B_o$ is the bulk modulus of oil, $B_c$ is the bulk modulus of the cylinder and $B_h$ is the bulk modulus of the connecting hoses modeled by $B_h = \num{10e5}  \sqrt{p_h}$.

\begin{equation}
    \label{eqn:bulk_mod}
    \frac{1}{B_e} = \frac{1}{B_o} + \frac{V_h}{V_t B_h} + \frac{V_c}{V_t B_c}
\end{equation}

The volume flower rate for each component of the cylinder is governed by the lumped fluid theory. The continuity equation for the double acting hydraulic cylinder can be written as in equation \ref{eqn:dyn_cont1} and \ref{eqn:dyn_cont2}.

\begin{equation}
    \label{eqn:dyn_cont1}
    \dot{p_A} = \frac{B_{e_1}}{V_A}(Q_A - A_1 \dot{x})
\end{equation}

\begin{equation}
    \label{eqn:dyn_cont2}
    \dot{p_B} = \frac{B_{e_2}}{V_B}(A_2 (l- \dot{x}) - Q_B)
\end{equation}

For the simulation these equations must be solved for $Q_A$ and $Q_B$ respectively. This derivation is performed as shown in \ref{eqn:lumped_fluid_incorrect} in the tutorial and lecture slides. 

\begin{equation}
    \label{eqn:lumped_fluid_incorrect}
    Q_n = \frac{\dot{p_n} V_n}{B_{e_n}} + A_n  \dot{x}
\end{equation}

This is close to the correct solution, but it makes a sign error. Solving equation \ref{eqn:dyn_cont1} for $Q_A$ results in equation \ref{eqn:lumped_fluid_correct1}.

\begin{equation}
    \label{eqn:lumped_fluid_correct1}
    Q_A = \frac{\dot{p_A} V_n}{B_{e_1}} + A_1  \dot{x}
\end{equation}

Solving equation \ref{eqn:dyn_cont2} for $Q_B$ results in equation \ref{eqn:lumped_fluid_correct2}.

\begin{equation}
    \label{eqn:lumped_fluid_correct2}
    Q_B = -\frac{\dot{p_B} V_n}{B_{e_2}} + A_2 (l- \dot{x}) \dot{x}
\end{equation}

These are nearly the same equations except for the minus sign in front of the leading term on the right side in equation \ref{eqn:lumped_fluid_correct2}. This is \textbf{critical} to the proper functioning of the hydraulic system. Without it, the system will become a positive feedback loop that will cause the pressure to infinitely increase until the solver fails. 

The output force of the hydraulic cylinder is modeled by equation \ref{eqn:hyraulic_force}

\begin{equation}
    \label{eqn:hyraulic_force}
    F = (A_1  p_A - A_2  p_B) - \dot{x} \left | A_1  p_A - A_2  p_B \right | * ( 1 - \eta)
\end{equation}

\subsection{Flexible Model}
\label{ref:sec_flex_model}

In this section the flexibility of the boom was analyzed and modeled.

\subsubsection{Frequency analysis}

Frequency analysis was conducted in SolidWorks to identify the critical frequency modes. The program identified 5 modes. Modes 1 and 3 shown in figure \ref{fig:flex_mode_3} identify deformation in the lateral (z-plane) direction which is not very important in this model since there is not a force being applied in the Z direction. Modes 2 and 5 shown in figures \ref{fig:flex_mode_2} and \ref{fig:flex_mode_5} show that the frequency 6.56 Hz is important for out model since that frequency causes lateral deformation of the beam along the x plane which is the plane where the load and consequently the force are being applied. Additionally mode 4 shown in figure \ref{fig:flex_mode_4} shows another important frequency that describes a wave-like bend along the beam in the x-plane at 43.24 Hz. This analysis shows that the two important frequencies that need to be considered in the simulation model are 6.56 Hz and 43.24 Hz. These are both relatively low frequency modes and as such will be considered in the simulation modeling in section \ref{ref:flex_beam_model}

 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/mode-2.PNG}
    \caption{Flexible Beam Frequency Analysis Mode 2 (6.56 Hz)}
    \label{fig:flex_mode_2}
 \end{figure}
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/mode-5.PNG}
    \caption{Flexible Beam Frequency Analysis Mode 5 (6.56 Hz)}
    \label{fig:flex_mode_5}
 \end{figure}
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/mode-3.PNG}
    \caption{Flexible Beam Frequency Analysis Mode 3 (6.56 Hz)}
    \label{fig:flex_mode_3}
 \end{figure}
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/mode-4.PNG}
    \caption{Flexible Beam Frequency Analysis Mode 4 (43.24 Hz)}
    \label{fig:flex_mode_4}
 \end{figure}

\subsubsection{Flexible Beam Modeling }
\label{ref:flex_beam_model}

 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/boom_flexible_solidworks.PNG}
    \caption{Flexible Beam SolidWorks Model}
    \label{fig:flex_boom_solidworks}
 \end{figure}
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/system_model_flexible.PNG}
    \caption{Flexible Beam Simulink Model}
    \label{fig:flex_model}
 \end{figure}
 
 In this iteration the boom was not modeled as a rigid body but as a flexible one. Following the steps in the tutorial, the boom was modeled as a reduced order flexible solid as shown in figure \ref{fig:flex_model}. The frames and coordinate system were added to the SolidWorks model as shown in figure \ref{fig:flex_boom_solidworks}. Then the beam was added to the Simulink model as shown in figure \ref{fig:flex_model}. This required making the flexibility calculations in the Matlab live script. It was critical to export the STL file correctly as I initially faced challenges because the file exported with millimeters but Matlab expected meters so when I ran the program it couldn't run because of the order of magnitude increase in computational complexity. When I corrected the unit export issue the script ran well. This iteration also introduced the real control signal into the system with the `From Spreadsheet' block. 
 
 \subsection{PID Model}
 
 
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/system_model_PID.PNG}
    \caption{PID Addition to Simulink Model}
    \label{fig:pid_model}
 \end{figure}

 A PID controller was added to the model as shown in figure \ref{fig:pid_model}. It was initially connected to and tuned with the step function shown. The step function was set to a predetermined value of 100mm and the PID was tuned accordingly. The values obtained from tuning were P = 80, I =15, and D = 1. Figure \ref{fig:pid_tune} that the end effector was able to reach a position of 100mm using the designed PID controller. The initial drop in position is caused by the model being started at the position used in the laboratory which is not 0 as the step function is desiring. The oscillation in the signal is due to the flexibility of the beam which was modeled in section \ref{ref:sec_flex_model}.
 
 
  \begin{figure}[H]
    \centering
    \begin{tikzpicture}
    \begin{axis}[xlabel=Time (s),ylabel=Position (m),legend pos=south east, yticklabel style={
/pgf/number format/fixed,
/pgf/number format/precision=2
}]
    \addplot [legend entry=Target,samples=100, domain=0:25,mark=none,dashed] {0.1}; 
    \addplot +[legend entry=Sim Pos] table [x index=0, y=ps-pos:1,mark=none, col sep=comma] {data_file/simulation_pid_tune.csv};
    \end{axis}
    \end{tikzpicture}
    \caption{PID Tuneing}
    \label{fig:pid_tune}
 \end{figure}
 