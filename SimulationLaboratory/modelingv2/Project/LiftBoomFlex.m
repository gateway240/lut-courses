%Step 1
stlFile = 'Lift BoomFlex2.STL';
figure
trisurf(stlread(stlFile))
axis equal

E = 200e9;     % Young's modulus in Pa
nu = 0.26;     % Poisson's ratio (nondimensional)
rho = 7800;    % Mass density in kg/m^3

%Step 2
origins = [-1.90547245925   0.00142540868      -0.00000433546     % Frame 1: LiftArm_Lug2
           -1.61190791298  -0.08857459132   -0.00025419668      % Frame 2: LiftArm_Lug1Top
            1.09452754075   0.00142540868      -0.00000433546];    % Frame 3: Mass3000mm
numFrames = size(origins,1);

%Step 3
feModel = createpde('structural','modal-solid');
importGeometry(feModel,stlFile);
structuralProperties(feModel, ...
    'YoungsModulus',E, ...
    'PoissonsRatio',nu, ...
    'MassDensity',rho);
generateMesh(feModel, ...
    'GeometricOrder','quadratic', ...
    'Hmax',0.05, ...
    'Hmin',0.02);

%Step 4
figure
pdegplot(feModel,'FaceLabels','on','FaceAlpha',0.5)

%faceIDs = [45,33,43];    % List in the same order as the interface frame origins
faceIDs = [3,15,5];    % List in the same order as the interface frame origins


figure
pdemesh(feModel,'FaceAlpha',0.5)
hold on
colors = ['rgb' repmat('k',1,numFrames-3)];
assert(numel(faceIDs) == numFrames);
for k = 1:numFrames
    nodeIdxs = findNodes(feModel.Mesh,'region','Face',faceIDs(k));
    scatter3( ...
        feModel.Mesh.Nodes(1,nodeIdxs), ...
        feModel.Mesh.Nodes(2,nodeIdxs), ...
        feModel.Mesh.Nodes(3,nodeIdxs), ...
        'ok','MarkerFaceColor',colors(k))
    scatter3( ...
        origins(k,1), ...
        origins(k,2), ...
        origins(k,3), ...
        80,colors(k),'filled','s')
end
hold off

for k = 1:numFrames
    structuralBC(feModel, ...
        'Face',faceIDs(k), ...
        'Constraint','multipoint', ...
        'Reference',origins(k,:));
end

%Step 5
rom = reduce(feModel,'FrequencyRange',[0 5000]);
arm.P = rom.ReferenceLocations';  % Interface frame locations (n x 3 matrix)
arm.K = rom.K;                    % Reduced stiffness matrix
arm.M = rom.M;                    % Reduced mass matrix
dampingRatio = 0.02;
arm.C = computeModalDampingMatrix(dampingRatio,rom.K,rom.M);

frmPerm = zeros(numFrames,1);    % Frame permutation vector
dofPerm = 1:size(arm.K,1);       % DOF permutation vector

assert(size(arm.P,1) == numFrames);
for i = 1:numFrames
    for j = 1:numFrames
        if isequal(arm.P(j,:),origins(i,:))
            frmPerm(i) = j;
            dofPerm(6*(i-1)+(1:6)) = 6*(j-1)+(1:6);
            continue;
        end
    end
end
assert(numel(frmPerm) == numFrames);
assert(numel(dofPerm) == size(arm.K,1));

arm.P = arm.P(frmPerm,:);
arm.K = arm.K(dofPerm,:);
arm.K = arm.K(:,dofPerm);
arm.M = arm.M(dofPerm,:);
arm.M = arm.M(:,dofPerm);
arm.C = arm.C(dofPerm,:);
arm.C = arm.C(:,dofPerm);



