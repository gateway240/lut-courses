% Simscape(TM) Multibody(TM) version: 7.2

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(9).translation = [0.0 0.0 0.0];
smiData.RigidTransform(9).angle = 0.0;
smiData.RigidTransform(9).axis = [0.0 0.0 0.0];
smiData.RigidTransform(9).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [109.99999999999987 0 0];  % mm
smiData.RigidTransform(1).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(1).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(1).ID = 'B[Pillar-1:-:Lug1bottom-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [-69.999999999999972 -220 1.4210854715202004e-14];  % mm
smiData.RigidTransform(2).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(2).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(2).ID = 'F[Pillar-1:-:Lug1bottom-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [-2122.4999999999995 1.1102230246251565e-13 49.999999999999986];  % mm
smiData.RigidTransform(3).angle = 0;  % rad
smiData.RigidTransform(3).axis = [0 0 0];
smiData.RigidTransform(3).ID = 'B[Lift Boom-1:-:Lug2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [140.00000000011579 77.000000000371045 49.99999999999995];  % mm
smiData.RigidTransform(4).angle = 1.1294433232783419e-12;  % rad
smiData.RigidTransform(4).axis = [-1.6004574195733622e-05 0.99999999987192678 -9.0381297324845456e-18];
smiData.RigidTransform(4).ID = 'F[Lift Boom-1:-:Lug2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [-110.0000000000001 1020 124.99999999999996];  % mm
smiData.RigidTransform(5).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(5).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(5).ID = 'B[Pillar-1:-:Lug2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [-109.99999999964068 -4.5844420359791432e-10 125.00000000000004];  % mm
smiData.RigidTransform(6).angle = 2.0943951023957403;  % rad
smiData.RigidTransform(6).axis = [0.5773502691885809 0.57735026919047394 0.57735026918982224];
smiData.RigidTransform(6).ID = 'F[Pillar-1:-:Lug2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [-4.4408920985006262e-13 -90.000000000000085 0];  % mm
smiData.RigidTransform(7).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(7).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(7).ID = 'B[Lift Boom-1:-:Lug1top-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [1826.5000000000225 69.999999999990678 -0.12778738101287013];  % mm
smiData.RigidTransform(8).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(8).axis = [0.57735026918962462 -0.57735026918962717 0.57735026918962562];
smiData.RigidTransform(8).ID = 'F[Lift Boom-1:-:Lug1top-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [28.176473151532903 8.4592386672559527 -20.615422763677934];  % mm
smiData.RigidTransform(9).angle = 0;  % rad
smiData.RigidTransform(9).axis = [0 0 0];
smiData.RigidTransform(9).ID = 'RootGround[Pillar-1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(5).mass = 0.0;
smiData.Solid(5).CoM = [0.0 0.0 0.0];
smiData.Solid(5).MoI = [0.0 0.0 0.0];
smiData.Solid(5).PoI = [0.0 0.0 0.0];
smiData.Solid(5).color = [0.0 0.0 0.0];
smiData.Solid(5).opacity = 0.0;
smiData.Solid(5).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 4.0109144242379697;  % kg
smiData.Solid(1).CoM = [0 43.913317800315227 0];  % mm
smiData.Solid(1).MoI = [5787.232742943037 12275.748486393573 12722.180162749532];  % kg*mm^2
smiData.Solid(1).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(1).color = [0.52941176470588236 0.5490196078431373 0.5490196078431373];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'Lug1top*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 11.134177318554606;  % kg
smiData.Solid(2).CoM = [31.237169160824426 23.780195114176276 -0.0065320612617118393];  % mm
smiData.Solid(2).MoI = [54060.132879804536 114752.83519324136 77110.514067556447];  % kg*mm^2
smiData.Solid(2).PoI = [3.8706299433131997 9.4193550922605755 -15795.299168148724];  % kg*mm^2
smiData.Solid(2).color = [0.52941176470588236 0.5490196078431373 0.5490196078431373];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'Lug2*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 88.714182656696153;  % kg
smiData.Solid(3).CoM = [-183.85507215418914 -1.4608786868190933 0.0044433422520874285];  % mm
smiData.Solid(3).MoI = [462199.19742196286 143444406.32822013 143588352.4897176];  % kg*mm^2
smiData.Solid(3).PoI = [32.930075390351234 647.51005832207602 -212887.864596214];  % kg*mm^2
smiData.Solid(3).color = [0.52941176470588236 0.5490196078431373 0.5490196078431373];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'Lift Boom*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 121.46471522642105;  % kg
smiData.Solid(4).CoM = [-0.20330644731620709 373.60810629748477 0];  % mm
smiData.Solid(4).MoI = [16750574.672950489 2243900.592461417 16872147.861754324];  % kg*mm^2
smiData.Solid(4).PoI = [1.5712591931899882e-09 0 -44387.11421126729];  % kg*mm^2
smiData.Solid(4).color = [0.52941176470588236 0.5490196078431373 0.5490196078431373];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'Pillar*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 4.0109144242379706;  % kg
smiData.Solid(5).CoM = [-43.913317800315227 0 0];  % mm
smiData.Solid(5).MoI = [12275.748486393573 5787.232742943037 12722.180162749533];  % kg*mm^2
smiData.Solid(5).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(5).color = [0.52941176470588236 0.5490196078431373 0.5490196078431373];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'Lug1bottom*:*Default';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(1).Rz.Pos = 0.0;
smiData.RevoluteJoint(1).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = -19.449700660949443;  % deg
smiData.RevoluteJoint(1).ID = '[Lift Boom-1:-:Lug2-1]';

