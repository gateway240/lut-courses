component custom_Hactuator_AB
% This is .ssc file template for defining a custom simscape component block
% Nodes are connected to other blocks using the foundation domains.
% domainless inputs and outputs are Simscape signal type.

% Variables include units in them, use '1' if variable has no units,
% value(var,'unit') can be used to strip units from variable and
% {var, 'unit'} to define units for an unitless variable.

nodes
    % Define connection nodes and their name:location in block
    % foundation.hydraulic.hydraulic is for hydraulic connection
    
    % for hydraulic connection on the right side:
    % node = foundation.hydraulic.hydraulic; % name:right
    A = foundation.hydraulic.hydraulic; % A:right
    B = foundation.hydraulic.hydraulic; % B:right
    
    % for rotational mechanical connection:
    % foundation.mechanical.rotational.rotational 
    
    % for translational mechanical connection:
    % foundation.mechanical.translational.translational     
end

inputs
    % Define inputs to block with name and location
    % input = {start value, 'unit'}; % name:location
    x = {0, 'm'}; % x:left
    xdot = {0, 'm/s'}; % xd:left
end

outputs
    % Define outputs from block with name and location
    % output = {start value, 'unit'}; % name:location
    F = {0, 'N'}; % F:left
end

parameters
    % Define the parameters and default values, these can be modified by the user
    % in the component dialog opened by double clicking the block
    % comment after the definition functions as the parameter description in the block
    
    % param = {default value, 'unit'}; % description
    A1 = {pi*(0.05)^2, 'm^2'}; % Area on the piston-side
    A2 = {pi*((0.05)^2 - (0.03)^2), 'm^2'}; % Area on the piston-rod-side
    l = {0.500, 'm'}; % length of cylinder
    V0 = {1.0688e-4, 'm^3'}; % Volume of pipes and hoses
    Vh = {3e-3, 'm^3'}; % Volume of connecting hoses
    Bo = {1500e6, 'Pa'}; % Bulk modulus of Oil
    Bc = {21000e6, 'Pa'}; % Bulk modulus of cylinder
    Bh = {10e5*sqrt(150e5), 'Pa'}; % Bulk modulus of hoses
    eta = {0.9, '1'}; % Efficiency of the cylinder
end

variables
    % Inner variables for the block, start value can be modified by user
    % The variable priority can be set to none (default), low or high if needed
    % priority determines if the solver should try to set the value at beginning of simulation

    % var = {{start value, 'unit'}, priority = priority.none }; % description
    % var = {start value, 'unit'}; % description
    pA = {0, 'Pa'}; % Preassure on piston-side
    qA = {0, 'm^3/s'}; % Flow-rate on piston-side
    pB = {0, 'Pa'}; % Preassure on piston-rod side
    qB = {0, 'm^3/s'}; % Flow-rate on piston-rod side
    Be1 = {0, 'Pa'}; % Effective Bulk modulus on piston-side
    Be2 = {0, 'Pa'}; % Effective Bulk modulus on piston-rod-side
end

variables (Access=private)
    % Inner variables for the block, these cannot be modified by the user
end

branches
    % Define flow of the through variables across the block nodes
    % designate variable qA to present the flow from port A to nowhere (no outlet port)
    
    % through_var: node1.var -> node2.var;
    % use -> *; if there is no second node where the flow goes 
    qA: A.q -> *;
    qB: B.q -> *;
end

equations
    % Define equations of the block here, equations are evaluated at once 
    % and therefore don't need to be arranged
    
    % let-in-end statement used to define parameters for equations
    let 
        % These are evaluated for use in the latter part
        % if-else-end can be used this way or the usual matlab way
        % else statement is mandatory!
        
        % subvar = if condition, value1 else value2 end;
        VA = if A1*x <= V0, V0 else A1*x end;
        VB = if A2*(l-x) <= V0, V0 else A2*(l-x) end;
    in
        % define the equation where subvar is used here
        % differential variable derivatives can be used with var.der notation
        % '==' defines equal statements
        
        %var == (var2.der * subvar)/const;
        Be1 == ((1/Bo) + (Vh/((Vh+VA)*Bh)) + (VA/((Vh+VA)*Bc)))^-1
        Be2 == ((1/Bo) + (Vh/((Vh+VB)*Bh)) + (VB/((Vh+VB)*Bc)))^-1
        qA == (pA.der * VA)/Be1 + A1*xdot;
        qB == (pB.der * VB)/Be2 + A2*xdot;
    end 
    
    % you can define other equations here algebraic or differential,
    % equation and variable count should match.
    pA == A.p; 
    pB == B.p;
    F == (A1*pA - A2*pB) - xdot/{1,'m/s'}*abs(A1*pA - A2*pB)*(1-eta);
end
end