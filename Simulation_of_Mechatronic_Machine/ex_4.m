syms theta;
syms L;
c_q = [1 0  L/2*cos(theta);
       0 1  L/2*sin(theta);
       0 1 -L/2*sin(theta);];
c_q_inv = inv(c_q);

syms R_x_a R_y_a R_x_a;
syms t;
constraint_matrix = [R_x_a - L/2*cos(theta) - (sin(t)/4) + 1/2;
                     R_y_a - L/2*sin(theta);
                     R_x_a + L/2*cos(theta);];
newton_difference = c_q_inv * constraint_matrix