% Author: Alex Beattie Date 13.10.2020
% Code: Simple Mechanism Modeling

clc; clear; close all;

syms R1_x R1_y theta_1 R2_x R2_y theta_2 ...
     dR1_x dR1_y dtheta_1 dR2_x dR2_y dtheta_2
syms L1 L2 m1 g real

q = [R1_x; R1_y; theta_1; R2_x; R2_y; theta_2]; %generalized coordinates

dq = [dR1_x; dR1_y; dtheta_1; dR2_x; dR2_y; dtheta_2];


% Constraint Equations
C1 = R1_x; % x-component
C2 = R1_y; % y-component
C3 = R2_x - cos(theta_1) * L1/2 - cos(theta_2) * L2/2;
C4 = R2_y - sin(theta_1) * L1/2 - sin(theta_2) * L2/2;
C5 = R2_x + cos(theta_2) * L2/2;

C = [C1; C2; C3; C4; C5];

Cq = jacobian(C,q)

% Step 3: Construcing Generalized Force Vector (Qe)

Qe = [0; -m1*g; 0];

% Step 4: Constructing Quadratic Velocity Vector (Qv)
Qv = [0; 0; 0];

% Step 5: Constructing Constrains Vector (Qc)
intermediate = Cq * dq;
Qc = -jacobian(intermediate,q)*dq

