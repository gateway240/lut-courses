% Author: Alex Beattie Date 13.10.2020
% Code: Simple Mechanism Modeling

clc; clear; close all;

syms R1_x R1_y theta_1 ...
     dR1_x dR1_y dtheta_1
syms L1 m1 g real

q = [R1_x; R1_y; theta_1]; %generalized coordinates
dq = [dR1_x; dR1_y; dtheta_1];
M = [m1, 0, 0;
     0  m1, 0;
     0,  0, (m1*L1^2/12)];
 
% Step 2: Construct the Jacobian

R1 = [R1_x; R1_y];

Aa1 = [cos(theta_1) -sin(theta_1);
       sin(theta_1) cos(theta_1)];
   
ubarA = [+L1/2; 0];
ubarB = [-L1/2; 0];

rA = R1 + Aa1*ubarA;
rB = R1 + Aa1*ubarB;

% Constraint Equations
C1 = rA(2); % y-component
C2 = rB(1); % x-component

C = [C1; C2];

Cq = jacobian(C,q);

% Step 3: Construcing Generalized Force Vector (Qe)

Qe = [0; -m1*g; 0];

% Step 4: Constructing Quadratic Velocity Vector (Qv)
Qv = [0; 0; 0];

% Step 5: Constructing Constrains Vector (Qc)
intermediate = Cq * dq;
Qc = -jacobian(intermediate,q)*dq

% Step 6: Combining the matricies and vectors
O = zeros(2);
LHS = [M, Cq';
       Cq, O];
RHS = [Qe+Qv;
       Qc];
   
EOM = LHS\RHS; % This line is Not Recommended