phi = pi/2;
theta = pi/6;
psi = -pi/2;

A = [cos(psi)*cos(phi) - cos(theta)*sin(phi)*sin(psi), -sin(psi)*cos(phi) - cos(theta)*sin(phi)*cos(psi), sin(theta)*sin(phi);
     cos(psi)*sin(phi) + cos(theta)*cos(phi)*sin(psi), -sin(psi)*sin(phi) + cos(theta)*sin(phi)*cos(psi), -sin(theta)*cos(phi);
     sin(theta)*sin(psi), sin(theta)*cos(psi),cos(theta)]
