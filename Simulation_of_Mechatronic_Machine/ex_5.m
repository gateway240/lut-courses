syms theta;
syms L;
dr_dq_a = [1 0  -L/2*sin(theta);
         0 1  L/2*cos(theta);];
syms F_y;
F = [0;
    F_y/2];

Q_e_a = transpose(dr_dq_a)*F

dr_dq_b = [1 0  L/2*sin(theta);
         0 1  -L/2*cos(theta);];
     
Q_e_b = transpose(dr_dq_b)*F

Q = [Q_e_a;Q_e_b]



