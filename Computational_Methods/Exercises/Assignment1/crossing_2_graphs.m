N = input("Input the number of trials: ");
eps = input("Input the value for epsilon (accuracy): ");

x = linspace(-4,4,N); % Generate N points on the interval provided

graph_cross_vec = abs(x - x.^2); % subtract the two functions and take the absolute vvalue

cross_points = find(graph_cross_vec < eps); % Find a logical array of where the graphs intersect

result = x(cross_points); % Determine the values at those points

fprintf("When x is %1.3f the functions x and x^2 intersect on the interval [-4,4] \n",result);


