%%% START MAIN SCRIPT
y_i = [4.4 2.0 11.0 21.5 7.5];

% Part b)
lin_interp_loop(y_i);

% t values requested from part (c)
t = [2.5 3.1]; 
for i = 1:length(t)
    y = lin_interp(y_i,t(i));
    fprintf("Interpolated value for f(%0.2f) = %0.2f \n",t(i),y);
end
%%% END MAIN SCRIPT

% Part a)
function y = lin_interp(y_i,t)
    % matlab starts arrays at 1 which would fail in this case 
    % with t<1. This fixes that.
    t = t+1; 
    low_point = floor(t); % floor the t value to get the low point.
    
    if t > length(y_i) || t < 0
       fprintf("Invalid time entered. Enter a time on the interval [0, %d] and try again \n", length(y_i))
       y = "invalid";
       return
    elseif t == length(y_i) % There is no next point to interpolate in this case
        y = y_i(t);
        return
    end
    
    % Construct the x and y vectors for the two points
    x = [low_point low_point+1 ];
    y = [y_i(low_point) y_i(low_point+1)];
    m = (y(2) - y(1)) / (x(2) - x(1)); % Slope calculation
    b = y(1) - m*x(1); % Intercept calculation
    y = m*t + b; % Equation of the line
end

% Part c)
function lin_interp_loop(y_i)
    disp("You can exit the loop by entering a value < 0");
    while 1 % Repeat until the break condiion is triggered
        t = input(sprintf("Enter a time on the interval [0, %d]: ", length(y_i)));
        if t < 0
            break
        end
        y = lin_interp(y_i,t);
        fprintf("Interpolated value for f(%0.2f) = %0.2f \n",t,y);
    end
        
end