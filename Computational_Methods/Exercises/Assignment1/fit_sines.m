%%% MAIN FUNCTION
function fit_sines()
    test_sinenum()
    N = 3;
    M = 500;
    f = @(t) (1/pi)*t; 
    trial(f,N);

    plot_compare(f,N,M);
    
end

% Part a)
function S = sinesum(t, b)
    S = zeros(length(t),1);
    for N = 1:length(t)
        for n = 1:length(b)
            S(N) = S(N) + b(n) * sin(n*t(N));
        end
    end
end

% Part b)
function test_sinenum()
t = [-pi/2 pi/4];
b = [4 -3];
disp("test_sinenum output:")
disp(sinesum(t,b));
end

% Part c)
function plot_compare(f, N, M, b)
    if ~exist('b','var')
     % b parameter does not exist, so defaulting to best approximation
      b = findcoefficents(f,N,M);
    end
    interval = linspace(-pi, pi, M);
    y = f(interval);
    S = sinesum(interval, b);
    plot(interval,y,'-k',interval,S,'r--');
    xlabel('Time');
    ylabel('Amplitude');
    legend('f(t)','S_N(t)')
    
end

% Part d) 
function err = error(b, f, M)
    interval = linspace(-pi, pi, M);
    y = f(interval);
    S = sinesum(interval,b);
    err = 0;
    for i = 1:length(interval)
       err = err + sqrt((y(i) - S(i))^2); 
    end
end

% Part e)
function trial(f, N)
    M = 500;
    b = zeros(N,1);
    while 1
        for i = 1:N
            b(i) = input(sprintf("Input a value for b [%d more values after this]: ", N-i));
        end 
        plot_compare(f,N,M,b);
        fprintf("Calculated Error: %0.2f \n",error(b,f,M));
        if input("Would you like to run another trial? [y/n]: ", 's') == 'n'
            break
        end
    end
end

% Part g)
function b = findcoefficents(f,N,M)
    b = zeros(N,1) - 1;
    min_error = error(b,f,M);
    step = 0.1;
    for b1 = -1:step:1
        for b2 = -1:step:1
            for b3 = -1:step:1
                trial_b = [b1 b2 b3];
                current_error = error(trial_b,f,M);
                if current_error < min_error
                    b = trial_b;
                    min_error = current_error;
                end
            end
        end
    end
    
    fprintf("Minimum error discovered: %0.3f \n", min_error);
    fprintf('The corresponding b values are: [');
    fprintf('%0.2f, ', b(1:end-1));
    fprintf('%0.3f]\n', b(end));
end