function n = adaptive_integration(f, a, b, eps, method)
%ADAPTIVE_INTEGRATION Iteratively find the best value of n for integration
    if ~exist('method','var')
     % method parameter does not exist, so defaulting to midpoint
      method = "midpoint";
    end
    if ~(strcmp(method,"midpoint") || strcmp(method,"trapezoidal"))
        n = -1;
        return
    end
    fun = str2func(method);
    
    n_stop = 1e9; % Don't iterate forever
    n = 2;
    diff = 0;
    while true
       int_n = fun(f,a,b,n); 
       int_2n = fun(f,a,b,2*n);
       diff = abs(int_2n - int_n);
       if (diff < eps) || (n > n_stop)
           break
       else
           n = n * 2;
       end
    end
    if (diff > eps)
        n = -1;
        return
    end     
end



