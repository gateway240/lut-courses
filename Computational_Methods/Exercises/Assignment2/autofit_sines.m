%%% MAIN FUNCTION
function autofit_sines()
    close all
    
    test_integrate_coeff()
    
    % part e)
    
    N = 3;
    M = 500;
    f = @(t) (1/pi)*t; 
    fn_name = func2str(f);
    b = integrate_coeffs(f,N,M);
    fprintf('The corresponding b values for %s are: [', fn_name(5:end));
    fprintf('%0.2f, ', b(1:end-1));
    fprintf('%0.3f]\n', b(end));
    
    % part g)
    N = [3 6 12 24];
    for i = 1:length(N)
        filename = sprintf("output_N_%d.png",N(i));
        plot_approx(f,N(i),M,filename);
    end
    
    % part h)
    f_euler = @(t) exp(-(t-pi));
    plot_approx(f_euler,100,M,"output_euler.png");
end

% sinesum from last exercise
function S = sinesum(t, b)
    S = zeros(length(t),1);
    for N = 1:length(t)
        for n = 1:length(b)
            S(N) = S(N) + b(n) * sin(n*t(N));
        end
    end
end

% Part c)
function b = integrate_coeffs(f, N, M)
    b = zeros(1,N);
    for n = 1:N
        % Using equation computed in part b)
        b(n) = (1/pi)*trapezoidal(@(t) f(t) * sin(n*t),-pi,pi,M);
    end
end

% Part d)
function test_integrate_coeff()
disp("test_integrate_coeffs output:")
tol = 1e-15;
N = 5;
M = 25;
func = @(t) 1;
b = integrate_coeffs(func, N, M);
for i = 1:N
    error = abs(b(i));
    assert(error < tol, 'i=%d, err=%g', i, error);
    fprintf('b(%d) = %g\n', i, error)
end

end

% Part f)
function plot_approx(f, N, M, filename)
    b = integrate_coeffs(f,N,M);
    interval = linspace(-pi, pi, M);
    y = f(interval);
    S = sinesum(interval, b);
    figure();
    plot(interval,y,'-k',interval,S,'r--');
    xlabel('Time'); 
    ylabel('Amplitude');
    legend('f(t)','S_N(t)')
    saveas(gcf,filename);
    
end

