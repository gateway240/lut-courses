%% Part B - Check that the specified functions are integrated exactly
f = {@(x) x^2, @(x) sqrt(x)};
F = {@(x) (x^3)/3,@(x) 2/3 * x^(3/2) }; % Anti-derivative
a = 0; b = 2;

% tol = eps(expected) * 10;
for i = 1:length(f)
    eqn = func2str(f{i});%changes function name to a char array
    fprintf("\nFunction: %s\n", eqn(5:end));%removes the '@(x) handle and print
    for n = [10e-1 10e-10]
        expected = f{i}(b) - F{i}(a); % derefrence cell array first then function
        computed = adaptive_integration(f{i}, a, b, n, "trapezoidal");
        error = abs(expected - computed);
        assert(computed ~= -1, "Error in adaptive_integration function");

        fprintf("Expected: %g Computed: %g \n", expected,computed);
        fprintf('n=%d, err=%g\n', n, error)

    end
end

%% Part C - Graphing 
eps = linspace(1E-1,10E-10,10);
n_mid = zeros(size(eps));
n_trap = zeros(size(eps));
for i = 1:length(n_mid)
    % Use the secon function from the cell array above
    n_mid(i) = adaptive_integration(f{2}, a, b, eps(i), 'midpoint');
    n_trap(i) = adaptive_integration(f{2}, a, b, eps(i), 'trapezoidal');   
end

% Plotting

semilogx(eps,n_mid,'b-',eps,n_trap,'r-')
xlabel('log(\epsilon)')
ylabel('n')
legend('midpoint','trapezoidal')
grid on
saveas(gcf,"output_adaptive_integration.png")
