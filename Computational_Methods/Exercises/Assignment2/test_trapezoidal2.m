%% Compare one hand-computed result
v = @(t) 3*(t^2)*exp(t^3);
n = 2;
computed = trapezoidal(v, 0, 1, n);
expected = 2.463642041244344;
error = abs(expected - computed);
conv_val = eps(expected) * 1e3;
assert(error < conv_val, 'error=%g > tol=%g', error, conv_val);

%% Check that linear functions are integrated exactly
f = @(x) 6e8*x - 4e6;
F = @(x) 3e8*x^2 - 4e6*x; % Anti-derivative
a = 1.2; b = 4.4;
expected = F(b) - F(a);
conv_val = 1e-15;
for n = [2 20 21]
    computed = trapezoidal(f, a, b, n);
    error = abs(expected - computed)/expected; % compute relative error instead of absolute
    assert(error < conv_val, 'n=%d, err=%g', n, error);
    fprintf('n=%d, err=%g\n', n, error)
end

%% Check empirical convergence rates against the expected -2.
v = @(x) 6e8*x - 4e6;
V = @(x) 3e8*x^2 - 4e6*x; % Anti-derivative
a = 1.1; b = 1.9;
num_experiments = 14;
r = convergence_rates(v, V, a, b, num_experiments);
conv_val = 0; % Check if values converged to zero for linear function

error = abs(r);
[max_err, err_idx] = max(error);
assert(all(error == conv_val), 'Max error %f for i=%d',...
    max_err, err_idx + start_idx - 1);