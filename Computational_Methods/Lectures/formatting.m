t = [1, 1.55, 2.1];
f_t = t .* sin(t);

fprintf('Output: \n\n')
for i=1:length(f_t)
    fprintf('f(%.2f) = %6g \n',t(i),f_t(i))
end
fprintf('\n')