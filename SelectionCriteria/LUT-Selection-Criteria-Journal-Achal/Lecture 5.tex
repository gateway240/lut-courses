\section{Results}
\label{ref_results}

\subsection{Simulation Run Time}
When running the simulation and comparing the three different options for actuation the simulation time increases with increased model complexity and accuracy as shown in table \ref{tab:sim_time}. The DC Servo-Motor actuation method is the most realistic and accurate but it takes nearly 4 times as long to compile and run as compared to the torque actuation method. This is important to consider when designing the model because if the fidelity and accuracy provided by the DC Servo-Motor actuation method is not required for the desired simulation it can decrease model run and compile time and allow for faster model developement.

 \begin{table}[H]
\centering
\begin{tabular}{||c c ||} 
 \hline
 Actuation Method & Simulation Time [s]\\ [0.5ex] 
 \hline\hline
 Motion & 9.5 \\
 Torque & 33.3 \\
 DC Servo-Motor & 118.4\\ [1ex] 
 \hline
\end{tabular}
\caption{Simulation Run Time}
\label{tab:sim_time}
\end{table}

\subsection{Model Comparisons}
Figure \ref{fig_robot_motion} shows the robots position in each plane X, Y, and Z over time for each actuation technique. The most noticeable difference between the three techniques occurs in the X axis where the motor actuation technique creates the most consistent and accurate simulation. 

 
    \begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{Images/results/xyz.png}
    \caption{Robot Motion in the XYZ Planes}
    \label{fig_robot_motion}
 \end{figure}

\subsection{Comparison to Original Paper}

To compare the new model to the original paper the group must examine the actuation at the knee and hip joint (which are the only joints represented in the original work). Figure \ref{fig_knee_angle} represents the joint angle and joint torque for the knee over time for the three different actuation techniques. In the following analysis the group will focus on the motor actuation technique for comparison to the original paper as it is the most accurate. 

Figures \ref{fig_hip_pitch} and \ref{fig_knee_angle} show that the joint angles follow a predictable but irregular pattern. The figures show that the torque increases or decreases most significantly immediately before or after the joint angle steadily decreases or increases. This demonstrates the need for more sophisticated modeling of the motor control system because the simple motion actuator overshoots or overestimates the required motor torque for both joints. The torque based approach overshoots to a lesser degree in figure \ref{fig_hip_pitch} but the electrical servo motion approach provides the most accurate and precise torque control even though the motion profiles are similar. This in important for an assistive robot since the user will be able to detect the overcompensation and torque irregularities which could make use uncomfortable and jerky.

 \begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{Images/results/hip_pitch.png}
    \caption{Hip Pitch Angle and Torque}
    \label{fig_hip_pitch}
 \end{figure}



 \begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{Images/results/knee_degrees_torque.png}
    \caption{Knee Angle and Torque}
    \label{fig_knee_angle}
 \end{figure}
 
 Comparing the results of the original paper (figure \ref{fig_og_knee_speed}) and the new model results (figure \ref{fig_knee_speed}) shows a similar pattern. Both velocity plots follow a sinusoidal pattern for the tracked joints (hip pitch and knee). The new model provides a much closer estimation to a human gait over the original model. This follows with an intuitive assumption about gait. A normal walking pattern definitely does not have each joint maintaining a sinusoidally varying velocity while it is not in motion which is shown by the new model. The new model also shows how the alternating pattern is much closer together as the joints move in closer cordination to each other. Additionally the speed differences can be observed when the joints are preparing to make a large velocity change that is not modeled by the original system.
 
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{Images/results/original_paper_knee_speed.png}
    \caption{Original Model Knee Speed}
    \label{fig_og_knee_speed}
 \end{figure}  
 
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{Images/results/speed_knee_2.png}
    \caption{New Model Knee Speed}
    \label{fig_knee_speed}
 \end{figure}
 
  

\subsection{Weaknesses of Original System}

The original proposed system and model provides a very basic approximation of a robotic leg system system. There are a variety of factors that the system does not consider including but not limited to:

\begin{itemize}
    \item The power control of the system (PWM or otherwise)
    \item Interaction of the system with the ground or external forces
    \item Stability compensation of the leg
    \item The 3D manipulation and modeling of the joints
\end{itemize}

Failing to consider these parameters will create a model that does not accurately approximate the human gait. This can be beneficial for further work but with a goal of creating an assistive robotic leg, this can create non-intuitive and uncomfortable motion. Using a more sophisticated and accurate model will provide a much more natural feeling and allow the robotic leg to respond more predictably to user input stimuli. 

\subsection{Literature Review}

Robotic locomotion is an active research field with many recent and cutting-edge developments. \cite{lower_limb_review} provides a foundational review of the current state of the art in lower limb rehabilitation exoskeleton robots and summarizes the field as follows:

\begin{quote}
Exoskeleton robot technology is a comprehensive technology that integrates sensing, control, information and computer science to provide a wearable mechanical device. Many enterprises and research institutions have carried out relevant research work and achieved several milestones in the theory and application of these robots. According to their application, these robots are divided into two types, namely for treadmill-based and overground applications. Patients can receive gait training from treadmill-based exoskeleton robots on a treadmill. In these robots, in addition to the exoskeleton that is used to provide assistance to leg movement, a body weight support (BWS) system is required to reduce gravitational forces acting on the legs, ensure safety, and maintain balance; some examples of such robots include ALEX, Lokomat, and LOPES 
\end{quote}

The research stresses the importance of analyzing and understanding the human gait in creating exoskeleton robots which is an element that the original paper neglected but the new model includes. Human gait is determined by many parameters including walking speed and joint angle and that body height does not have as much of an impact as previously thought. Primitive models and mechanism oversimplify the joints like the hip and knee. The use of 3 parallel mechanisms for hip motion to allow for different rotations can be used to allow for a much smoother and more natural motion. Additionally, the knee is not a simple prismatic joint and must account for axial translational motion in additional to standard rotational motion. Advancements are also being made in passive support and actuation which does not require a motor, power, and a driver and reduces complexity of the overall design.

The exoskeleton robots, which are used extensively for rehabilitation, also need to provide feedback to the user to inform them of their performance. \cite{visual_feedback} propose a system for providing visual feedback to users using the C-ALEX locomotion system via an augmented reality headset. The researchers analyzed three types of feedback: haptic, haptic and visual, and just visual and each group showed an increase in normalized step height and decreased normalized trajectory tracking error but the visual group showed the biggest improvements in performance. This advancement is quite promising because it highlights the possibilities provided by interdisciplinary collaborations. Using computer vision and augmented reality improves outcomes for people seeking rehabilitation treatment with robotic exoskeletons and in the future further collaborations integrating machine vision, artificial intelligence, and deep learning promise to provide a more natural, efficent, and effective rehabilition process for patients.

\cite{quasi_passive_exoskeleton} describes a quasi-passive exoskeleton system that does not contain any actuators and uses only springs and dampers. They have shown "that with a 36 kg paylod, the exoskeleton transfers on average 80\% of the load to the ground during single support phase of walking." This work provides insight into the possibility of integrating passive and active systems to create user-customized exoskeletons. Each user of an exoskeleton will have different needs and abilities and having a wide variety of techniques and methods available is quite beneficial. These systems are not currently very accessible to users who need them. Physical therapy programs and research labs test and develop these exoskeletons but widespread consumer adoption has yet to happen because of the high price point and complexity of existing systems. For exoskeletons to truly benefit the broader population in the future the cost of the exoskeletons will need to decrease dramatically in addition to improvements in ease of use and reduction in complexity. With these advancements though, the field promises to improve and offer mobility improvements to those who otherwise would be unable to walk unassisted.