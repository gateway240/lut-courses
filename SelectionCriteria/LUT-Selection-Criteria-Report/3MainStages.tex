\section{Research}
\label{ref_mainstages}

\subsection{Batteries}
A significant portion of industry and academic research regarding EV's is focused around battery technology. Researchers are working on many cutting edge technologies for both improving the longevity, sustainability, and efficiency of batteries. This section will focus on the current requirements of EV manufacturers and different battery materials that satisfy those requirements.

\subsubsection{Requirements Profile} 

Researchers in collaboration with EV manufacturers have identified a series of critical functional requirements for EV batteries. These are essential to increase the market share of EVs in the future and drive their adoption in climates and region where they lack popularity due to temperature or higher costs compared to internal combustion vehicles (ICV). These requirements are shown in table \ref{tab:bat_functional_req}.

\begin{table}[H]
    \centering
    \begin{tabular}{||l|p{10cm}||} 
     \hline
     Category  & Functional Requirements \\ [0.5ex] 
     \hline\hline
     Wear Resistance & 
     \begin{itemize}
         \item The battery should be able to withstand a wide range of temperatures typically experienced during operation in hot or cold climates
    \end{itemize}
    \nointerlineskip\\ 
     \hline
     Corrosion Resistance & 
    \begin{itemize}
        \item The battery should have lifespan equal or grater than the lifespan of the EV 
        \item The battery should be able to be recharged quickly without significantly degrading battery performance
    \end{itemize}
    \nointerlineskip\\ 
     \hline
     Strength Properties & 
     \begin{itemize}
         \item The battery should have a high energy density to ensure sufficient driving range
    \end{itemize}
    \nointerlineskip\\ 
     \hline
     Reliability & 
     \begin{itemize}
         \item The battery should safely withstand impact, thermal, and vibrational loads placed upon it
         \item The battery should have sufficent safety measures (fuses, emergency shutdown systems, etc) to prevent catastrophic battery failure and fires
    \end{itemize}
    \nointerlineskip\\ 
    \hline
     Sustainability & 
     \begin{itemize}
         \item The battery cell should be able to be reused in less demanding applications, like stationary storage from solar cells
         \item The battery cell should be recyclable in a cost-effective manor
    \end{itemize} 
    \nointerlineskip\\
    \hline
    Cost Effectiveness & 
    \begin{itemize}
         \item Cost of the battery should not make the EV exceed the cost of a corresponding ICV 
    \end{itemize} 
    \nointerlineskip\\ [1ex]
    \hline
    \end{tabular}

    \caption{Battery Functional Requirements \cite{ev_battery_reqruiements_2020}}
    \label{tab:bat_functional_req}
\end{table}

\subsubsection{Pre-selection of Materials}

In this section the researchers have pre-selected potential materials for the EV that will be used in the following sections analysis. Materials are selected and analyzed from both novel and conventional materials. Solid state batteries will represent the next major advancement in battery technologies but they are not included here as they will not be ready for market until 5-10 years from now. Each of the subsequent paragraphs will provide information on one of the selected battery materials and insight into why it was selected.

\textbf{Lead-acid} batteries have long been the cheapest and most common vehicle batteries available and are still widely used in conventional automobiles. In this study sealed deep cycle lead acid batteries are focused on which are commonly used to drive smaller EVs like golf carts. Lead acid batteries were initially used in early EVs but have primarily faded away in recent years due to their low energy density and high weight. Additionally charging lead acid batteries release hydrogen, oxygen, and sulfur as part of a natural process which can cause an unpleasant smell inside the EV unless properly vented. Additionally lead acid batteries are remarkably well recycled. In the United States, ``Approximately 1.88 million tons of batteries in municipal solid waste weight are recycled every year for a remarkable 96.9\% recycling rate, aided by laws in 37 states requiring retailers to collect old lead-acid batteries from customers who buy new ones'' \cite{lead_acid_recycling}.

Lithium Ion batteries are currently the most popular and widely used batteries for electric vehicles due to their higher energy density compared to lead acid batteries. Lithium Ion batteries also have a longer general lifespan than lead acid batteries. The next few paragraphs will focus on common and novel kinds of Lithium Ion batteries available. There are many more types of Lithium Ion batteries available but these were selected because of their proven or promising usage as EV batteries.  

\textbf{Lithium Titanate Oxide (LTO)} batteries replace the conventional graphite anode in a Lithium Ion battery with a lithium titanate oxide-based anode. This provides many improvements and some downfalls in the properties of the battery. ``Due to the low theoretical specific capacity of spinel structured LTO and the high working potential of 1.55V during lithiation / delithiation (resulting in a lower full-cell voltage), the energy density of a battery cell is reduced. The main advantage of LTO results from its ”zero-strain” characteristic: Lithium insertion / extraction takes place in a highly reversible reaction happening without notable volume change.'' \cite{lto_battery}. What this means is that the cell will have exceptional life over other comparable lithium-ion battery cells. Some cells can last up to 60,000 cycles when normal lithium ion batteries last between 500 - 1500 cycles. LTO is also non-toxic and not reactive in the environment though it is still difficult to recycle. These cells are also able to charge in very cold temperatures, even down to -30 $\degree$C although their operating temperature is not significantly different from other lithium ion batteries. 

\textbf{Lithium Nickel Cobalt Aluminium Oxide (NCA)} batteries are less commonly use in EVs but are becoming more popular because of their high power density and life span compared to cost and safety factors. Although this technology has existed for two decades already it was until recently relegated to special and niche applications. The NCA battery adds aluminium to as an evolution of the lithium nickel oxide battery to increase battery chemistry stability. NC batteries offer some of the highest power and energy densities of commercially available EV batteries. ``Nickel-rich cathode material (NCA) (LiNixCoyAlzO2) batteries have been applied to TESLA Electric Vehicles (EVs) due to their superior performance and lower price compared to commercially established LCO batteries'' \cite{nca_battery}. NCA batteries are not as commonly in industry because of their higher cost, safety concerns, and harder fabriaction process.

\textbf{Lithium Nickel Manganese Cobalt Oxide (NMC)} batteries are very commonly used in power tools and electric bicycles. It is also a very common battery in current EVs on the market. On its own, nickel is very reactive but has a high specific energy. When combined, the manganese forms a spinal structure which provides low internal resistance that can stabilize the manganese which provides a nice balance of energy density and stability. ``The cathode mix of 33\% nickel, 33\% manganese and 34\% cobalt offers a novel mix that brings lower raw material costs because of the decreased cobalt content. For the perfect mix, manufacturers protect their specific formulas. Presently, this battery is in great demand for EV applications due to their high specific energy and minimum self-heating rate'' \cite{energy_management_of_ev_battery}. This battery technology is used by Tesla in some of their electric vehicle and home energy storage batteries. There is also research being conducted to recycle and recover the nickel, cobalt, and lithium in these batteries and reuse them for power grid storage applications. 

\textbf{Silicone Nanowire Li-Ion Cell (Si Nanowire)} batteries are an upcoming battery technology that incorporates Silicone Nanowires to dramatically increase the energy density. Conventional batteries discussed in the prior paragraphs utilize a carbon or metallic anode which has reaching have reached their energy density limits dictated by chemistry. Using silicone as an anode material has the potential to store up to 10 times more lithium to dramatically increase the energy density of the battery. The problem is that silicon swells and bulges when charged with lithium which causes cracking and subsequently the batteery stops working. ``In 2007, scientists at Stanford University discovered a solution to the swelling problem of silicon anodes. Silicon nanowires were shown to tolerate swell and resist cracking. Amprius has perfected this technology and the result is the world’s first 100\% silicon nanowire anode for lithium-ion batteries'' \cite{si_nanowire_ampirus}. Although this technology is under active development and not widely implemented in industry yet, it has the potential to dramatically improve battery energy densities and performance.

\subsubsection{Material Property Profile}

The following list contains qualitative requirements for each battery type for certain parameters that cannot be qualitatively calculated. For example the reliability requirements stating durability, safeguards, and measures to prevent catastrophic failures cannot be easily quantified. 
\begin{enumerate}
    \item Lead-acid Batteries \cite{sealed_lead_acid_battery}
    \begin{enumerate} 
        \item Valve regulated, spill proof construction allows safe operation in any position 
        \item Rugged impact resistant ABS case and cover
        \item U.L. listed
    \end{enumerate}
    \item Lithium batteries (NCA, NMC, and LTO) are subjected to the following tests to ensure safety and reliability:
    \begin{enumerate}
        \item U.L. 1642 - Independent product safety test certification for lithium batteries 
        \item Military Nail Penetration test
        \item Military Drop Test
        \item NASA Hot Box Test (110 $\degree$ C/1h)
        \item NASA Short Circut Test
        \item NASA Overcharge Test
        \item NASA Overdischarge/Reversal test
        \item UN38.3 Certification for safe shipping of Lithium Ion batteries
    \end{enumerate}
    \item Si Nanowires pass the same tests as the regular lithium batteries listed above \cite{slides_on_silicone_nanowire}
\end{enumerate}

Table \ref{tab:bat_mat_prop} shows the qualitative material properties for the selected materials that will be used the systematic material selection for the batteries. 

\input{batteries/battery_material_properties}

\subsection{Chassis}

The chassis in any vehicle is one of the most vital components that can make or break a vehicle. It is the foundation of any car build due to its role in vehicle dynamic behavior. Researchers have been working on ways to optimize chassis performance without compromising on overall structural weight gain. A few examples of recent research in EV chassis development is the design prototype of a skateboard chassis. Companies are trying to effectively produce this design which not only saves costs but also reduces complexity of overall chassis design as all the electrical and mechanical components can be integrated into it. 

\subsubsection{Requirements Profile}

The chassis houses all the components of an electric car including the batteries, motors, etc. Hence, the chassis needs to suit a specific set of requirements in various categories in order to help material selection process. 
\begin{table}[H]
    \centering
    \begin{tabular}{||l|p{10cm}||} 
     \hline
     Category  & Functional Requirements \\ [0.5ex] 
     \hline\hline
     Wear Resistance & 
     \begin{itemize}
         \item The chassis should be able to withstand a wide range of loads and temperatures that are experienced in off road conditions as well as extreme temperatures. 
    \end{itemize}
    \nointerlineskip\\ 
     \hline
     Corrosion Resistance & 
    \begin{itemize}
        \item The chassis should be able to resist basic atmospheric corrosion due to everyday exposure
        \item The chassis must also resist chemical corrosion that might arise from battery leakage or other factors. 
    \end{itemize}
    \nointerlineskip\\ 
     \hline
     Strength Properties & 
     \begin{itemize}
         \item The chassis must have high density and yield strength to carry the load of the components placed on it.
    \end{itemize}
    \nointerlineskip\\ 
     \hline
     Reliability & 
     \begin{itemize}
         \item The chassis should be able to withstand heavy impact from off-road conditions or crashes.
    \end{itemize}
    \nointerlineskip\\ 
    \hline
     Sustainability & 
     \begin{itemize}

         \item The chassis needs to be built in a way to reduce replacement or recycling.
    \end{itemize} 
    \nointerlineskip\\
    \hline
    Cost Effectiveness & 
    \begin{itemize}
         \item Cost of chassis build should be considered as equally as any other component in order to find the right compromise,
    \end{itemize} 
    \nointerlineskip\\ [1ex]
    \hline
    \end{tabular}

    \caption{Chassis Requirements}
    \label{tab:bat_functional_req}
\end{table}

\subsubsection{Pre-selection of Materials}

In this section, the different potential materials that are currently being used for EV chassis will be discussed and compared based on weight and strength which are arguably the most instrumental factors affecting chassis material selection. 

\textbf{High Structural Steel} is a complex material that goes through multiple heating and cooling processes before being used for an EV chassis. These treatments make HSS a high density and high strength material compared to other materials such as mild steel. However, it is a bit more pricey than its mild steel and alloy steel counterparts nevertheless, it is still more affordable than other materials showing similar strength.

\textbf{Magnesium} has had it's fair share of use in racing car chassis in the past. Due to the modern day increase in components and complexity of design, Magnesium has not been a popular option. It is highly malleable and is far lighter than Aluminium which in turn is lighter than steel. It's decent yield strength allows it to be used for some components in a car such as the gearbox, steering column but the strength, around 300 Mpa is not enough for high load bearing uses such as a typical EV chassis. Also, the cost of Magnesium, especially for this application, where there are other feasible alternatives, does not help its case. 

\textbf{Aluminium} is a very popular material for automotive chassis as it displays quite a lot of prior benefits such as light weight, corrosion resistance, high ductility, high mobility and high recyclability. It has a growing use in modern day hybrid vehicles made by brands like Audi, Toyota, Nissan, Mercedes which are all commercial car makers. However, Tesla, an EV company uses Aluminium for almost the entire body and chassis. Although Aluminium does not have a far greater yield strength than Magnesium, its balance of malleability and strength allows for experimenting with shapes and loads without risking material failure. 

\textbf{Titanium} is quite popular but not as abundant as its metal counterparts. It displays high yield strength often up to 880 MPa, high corrosion resistance and wear resistance. But, it has relatively mediocre density. Titanium is mostly used in engine components, such as valves, valve spring, retainers, and connecting rods where there high loads/temperatures acting at sudden intervals. It is far lighter than mild steel and other steel alloys. The only factors, albeit quite influential, which does not make Titanium the first choice for chassis material are the cost and its availability. 

\textbf{Carbon-Fibre Composites} are growing quite extensively in the manufacturing sector, primarily aerospace and automotive industries. With their excellent specific yield strength reaching up to 1230 MPa, they are suitable for extreme applications. The major drawback of this material is its high cost compared to commercially used materials combined with its exclusivity to high intensity application in motorsports and aerospace. 

\subsubsection{Material Property Profile}

The table below displays the most important properties of the materials above that affect the selection process. 

\input{Chassis/Chassis_material_properties}

 


\subsection{Motor}
The motor takes care of energy of electric car. Nowadays electric car is very powerful. Effective of electric motors is very high. There are several different types of electric motors. But all of them has two main components, stator and rotor. Also, for example different micro computers are inside motor. Therefore, strength block will cover expensive parts in engine. The block has also one other very important task, it will keep engine parts together.

\subsubsection{Requirements Profile}

\begin{table}[H]
    \centering
    \begin{tabular}{||l|p{10cm}||} 
     \hline
     Category  & Functional Requirements \\ [0.5ex] 
     \hline\hline
     Wear Resistance & 
     \begin{itemize}
         \item 	The block protects important and expensive parts so material needs high wear resistance properties
    \end{itemize}
    \nointerlineskip\\ 
     \hline
     Corrosion Resistance & 
    \begin{itemize}
        \item 	Corrosion resistance of block material must be high, expensive and sensitive parts protection
        \item Different climates, for example cold winter in Finland
    \end{itemize}
    \nointerlineskip\\ 
     \hline
     Strength Properties & 
     \begin{itemize}
         \item Attachment between motor and frame, fatigue and high torque
    \end{itemize}
    \nointerlineskip\\ 
     \hline
     Reliability & 
     \begin{itemize}
         \item  Expensive parts inside, possibility they breaks down if the block failures 
        \item Failure of the block can cause even accidents        
        \item Humans are inside car, all that of parts must be reliability
    \end{itemize}
    \nointerlineskip\\ 
    \hline
     Sustainability & 
     \begin{itemize}

         \item	Electric car are green technology so material of parts should be recyclability         		           \item Possibility to use recycled material
    \end{itemize} 
    \nointerlineskip\\
    \hline
    Cost Effectiveness & 
    \begin{itemize}
         \item	Manufacturability must be high, the block includes very difficult shapes to manufacture so costs reduce if material has high manufacturing properties
    \end{itemize} 
    \nointerlineskip\\ [1ex]
    \hline
    \end{tabular}

    \caption{Motor block Requirements}
    \label{tab:bat_functional_req}
\end{table}
\subsubsection{Pre-selection of Materials}

In this section, the researchers have pre-selected some potential materials for the EV. Comparing is based on strength, hardness, density and cost which are the most important properties for material of block.

\textbf{Duralumin} is aluminium alloy. It has very high strength compared to density. Mechanical properties of duralumin can be improved by applying the aging process. Nowadays duralumin is used in automotive and aerospace industries. \cite{Puspitasari_2017} Duralumin is used as the block material in EV.  \cite{Duralium}

\textbf{High strength steel} is hot rolled complex material. Strength properties of this material are very high. Tensile strength can be even 1100 MPa. High strength steels are used in applications which needs less material and high strength. Price of high strength steel is higher than normal structural steel but lower than duralumin. Hardness of high strength steel is high. It can make machining complicated. \cite{high_strength_steel}

\textbf{S355 }is widely used structural steel. Tensile strength of material is 355 MPa.  It can be manufactured by hot rolling or cold forming. Manufacturability of S355 is higher than high strength steel because hardness is lower. Density of steel materials are quite same, 7800kg/m^3 \cite{S355}

\input{Motor/motor_material_properties}
