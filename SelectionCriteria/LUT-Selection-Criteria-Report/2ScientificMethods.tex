\section{Methods}
\label{ref_methods}
When selecting a material for an engineering application it is critical to follow the systematic material selection process. This process, shown in figure \ref{fig_systematic_material}, outlines a series of criteria and steps that can be used to evaluate and select an appropriate material.   By following this process the researchers can empirically evaluate the suitability of proposed materials for the given component or structure of the electric vehicle. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/systematic_material_selection_process.PNG}
    \caption{The Systematic Material Selection Process \cite{slides_on_systematic}}
    \label{fig_systematic_material}
\end{figure}

As figure \ref{fig_systematic_material} shows, the main steps of the systematic material selection process are shown as follows:

\begin{enumerate}
  \item Elaboration of the requirements profile.
  \item Decision about the selection strategy
  \item Pre-selection of possible materials
  \item Elaboration of the materials' property profile
  \item Integration of the requirement and the property profiles 
  \item Monitoring and Feedback 
\end{enumerate}

Monitoring and feedback of the selection is beyond the scope of this report since the material selections described here would be suitable for real world implementation but due to the constraints of the course is not possible. As such, this work will focus primarily on steps 1 through 5. 

\subsection{Requirements Profile}
\label{ref_req_profile}

The researchers will construct a requirements profile for each component examined in this study. This requirements profile will at minimum include the components listed below but may include additional depending on the specific use case.

\begin{enumerate}[(a)]
    \item Wear Resistance
    \item Corrosion Resistance
    \item Strength Properties
    \item Manufacturability
    \item Reliability
    \item Environmental and Sustainability Aspects
    \item Cost Effectiveness
\end{enumerate}

\subsection{Selection Strategy}
\label{ref_selection_strategy}

Once the requirements profile has been established the study will choose four different selection strategies. The first selection strategy will seek to find a well balanced material and will weight each of the requirements equally. The second selection strategy will skew towards optimizing cost with less concern about environmental impact since cost reduction is desirable for increasing profits in industry. The third selection strategy will skew towards optimizing environmental and sustainability aspects with less regard toward cost since this is desirable for maintaining a sustainable future. The fourth selection strategy will optimize performance with less regard for sustainability and cost.  

\subsection{Pre-Selection of Materials}
\label{ref_pre_select}

For each selected component of the vehicle, the researchers will analyze available materials. There will be some materials selected that are commonly used in the given application. Additional novel and new materials will be selected that have promising use in the application but are not very common yet due to cost, lack of industry readiness, or other factors.

\subsection{Material Property Profile}
\label{ref_material_property}

After the requirements profile is defined as described in section \ref{ref_req_profile} and the materials are pre-selected as described in section \ref{ref_pre_select}, the researchers will obtain empirical values corresponding to each material property. For example, to analyze the strength properties of the selected materials the Young's modulus could be obtained for each material. Some requirements may have many empirical values while others may have very few. For requirements that cannot be specified quantitatively, quantitative requirements will be provided. 

\subsection{Requirement and Property Profile Integration}
\label{req_sec_requirement_integration}
Once the material properties have been defined as described in section \ref{ref_material_property}, each property will be categorized into one of four categories for the studies described in section \ref{ref_selection_strategy} and used to create a weighted decision matrix. These categories will include stability/reliability, performance, sustainability/ecological, and cost. If there are more than one property for a given category, each property will be assigned an equal sub-weight within the weighting for the category. For the first study all requirements will have equal weight as shown in figure \ref{fig_equal_weight}. For the weighted studies the weighted requirement will have a 70\% weight while each remaining category will have 10 \% as shown in figure \ref{fig_performance_weight}. While the performance study is shown the same concept applies to the subsequent studies.  

This will output a singular value on a scale of 0 to 1 for each material where the highest value indicates the specific material is the best to satisfy the requirements selected. The researchers will compare the outcomes of the four trials to determine the results. Additionally the researchers will investigate if any of the trials yield results inline with existing industry standard materials. If this is the case then the industry has performed and systematically selected materials based on either neutral, cost-skewed, performance-skewed or environmentally-skewed requirements. If the results do not align with the industry standard material it indicates the industry is considering different requirements or not performing a systematic material selection process.


\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \pie{25/Stability, 25/Performance, 25/Ecological, 25/Cost}
    \end{tikzpicture}
    \caption{Equal Weighting }
    \label{fig_equal_weight}
\end{figure}

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \pie{10/Stability, 70/Performance, 10/Ecological, 10/Cost}
    \end{tikzpicture}
    \caption{Category Weighting }
    \label{fig_performance_weight}
\end{figure}

\subsubsection{Data Computation}
For calculating the procedure described above, a matrix will be created in Matlab that contains all the material property profiles. Each row will represent a material and each column will represent its corresponding properties. This matrix will then be column normalized (range from 0-1) since each property will have different units. This ensures that no one requirement causes a bias in the results. Additionally since a higher value is considered better in this study, any parameter where a lower value is better, like cost will be accounted for.  To achieve this each value in that column will be converted to 1 subtract the current value. This will make it so that lower costs are ranked higher. For ranges of values where a higher range is better, the span of the range will be calculated and used the that columns values. For example a range of -40 - 40 would yield a cumulative range value of 80.  Then the weighting matrix will be constructed where each column represents the weights for each requirement for a single study (neutral, eco, etc.). Then these two matrices are multiplied together to yield a value ranging from 0 to 1 for each material. The highest value in each column indicates the optimal material selection for the weighting of that study.