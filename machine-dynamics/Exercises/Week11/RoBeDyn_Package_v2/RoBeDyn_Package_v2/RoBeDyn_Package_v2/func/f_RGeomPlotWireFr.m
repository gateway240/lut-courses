function f_RGeomPlotWireFr(Inp,xxplot,pnum,ltype)
% ===================================================================
% Function f_RGeomPlotWireFr plots Rotor Geometry as a WireFRame plot
% f_RGeomPlotWireFr(Inp,xxplot,pnum,ltype)
% Parameters:
% -----------------
% Inp     = Model input data (Structural array)
% xxplot  = component plot [E N B D M], where
%           E = plot elements (0=off,1=on)
%           N = plot nodes (0=off,1=on)
%           B = plot bearings (0=off,1=on)
%           D = plot constraints (0=off,1=on)
%           M = plot masspoints (0=off,1=on)
% pnum    = plot element and node numbers [E, N] (0=off,1=on)
% ltype   = cell array, which contains the line type and linewidth of the
%           element line e.g. {'-b',3}


% Written by Jussi Sopanen, LUT, 2004 (version 2.1 2006)
% ===================================================================

%default arvojen asetus
if nargin<2
    xxplot=[1 1 1 1 1]; %tulostetaan kaikki komponentit
end
if nargin<3 %
    pnum=[0 1]; %ei tulosteta elementtinumeroita, tulostetaan solmunumerot
end
if nargin<4;
    ltype={'-b',3}; %roottorin keskiviivan tyyppi
end

% Input arvojen tallennus uudelle nimelle
Node=Inp.Node;
Elem=Inp.Elem;
Disp=Inp.Disp;
Real=Inp.Real;

% uusi kuva
figure   
% kuva asetukset
set(gcf,'Units','normalized')		   % Units normalized (always)
set(gcf,'Position',[0.1 0.1 0.8 0.8])  % Position set to given
set(gcf,'Color',[1 1 1])			   % Background color white (always)

%Mallin mittojen laskenta--------------------------------------
model_size=[max(Node(:,2))-min(Node(:,2))  max(Node(:,3))-min(Node(:,3)) max(Node(:,4))-min(Node(:,4))];
%node number offset (solmunumeron asema=(xnode,ynode+nnos,znode+nnos)
nnos=max(model_size)*0.02; % 2 prosenttia mallin maksimi koosta

% legendin j�rjestysnumeron laskuri
NumLeg=0;

%Elementtien piirto----------------------------------------------------
if xxplot(1)==1
    
    for ii=1:size(Elem,1)
        
        if ~Elem(ii,3) == 0  % Jos ei ole massaelementti
            %for jj=1:size(Node,1)
            %    %Haetaan I noden rivi Node matriisista
            %    if Elem(ii,2)==Node(jj,1)
            %        Iindx=jj; 
            %    end
            %    %Haetaan I noden rivi Node matriisista
            %    if Elem(ii,3)==Node(jj,1)
            %        Jindx=jj; 
            %    end
            %end
            % n�m� rivit tekev�t saman asian kuin edellinen for-if h�ss�kk�
            Iindx= (Elem(ii,2)==Node(:,1));
            Jindx= (Elem(ii,3)==Node(:,1));
            
            %Elementin keskiviiva ltype m��rittelyn mukaisesti
            h_leg(1)=plot3( [Node(Iindx,2) Node(Jindx,2)], [Node(Iindx,3) Node(Jindx,3)],...
                [Node(Iindx,4) Node(Jindx,4)],ltype{1},'linewidth',ltype{2});
            t_leg{1}='Centerline'; %legendin teksti
            hold on
            
            %---------------------------------------------------------
            %Piirret��n solmupisteisiin ympyr� kuvaamaan akselin
            %paksuutta. Pit�isi toimia yleisess� tapauksessa, eli
            %solmupisteet voisivat olla vapaasti avaruudessa, eik�
            %pelk�st��n x-akselilla...
            
            %ind=find(Real(:,1)==Elem(ii,5)); % etsit��n real constanttia vastaava rivi
            %dia=Real(ind,5); % Akselin halkaisija (Elem(ii,5)=Real constantin numero) 
            % vanha versio
            %dia=Real(Real(:,1)==Elem(ii,5),5); %nopeampi kuin edell�...?
            %[xc,yc,zc]=cylinder(dia/2,20); %generoidaan sylinteri
            
            % Muutos 24.3.2010 Real constant sarake 5 on d_out ja 6 d_in
            d_out=Real(Real(:,1)==Elem(ii,5),5); 
            d_in=Real(Real(:,1)==Elem(ii,5),6); 
            
            [xc,yc,zc]=cylinder(d_out/2,20); %generoidaan sylinteri
            if ~(d_in==d_out || d_in==0)
                [xci,yci,zci]=cylinder(d_in/2,20); %generoidaan sylinteri
            end
            % sylinterin k��nt� globaalin koordinaatiston suuntaiseksi
            v=[0 1 0]'; % Vektori, jonka ymp�ri kierto tapahtuu
            theta=pi/2; % kiertym�n suuruus
            A=eye(3)+skew(v)*sin(theta)+2*skew(v)^2*sin(theta/2)^2;  %Rotaatiomatriisi (Rodriquesis kaava)      
            % lasketaan kiertomatriisi elementin solmupisteist� (suuntakosinien avulla)
            startp=[Node(Iindx,2) Node(Iindx,3) Node(Iindx,4)]';
            endp=[Node(Jindx,2) Node(Jindx,3) Node(Jindx,4)]';
            TI=T3D(startp,endp); 
            % lasketaan ympyr�n pisteiden asemat
            for kk=1:length(xc)
                %cylinterin coord ulkopuoli
                rotv1=(startp) + TI'*(A*[xc(1,kk) yc(1,kk) zc(1,kk)]');
                xr1(1,kk)=rotv1(1); yr1(1,kk)=rotv1(2); zr1(1,kk)=rotv1(3);
                rotv2=(endp) + TI'*(A*[xc(1,kk) yc(1,kk) zc(1,kk)]');
                xr2(1,kk)=rotv2(1); yr2(1,kk)=rotv2(2); zr2(1,kk)=rotv2(3);                
                
                %sis�puoli
                if ~(d_in==d_out || d_in==0)
                    rotv1=(startp) + TI'*(A*[xci(1,kk) yci(1,kk) zci(1,kk)]');
                    xr1i(1,kk)=rotv1(1); yr1i(1,kk)=rotv1(2); zr1i(1,kk)=rotv1(3);
                    rotv2=(endp) + TI'*(A*[xci(1,kk) yci(1,kk) zci(1,kk)]');
                    xr2i(1,kk)=rotv2(1); yr2i(1,kk)=rotv2(2); zr2i(1,kk)=rotv2(3);
                end
            end
            % Piirret��n ympyr�t ulkopuoli
            plot3(xr1,yr1,zr1,'-c','linewidth',1)
            plot3(xr2,yr2,zr2,'-c','linewidth',1)
            %sis�puoli
            if ~(d_in==d_out || d_in==0)
                plot3(xr1i,yr1i,zr1i,'--b','linewidth',1)
                plot3(xr2i,yr2i,zr2i,'--b','linewidth',1)
            end
            % piirret��n viivat ympyr�iden v�lille 90 asteen v�lein
            for kk=1:5:20
                h_leg(2)=plot3([xr1(kk) xr2(kk)],[yr1(kk) yr2(kk)],[zr1(kk) zr2(kk)],'-c','linewidth',1);
                if ~(d_in==d_out || d_in==0)
                plot3([xr1i(kk) xr2i(kk)],[yr1i(kk) yr2i(kk)],[zr1i(kk) zr2i(kk)],'--b','linewidth',1)
                end
                t_leg{2}='Shaft'; %asetetaan legendin teksti
            end
        end %palkkielementtien piirto

        %elementtinumerot tekstin� 
        if pnum(1)==1
            % teksti hieman sivuun nodesta (sama x-koord.)
            text(mean([Node(Iindx,2) Node(Jindx,2)]),...
                mean([Node(Iindx,3) Node(Jindx,3)]+nnos),...
                mean([Node(Iindx,4) Node(Jindx,4)]+nnos),...
                int2str(Elem(ii,1) ),'color',[0 0 1])
        end
    end
    
    % legendin j�rjestysnumeron laskuri
    NumLeg=NumLeg+2;
    
    
end % if xxplot(1)==1

%Solmujen piirto & solmunumerot---------------------------------- 
if xxplot(2)==1
    % legendin j�rjestysnumeron laskuri
    NumLeg=NumLeg+1;
    for ii=1:size(Node,1)
        % solmu tulostetaan pallona
        h_leg(NumLeg)=plot3( Node(ii,2), Node(ii,3), Node(ii,4), 'ok','linewidth',2);
        hold on
        t_leg{NumLeg}='Node'; %legendin teksti
        %solmunumerot tekstin� solmun viereen 
        if pnum(2)==1
            text(Node(ii,2),Node(ii,3)+nnos,Node(ii,4)+nnos,int2str(Node(ii,1) ),...
                'HorizontalAlignment','center','color',[0 0 0])
        end
    end
end

% Piirret��n laakerit
if xxplot(3)==1
    
    % legendin j�rjestysnumeron laskuri
    if size(Inp.Bearing,1)>0; NumLeg=NumLeg+1; end;
    
    for ii=1:size(Inp.Bearing,2)
        % laakerin halkaisija ja leveys
        if (~isfield(Inp.Bearing(ii),'D') || isempty(Inp.Bearing(ii).D)) % jos ei halkaisijaa m��ritelty
            dia=max(model_size)*0.05; % jos ei m��ritelty, niin k�ytet��n 5% mallin koosta
        else
            dia=Inp.Bearing(ii).D*1e-3; %muunnos mm-->m
        end
        if (~isfield(Inp.Bearing(ii),'B') || isempty(Inp.Bearing(ii).B)) % jos ei halkaisijaa m��ritelty
            B=dia/4;
        else
            B=Inp.Bearing(ii).B*1e-3; %muunnos mm-->m
        end
        [xc,yc,zc]=cylinder(dia/2,20); %generoidaan sylinteri
        % sylinterin k��nt� globaalin koordinaatiston suuntaiseksi
        v=[0 1 0]'; % Vektori, jonka ymp�ri kierto tapahtuu
        theta=pi/2; % kiertym�n suuruus
        A=eye(3)+skew(v)*sin(theta)+2*skew(v)^2*sin(theta/2)^2;  %Rotaatiomatriisi (Rodriquesis kaava)
        % etsit��n solmua vastaava rivi
        ind=find(Node(:,1)==Inp.Bearing(ii).Inode);
        % laakerin alkupiste ja loppupiste
        startp=[Node(ind,2) Node(ind,3) Node(ind,4)]'+[-B/2 0 0]';
        endp=[Node(ind,2) Node(ind,3) Node(ind,4)]'+[B/2 0 0]';        
        
        % lasketaan ympyr�n pisteiden asemat kiertomatriisien avulla
        for kk=1:length(xc)
            %cylinterin coord
            rotv1=(startp) + (A*[xc(1,kk) yc(1,kk) zc(1,kk)]');
            xr1(1,kk)=rotv1(1);
            yr1(1,kk)=rotv1(2);
            zr1(1,kk)=rotv1(3);
            rotv2=(endp) + (A*[xc(1,kk) yc(1,kk) zc(1,kk)]');
            xr2(1,kk)=rotv2(1);
            yr2(1,kk)=rotv2(2);
            zr2(1,kk)=rotv2(3);
        end
        % Piirret��n ympyr�t
        plot3(xr1,yr1,zr1,'-r','linewidth',2); hold on;
        plot3(xr2,yr2,zr2,'-r','linewidth',2)
        % piirret��n viivat ympyr�iden v�lille 90 asteen v�lein
        for kk=1:5:20
            h_leg(NumLeg)=plot3([xr1(kk) xr2(kk)],[yr1(kk) yr2(kk)],[zr1(kk) zr2(kk)],'-r','linewidth',2);
            t_leg{NumLeg}='Bearing'; %legendin teksti
        end
        
    end
end %xxplot(3)==1


%RAJOITTEET******************************************************
%k�yd��n Disp matriisi l�pi [Node, Dof, Value] (Dof: 1=UX, 2=UY, 3=UZ, 4=ROTX, 5=ROTY, 6=ROTZ)
if xxplot(4)==1
    
    % legendin j�rjestysnumeron laskuri
    if size(Disp,1)>0; NumLeg=NumLeg+1; end;
    
    % alustetaan stringi muuttuja
    %cnums{size(Disp,1)}='';
    cnums{max(Disp(:,1))}='';
    
    for ii=1:size(Disp,1)
        %for jj=1:size(Node,1)
        %    %Haetaan Disp noden rivi Node matriisista
        %    if Disp(ii,1)==Node(jj,1) %
        %        Dindx=jj; 
        %    end
        %end
        % t�m� tekee saman kuin aikaisempi for-if h�ss�kk�
        Dindx=Disp(ii,1)==Node(:,1); %
        
        %neli�n sivun pituus
        a=nnos; % k�ytet��n samaa mittaa kuin node-number-offset
        %solmupisteen koord
        x1=Node(Dindx,2);
        y1=Node(Dindx,3);
        z1=Node(Dindx,4);
        
        % tallennetaan stringi muuttuja rajoitteen numeroista, esim 1256
        cnums{Disp(ii,1)}=strcat(cnums{Disp(ii,1)},num2str(Disp(ii,2)));
        % tallennetaan koordinaatit rajoitteiden piirtoa varten
        numcoord(Disp(ii,1),:)=[x1 y1 z1];
       
        % piirret��n neli�t
        % xy-taso
        h_leg(NumLeg)=plot3([x1-a/2 x1+a/2 x1+a/2 x1-a/2 x1-a/2], [y1-a/2 y1-a/2 y1+a/2 y1+a/2 y1-a/2],...
            [z1 z1 z1 z1 z1],'g','linewidth',2);
        hold on
        % xz-taso
        plot3([x1-a/2 x1+a/2 x1+a/2 x1-a/2 x1-a/2], [y1 y1 y1 y1 y1],...
            [z1-a/2 z1-a/2 z1+a/2 z1+a/2 z1-a/2],'g','linewidth',2);
        
        t_leg{NumLeg}='Constraint'; %legendin teksti
    end
    
    % numerot tekstin�     
    for ii=1:size(Disp,1)
        text(numcoord(Disp(ii,1),1)+0*nnos,numcoord(Disp(ii,1),2)-nnos,numcoord(Disp(ii,1),3)-nnos,cnums{Disp(ii,1)},...
                'HorizontalAlignment','center','color','g')
    end
       
end

% Piirret��n kiekot eli massapisteet
% MassPoints=[ID, node, mass, Jxx,   Jyy,  Jzz,   h,   L, dia_out, dia_in];
if xxplot(5)==1

    % legendin j�rjestysnumeron laskuri
    if size(Inp.MassPoints,1)>0; NumLeg=NumLeg+1; end;
    
    for ii=1:size(Inp.MassPoints,1)
        
        ltm='-r'; %asetetaan ltype masspointtien piirrolle
        
        dia=Inp.MassPoints(ii,9); % kiekon halkaisija
        dia_in=Inp.MassPoints(ii,10); %kiekon sis�halkaisija
        h=Inp.MassPoints(ii,7); %offset
        L=Inp.MassPoints(ii,8); %leveys/pituus
        
        [xc,yc,zc]=cylinder(dia/2,20); %generoidaan sylinteri
        % synlinterin k��nt� globaalin koordinaatiston suuntaiseksi
        v=[0 1 0]'; % Vektori, jonka ymp�ri kierto tapahtuu
        theta=pi/2; % kiertym�n suuruus
        A=eye(3)+skew(v)*sin(theta)+2*skew(v)^2*sin(theta/2)^2;  %Rotaatiomatriisi (Rodriquesis kaava)
        % etsit��n solmua vastaava rivi
        ind=find(Node(:,1)==Inp.MassPoints(ii,2));
                
        % sylinterin alkupiste ja loppupiste
        startp=[Node(ind,2) Node(ind,3) Node(ind,4)]'+...
               [-L/2+h 0 0]';
        endp=[Node(ind,2) Node(ind,3) Node(ind,4)]'+...
               [L/2+h 0 0]';        
        
        % lasketaan ympyr�n pisteiden asemat kiertomatriiseja k�ytt�en
        for kk=1:length(xc)
            %cylinterin coord
            rotv1=(startp) + (A*[xc(1,kk) yc(1,kk) zc(1,kk)]');
            xr1(1,kk)=rotv1(1);
            yr1(1,kk)=rotv1(2);
            zr1(1,kk)=rotv1(3);
            rotv2=(endp) + (A*[xc(1,kk) yc(1,kk) zc(1,kk)]');
            xr2(1,kk)=rotv2(1);
            yr2(1,kk)=rotv2(2);
            zr2(1,kk)=rotv2(3);
        end
        % Piirret��n ympyr�t
        plot3(xr1,yr1,zr1,ltm,'linewidth',1)
        plot3(xr2,yr2,zr2,ltm,'linewidth',1)
        %piirret��n sis�halkaisijaan ympyr� 
        dr=dia_in/dia; %diameter ratio
        plot3(xr1,yr1*dr,zr1*dr,ltm,'linewidth',1)
        plot3(xr2,yr2*dr,zr2*dr,ltm,'linewidth',1)
        % piirret��n viivat ympyr�iden v�lille 90 asteen v�lein
        for kk=1:5:20
            h_leg(NumLeg)=plot3([xr1(kk) xr2(kk)],[yr1(kk) yr2(kk)],[zr1(kk) zr2(kk)],ltm,'linewidth',1);
            t_leg{NumLeg}='MassPoint';
        end
        % piirret��n viivat nodesta sis�halkaisijaan
        gx=[Node(ind,2) Node(ind,2) Node(ind,2) Node(ind,2)
            startp(1)   startp(1)   startp(1)   startp(1)];
        gy=[Node(ind,3) Node(ind,3) Node(ind,3) Node(ind,3)
            startp(2)+dia_in/2   startp(2)-dia_in/2   startp(2)   startp(2)];
        gz=[Node(ind,4) Node(ind,4) Node(ind,4) Node(ind,4)
            startp(3)   startp(3) startp(3)+dia_in/2   startp(3)-dia_in/2   ];
        plot3(gx,gy,gz,ltm) %alkupisteest� nodeen
        gx=[Node(ind,2) Node(ind,2) Node(ind,2) Node(ind,2)
            endp(1)   endp(1)   endp(1)   endp(1)];
        gy=[Node(ind,3) Node(ind,3) Node(ind,3) Node(ind,3)
            endp(2)+dia_in/2   endp(2)-dia_in/2   endp(2)   endp(2)];
        gz=[Node(ind,4) Node(ind,4) Node(ind,4) Node(ind,4)
            endp(3)   endp(3) endp(3)+dia_in/2   endp(3)-dia_in/2   ];
        plot3(gx,gy,gz,ltm) %loppupisteest� nodeen
        % piirret��n vaakaviivat sis�halkaisijan kohdalle
        gx=[startp(1) startp(1) startp(1) startp(1)
            endp(1)   endp(1)   endp(1)   endp(1)];
        gy=[startp(2)+dia_in/2 startp(2)-dia_in/2 startp(2) startp(2)
            endp(2)+dia_in/2   endp(2)-dia_in/2   endp(2)   endp(2)];
        gz=[startp(3) startp(3) startp(3)+dia_in/2 startp(3)-dia_in/2
            endp(3)   endp(3) endp(3)+dia_in/2   endp(3)-dia_in/2   ];
        plot3(gx,gy,gz,ltm)
    end % for masspoints
end %if

% kuvan yleiset asetukset
grid on
xlabel('X','fontsize',13)
ylabel('Y','fontsize',13)
zlabel('Z','fontsize',13)
title(Inp.model_title,'fontsize',14)

% legend
legend(h_leg,t_leg,'Location', 'South');

axis equal;  view(2); axis vis3d;


% -----------------------------------------------------------------------------
function [T]=T3D(startp,endp)
% T3D palauttaa siirtomatriisin elementin lokaalista koordinaatistosta
% globaaliin koordinaatistoon.
% function [T]=T3D(start,end)
%
% startp = alkupiste
% endp   = loppupiste
% 
% Written by Jussi Sopanen, LUT/IMVe, 2002
% ======================================================================================

theta=0;
% Elementin solmujen koordinaatit
X1=startp(1);
X2=endp(1);
Y1=startp(2);
Y2=endp(2);
Z1=startp(3);
Z2=endp(3);
% pituus
L=sqrt((X2-X1)^2+(Y2-Y1)^2+(Z2-Z1)^2);
% xy-tasoon projisoitu pituus
Lxy=sqrt(L^2-(Z2-Z1)^2);

% Trigonometristen funktioiden tarkkuusraja
d=0.0001*L;
% Siirtomatriisin termit
if Lxy>d
    S1=(Y2-Y1)/Lxy;
    C1=(X2-X1)/Lxy;
else
    S1=0;
    C1=1;
end

S2=(Z2-Z1)/L;
S3=sin(theta);
C2=Lxy/L;
C3=cos(theta);
% Osamatriisi
T=[  C1*C2             S1*C2            S2
   (-C1*S2*S3-S1*C3) (-S1*S2*S3+C1*C3)  S3*C2
   (-C1*S2*C3+S1*S3) (-S1*S2*C3-C1*S3)  C3*C2];
