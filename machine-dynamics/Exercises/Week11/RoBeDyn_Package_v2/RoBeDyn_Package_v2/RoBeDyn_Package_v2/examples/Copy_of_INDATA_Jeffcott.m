% INPUT FILE FOR ROTOR-BEARING DYNAMICS CODE (RoBeDyn)

%-Model name--------------------------------------------%
Inp.model_title='Overhung Jeffcott Rotor';



% material properties
E=2.0e11;
nuxy=0.28;
rho=7800;
ks=6*(1+nuxy)/(7+6*nuxy); %shear correction factor 
%-material--------------------------------------------%
%---- ID, E, nuxy, rho
Inp.Mat=[1, E, nuxy, rho
          ];
      
% Rotor parameters
D1=0.030;    % shaft diameter
D2=0.040;    % shaft diameter
% pi*D1^4/64
%-Real Constant-----------------------------------------% 
%--        ID,   A     ,     Izz,   Iyy      ,H, B,theta,istrn,    Ixx  , shearz,sheary
Inp.Real=[ 1 ,pi*D1^2/4,pi*D1^4/64,pi*D1^4/64,D1,D1, 0,    0  ,pi*D1^4/32,  ks,   ks  %akseli D1
           2 ,pi*D2^2/4,pi*D2^4/64,pi*D2^4/64,D2,D2, 0,    0  ,pi*D2^4/32,  ks,   ks
             ];

%keypoints [X, Y, Z]
k1=[0,0,0]'; % starting point
k2=[0.10,0,0]'; %  bearing location
k3=[0.20,0,0]'; %  changing in cross section
k4=[0.50,0,0]'; %  Disk connected to the main shaft
k5=[0.80,0,0]'; %  changing in cross section
k6=[0.90,0,0]'; %  bearing location
k7=[1,0,0]'; % end of the rotor
%------------------------------------------------------------
% generate Nodes and elements between generation of keypoint
% [Node,Elem,MaxNodeNro,MaxElemNro]=
%                     Lmesh(startp,endp, Nelem,  MatID,RealID,   nodestart,elemstart,start_node)
[Node1,Elem1,MxN,MxE]=Lmesh(  k1  , k2 ,   1   ,    1  ,  1   ,      1     ,   1     ,    1);   % 
[Node2,Elem2,MxN,MxE]=Lmesh(  k2  , k3    ,1   ,    1  ,  1  ,      MxN   ,   MxE   ,    0 ); % 
[Node3,Elem3,MxN,MxE]=Lmesh(  k3  , k4    ,2   ,    1  ,  2  ,      MxN   ,   MxE   ,    0 ); %
[Node4,Elem4,MxN,MxE]=Lmesh(  k4  , k5    ,2   ,    1  ,  2  ,      MxN   ,   MxE   ,    0 ); %
[Node5,Elem5,MxN,MxE]=Lmesh(  k5  , k6    ,1   ,    1  ,  1  ,      MxN   ,   MxE   ,    0 ); %
[Node6,Elem6,MxN,MxE]=Lmesh(  k6  , k7    ,1   ,    1  ,  1  ,      MxN   ,   MxE   ,    0 ); %

%-Nodes-------------------------------------------------%
%-----ID,   X,   Y,   Z
Inp.Node=[Node1; Node2; Node3; Node4; Node5; Node6]; % collected node matrix
for ii=1:6; eval(strcat('clear Node', num2str(ii))); end % delete unwanted variables

%-Elements----------------------------------------------%   
%------ID, I,  J, Mat, Real   
Inp.Elem=[Elem1; Elem2; Elem3; Elem4; Elem5; Elem6;]; % collected element matrix
for ii=1:2; eval(strcat('clear Elem', num2str(ii))); end % delete unwanted variables

%-constraints--------------------------------------------%
%-----Node, Dof, Value
% axial displacement and torsion are restricted
for k=1:size(Inp.Node,1)
    if k==1
        Inp.Disp=[Inp.Node(k,1), 1, 0
                  Inp.Node(k,1), 4, 0
                  ];
    else
        Inp.Disp=[Inp.Disp
                  Inp.Node(k,1), 1, 0
                  Inp.Node(k,1), 4, 0
                  ];
    end
end
% Inp.Disp=[];  


%-PointMass Elements-------------------------------------%
% Pointmasses defined in the global coordinate system
% MassPoints=  [ID, node, mass, Jxx,   Jyy,  Jzz,       ,h    L, dia, dia_in];
Inp.MassPoints=[ 1,   5  , 10  ,  0.0200,0.0400,0.0400, 0 , 0.040, 0.30, 0.03% fan 
                 ];    
%-SpringDamper ------------------------------------------%                
%   SpringDamper=[ID, Inode, Jnode, Type, Dir,  Value];
Inp.SpringDamper=[ 
                 ];
% Type = 1 --> spring
% Type = 2 --> damper
% Dir 1=X, 2=Y, 3=Z
% If the node J is 0, then the spring is attached to ground


% Unbalance masses-------------------------------------------
% -- Node, value (kg*m), angle
Inp.UB=[4, 0.001*200e-3, 0
];


%-lumped mass, if lumpm = 1 ------------------------------%
Inp.lumpm=0;

% modal damping ratios
%                      Mode#, damping
% Inp.ModalDamping=[ 1,    10*1e-3   %1. bending
%                    2,    10*1e-3 
%                    3,    1.5e-3  %2. bending
%                    4,    1.5e-3
%                    5,    2e-3   %3. bending
%                    6,    2e-3];
Inp.ModalDamping=[];


% Bearing parameters ------------------------------------%
if 1
     
    % bearing matrices input-------------------------------------%
    jj=1;
    Inp.Bearing(jj).type='Bearing Matrix'; %String that describes the bearing
    Inp.Bearing(jj).Inode=2;     % The node where the bearing is attached 
    Inp.Bearing(jj).Jnode=0;     % Support node where the bearing is attached (for ground 0)
    
    % Stiffness matrix (directions in global coordinate system, x=axial dir. yz=radial)
    %     Kb=[kxx  kxy  kxz
    %         kyx  kyy  kyz
    %         kzx  kzy  kzz];
    Inp.Bearing(jj).Kb=[0    0       0
                         0    5e8       0
                         0      0     5e8];
    % Damping matrix (directions in global coordinate system, x=axial dir. yz=radial)
    %     Cb=[cxx  cxy  cxz
    %         cyx  cyy  cyz
    %         czx  czy  czz];
    Inp.Bearing(jj).Cb=0.00*1e-5*Inp.Bearing(jj).Kb;
                        
    % a second bearing
    Inp.Bearing(2)=Inp.Bearing(1);
    Inp.Bearing(jj).Inode=8;     % The node where the bearing is attached 
    Inp.Bearing(jj).Jnode=0;     % Support node where the bearing is attached (for ground 0)
                         
    %  end of Bearing matrix input-------------------------------%
else
    Inp.Bearing=[];
end   






%----------------------%

% Add pointmasses elements to element matrix
% for jj=1:size(Inp.MassPoints,1)
%     
%     ElemMP(jj,1)=MxE+jj; % the maximum element number + 1
%     ElemMP(jj,2)=Inp.MassPoints(jj,2); %The node number
%     ElemMP(jj,3:5)=0; % J node and other parameters of zeros
%     
% end
% % Updating 
% if exist('ElemMP')
%     Inp.Elem=[Inp.Elem
%         ElemMP];
% end


      
%-Force------------------------------------------------%
%-----Node, Dof, Value
% Force=[];
Inp.Force=[];

%-Cleanup------------------------------------------------------%
% Delete the temporary variables
save ModelInp.mat Inp
clear all
load ModelInp.mat 
%---------------------------------------------------------------%


