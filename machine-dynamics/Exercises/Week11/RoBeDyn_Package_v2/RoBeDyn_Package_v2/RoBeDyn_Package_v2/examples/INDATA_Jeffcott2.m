% INPUT FILE FOR ROTOR-BEARING DYNAMICS CODE (RoBeDyn)
%STEP 1: Model name
Inp.model_title='Robedyn modelling example rotor';
%------------------------------------------------------------------------

% STEP 2 Rotor diameters
D1=33e-3; % diamter 1

%------------------------------------------------------------------------

%STEP 3: Materiaaliarvot (material properties):material for rotor shaft
E1=2.3e11; % Youngs mopdulus
nuxy1=0.28;% Poisson's ratio
rho1=8100; %Density
ks1=6*(1+nuxy1)/(7+6*nuxy1); %shear correction factor
% define more material properties if required. The material of inpelleror
% fan is not defined because, its properties will be taken from CADmodel.

%------------------------------------------------------------------------

%STEP 4: Define Inp.Mat
%Inp.Mat=[ 1, E, nuxy, rho];
Inp.Mat= [1, E1, nuxy1, rho1 % material of rotor shaft step 3.1
];
% add more material if you have more in step 3.
%------------------------------------------------------------------------

%STEP 5: Define cross section properties of two rotor diameters
%-Real Constant %-ID, A, Izz, Iyy, H,B, theta, istrn, Ixx, shearz, sheary

Inp.Real= [ 1, pi*D1^2/4, pi*D1^4/64, pi*D1^4/64, D1,D1, 0, 0, pi*D1^4/32, ks1, ks1 %For rotor profilewith diamter D1
% 2, pi*D2^2/4, pi*D2^4/64, pi*D2^4/64, D2,D2, 0, 0, pi*D2^4/32, ks1, ks1 %For rotor profilewith diamter D2
% 3, tube_calc(D_out_lamination,D_in_lamination, nuxy2)
% this function can also be used to input sectional properties.
];
% Add more in Inp.Real if you have more section of rotor profile withmore
% diamters.
%------------------------------------------------------------------------

%STEP 6: Defining keypoints (as in figure 2)
% k=[X,Y,Z]; define keypoints as x , y and z corodinates
k1=[0,0,0]'; % start of rotor at keypoint 1 which is (0,0,0)
k2=[100e-3,0,0]'; % bearing center of left hand side bearing
k3=[406e-3,0,0]'; % bearing center of right hand side bearing
k4=[506e-3,0,0]'; % start of rotor section with diameter D1
% k5=[506e-3 +(D1/2),0,0]'; % end of rotor section with daimter D1


%------------------------------------------------------------------------

%STEP 7: function Lmesh
%STEP 7.1: Defining number of elements and nodes
% Solmujen ja elementtien generointi keypointtien välille.....
% [Node,Elem,MaxNodeNro,MaxElemNro]=
%                  Lmesh(startp,endp, Nelem, MatID,RealID,nodestart,elemstart,start_node)
[Node1,Elem1,MxN,MxE]=Lmesh(k1, k2 ,2, 1, 1, 1, 1, 1); % Between keypoints k1 and k2, we want 2 elements whose material ID is 1, real ID is 1.
[Node2,Elem2,MxN,MxE]=Lmesh(k2, k3 ,2, 1, 1, MxN,MxE+1, 0); %
[Node3,Elem3,MxN,MxE]=Lmesh(k3, k4 ,2, 1, 1, MxN,MxE+1, 0); % until k4, real ID is 1
% [Node4,Elem4,MxN,MxE]=Lmesh(k4, k5 ,4, 1, 2, MxN,MxE+1, 0); % real ID is 2 between k4 and k5
% [Node5,Elem5,MxN,MxE]=Lmesh(k5, k6 ,2, 1, 1, MxN,MxE+1, 0); %
% [Node6,Elem6,MxN,MxE]=Lmesh(k6, k7 ,2, 1, 1, MxN,MxE+1, 0); %
% [Node7,Elem7,MxN,MxE]=Lmesh(k7, k8 ,2, 1, 1, MxN,MxE+1, 0); % k8 is end of the rotor
%STEP 7.2: Create Nodes
%Inp.Node will be of form ID, X, Y, Z
Inp.Node=[Node1; Node2; Node3;]; % kootaan node matriisi. In last line (k7 to k8), we have Node 7 and Elem 7, thus we have Inp.Node until Node7
for ii=1:3; eval(strcat('clear Node', num2str(ii))); end % tuhotaan turhat muuttujat

%STEP 7.3: Create Elements
%Inp.Elem will be form ID, I, J, Mat, Real
Inp.Elem=[Elem1; Elem2; Elem3;]; % kootaanElem matriisi
for ii=1:3; eval(strcat('clear Elem', num2str(ii))); end % tuhotaanturhat muuttujat
%coloring the elements
Inp.ElemColor = [];
ElemColor_old = 0;
for color=1:size(Inp.Elem,1)-ElemColor_old
Inp.ElemColor = [Inp.ElemColor; 0 1 1]; % cyan
end
ElemColor_old = size(Inp.Elem,1);
%------------------------------------------------------------------------
%STEP 8: Defining constraints-Rajoitteet
%Inp.Disp=Node, Dof, Value
%axial displacement and torsion are restricted
for k=1:size(Inp.Node,1)
if k==1
Inp.Disp=[Inp.Node(k,1), 1, 0
Inp.Node(k,1), 4, 0
];
else
Inp.Disp=[Inp.Disp
Inp.Node(k,1), 1, 0
Inp.Node(k,1), 4, 0
];
end
end
%In this case, for each node translation in x axis and rotation around x
%axis is constraint. Thus each node has 4 dofs and thus each element has 8
%dofs.
%------------------------------------------------------------------------
%STEP 9: Adding fans/ disks etc in the model as PointMass Elements
% Pistemassojen inertiat määritelty globaalissa koordinaatistossa
% 9.1) Simple sysmetrical disks without features such as holes
% calculation of inertia properties for disk in figure 2
Mass_disk=5; % mass of disk in kg
D_out_disk=140e-3; % outer diameter of disk
D_in_disk=D1; % Inner diamter of disk = D1
L_disk=20e-3; % length of disk
Jxx_disk=(Mass_disk/8)*(D_out_disk^2+D_in_disk^2); % Calculation of Jxx of disk
Jyy_disk=((1/2)*Jxx_disk)+(1/12)*(Mass_disk*L_disk^2);%Jyy of the disk
Jzz_disk=Jyy_disk; % same as Jyy

%9.2)Inertia properties of fan/ impeller or even disk can be obtained from CAD model
% Now adding masspoint elements in Inp.MassPoints
% MassPoints= [ID, node, mass, Jxx, Jyy,Jzz, , h L, dia,dia_in];
Inp.MassPoints=[ 1, 2, 7, 0.00675318,0.00457132, 0.00454920, 0, 20e-3, 160e-3, D1 % Impeller/fan from CAD properties
%     2, 16, Mass_disk, Jxx_disk,Jyy_disk, Jzz_disk, 0, 20e-3,D_out_disk, D_in_disk % Disk properties through calcualtion
    ];
% Location of fan is at node 2. if you look step 7, between k1 and k2 there
% is 2 elements. That means first node is at (0,0,0) and second node is at
% (10e-3,0,0). And this is the location where fan is. And similarly, disk
% lies at node 16.
%------------------------------------------------------------------------

%STEP 10: Machine frame and support
%-SpringDamper
% Type = 1 --> spring
% Type = 2 --> damper
% Dir 1=X, 2=Y, 3=Z
% Jos J node on 0, niin jousi on maassa kiinni
% SpringDamper=[ID, Inode, Jnode, Type, Dir, Value];
Inp.SpringDamper=[ 
                 ];
% not added any spring and damper for frame and support in this example
%------------------------------------------------------------------------

%STEP 11: Adding bearing
% Bearing parameters
if 1
% Laakerin matriisien input%
jj=1;
Inp.Bearing(jj).type='Bearing Matrix'; % Stringi, joka kuvaalaakeria-type of bearing
Inp.Bearing(jj).Inode=5; % Akselin solmu, johon laakeri onliitetty-node number of rotor or shaft where bearing is attached
Inp.Bearing(jj).Jnode=0; % Tuennan solmu, johon laakeri onliitetty-Node number on support. 0 means it is attached to ground
% Stiffness matrix (directions in global coordinate system, x=axialdir. yz=radial)
% Kb=[kxx kxy kxz
% kyx kyy kyz
% kzx kzy kzz];
Inp.Bearing(jj).Kb=[ 0 0 0
0 5e6 0
0 0 5e6];
% Damping matrix (directions in global coordinate system, x=axialdir. yz=radial)
% Cb=[cxx cxy cxz
% cyx cyy cyz
% czx czy czz];
Inp.Bearing(jj).Cb=1e-5*Inp.Bearing(jj).Kb;
% Toinen laakeri- second bearing
Inp.Bearing(2)=Inp.Bearing(1);
Inp.Bearing(jj).Inode=13; % Akselin solmu, johon laakeri onliitetty
Inp.Bearing(jj).Jnode=0; % Tuennan solmu, johon laakeri onliitetty
% Laakeri matriisi input loppuu
else
Inp.Bearing=[];
end
%------------------------------------------------------------------------
% STEP 12: Unbalance information
% -- Node, value (kg*m), angle
Inp.UB=[2, 20e-3*150e-3, 0 % unbalance of 20 gm at 150 mm in fan/impeller
16, 10e-3*130e-3, 0]; % unbalance of 10 gm at 130 mm in disk
%------------------------------------------------------------------------

%STEP 13: lumped or consistent mass matrix
%-lumped massa, jos lumpm=1
Inp.lumpm=0; %consistent mass matrix
%------------------------------------------------------------------------

%-Cleanup%
% Poistetaan väliaikaiset muuttujat
% Voi kommentoida pois debuggausta varte
save ModelInp.mat Inp
clear all
load ModelInp.mat
%------------------------------------------------------------------------