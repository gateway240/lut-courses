%robedynini
%
%
% adds robedyn directories to the matlab path
%

robedyn_path=pwd;
addpath([robedyn_path '/func'])
clear robedyn_path;