
% Model parameters


%-Model name--------------------------------------------%
model_title='simply support beam';
disp(model_title)

% Always use SI-units!

% Geometry
H=0.1 ; % height of beam [m]
B=0.1 ; % width of beam [m]
A=B*H; % cross section area [m^2]
I=B*H^3/12; % second moment of area [m^4]
shearz=6/5; % shear correction factor [-]


% Material properties
E=2.10E+011; % Elastic modulus [Pa]
nuxy=0.3; % Poisson's ratio [-]
rho=7800; % Density [kg/m^3] 

%-Nodes-------------------------------------------------%
%-----ID,   X,   Y
Node=[
      1,    0,      0
      2,    0.25    0
      3,    0.5    0
      4,    0.75    0
      5,    1    0
      6,    1.25    0
      7,    1.50    0
      8,    1.75    0
      9,    2    0 
       ];

%-Real Constant-----------------------------------------%
%--ID,A,I,B,H,shearz
Real=[
      1, B*H, B*H^3/12, H, shearz
       ];
%-Elements----------------------------------------------%   
%------ID, I,  J, Mat, Real   
Elem=[ 
       1,  1,  2, 1,  1
       2,  2,  3, 1,  1
       3,  3,  4, 1,  1
       4,  4,  5, 1,  1
       5,  5,  6, 1,  1
       6,  6,  7, 1,  1
       7,  7,  8, 1,  1
       8,  8,  9, 1,  1
           ];

%-Materials---------------------------------------------%
%---- ID, E, nuxy, rho
Mat=[ 1,  E, nuxy, rho];
%-Forces------------------------------------------------%
%-----Node, Dof, Value
Force=[];

%-bearing supports-------------------------------------------%
%-----Node, Dof, Value

Kb=[
    ];

%-Constraints-------------------------------------------%
%-----Node, Dof, Value
Disp=[
      1,  1, 0
      1,  2, 0
      9,  2, 0
      ];

%-Master DOFs-------------------------------------------%
%-----Node, Dof, Value
MasterDOF=[
        3 , 1, 0
        3 , 2, 0
        3 , 3, 0
        6 , 1, 0
        6 , 2, 0
        6 , 3, 0
        
      ];
  



%-if using lumped mass -> lumpm=1-----------------------%
lumpm=0;

