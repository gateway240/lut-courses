
% Model parameters


%-Model name--------------------------------------------%
model_title='cantilever beam';
disp(model_title)

% Always use SI-units!

% Geometry
H=0.1 ; % height of beam [m]
B=0.1 ; % width of beam [m]
A=B*H; % cross section area [m^2]
I=B*H^3/12; % second moment of area [m^4]
shearz=6/5; % shear correction factor [-]


% Material properties
E=2.10E+011; % Elastic modulus [Pa]
nuxy=0.3; % Poisson's ratio [-]
rho=7800; % Density [kg/m^3] 


%-Nodes-------------------------------------------------%
%-----ID,   X,   Y
Node=[ 
      1,  0,    0
      2,  0.50, 0
      3,  1,    0
      4,  1.50, 0
      5,  2,    0
       ];


%-Elements----------------------------------------------%   
%------ID, I,  J, Mat, Real   
Elem=[ 
      1, 1, 2,  1, 1
      2, 2, 3,  1, 1
      3, 3, 4,  1, 1
      4, 4, 5,  1, 1
       ];


%-Forces------------------------------------------------%
%-----Node, Dof, Value
Force=[];


%-bearing supports-------------------------------------------%
%-----Node, Dof, Value
Kb=[
    ];

% Kb=[
%     ];

%-Constraints-------------------------------------------%
%-----Node, Dof, Value
Disp=[
      1, 1, 0
      1, 2, 0
      1, 3, 0
      ];

%-Master DOFs-------------------------------------------%
%-----Node, Dof, Value
  
  
%-Real Constant-----------------------------------------%
%--ID,A,I,B,H,shearz
Real=[ 1, B*H, B*H^3/12, H, shearz
       ];


%-Materials---------------------------------------------%
%---- ID, E, nuxy, rho
Mat=[ 1,  E, nuxy, rho];


%-if using lumped mass -> lumpm=1-----------------------%
lumpm=0;

