clear all
close all
% BEAM2d analysis script 27.01.2002 by Jussi Sopanen
% 
% Modifications 19.10.2015 made by Eerik Sikanen:
% - all comments translated to English
% - a static analysis function added
%====================================================================


for i=1:20
    disp(' ')
end


%% Step 1: Reading INPUT data
INDATA_hwk10

%% Step 2: Drawing the model
% prep_modelplot(1,model_title,Node,Elem,Disp,Force, [1 1 1 1 1], [1 1 1 1 1],{'-b',3},Kb);

%% Step 3: Calculating matrices
[Kg,Mg,Kc,Mc,FR,FRc,nnum2gdof,nnum2cdof]=Matrices_Beam2D(Node,Elem,Real,Mat,Disp,Force,lumpm,Kb);

%% Step 4:Solve natural requencies ans modes of the constrained system
[FiiN,Freq]=Mode_shape(Kc,Mc,Disp,nnum2gdof);

%% Step 5: Animating modes
% Animate_modes2D(Node,Elem,nnum2gdof, FiiN, Freq, 'Constrained mode No.:',1,8,1,Kb)

%% Step 6: Guyan reduction
[Kr,Mr,Ts,node2gdof]=Guyan2D(Kc,Mc,MasterDOF,nnum2gdof);

% Solve natural requencies and modes of the reduced constrained system
[FiiNr dr]=eig(Kr,Mr);

% Sorting matrices according the frequencies
[Fcr, idxr]=sort(sqrt(diag(dr))/(2*pi));
for i=1:length(idxr)
    Fiiapur(:,i)=FiiNr(:,idxr(i));
end
FiiNr=Fiiapur;

% Compare Frequencies of Full System and Reduced System
disp('=============================================')
disp('Guyan Reduction    Full System Frequencies (Hz)')
disp('---------------    ----------------------------')

disp([Fcr(1:3) Freq(1:3) ])

%% Step 7: Dynamic Guyan reduction
omega0=50*2*pi; %Target frequency in rad/s
[Kr,Mr,Td,node2gdof]=GuyanDyn2D(Kc,Mc,MasterDOF,omega0,nnum2gdof);

% Solve natural requencies and modes of the reduced constrained system
[FiiNr dr]=eig(Kr,Mr);

% Sorting matrices according the frequencies
[Fcr, idxr]=sort(sqrt(diag(dr))/(2*pi));
for i=1:length(idxr)
    Fiiapur(:,i)=FiiNr(:,idxr(i));
end
FiiNr=Fiiapur;


% Compare Frequencies of Full System and Reduced System
disp('=============================================')
disp('Dynamic Reduction    Full System Frequencies (Hz)')
disp('---------------    ----------------------------')

disp([Fcr(1:3) Freq(1:3) ])


