% matlab code
% Torsional vibration
% Machine Dynamic
% Ex2
clc
clear
clear all
close all
close
%% Input Data
k = 22000; %Nm/rad
J = 8; %kgm^2
n = 3;

J1 = 50*J;
J2 = 10*J;
J3 = 5*J;
J4 = 20*J;
J5 = 20*J;

k1 = 4*k;
k2 = 2*k;
k3 = k;
%% calculation

% computation of the system mass matrix
M=[J1  0  0 0;
    0  J2+n^2*J3 0 0;
    0  0  n^2*J4 0;
    0  0  0     n^2*J5];
% computation of the system stiffness matrix
K=[k1 -k1        0       0;
   -k1 k1+n^2*k2 -n^2*k2 0;
   0    -n^2*k2  n^2*(k2+k3)  -n^2*k3;
   0    0       -n^2*k3        n^2*k3];
%% dynamic analysis
[Fii d]=eig(K,M)
Freq=sort(sqrt(diag(d)))/(2*pi) %Hz
%% plot
figure
for i=1:4
subplot(4,1,i)
%Normalized mode shape
Fii(:,i)=Fii(:,i)/Fii(1,i);
plot(Fii(:,(i)),'--ro');
title (['Mode' num2str(i) '--->' num2str(real(Freq(i))) '(Hz)'])
end 