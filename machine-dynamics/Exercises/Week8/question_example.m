% matlab code
% Torsional vibration
% Machine Dynamic
% Ex2
clc
clear
clear all
close all
close
%% Input Data
d1=20e-3;
d2=30e-3;
l1=0.5;
l2=0.75;
J1=40;
J2=24;
G=80e+9; % Elastic Modulus
%% calculation
% Polar area moments
Ip1=pi*d1^4/32;
Ip2=pi*d2^4/32;
% Torsion springs
k1=G*Ip1/l1;
k2=G*Ip2/l2;
% Springs in series, equvalent stiffness
ke=(1/k1+1/k2)^-1;
% computation of the system mass matrix
M=[J1 0;
0 J2];
% computation of the system stiffness matrix
K=ke*[1 -1;
-1 1];
%% dynamic analysis
[Fii d]=eig(K,M)
Freq=sort(sqrt(diag(d)))/(2*pi) %Hz
%% plot
figure
for i=1:2
subplot(2,1,i)
%Normalized mode shape
Fii(:,i)=Fii(:,i)/Fii(1,i);
plot(Fii(:,(i)),'--ro');
title (['Mode' num2str(i) '--->' num2str(real(Freq(i))) '(Hz)'])
end 