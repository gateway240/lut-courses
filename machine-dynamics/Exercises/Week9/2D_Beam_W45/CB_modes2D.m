
function [Mort,Kort,Fiiort,FC,node2gdof,Nodal_mass]=CB_modes2D(Krak,Mrak,Disp,nnum2gdof,Nmuoto)
%============================================================================
% function [Mort,Kort,Fiiort,FC,node2gdof]=CB_modes2D(Krak,Mrak,Disp,nnum2gdof)
% This function calculates Craig-Bampton eigen modes and eigen frequencies for 2D beam element
% 
% OUTPUT:
% -------
% Mort    = orthogonalized and orthonormalized mass matrix [I]
% Kort    = orthogonalized and orthonormalized stiffness matrix [omega^2]
% Fiiort  = orthogonal Craig-Bampton modes
% FC      = eigen frequencies (Hz)
% node2gdof= index matrix for re-organized mass and stiffness matrix
%            node2gdof(node,:)=[UX,  UY, ROTZ]    
%                - example how to retrieve global dof
%                  node 10, dof UX -->  gdof=node2gdof(10,1) 
% Nodal_mass= matrix including translational masses of nodes [node, mass]
% 
%  INPUT:
%  ------
%  Krak   = system stiffness matrix
%  Mrak   = system mass matrix (lumped)
%  Disp   = constraint matrix
%  nnum2gdof =  index matrix mass and stiffness matrices
%  Nmuoto = number of requested eigen modes
%
% Written by Jussi Sopanen, LTKK/KoA, 2002 (pari rivi� by Asko Rouvinen... :-)
%=============================================================================

%Updates-------------------------------------------------------------------
% 10.2.2002 added Nmuoto parameter
% 26.2.2002 output Nodal_mass
%--------------------------------------------------------------------------

% Formatting counters
% k=3+3*Nelem;
k=size(Krak,1);
Nraj=size(Disp,1);		    

% Modifying matrix in order to take into account constraints---------------
% Moving constrained row and columns
%   |Kbb Kbi|
% K=|       |
%   |Kib Kii|

Kapu=Krak;
Mapu=Mrak;

node2gdof=nnum2gdof;
% Replacing rows
for i=1:size(Disp,1) %number of dof 1 constrainst
   for j=1:k  %1-DOFs
      gdof=nnum2gdof(Disp(i,1),Disp(i,2));
      [a,b]=find(nnum2gdof==i);
      Krak(gdof,j)=Kapu(i,j); 
      Krak(i,j)=Kapu(gdof,j);
      Mrak(gdof,j)=Mapu(i,j); 
      Mrak(i,j)=Mapu(gdof,j);
      %updating index matrix
      node2gdof(Disp(i,1),Disp(i,2))=i;
      node2gdof(a,Disp(i,2))=gdof;
   end
end
Kapu=Krak;
Mapu=Mrak;
% Replacing columns
for j=1:size(Disp,1)
   for i=1:k
      gdof=nnum2gdof(Disp(j,1),Disp(j,2)); 
      Krak(i,gdof)=Kapu(i,j); 
      Krak(i,j)=Kapu(i,gdof);
      Mrak(i,gdof)=Mapu(i,j);
      Mrak(i,j)=Mapu(i,gdof);
  end
end
clear Mapu Kapu
% matrix including nodal masses
jj=0;
for ii=1:size(node2gdof,1)
    %node number is unused if row of index vector is empty
    if (node2gdof(ii,1) & node2gdof(ii,2) & node2gdof(ii,3))
        jj=jj+1;
        Nodal_mass(jj,1)=ii;
        Nodal_mass(jj,2)=Mrak(node2gdof(ii,1),node2gdof(ii,1));
    end
end
clear jj

% Splitting stiffness and mass matrix into shared and internal dofs
% KrakBB=Krak(1:Nraj,1:Nraj);
KrakIB=Krak(1+Nraj:k,1:Nraj);
% KrakBI=Krak(1:Nraj,1+Nraj:k);
KrakII=Krak(1+Nraj:k,1+Nraj:k);
% MrakBB=Mrak(1:Nraj,1:Nraj);
% MrakIB=Mrak(1+Nraj:k,1:Nraj);
% MrakBI=Mrak(1:Nraj,1+Nraj:k);
MrakII=Mrak(1+Nraj:k,1+Nraj:k);

% Solution of eigenvalue problem (Normal modes for supported beam)
[FiiN d]=eig(KrakII,MrakII);
size(d)
% Static correction modes
FiiC=-KrakII^(-1)*KrakIB;

% Sorting amtrices according the frequencies
[FC, idx]=sort(sqrt(diag(d))/(2*pi));
for i=1:length(idx)
    Fiiapu(:,i)=FiiN(:,idx(i));
end
FiiN=Fiiapu;


% Selceting number of modes (number of Nmuoto lowest eigen modes)
% Nmuoto=4;
FiiNn=FiiN(:,1:Nmuoto);

% Mass normalization
for i=1:Nmuoto
   FiinormNN(:,i)=FiiNn(:,i)/sqrt(FiiNn(:,i)'*MrakII*FiiNn(:,i));
end

%Generalized stiffness and mass matrix
%Kyl=Fii'*Krak*Fii
%Myl=Fii'*Mrak*Fii
% KrakCC=KrakBB+KrakBI*FiiC;
% KrakNN=FiinormNN'*KrakII*FiinormNN;
% 
% MrakCC=MrakBB+FiiC'*MrakII*FiiC;
% MrakNN=FiinormNN'*MrakII*FiinormNN;
% MrakCN=FiiC'*MrakII*FiinormNN; 
% MrakNC=MrakCN';

% Compiling generalized stiffness and mass matrix
% Kyl=zeros(Nraj+Nmuoto,Nraj+Nmuoto);
% Myl=Kyl;
% %upper-left section CC
% for i=1:Nraj
%    for j=1:Nraj
%       Kyl(i,j)=KrakCC(i,j);
%       Myl(i,j)=MrakCC(i,j);
%    end
% end
% %lower-right section NN
% for i=1+Nraj:Nraj+Nmuoto
%    for j=1+Nraj:Nraj+Nmuoto
%       Kyl(i,j)=KrakNN(i-Nraj,j-Nraj);
%       Myl(i,j)=MrakNN(i-Nraj,j-Nraj);
%    end
% end
% %upper-right section CN
% for i=1:Nraj
%    for j=1+Nraj:Nraj+Nmuoto
%       Myl(i,j)=MrakCN(i,j-Nraj);
%   end
% end
% %lower-left section NC
% for i=1+Nraj:Nraj+Nmuoto
%    for j=1:Nraj
%        Myl(i,j)=MrakNC(i-Nraj,j);
%    end
% end


% Compiling eigen mode matrix
% Graig-Bampton modes
Fii=[diag(ones(1,Nraj),0) zeros(Nraj,Nmuoto);
         FiiC                FiinormNN];
%Other way to calculate Kyl matrix if rotational dofs of FiiC are not zero
% If rotational dofs of FiiC are zeros then Kyl matrix is not the same than
% one calculated before
Kyl=Fii'*Krak*Fii;
Myl=Fii'*Mrak*Fii;



% Orthonormalization------------------------------------------------------
[N dc]=eig(Kyl,Myl);
size(dc)
% Frequencies
[FC, idx]=sort(sqrt(diag(dc))/(2*pi));
% Sorting matrices according the frequencies
for i=1:length(idx)
    Napu(:,i)=N(:,idx(i));
end
N=Napu;

% Mass normalization
for i=1:Nraj+Nmuoto
   Nnorm(:,i)=N(:,i)/sqrt(N(:,i)'*Myl*N(:,i));
end

% Transformation into orthogonal coordinates
Fiiort=Fii*Nnorm;

% Orthonormalizeed stiffness and mass matrices
Kort=Nnorm'*Kyl*Nnorm;
Mort=Nnorm'*Myl*Nnorm;




