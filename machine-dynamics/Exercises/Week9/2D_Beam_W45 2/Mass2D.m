function [M2]=Mass2D(A,E,rho,I,L,nuxy,shearz,lumpm,Igdof,Jgdof,DOFs, theta);
%========================================================================
%2D beam element mass matrix:
%[M2]=Mass2D(A,E,rho,I,L,nuxy,shearz,lumpm,Igdof,Jgdof,DOFs, theta)
%------------------------------------------------------------------
%This function calculates global mass matrix for 2D Timoshenko beam element
%
% Parameters:
% -----------
%
% - A     = Element cross sectional area
% - E     = Young's modulus
% - rho   = Material density
% - I     = Second moment of area of beam
% - L     = Beam length
% - nuxy  = Poisson's ratio
% - shearz= Shear correction factor
% - lumpm = type of mass matrix (1=lumped, 0=consistent)
% - Igdof = global dof number of I node of beam (=UX global)
% - Jgdof = global dof number of J node of beam (=UX global)
% - DOFs  = number of dofs
% - theta = beam orientation around Z-axis
% 
% Written by Jussi Sopanen, LTKK/KoA, 2002
% ========================================================================


%radius of gyration
r=sqrt(I/A);

%Shear deformation
if shearz==0
    fii=0; %No shear deformation taken into account
else
    G=E/(2*(1+nuxy));
    fii=12*E*I/(G*(A/shearz)*L^2);
end

%Forming mass amtrix
if lumpm==0
    %Cells of consistent mass matrix (ANSYS Theory)
    AA=(13/35+7/10*fii+1/3*fii^2+6/5*(r/L)^2)/(1+fii)^2;
    BB=(9/70+3/10*fii+1/6*fii^2-6/5*(r/L)^2)/(1+fii)^2;
    CC=(11/210+11/120*fii+1/24*fii^2+(1/10-1/2*fii)*(r/L)^2)*L/(1+fii)^2;
    DD=(13/420+3/40*fii+1/24*fii^2-(1/10-1/2*fii)*(r/L)^2)*L/(1+fii)^2;
    EE=(1/105+1/60*fii+1/120*fii^2+(2/15+1/6*fii+1/3*fii^2)*(r/L)^2)*L^2/(1+fii)^2;
    FF=(1/140+1/60*fii+1/120*fii^2+(1/30+1/6*fii-1/6*fii^2)*(r/L)^2)*L^2/(1+fii)^2;
    
    %consistent

    M=rho*A*L*[  1/3   0    0   1/6    0    0
                  0    AA   CC   0    BB   -DD
                  0    CC   EE   0    DD   -FF
                 1/6   0    0   1/3    0    0
                  0    BB   DD   0    AA   -CC
                  0   -DD  -FF   0   -CC    EE];

   % M=[140 0 0 70 0 0;
   %   0 156 22*L 0 54 -13*L;
   %   0 22*L 4*L^2 0 13*L -3*L^2;
   %   70 0 0 140 0 0;
   %   0 54 13*L 0 156 -22*L;
   %   0 -13*L -3*L^2 0 -22*L 4*L^2]*rho*A*L/420
    
else
    
    %Lumped 
    M=rho*A*L/2*[ 1   0   0   0   0   0
                  0   1   0   0   0   0
                  0   0   0   0   0   0
                  0   0   0   1   0   0
                  0   0   0   0   1   0
                  0   0   0   0   0   0]; 
    
end

%Transformation matrix
T=zeros(6);
T(1,1)=cos(theta);
T(1,2)=sin(theta);
T(2,1)=-sin(theta);
T(2,2)=cos(theta);
T(3,3)=1;
%---
T(4,4)=cos(theta);
T(4,5)=sin(theta);
T(5,4)=-sin(theta);
T(5,5)=cos(theta);
T(6,6)=1;

%Global mass matrix of element
MG=T'*M*T;

%Allocating global matrix for compiling system mass matrices
M2=zeros(DOFs); %DOFS

%nodal dofs = 3
for ii=1:3 %rows
    for jj=1:3 %columns
        
         %upper-left section
         M2(Igdof+(ii-1),Igdof+(jj-1))=MG(ii,jj);
         %upper-right section
         M2(Igdof+(ii-1),Jgdof+(jj-1))=MG(ii,jj+3);
         %lower-left section
         M2(Jgdof+(ii-1),Igdof+(jj-1))=MG(ii+3,jj);
         %lower-right section
         M2(Jgdof+(ii-1),Jgdof+(jj-1))=MG(ii+3,jj+3);

    end
end



