function []=static_analysis(model_title,Node,Elem,Disp,Force,Kc,FRc,nnum2gdof,deformationScale,Kb)
% Performs static analysis and plots deformed shape of model
% []=static_analysis(model_title,Node,Elem,Disp,Force,Kc,FRc,nnum2gdof,deformationScale)
%
% model_title = model name
% Node    = node matrix    [ID, X, Y]
% Elem    = element matrix [ID, I, J, Mat, Real]
% Disp    = constraints    [Node, Dof, Value] (Dof: 1=UX, 2=UY, 3=ROTZ)
% Force   = node forces    [Node, Dof, Value]
% Kc      = global stiffness matrix of system including constraints
% FRc     = vector of generalized forces including constraints
% nnum2gdof = node numbers and corresponding global degrees of freedom
% deformationScale = scale for plotting deformed shape
%
% Written by Eerik Sikanen 19.10.2015
% =========================================================================


% Calculating nodal displacements
u = inv(Kc)*FRc;

gdof_constrained = []; % vector for constrained dof numbers
for ii=1:size(Disp,1)
    gdof=nnum2gdof(Disp(ii,1),Disp(ii,2)); %Getting global dof
    gdof_constrained = [gdof_constrained; gdof];
end

% Compiling global dof displacement vector INCLUDING constrained dofs
numberOfDOFs = max(max(nnum2gdof)); % number of dofs
n = 0; % dof counter
nodalDisplacement = []; % matrix for nodal displacements size Nx3
for i = 1:numberOfDOFs
    if sum(i == gdof_constrained) % if dof is constrained
        nodalDisplacement = [nodalDisplacement; 0]; % adding 0
    else
        n = n +1;
        nodalDisplacement = [nodalDisplacement; u(n)]; % else adding real displacement
    end
end

translationalDisplacements = []; % X and Y-displacement for plotting deformed shape
for i = 1:numberOfDOFs/3 % oging though every element
    translationalDisplacements = [translationalDisplacements; nodalDisplacement((i-1)*3+1:(i-1)*3+2)';];
end

% plotting model with constraints and loads
prep_modelplot(3,model_title,Node,Elem,Disp,Force,[1 1 1 1 1],[1 1 1 1 1],{'-b',3},Kb);

% calculating coordinates for deformed nodal locations
NodeDef = Node;
NodeDef(:,2:3) = NodeDef(:,2:3) +deformationScale*translationalDisplacements;

% plotting deformed nodal coordinates
prep_modelplot(3,model_title,NodeDef,Elem,Disp,Force,[1 1 0 0 0],[0 0 0 0 0],{'--r',3},Kb);
hold off

% printing nodal displacements of nodes where force is attached
for n = 1:size(Force,1)
    disp(' ')
    disp(['Translational displacements at node ' num2str(Force(n)) ':'])
    disp(['x = ' num2str(translationalDisplacements(Force(n),1)) ...
        ' m, y = ' num2str(translationalDisplacements(Force(n),2)) ' m'])
end


