
% Model parameters


%-Model name--------------------------------------------%
model_title='INDATA_rotor_bearing';
disp(model_title)

% Always use SI-units!

% Geometry
d1=150e-3; % diameter of beam [m]
d2=320e-3;
d2_in=280e-3;

% A=pi*D^2/4; % cross section area [m^2]
% I=pi*D^4/64; % second moment of area [m^4]
shearz=6/5; % shear correction factor [-]




%-Real Constant-----------------------------------------%
%--      ID,   A,        I,        H,   shearz
Real=[ 
         1, pi*d1^2/4, pi*d1^4/64, d1, shearz
         2, pi*d2^2/4, pi*d2^4/64, d2, shearz
         3, pi*(d2^2-d2_in^2)/4, pi*(d2^4-d2_in^4)/64, d2, shearz
       ];


%-Materials---------------------------------------------%
% Material properties
E=2.10E+011; % Elastic modulus [Pa]
nuxy=0.3; % Poisson's ratio [-]
rho=7800; % Density [kg/m^3]

%---- ID, E, nuxy, rho
Mat=[ 1,  E, nuxy, rho];


% key points -------------------------------------------------%
k1=0e-3;
k2=300e-3; % bearing location
k3=640e-3;
k4=690e-3;
k5=1470e-3;
k6=2250e-3;
k7=3030e-3;
k8=3810e-3;
k9=4590e-3;
k10=4640e-3;
k11=4990e-3; % bearing location
k12=5290e-3;

%-Nodes-------------------------------------------------%
%-----ID,   X,   Y
Node=[ 
       1,  k1,  0;
       2,  k2,  0;
       3,  k3,  0;
       4,  k4,  0;
       5,  k5,  0;
       6,  k6,  0;
       7,  k7,  0;
       8,  k8,  0;
       9,  k9,  0;
       10, k10,  0;
       11, k11,  0;
       12, k12,  0;
       ];


%-Elements----------------------------------------------%   
%------ID, I,  J, Mat, Real   
Elem=[ 
       1,  1,  2,    1,    1
       2,  2,  3,    1,    1
       3,  3,  4,    1,    2  
       4,  4,  5,    1,    3  
       5,  5,  6,    1,    3  
       6,  6,  7,    1,    3  
       7,  7,  8,    1,    3  
       8,  8,  9,    1,    3  
       9,  9,  10,   1,    2  
       10, 10, 11,   1,    1 
       11, 11, 12,   1,    1 
       ];


%-Forces------------------------------------------------%
%-----Node, Dof, Value
Force=[
    ];


%-bearing supports-------------------------------------------%
%-----Node, Dof, Value
Kb=[
       2, 1 , 1e12
       2, 2 , 1e12
       11,1 , 1e12
       11,2 , 1e12
     ];

%-Constraints-------------------------------------------%
%-----Node, Dof, Value
Disp=[
        1,    1,   0
      ];
size(Disp,1)



%-if using lumped mass -> lumpm=1-----------------------%
lumpm=1;

% Degrres of freefom
gdof=size(Node,1)*3;
cdof=gdof-size(Disp,1);

% number of modes
Nmuoto=4;
