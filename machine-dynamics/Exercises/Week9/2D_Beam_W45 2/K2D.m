function [K2]=K2D(A,E,I,L,nuxy,shearz,Igdof,Jgdof,DOFs, theta);
%==============================================================
%2D beam element stiffness matrix:
%[K2]=K2D(A,E,I,L,nuxy,shearz,Igdof,Jgdof,DOFs, theta);
% -----------------------------------------------------
%This function calculates global stiffness matrix for 2D Timoshenko beam element
%
%Parametrit:
%=========== 
%
% - A     = Element cross sectional area
% - E     = Young's modulus
% - I     = Second moment of area of beam
% - L     = Beam length
% - nuxy  = Poisson's ratio
% - shearz= Shear correction factor
% - Igdof = global dof number of I node of beam (=UX global)
% - Jgdof = global dof number of J node of beam (=UX global)
% - DOFs  = number of dofs
% - theta = beam orientation around Z-axis
% 
% Written by Jussi Sopanen, LTKK/KoA, 2002
% ===============================================================


%If shearz =0 no shear deformation taken into account
if shearz==0
    fii=0;
else
    G=E/(2*(1+nuxy)); %shear modulus
    fii=12*E*I/(G*(A/shearz)*L^2);
end


%Cells of stiffness matrix (ANSYS Theory)
AA=E*A/L;
BB=12*E*I/(L^3*(1+fii));
CC=6*E*I/(L^2*(1+fii));
DD1=E*I*(4+fii)/(L*(1+fii));
DD2=E*I*(2-fii)/(L*(1+fii));

K=[  AA   0    0   -AA    0    0
     0    BB   CC   0    -BB   CC
     0    CC   DD1  0    -CC   DD2
    -AA   0    0    AA    0    0
     0   -BB  -CC   0     BB  -CC
     0    CC   DD2  0    -CC   DD1]; 

%Transformation matrix

T=zeros(6);
T(1,1)=cos(theta);
T(1,2)=sin(theta);
T(2,1)=-sin(theta);
T(2,2)=cos(theta);
T(3,3)=1;
%---
T(4,4)=cos(theta);
T(4,5)=sin(theta);
T(5,4)=-sin(theta);
T(5,5)=cos(theta);
T(6,6)=1;


%Global stiffness matrix of element
KG=T'*K*T;

%Global stiffness matriisi for system
K2=zeros(DOFs); %DOFS

%nodal dofs = 3
for ii=1:3 %rows
    for jj=1:3 %columns
        
        %upper-left section
        K2(Igdof+(ii-1),Igdof+(jj-1))=KG(ii,jj);
        %upper-right section
        K2(Igdof+(ii-1),Jgdof+(jj-1))=KG(ii,jj+3);
        %lower-left section
        K2(Jgdof+(ii-1),Igdof+(jj-1))=KG(ii+3,jj);
        %lower-right section
        K2(Jgdof+(ii-1),Jgdof+(jj-1))=KG(ii+3,jj+3);
        
    end
end




