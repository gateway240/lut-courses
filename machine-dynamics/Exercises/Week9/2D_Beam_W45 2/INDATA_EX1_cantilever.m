
% Model parameters


%-Model name--------------------------------------------%
model_title='cantilever beam';
disp(model_title)

% Always use SI-units!

% Geometry
H=0.1; % height of beam [m]
B=0.1; % width of beam [m]
A=B*H; % cross section area [m^2]
I=B*H^3/12; % second moment of area [m^4]
shearz=6/5; % shear correction factor [-]


% Material properties
E=2.10E+011; % Elastic modulus [Pa]
nuxy=0.3; % Poisson's ratio [-]
rho=7800; % Density [kg/m^3] 

%-Real Constant-----------------------------------------%
%--   ID,  A,    I,H,shearz
Real=[
       1,  A, I, H, shearz
];

%-Materials---------------------------------------------%
%---- ID, E, nuxy, rho
Mat=[ 1,  E, nuxy, rho];
%-Nodes-------------------------------------------------%
%-----ID,   X,   Y
Node=[ 
      1, 0    ,0    
      2, 0.5  ,0
      3, 1    ,0
      4, 1.5  ,0
      5, 2    ,0 
       ];


%-Elements----------------------------------------------%   
%------ID, I,  J, Mat, Real 
% I=staring node for element
% J=endin node for element
Elem=[ 
       1, 1, 2, 1, 1  
       2, 2, 3, 1, 1
       3, 3, 4, 1, 1
       4, 4, 5, 1, 1
       ]; 

%-Forces------------------------------------------------%
%-----Node, Dof, Value
Force=[5, 2, -5000];


%-bearing supports-------------------------------------------%
%-----Node, Dof, Value
Kb=[
    ];

%-Constraints-------------------------------------------%
%-----Node, Dof, Value
Disp=[
      1 , 1, 0
      1 , 2, 0
      1 , 3 ,0
      ];

% Degrees of freefom
gdof=size(Node,1)*3;
cdof=gdof-size(Disp(:,1));


%-if using lumped mass -> lumpm=1-----------------------%
lumpm=1;

% number of modes
Nmuoto=4;


clear A B E H i I nuxy rho shearz