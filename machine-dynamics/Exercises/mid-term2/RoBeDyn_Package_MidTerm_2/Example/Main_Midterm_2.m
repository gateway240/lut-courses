% Simple Rotor with an overhung disk
% Rotor-Bearing Dynamic Analysis (RoBeDyn) Main Script
%
% ======================================================
clear all; close all;
for i=1:20; disp(' ');  end; clear i;
disp('Start of RoBeDyn Analysis'); disp(' ');


% PREPROCESSING...........................................................
%% Read Input Data
INDATA_MidTerm_2;  % Simple Rotor-Bearing System

%% Draw Model as a 3D WireFrame plot
f_RGeomPlotWireFr(Inp, [1 1 1 0 1], [0 1],{'-b',3});
view(2); axis equal; axis vis3d; view3d rot %figure settings

%% % Draw Model as a Shaded 3D plot
 f_RGeomPlotShaded(Inp)
 view(3);   axis vis3d;  %figure settings  
 light; lighting gouraud; shading interp; view3d rot;
 axis equal;

%% Free-free Modes^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Request.FreeModes.NroFreqs=6;      % Number of frequencies to be solved and plotted
Request.FreeModes.PlotModes='Yes'; % Plot mode shapes 'Yes' or 'No'
% Mode plotting options
%                           Mode#, ScaleFactor, linetype, linewidth
Request.FreeModes.ModePlot={
                            1, 1200, '-k',  2
                            2, 1200, '--k', 2
                            3, 3000, '-r',  2
                            4, 1200, '--r', 2  
                            5, 2000, '-g',  2
                            6, 2000, '--g', 2  
                            };
Request.FreeModes.Options={'view','XY',            % plot view. 'XY'=XY-plane, 'XZ'=XZ-plane, '3D'=3D view
                           %'originloc', [-0.3,0.0,0],% location of origin triad (default = [0,0,0]
                           'shading', 'on'         % wireframe or surface plot (default = wireframe)
                           'transparency',0.3      % transpacenrency of faces (0..1) if shaded plot (default=1)  
                           };   
% Calculate Free-Free Modes
% [FrqR,DampR,FiiR,Matrx]=f_FreeModes(Inp,Request)
[FrqR,DampR,FiiR,Matrx]=f_FreeModes(Inp,Request);

%End-of-free modes^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


%% CAMPBELL DIAGRAM+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Request.Campbell.NroFreqs=8;                    % Number of solved frequencies
Request.Campbell.Spin_vec=(0:500:40000)*2*pi/60; % Speed range
Request.Campbell.Type='Full';                   % Use 'Full' method to reduce system DOFs
Request.Campbell.PlotDamping='No';              % Plot damping ratios 'Yes' or 'No'
% Campbell diagram calculation and plotting
[FrqR,DampR,Matrx]=f_Campbell(Inp,Request);
% END OF CAMPBELL DIAGRAM++++++++++++++++++++++++++++++++++++++++++++++++++


%% % STABILITY ANALYSIS-------------------------------------------------------
% % Hunt of Instability Threshold Speed
% Request.Instability.Spin_Range=[100 10000]*2*pi/60;% Speed range
% Request.Instability.Speed_Toler=1*2*pi/60;         % Speed Tolerance
% % Request.Instability.Type='Full';                 % Use Full Matrices
% Request.Instability.Type='PseudoModal';            % Use Pseudo Modal method to reduce system DOFs
% Request.Instability.NroModes=30;                   % Number of Modes used in Pseudo Modal method
% 
% % Run Instability analysis
% [FrqR,DampR]=f_Instability(Inp,Request);
% % END OF STABILITY ANALYSIS------------------------------------------------


%% UNBALANCE RESPONSE*******************************************************
Request.UBResp.Spin_vec=(0:100:40000)*2*pi/60;       % Speed range
Request.UBResp.OutputNodes=[2, 4]; % List of Output Nodes

% Calculate and Plot Unbalance response of given nodes
[MaxAmp,U_vector]=f_UBResponse(Inp,Request);

% % Plot Rotor Deformed Shape (Requires pre-calculated rotor response)
Request.RespPlot.Spin_speed=10000*2*pi/60;    % Rotation Speed for deformed shape plot
% % Plot Rotor Deformed Shape at given speed
f_RespShapePlot(Inp,Request,U_vector,MaxAmp,1,10000);
% END OF UNBALANCE RESPONSE************************************************


%% ROTOR MODE SHAPE PLOT====================================================
Request.ModePlot.Spin_speed=15000*2*pi/60; % Rotation Speed for mode shape plot
Request.ModePlot.PlotGeom='Yes';           % Plot rotor geometry 'Yes' or 'No'
Request.ModePlot.Options={ 'originloc', [0.0,0.0,0],% location of origin triad (default = [0,0,0]
                           %'shading', 'off'         % wireframe or surface plot (default = surface)
                           'transparency',0.1       % transpacenrency of faces (0..1) if shaded plot (default=0.1)  
                           };    
sf=[1e3 1e3 2e3 2e3]; %skaalauskertoimet eri muodoille
% Plot 4 first modes
for ii=1:4
    Request.ModePlot.ModeNro=ii; 
    Request.ModePlot.ScaleFactor=sf(ii);
    % Calculate and Plot Mode Shape
    f_ModeShapePlot(Inp,Request);
end
clear ii
% END OF ROTOR MODE SHAPE PLOT==============================================
