function [Matrx]=f_Rotor_Matrix(Inp)
%========================================================================
% [Matrx]=f_Rotor_Matrix(Inp)
% Funktio laskee roottorin matriisit. Roottorit mallinnetaan 3D-palkki-
% elementeill�in (Timoshenko) ja j�ykill� kiekoilla. Tuennan massat ja
% j�ykkyys- ja vaimennus 
% Funktiot perustuvat ANSYS 5.7 teoriamanuaaliin (BEAM4)
%
% OUTPUT: Matrx.
% -------
% Kg     = rakenteen globaali j�ykkyysmatriisi
% Mg     = rakenteen globaali massamatriisi
% Kc     = globaali j�ykkyysmatriisi, jossa rajoitteet huomioitu
% Mc     = globaali j�ykkyysmatriisi, jossa rajoitteet huomioitu
% FR     = ulkoisten voimien vektori
% nnum2gdof= solmunumerot ja niit� vastaavat globaalit vapausasteet
%            nnum2gdof(node,:)=[UX, UY, UZ, ROTX, ROTY, ROTZ]    
%                - globaali dof saadaan esim. 
%                  node 10, dof UX -->  gdof=nnum2gdof(10,1)            
% INPUT: Inp.
% ------
% Node    = solmumatriisi     [ID, X, Y, Z]
% Elem    = elementtimatriisi [ID, I, J, Mat, Real]
% MassPoints= pistemassojen mat. [ID, node, mass, Jxx, Jyy, Jzz, L, dia]
% Real    = Real Constant     [ID, A, Ixx, Iyy, Izz, B, H, sheary, shearz]
% Mat     = materiaali om.    [ID, E, G, nuxy, rho]
% Disp    = rajoitteet        [Node, Dof, Value] (Dof: 1=UX, 2=UY, 3=UZ, 4=ROTX, 5=ROTY, 6=ROTZ)
% Force   = solmuvoimat       [Node, Dof, Value]
% lumpm   = massamatriisin tyyppi, 0=konsistenssi, 1=lumped
% 
% Written by Jussi Sopanen, LTKK/KoA, 2002
% ========================================================================

%-Muutokset------------------------
% 24.2.2006 -Muutettu kiekon gyromatriisin termien merkit oikeinp�in
%           -Nimetty uudelleen F2 --> Fs ja F3 --> Fc
%----------------------------------

% Tallennetaan Inputit
Node=Inp.Node;
Elem=Inp.Elem;
MassPoints=Inp.MassPoints;
Real=Inp.Real;
Mat=Inp.Mat;
Force=Inp.Force;
lumpm=Inp.lumpm;
Disp=Inp.Disp;
UB=Inp.UB;
SpringDamper=Inp.SpringDamper;


%Lasketaan mallin vapausasteiden lkm-------------------------
doftmp=zeros( max(max(Elem(:,2:3))),1 ); %vektori maksimi solmunumeroon asti

% Lasketaan montako eri solmua on k�ytetty Elem -matriisissa
%K�yd��n kaikki solmunumerot l�pi v�lilt� min-max node
MinNode=max([1 min(min(Elem(:,2:3)))]);
MaxNode=max(max(Elem(:,2:3)));
for aa=MinNode:MaxNode
    for bb=2:3
        for cc=1:size(Elem,1)
            %Jos solmunumero on elementin I tai J solmu, niin taulukkoon tulee 1
            if aa==Elem(cc,bb)
                doftmp(aa)=1;
            end
        end    
    end
end
%Aktiivisten vapausasteiden lkm
DOFs=sum(doftmp)*6;
% str=['Vapausasteiden lkm=' int2str(DOFs)];
% disp(str)
clear doftmp aa bb cc MinNode MaxNode     
%----------------------------------------------------------


%Globaalien vapausasteiden numerointia varten j�rjestet��n Node matriisi
[Y,indx]=sort(Node(:,1)); %Y on solmunumero, indx on sen indeksi node matriisissa

%Loopissa k�yd��n elementit l�pi ja muodostetaan matriisit
for ii=1:size(Elem,1)
    
    
    
    %Lasketaan globaalisten dof ja I ja J solmujen yhteys
    for jj=1:size(Node,1)
        %Haetaan I noden rivi Node matriisista
        if Elem(ii,2)==Node(jj,1) %
            Iindx=jj; 
            
            %solmun I globaalien vapausasteiden alkuindeksi
            % Y sis�lt�� solmunumerot j�rjestettyn� suurimmasta pienimp��n
            % -jos I noden solmunumero on pienin, niin globaali vapausaste Igdof on 1
            % -jos taas 2. pienin, niin globaali vapausaste Igdof on 7
            % -jne.
            for nn=1:size(Node,1)
                if Node(Iindx,1)==Y(nn)
                    Igdof=(nn-1)*6+1;
                    %Solmunumero ja sen globaalit vapausasteet
                    %     nnum2gdof(node,:)=[UX, UY, UZ, ROTX, ROTY, ROTZ]    
                    %     -globaali dof saadaan: 
                    %        node 10, dof UX -->  gdof=nnum2gdof(10,1)
                    nnum2gdof(Y(nn),:)=[(nn-1)*6+1 (nn-1)*6+2 (nn-1)*6+3 (nn-1)*6+4 (nn-1)*6+5 (nn-1)*6+6 ];
                end
            end
        end
        %Haetaan J noden rivi Node matriisista
        if Elem(ii,3)==Node(jj,1)
            Jindx=jj; 
            
            %solmun J globaalien vapausasteiden alkuindeksi
            % Y sis�lt�� solmunumerot j�rjestettyn� suurimmasta pienimp��n
            % -jos J noden solmunumero on pienin, niin globaali vapausaste Jgdof on 1
            %- jos taas 2. pienin, niin globaali vapausaste Jgdof on 7
            % -jne.
            for nn=1:size(Node,1)
                if Node(Jindx,1)==Y(nn)
                    Jgdof=(nn-1)*6+1;
                    %Solmunumero ja sen globaalit vapausasteet
                    %     nnum2gdof(node,:)=[UX, UY, UZ, ROTX, ROTY, ROTZ]         
                    nnum2gdof(Y(nn),:)=[(nn-1)*6+1 (nn-1)*6+2 (nn-1)*6+3 (nn-1)*6+4 (nn-1)*6+5 (nn-1)*6+6  ];
                end
            end    
        end
    end
    
    
    if ~Elem(ii,3) == 0  % Jos ei ole massaelementti
        
        % Elementin pituus
        L=sqrt((Node(Jindx,2)-Node(Iindx,2) )^2+ (Node(Jindx,3)-Node(Iindx,3))^2+...
            (Node(Jindx,4)-Node(Iindx,4))^2);
        
        % Elementin solmujen koordinaatit
        coord=[Node(Iindx,2) Node(Jindx,2) Node(Iindx,3) Node(Jindx,3) Node(Iindx,4) Node(Jindx,4)];
        
        % Elementin ominaisuudet
        ind=find(Real(:,1)==Elem(ii,5)); % etsit��n real constanttia vastaava rivi
        A=Real(ind,2);    %pinta-ala 
        Izz=Real(ind,3);  %j�yhyysYY
        Iyy=Real(ind,4);  %j�yhyysZZ
        theta=Real(ind,7);
        if Real(ind,8)==0; %Jos v��nt�j�ykkyys ei m��ritelty, niin k�ytet��n Ixx ki laskennassa
            Iv=Real(ind,9); 
        else; 
            Iv=Real(ind,8); 
        end 
        Ixx=Real(ind,9);
        shearz=Real(ind,10); %leikkaus
        sheary=Real(ind,11); %leikkaus
        
        
        % Materiaaliominaisuudet
        ind=find(Mat(:,1)==Elem(ii,4)); % etsit��n materiaalia vastaava rivi
        E=Mat(ind,2);
        nuxy=Mat(ind,3);
        rho=Mat(ind,4);
        
        %3D-palkkielementin j�ykkyysmatriisi
        %function [K3]=K3D(A,E,Ixx,Iyy,Izz,L,nuxy,sheary,shearz,Igdof,Jgdof,DOFs,theta,coord);
        [ki]=K3D(A,E,Iv,Iyy,Izz,L,nuxy,sheary,shearz,Igdof,Jgdof,DOFs,theta,coord);
        
        %Summataan elementtien j�ykkyysmatriisit
        if ii==1
            Kg=ki;
        else
            Kg=Kg+ki;
        end
        
        %3D-palkkielementin Massamatriisi
        % function [M3]=Mass3D(A,E,rho,Iyy,Izz,L,nuxy,sheary,shearz,lumpm,Igdof,Jgdof,DOFs,theta,coord);
        [mi]=Mass3D(A,E,rho,Iyy,Izz,L,nuxy,sheary,shearz,lumpm,Igdof,Jgdof,DOFs,theta,coord);
        
        %Summataan elementtien Massamatriisit
        if ii==1
            Mg=mi;
        else
            Mg=Mg+mi;
        end
        
        %3D-palkkielementin Gyroskooppinen matriisi
        % function [G3]=Gyro3D(A,E,rho,I,L,nuxy,shear,Igdof,Jgdof,DOFs,coord)
        [gi]=Gyro3D(A,E,rho,Iyy,L,nuxy,sheary,Igdof,Jgdof,DOFs,coord);
        
        %Summataan elementtien gyromatriisit
        if ii==1
            Gg=gi;
        else
            Gg=Gg+gi;
        end
    end % if
end

%**************************************************************************
%Lis�t��n pistemassan arvot Massamatriisiin ja Gyromatriisiin
%MassPoints=[ID, node, mass, Jxx,Jyy,Jzz,L,dia]
for ii=1:size(MassPoints,1)
    
    Node_m1=MassPoints(ii,2); %lis�massan solmunumero
    h=MassPoints(ii,7); %offset
    % solmun massamatriisin termit
    mp=zeros(6,6);
    mp(1,1)=MassPoints(ii,3); 
    mp(2,2)=MassPoints(ii,3); 
    mp(3,3)=MassPoints(ii,3); 
    mp(4,4)=MassPoints(ii,4); %Jxx = Jp
    mp(5,5)=MassPoints(ii,5)+MassPoints(ii,3)*h^2; %Jyy=Jd
    mp(6,6)=MassPoints(ii,6)+MassPoints(ii,3)*h^2; %Jzz=Jd
    % offset termit
    mp(2,6)=MassPoints(ii,3)*h;
    mp(6,2)=MassPoints(ii,3)*h;
    mp(3,5)=-MassPoints(ii,3)*h;
    mp(5,3)=-MassPoints(ii,3)*h;
    
    % solmun gyromatriisin termit 
    %HUOM! T�ss� oletetaan, ett� roottori on x-akselin suuntainen
    gp=zeros(6,6);
    gp(5,6)=+MassPoints(ii,4); %Omega*Jxx kerrotaan my�hemmin Spin*
    gp(6,5)=-MassPoints(ii,4); %-Omega*Jxx kerrotaan my�hemmin Spin*

    %Lis�t��n termit matriiseihin
    for i=1:6 %rivit 1-6
        for j=1:6 % sarakkeet 1-6
            
            % lis�massan globaalit vapausasteet
            gdof_r=nnum2gdof(Node_m1,i); %Haetaan globaali Dof riville
            gdof_c=nnum2gdof(Node_m1,j); %Haetaan globaali Dof sarakkeelle
            % Massamatriisi
            Mg(gdof_r,gdof_c)=Mg(gdof_r,gdof_c) + mp(i,j); %lis�t��n pistemassan arvot 
            % Gyromatriisi
            Gg(gdof_r,gdof_c)=Gg(gdof_r,gdof_c) + gp(i,j); %lis�t��n pistemassan arvot 
            
        end
    end
end
clear i j gdof_r gdof_c mp gp Node_m1


% Lis�t��n Spring-Damperien arvot J�ykkys ja vaimennusmatriisin
Cg=zeros(DOFs,DOFs); %Alustetaan vaimennusmatriisi       
%   SpringDamper=[ID, Inode, Jnode, Type, Dir,  Value];
% Type = 1 --> spring
% Type = 2 --> damper
% Dir 1=X, 2=Y, 3=Z
% Jos J node on 0, niin jousi on maassa kiinni
for ii=1:size(SpringDamper,1)
    
    NodeI=SpringDamper(ii,2); %Jousen I solmu
    NodeJ=SpringDamper(ii,3); %Jousen J solmu
    
    % jousen globaalit vapausasteet
    Igdof=nnum2gdof(NodeI,SpringDamper(ii,5)); %Haetaan globaali Dof 
    
    % Sijoitetaan j�ykkyys- tai vaimennuskerroin
    if SpringDamper(ii,4)==1 % jos kyseess� jousi
        Kg(Igdof,Igdof)=Kg(Igdof,Igdof) + SpringDamper(ii,6);
    else
        Cg(Igdof,Igdof)=Cg(Igdof,Igdof) + SpringDamper(ii,6);
    end
    
    if ~NodeJ==0 % Jos toinen p�� ei ole groundissa kiinni
        Jgdof=nnum2gdof(NodeJ,SpringDamper(ii,5)); %Haetaan globaali Dof 
        
        % Sijoitetaan j�ykkyys- tai vaimennuskerroin
        if SpringDamper(ii,4)==1 % jos kyseess� jousi
            Kg(Jgdof,Jgdof)=Kg(Jgdof,Jgdof) + SpringDamper(ii,6);
            %ristitermit
            Kg(Igdof,Jgdof)=Kg(Igdof,Jgdof) - SpringDamper(ii,6);
            Kg(Jgdof,Igdof)=Kg(Jgdof,Igdof) - SpringDamper(ii,6);
        else
            Cg(Jgdof,Jgdof)=Cg(Jgdof,Jgdof) + SpringDamper(ii,6);
            %ristitermit
            Cg(Igdof,Jgdof)=Cg(Igdof,Jgdof) - SpringDamper(ii,6);
            Cg(Jgdof,Igdof)=Cg(Jgdof,Igdof) - SpringDamper(ii,6);
        end
        
    end
end
clear ii gdof NodeI NodeJ

%Voimavektori-------------------------------------------------
%Voimat
%-----Node, Dof, Value
% esim. Force=[10,  1,   -1000];
FR=zeros(DOFs,1);
for ii=1:size(Force,1)
    gdof=nnum2gdof(Force(ii,1),Force(ii,2)); %haetaan globaali DOF
    FR(gdof)=Force(ii,3); %lis�t��n lukuarvo voimavektoriin
end
clear gdof

% Unbalance masses-------------------------------------------
% -- Node, value (kg*m), angle
% UB=[1, 0.001*0.5, 0];
F2=zeros(DOFs,1); F3=zeros(DOFs,1);
for ii=1:size(UB,1)
    gdof_Y=nnum2gdof(UB(ii,1),2); %haetaan globaali DOF Y
    gdof_Z=nnum2gdof(UB(ii,1),3); %haetaan globaali DOF Y
    % Ep�tasapainovoimat
    % UB(ii,3)=ep�tasapainon vaihekulma alfa, joka alkaa pos. Y-akselilta
    % ja kiert�� my�t�p�iv��n (ks. theory manual)
    F2(gdof_Y)= -UB(ii,2)*sin(UB(ii,3)) ; %lis�t��n lukuarvo voimavektoriin
    F2(gdof_Z)= UB(ii,2)*cos(UB(ii,3)) ; %lis�t��n lukuarvo voimavektoriin
    F3(gdof_Y)= UB(ii,2)*cos(UB(ii,3))  ; %lis�t��n lukuarvo voimavektoriin
    F3(gdof_Z)= UB(ii,2)*sin(UB(ii,3)) ; %lis�t��n lukuarvo voimavektoriin
end




%Reunaehtojen huomioiminen -----------------------------------------------------

%Rajoitteet
%----------Node,  Dof, Value
% esim. Disp=[5,    1,   0
%             5,    2,   0
%             5,    3,   0];

%--------------------------------------------------------------------------
%indeksimatriisi, jossa rajoitteet huomioitu
nnum2cgdof=nnum2gdof;
% K�yd��n rajoitematriisin kaikki loopit l�pi
for ii=1:size(Disp,1)
    % solmunumero ja dof
    node=Disp(ii,1);
    dof=Disp(ii,2);
    %k�yd���n indeksi matriisin jokainen elementti l�pi
    for jj=1:size(nnum2gdof,1)
        for kk=1:6
            % jos rajoitettu dof pienempi, niin v�hennet��n 1
            if nnum2gdof(node,dof) < nnum2gdof(jj,kk)
                nnum2cgdof(jj,kk) = max(0,nnum2cgdof(jj,kk)-1); %max, ettei mene negatiiviseksi
            % jos rajoitettu dof = gdof, niin matriisiin 0    
            elseif nnum2gdof(node,dof) == nnum2gdof(jj,kk)
                nnum2cgdof(jj,kk) = 0;
            end
        end
    end
end
clear ii jj node dof

%Huomioidaan rajoitteet-------------------------------------------------
% tallennetaan rajoitettujen globaalien vapausasteiden numerot vektoriin
% Alustetaan rajoitetut matriisit
Kc=Kg;  Mc=Mg;   Gc=Gg; Cc=Cg;

if ~isempty(Disp)
    for ii=1:size(Disp,1)
        gdof(ii)=nnum2gdof(Disp(ii,1),Disp(ii,2)); %Haetaan globaali Dof
    end
    gdof=sort(gdof); % j�rjestys pienimm�st� suurimpaan
    

    % Poistetaan rajoitettuja vapausasteita vastaavat rivit ja sarakeet
    for ii=1:size(gdof,2)
        % J�ykkyysmatriisi
        Kc(:,gdof(ii))=[]; %poistetaan sarake
        Kc(gdof(ii),:)=[]; %poistetaan rivi
        % Massamatriisi
        Mc(:,gdof(ii))=[]; %poistetaan sarake
        Mc(gdof(ii),:)=[]; %poistetaan rivi
        % Vaimennus matriisi
        Cc(:,gdof(ii))=[]; %poistetaan sarake
        Cc(gdof(ii),:)=[]; %poistetaan rivi
        % Gyroskooppinen matriisi
        Gc(:,gdof(ii))=[]; %poistetaan sarake
        Gc(gdof(ii),:)=[]; %poistetaan rivi
        % Voimavektori
        FR(gdof(ii))=[]; %poistetaan rivi        
        % Ep�tasapaino voimavektorit
        F2(gdof(ii))=[]; %poistetaan rivi 
        F3(gdof(ii))=[]; %poistetaan rivi 
              
        
        % v�hennet��n globaaleista vapausasteista 1
        gdof=gdof-1;
    end
end

if isfield(Inp,'ModalDamping')
    % Luodaan vaimennusmatriisi modaalisista vaimennuskerotimista
    % Ominaisarvotehtavan ratkaisu
    [FiiN d]=eig(Kc,Mc);
    % Matriisien jarjestely taajuuden mukaan.
    [omega, idx]=sort(sqrt(diag(d)));
    FiiN=FiiN(:,idx); clear idx d;

    % Yritet��n poistaa j�yk�n kappaleen muodot
    % omega2=omega(find(omega>0.1));

    nrbm=min(find(omega>0.1))-1; % j�yk�n kappaleen muotojen lkm

    Ctmp=zeros(size(Cc));
    for i=1:size(Inp.ModalDamping,1)
        % asetetaan vaimennussuhteet diagonaalille
        mode=Inp.ModalDamping(i,1);
        Ctmp(mode+nrbm,mode+nrbm)=2*Inp.ModalDamping(i,2)*omega(mode+nrbm);
    end
    % Vaimennusmatriisi modaalisista kertoimista
    Cm=(FiiN')^(-1)*Ctmp*FiiN^(-1);
else
    Cm=zeros(size(Kc));
end




% Luodaan structure ulosviet�vist� parametreist�
% [Kg,Mg,Gg,Kc,Mc,Gc,FR,F2,F3,nnum2gdof,nnum2cgdof]

Matrx.Kc=Kc; % rajoitettu j�ykkyysmatriisi
Matrx.Cc=Cc+Cm; % rajoitettu Damping matrix
Matrx.Gc=Gc; % rajoitettu gyromatriisi (kerrotaan my�hemmin py�rimisnopeudella)
Matrx.Mc=Mc; % rajoitettu massamatriisi
Matrx.FR=FR; % Ulkoisten voimien vektori
Matrx.F2=F2; % ep�tasapainovektori
Matrx.F3=F3; % ep�tasapainovektori
Matrx.nnum2gdof=nnum2gdof; % rajoittamattomien vapausasteiden indeksivektori
Matrx.nnum2cgdof=nnum2cgdof; % rajoitettujen vapausasteiden indeksivektori

%FUNKTIOT******************************************************************
%**************************************************************************

function [K3]=K3D(A,E,Ixx,Iyy,Izz,L,nuxy,sheary,shearz,Igdof,Jgdof,DOFs,theta,coord);
%==============================================================
%3D-palkkielementin j�ykkyysmatriisi:
%[K3]=K3D(A,E,Ixx,Iyy,Izz,L,nuxy,sheary,shearz,Igdof,Jgdof,DOFs,theta,coord);
% -----------------------------------------------------
%T�m� funktio laskee 3D-palkkielementin (Timoshenko) 
%globaalin j�ykkyysmatriisin.
%
%Parametrit:
%=========== 
%
% - A     = Elementin poikkileikkauksen pinta-ala
% - E     = Materiaalin kimmomoduli
% - Ixx   = Palkin j�yhyys
% - Iyy   = Palkin j�yhyys
% - Izz   = Palkin j�yhyys
% - L     = Palkin pituus
% - Lxy   = Palkin xy-tasoon projisoitu pituus
% - nuxy  = Poissonin vakio
% - sheary= Leikkausmuodonmuutos kerroin
% - shearz= Leikkausmuodonmuutos kerroin
% - Igdof = palkin I solmun globaalin vapausasteen numero (=UX global)
% - Jgdof = palkin J solmun globaalin vapausasteen numero (=UX global)
% - DOFs  = vapausasteiden lkm
% - theta = palkin orientaatio x-akselin ymp�ri
% - coord = Elementin solmujen koordinaatit
%
% Written by Jussi Sopanen, LTKK/KoA, 2002
% ===============================================================

G=E/(2*(1+nuxy)); %liukumoduli
%Jos shearz =0 leikkausmuodonmuutosta ei huomioida
if sheary==0
    fiiz=0;
else
    fiiz=12*E*Iyy/(G*(A*sheary)*L^2);
end

%Jos sheary =0 leikkausmuodonmuutosta ei huomioida
if shearz==0
    fiiy=0;
else
    fiiy=12*E*Izz/(G*(A*shearz)*L^2);
end

%J�ykkyysmatriisin alkiot (ANSYS Theory)
J=Ixx;
AA=E*A/L;
BB=G*J/L;
Ay=12*E*Iyy/(L^3*(1+fiiz));
Az=12*E*Izz/(L^3*(1+fiiy));
By=-12*E*Iyy/(L^3*(1+fiiz));
Bz=-12*E*Izz/(L^3*(1+fiiy));
Cy=6*E*Iyy/(L^2*(1+fiiz));
Cz=6*E*Izz/(L^2*(1+fiiy));
Dy=-6*E*Iyy/(L^2*(1+fiiz));
Dz=-6*E*Izz/(L^2*(1+fiiy));
Ey=E*Iyy*(4+fiiz)/(L*(1+fiiz));
Ez=E*Izz*(4+fiiy)/(L*(1+fiiy));
Fy=E*Iyy*(2-fiiz)/(L*(1+fiiz));
Fz=E*Izz*(2-fiiy)/(L*(1+fiiy));


K=[  AA   0    0    0     0    0    -AA   0    0    0     0    0
     0    Az   0    0     0    Cz    0    Bz   0    0     0    Cz
     0    0    Ay   0     Dy   0     0    0    By   0     Dy   0
     0    0    0    BB    0    0     0    0    0   -BB    0    0
     0    0    Dy   0     Ey   0     0    0    Cy   0     Fy   0
     0    Cz   0    0     0    Ez    0    Dz   0    0     0    Fz
    -AA   0    0    0     0    0     AA   0    0    0     0    0
     0    Bz   0    0     0    Dz    0    Az   0    0     0    Dy
     0    0    By   0     Cy   0     0    0    Ay   0     Cz   0
     0    0    0   -BB    0    0     0    0    0    BB    0    0
     0    0    Dy   0     Fy   0     0    0    Cz   0     Ey   0
     0    Cz   0    0     0    Fz    0    Dy   0    0     0    Ez]; 

%Transformaatiomatriisi
T=T3D(theta,L,coord);

%Globaali j�ykkyysmatriisi (elementin)
KG=T'*K*T;

%Globaali j�ykkyysmatriisi (koko rakenteen)
K3=zeros(DOFs); %DOFS

%6=solmun vapausasteiden lkm
for ii=1:6 %rows
    for jj=1:6 %columns
        
        %yl�-vasen
        K3(Igdof+(ii-1),Igdof+(jj-1))=KG(ii,jj);
        %yl�-oikea
        K3(Igdof+(ii-1),Jgdof+(jj-1))=KG(ii,jj+6);
        %ala-vasen
        K3(Jgdof+(ii-1),Igdof+(jj-1))=KG(ii+6,jj);
        %ala-oikea
        K3(Jgdof+(ii-1),Jgdof+(jj-1))=KG(ii+6,jj+6);
        
    end
end

function [M3]=Mass3D(A,E,rho,Iyy,Izz,L,nuxy,sheary,shearz,lumpm,Igdof,Jgdof,DOFs,theta,coord);
%========================================================================
% 3D-palkkielementin Massamatriisi:
% [M3]=Mass3D(A,E,rho,Iyy,Izz,L,nuxy,sheary,shearz,lumpm,Igdof,Jgdof,DOFs,theta,coord)
%------------------------------------------------------------------
% T�m� funktio laskee 3D-palkkielementin (Timoshenko)
% globaalin massamatriisin. 
%
% Parametrit:
% -----------
%
% - A     = Elementin poikkileikkauksen pinta-ala
% - E     = Materiaalin kimmomoduli
% - rho   = Materiaalin tiheys
% - I     = Palkin j�yhyys
% - L     = Palkin pituus
% - nuxy  = Poissonin vakio
% - sheary= Leikkausmuodonmuutos kerroin
% - shearz= Leikkausmuodonmuutos kerroin
% - lumpm = massamatriisin tyyppi (1=lumped, 0=konsistenssi)
% - Igdof = palkin I solmun globaalin vapausasteen numero (=UX global)
% - Jgdof = palkin J solmun globaalin vapausasteen numero (=UX global)
% - DOFs  = vapausasteiden lkm
% - theta = palkin orientaatio x-akselin ymp�ri
% - coord = Elementin solmujen koordinaatit
% 
% Written by Jussi Sopanen, LTKK/KoA, 2002
% ========================================================================


%poikkipinnan hitauss�de
ry=sqrt(Iyy/A); %radius of gyration
rz=sqrt(Izz/A); %radius of gyration
%polar moment of inertia
Jx=Iyy+Izz;

%Jos shearz =0 leikkausmuodonmuutosta ei huomioida
if sheary==0
    fiiz=0;
else
    G=E/(2*(1+nuxy)); %liukumoduli
    fiiz=12*E*Iyy/(G*(A*sheary)*L^2);
end
% fiiz=0
%Jos sheary =0 leikkausmuodonmuutosta ei huomioida
if shearz==0
    fiiy=0;
else
    G=E/(2*(1+nuxy)); %liukumoduli
    fiiy=12*E*Izz/(G*(A*shearz)*L^2);
end
% fiiy=0
%massamatriisin muodostus
if lumpm==0
    %Konsistenssin massamatriisin alkiot  (ANSYS Theory)
    AAz=(13/35+7/10*fiiy+1/3*fiiy^2+6/5*(rz/L)^2)/(1+fiiy)^2;
    AAy=(13/35+7/10*fiiz+1/3*fiiz^2+6/5*(ry/L)^2)/(1+fiiz)^2;
    
    BBz=(9/70+3/10*fiiy+1/6*fiiy^2-6/5*(rz/L)^2)/(1+fiiy)^2;
    BBy=(9/70+3/10*fiiz+1/6*fiiz^2-6/5*(ry/L)^2)/(1+fiiz)^2;
    
    CCz=(11/210+11/120*fiiy+1/24*fiiy^2+(1/10-1/2*fiiy)*(rz/L)^2)*L/(1+fiiy)^2;
    CCy=(11/210+11/120*fiiz+1/24*fiiz^2+(1/10-1/2*fiiz)*(ry/L)^2)*L/(1+fiiz)^2;
    
    DDz=(13/420+3/40*fiiy+1/24*fiiy^2-(1/10-1/2*fiiy)*(rz/L)^2)*L/(1+fiiy)^2;
    DDy=(13/420+3/40*fiiz+1/24*fiiz^2-(1/10-1/2*fiiz)*(ry/L)^2)*L/(1+fiiz)^2;
    
    EEz=(1/105+1/60*fiiy+1/120*fiiy^2+(2/15+1/6*fiiy+1/3*fiiy^2)*(rz/L)^2)*L^2/(1+fiiy)^2;
    EEy=(1/105+1/60*fiiz+1/120*fiiz^2+(2/15+1/6*fiiz+1/3*fiiz^2)*(ry/L)^2)*L^2/(1+fiiz)^2;
    
    FFz=-(1/140+1/60*fiiy+1/120*fiiy^2+(1/30+1/6*fiiy-1/6*fiiy^2)*(rz/L)^2)*L^2/(1+fiiy)^2;
    FFy=-(1/140+1/60*fiiz+1/120*fiiz^2+(1/30+1/6*fiiz-1/6*fiiz^2)*(ry/L)^2)*L^2/(1+fiiz)^2;
    
    %konsistenssi
    Mtmp=zeros(12,12);
    % Sarake 1 + symmetria termit
    Mtmp(1,1)=1/3;    
    Mtmp(7,1)=1/6;    Mtmp(1,7)=1/6;  
    % Sarake 2 + symmetria termit
    Mtmp(2,2)=AAz;    
    Mtmp(6,2)=CCz;    Mtmp(2,6)=CCz; 
    Mtmp(8,2)=BBz;    Mtmp(2,8)=BBz;
    Mtmp(12,2)=-DDz;  Mtmp(2,12)=-DDz;
    % Sarake 3 + symmetria termit
    Mtmp(3,3)=AAy;    
    Mtmp(5,3)=-CCy;   Mtmp(3,5)=-CCy; 
    Mtmp(9,3)=BBy;    Mtmp(3,9)=BBy;
    Mtmp(11,3)=DDy;   Mtmp(3,11)=DDy;
    % Sarake 4 + symmetria termit
    Mtmp(4,4)=Jx/(3*A);    
    Mtmp(10,4)=Jx/(6*A);   Mtmp(4,10)=Jx/(6*A);   
    % Sarake 5 + symmetria termit
    Mtmp(5,5)=EEy;    
    Mtmp(9,5)=-DDy;   Mtmp(5,9)=-DDy; 
    Mtmp(11,5)=FFy;   Mtmp(5,11)=FFy;
    % Sarake 6 + symmetria termit
    Mtmp(6,6)=EEz;    
    Mtmp(8,6)=DDz;    Mtmp(6,8)=DDz; 
    Mtmp(12,6)=FFz;   Mtmp(6,12)=FFz;
    % Sarake 7
    Mtmp(7,7)=1/3;  
    % Sarake 8 + symmetria termit
    Mtmp(8,8)=AAz;    
    Mtmp(12,8)=-CCz;   Mtmp(8,12)=-CCz;
    % Sarake 9 + symmetria termit
    Mtmp(9,9)=AAy;    
    Mtmp(11,9)=CCy;   Mtmp(9,11)=CCy;
    % Sarake 10
    Mtmp(10,10)=Jx/(3*A); 
    % Sarake 11
    Mtmp(11,11)=EEy; 
    % Sarake 12
    Mtmp(12,12)=EEz; 
    
    % Lopullinen matriisi
    M=rho*A*L*Mtmp;
    
    
else
    
    %Lumped
    M=rho*A*L/2*[ 1   0   0   0   0   0   0   0   0   0   0   0 
                  0   1   0   0   0   0   0   0   0   0   0   0
                  0   0   1   0   0   0   0   0   0   0   0   0
                  0   0   0   0   0   0   0   0   0   0   0   0
                  0   0   0   0   0   0   0   0   0   0   0   0
                  0   0   0   0   0   0   0   0   0   0   0   0
                  0   0   0   0   0   0   1   0   0   0   0   0
                  0   0   0   0   0   0   0   1   0   0   0   0
                  0   0   0   0   0   0   0   0   1   0   0   0
                  0   0   0   0   0   0   0   0   0   0   0   0
                  0   0   0   0   0   0   0   0   0   0   0   0
                  0   0   0   0   0   0   0   0   0   0   0   0];
    
end

%Transformaatiomatriisi
T=T3D(theta,L,coord);

%Globaali j�ykkyysmatriisi
MG=T'*M*T;

%Alustetaan globaali koko rakenteen massamatriisi
M3=zeros(DOFs); %DOFS

%6=solmun vapausasteiden lkm
for ii=1:6 %rows
    for jj=1:6 %columns
        
         %yl�-vasen
         M3(Igdof+(ii-1),Igdof+(jj-1))=MG(ii,jj);
         %yl�-oikea
         M3(Igdof+(ii-1),Jgdof+(jj-1))=MG(ii,jj+6);
         %ala-vasen
         M3(Jgdof+(ii-1),Igdof+(jj-1))=MG(ii+6,jj);
         %ala-oikea
         M3(Jgdof+(ii-1),Jgdof+(jj-1))=MG(ii+6,jj+6);

    end
end



function [G3]=Gyro3D(A,E,rho,I,L,nuxy,shear,Igdof,Jgdof,DOFs,coord);
%========================================================================
% 3D-palkkielementin gyroskooppinen ep�symmetrinen vaimennusmatriisi:
% [M3]=Mass3D(A,E,rho,I,L,nuxy,shear,Igdof,Jgdof,DOFs,coord)
%------------------------------------------------------------------
%
% Parametrit:
% -----------
%
% - Spin  = Kulmanopeus lokaalin x-akselin ymp�ri
% - A     = Elementin poikkileikkauksen pinta-ala
% - E     = Materiaalin kimmomoduli
% - rho   = Materiaalin tiheys
% - I     = Palkin j�yhyys
% - L     = Palkin pituus
% - nuxy  = Poissonin vakio
% - shear = Leikkausmuodonmuutos kerroin
% - Igdof = palkin I solmun globaalin vapausasteen numero (=UX global)
% - Jgdof = palkin J solmun globaalin vapausasteen numero (=UX global)
% - DOFs  = vapausasteiden lkm
% - theta = palkin orientaatio x-akselin ymp�ri
% - coord = Elementin solmujen koordinaatit
% 
% Written by Jussi Sopanen, LTY/IMVe, 2004
% ========================================================================


%poikkipinnan hitauss�de 
%Oletetaan, ett� poikkipinta on py�re� eli Iyy=Izz ja sheary=shearz
r=sqrt(I/A); %radius of gyration

%Jos shear =0 leikkausmuodonmuutosta ei huomioida
if shear==0
    fii=0;
else
    G=E/(2*(1+nuxy)); %liukumoduli
    fii=12*E*I/(G*(A*shear)*L^2);
end
% fii=0

%gyromatriisin muodostus

%Konsistenssin massamatriisin alkiot  (ANSYS Theory PIPE16)
gg=(6/5*r^2)/(L^2*(1+fii)^2);
hh= -(1/10-1/2*fii)*r^2/(L*(1+fii)^2);
ii= (2/15+1/6*fii+1/3*fii^2)*r^2/(1+fii)^2;
jj=-(1/30+1/6*fii-1/6*fii^2)*r^2/(1+fii)^2;

%konsistenssi
Gtmp=zeros(12,12);
% Sarake 2 + antisymmetria termit
Gtmp(3,2)=-gg;    Gtmp(2,3)=gg;
Gtmp(5,2)=-hh;    Gtmp(2,5)=hh; 
Gtmp(9,2)=gg;     Gtmp(2,9)=-gg; 
Gtmp(11,2)=-hh;   Gtmp(2,11)=hh;
% Sarake 3 + antisymmetria termit
Gtmp(6,3)=-hh;    Gtmp(3,6)=hh;
Gtmp(8,3)=-gg;    Gtmp(3,8)=gg; 
Gtmp(12,3)=-hh;   Gtmp(3,12)=hh; 
% Sarake 5 + antisymmetria termit
Gtmp(6,5)=-ii;    Gtmp(5,6)=ii;
Gtmp(8,5)=-hh;    Gtmp(5,8)=hh; 
Gtmp(12,5)=-jj;   Gtmp(5,12)=jj; 
% Sarake 6 + antisymmetria termit
Gtmp(9,6)=-hh;    Gtmp(6,9)=hh;
Gtmp(11,6)=jj;    Gtmp(6,11)=-jj; 
% Sarake 8 + antisymmetria termit
Gtmp(9,8)=-gg;    Gtmp(8,9)=gg;
Gtmp(11,8)=hh;    Gtmp(8,11)=-hh; 
% Sarake 9 + antisymmetria termit
Gtmp(12,9)=hh;    Gtmp(9,12)=-hh;
% Sarake 11 + antisymmetria termit
Gtmp(12,11)=-ii;    Gtmp(11,12)=ii;


% Lopullinen matriisi
G1=2*rho*A*L*Gtmp; % kerrotaan lopuksi kulmanopeudella Spin = Omega eli G1=2*Spin*rho*A*L*Gtmp;

theta=0;
%Transformaatiomatriisi
T=T3D(theta,L,coord);

%Globaali gyromatriisi
GG=T'*G1*T;

%Alustetaan globaali koko rakenteen gyromatriisi
G3=zeros(DOFs); %DOFS

%6=solmun vapausasteiden lkm
for ii=1:6 %rows
    for jj=1:6 %columns
        
         %yl�-vasen
         G3(Igdof+(ii-1),Igdof+(jj-1))=GG(ii,jj);
         %yl�-oikea
         G3(Igdof+(ii-1),Jgdof+(jj-1))=GG(ii,jj+6);
         %ala-vasen
         G3(Jgdof+(ii-1),Igdof+(jj-1))=GG(ii+6,jj);
         %ala-oikea
         G3(Jgdof+(ii-1),Jgdof+(jj-1))=GG(ii+6,jj+6);

    end
end


function [T]=T3D(theta,L,coord)
% Funktio T3D laskee rotaatiomatriisin palkkielementille 


T=zeros(12);
d=0.0001*L;

X1=coord(1);
X2=coord(2);
Y1=coord(3);
Y2=coord(4);
Z1=coord(5);
Z2=coord(6);

% Elementin xy-tasoon projisoitu pituus
Lxy=sqrt(L^2-(Z2-Z1)^2);

if Lxy>d
    S1=(Y2-Y1)/Lxy;
    C1=(X2-X1)/Lxy;
else
    S1=0;
    C1=1;
end

S2=(Z2-Z1)/L;
S3=sin(theta);
C2=Lxy/L;
C3=cos(theta);

Tpart=[  C1*C2             S1*C2            S2
       (-C1*S2*S3-S1*C3) (-S1*S2*S3+C1*C3)  S3*C2
       (-C1*S2*C3+S1*S3) (-S1*S2*C3-C1*S3)  C3*C2];
   
T(1:3,1:3)=Tpart;
T(4:6,4:6)=Tpart;
T(7:9,7:9)=Tpart;
T(10:12,10:12)=Tpart;









