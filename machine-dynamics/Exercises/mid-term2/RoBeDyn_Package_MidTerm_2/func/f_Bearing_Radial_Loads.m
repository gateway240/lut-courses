function [FB1,FB2]=f_Bearing_Radial_Loads(Inp,grav)
% Funktio laskee kahdella laakerilla tuetun roottorin laakerivoimat
% Oletus: Roottori on X-akselin suuntainen!!!!!!
% INPUT: 
% Node = solmumatriisi
% Node_B1 = laakerin 1 solmu
% Node_B2 = laakerin 2 solmu
% Mg = keskitetty massamatriisi
% grav = gravitaatiovektori
% nnum2gdof = indeksimatriisi

% Calculation of Rotor Matrices------------------------------------------
Inp.lumpm=1; % k�ytet��n lumped massamatriisia
% These matrices are constant with respect to rotation speed
[Matrx]=f_Rotor_Matrix(Inp);
% Contents of structure Matrx:
% Matrx.Kc=Kc; % Stiffness Matrix
% Matrx.Cc=Cc; % Damping matrix
% Matrx.Gc=Gc; % Gyroscopic matrix (must be multiplied with Spin Speed)
% Matrx.Mc=Mc; % Mass matrix
% Matrx.FR=FR; % Vector of external applied forces
% Matrx.F2=F2; % Unbalance vector (see documentation)
% Matrx.F3=F3; % Unbalance vector (see documentation)
% Matrx.nnum2gdof=nnum2gdof; % Indexing matrix of unconstrained dofs
% Matrx.nnum2cgdof=nnum2cgdof; % Indexing matrix of constrained dofs


mg=diag(Matrx.Mc);
disp(' ')
disp(['Rotor mass : ' num2str(sum(mg)/3,4) ' kg'] )
%Voimavektori gravitaatiosta-------------------------------------------------

Node=Inp.Node;
nnum2cgdof=Matrx.nnum2cgdof;

Fgrav=zeros(size(mg));
Mgrav=zeros(size(mg));
for ii=1:size(Node,1)
    
    % poistetaan laakeripesien massat
    % jos Node(ii,1) on yht�kuin laakerin Jnode, niin ei oteta mukaan
    % laskentaan
    if ~(Node(ii,1)==Inp.Bearing(1).Jnode | Node(ii,1)==Inp.Bearing(2).Jnode)
        
        % haetaan globaalit vapausasteet
        gdofX=nnum2cgdof(Node(ii,1),1); 
        gdofY=nnum2cgdof(Node(ii,1),2); 
        gdofZ=nnum2cgdof(Node(ii,1),3); 
        
        % Solmun massamatriisi
        mtmp=zeros(3);
        if ~gdofX==0; mtmp(1,1)=mg(gdofX); end;
        if ~gdofY==0; mtmp(2,2)=mg(gdofY); end;
        if ~gdofZ==0; mtmp(3,3)=mg(gdofZ); end;
        
        % Solmun gravitaatiovoima
        Ftmp=mtmp*grav;
        
        % Sijoitetaan solmuvoimat oikeisiin koordinaatteihin
        if ~gdofX==0; Fgrav(gdofX)=Ftmp(1);  end;
        if ~gdofY==0; Fgrav(gdofY)=Ftmp(2);  end;
        if ~gdofZ==0; Fgrav(gdofZ)=Ftmp(3);  end;
        
        % Gravitaation aiheuttama momentti M=F*x
        if ~gdofX==0; Mgrav(gdofX)=Ftmp(1)*Node(ii,2); end;
        if ~gdofY==0; Mgrav(gdofY)=Ftmp(2)*Node(ii,2); end;
        if ~gdofX==0; Mgrav(gdofZ)=Ftmp(3)*Node(ii,2); end;
    end
end

Node_B2=Inp.Bearing(2).Inode;
Node_B1=Inp.Bearing(1).Inode;
% Laakerivoimat voidaan ratkaista seuraavista yht�l�ist�:
%    FB1+FB2+sum(Fgrav)=0                                    (1)
%    FB1*Node(Node_B1,2)+FB2*Node(Node_B2,2)+sum(Mgrav)=0;   (2)
% (1) ==> 
%    FB2=(-sum(Fgrav)-FB1)
% sij. (2) ==>
%    FB1*Node(Node_B1,2)+(-sum(Fgrav)-FB1)*Node(Node_B2,2)+sum(Mgrav)=0; ==>
%    FB1*(Node(Node_B1,2)-Node(Node_B2,2)) -sum(Fgrav)*Node(Node_B2,2) + sum(Mgrav)=0 ==>

FB1= (+sum(Fgrav)*Node(Node_B2,2) - sum(Mgrav))/(Node(Node_B1,2)-Node(Node_B2,2));
FB2=(-sum(Fgrav)-FB1);
