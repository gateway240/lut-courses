% matlab code
% Torsional vibration
% Machine Dynamic
% Ex2
clc
clear
clear all
close all
close
%% Input Data

J1 = 100;
J2 = 10;
J3 = 50;
J4 = 10;
J5 = 80;

k1 = 25;
k2 = 25;
%% calculation

% computation of the system mass matrix
M=[J1  0  0 0 0;
    0  J2 0 0 0;
    0  0  J3 0 0;
    0  0  0  J4 0;
    0  0  0   0 J5];
% computation of the system stiffness matrix
K=[k1 -k1 0  0   0;
   -k1 2*k1 -k1 0 0;
   0  -k1 k2+k1 -k2 0;
   0  0 -k2 2*k2 -k2;
   0  0  0  -k2   k2];
%% dynamic analysis
[Fii d]=eig(K,M)
Freq=sort(sqrt(diag(d)))/(2*pi)*1000 %Hz
%% plot
figure
for i=1:5
subplot(5,1,i)
%Normalized mode shape
Fii(:,i)=Fii(:,i)/Fii(1,i);
plot(Fii(:,(i)),'--ro');
title (['Mode' num2str(i) '--->' num2str(real(Freq(i))) '(Hz)'])
end 