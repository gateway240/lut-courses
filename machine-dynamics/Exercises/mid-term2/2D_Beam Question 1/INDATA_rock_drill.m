
% Model parameters


%-Model name--------------------------------------------%
model_title='rock-drill';
disp(model_title)

% Always use SI-units!

% Geometry
H=0.1; % height of beam [m]
% B=0.1; % width of beam [m]
A1=2827e-6; % cross section area [m^2]
A2=1134e-6;
A3=2376e-6;
I1=A1^3/12; % second moment of area [m^4]
I2=A2^3/12; % second moment of area [m^4]
I3=A3^3/12; % second moment of area [m^4]
shearz=6/5; % shear correction factor [-]

L1 = 150e-3;
L2 = 500e-3;
L3 = 75e-3;



% Material properties
E=2.10E+011; % Elastic modulus [Pa]
nuxy=0.3; % Poisson's ratio [-]
rho=7800; % Density [kg/m^3] 

k1 = (E * A1 )/L1;
k2 = (E * A2 )/L2;
k3 = (E * A3 )/L3;

k_stifness=[k1+k2 -k2 0
            -k2 k2+k3 -k3
            0  -k3 k3
            ]

m1 = (rho * A1 * L1)/6;
m2 = (rho * A2 * L2)/6;
m3 = (rho * A3 * L3)/6;

m_final =[2*m1 + 2*m2 m2 0
            m2 2*m2 + 2*m3 m3
            0  m3 2*m3
            ]

%-Real Constant-----------------------------------------%
%--   ID,  A,    I,H,shearz
Real=[
       1,  A1, I1, H, shearz
       2,  A2, I2, H, shearz
       3,  A3, I3, H, shearz
];

%-Materials---------------------------------------------%
%---- ID, E, nuxy, rho
Mat=[ 1,  E, nuxy, rho];
%-Nodes-------------------------------------------------%
%-----ID,   X,   Y
Node=[ 
      1, 0    ,0    
      2, L1  ,0
      3, L1+L2  ,0
      4, L1+L2+L3  ,0 
       ];


%-Elements----------------------------------------------%   
%------ID, I,  J, Mat, Real 
% I=staring node for element
% J=endin node for element
Elem=[ 
       1, 1, 2, 1, 1  
       2, 2, 3, 1, 2
       3, 3, 4, 1, 3
       ]; 

%-Forces------------------------------------------------%
%-----Node, Dof, Value
Force=[];


%-bearing supports-------------------------------------------%
%-----Node, Dof, Value
Kb=[
    ];

%-Constraints-------------------------------------------%
%-----Node, Dof, Value
Disp=[
      1 , 1, 0
      1 , 2, 0
      1 , 3 ,0
      ];

% Degrees of freefom
gdof=size(Node,1)*3;
cdof=gdof-size(Disp(:,1));


%-if using lumped mass -> lumpm=1-----------------------%
lumpm=1;

% number of modes
Nmuoto=3;


clear A B E H i I nuxy rho shearz