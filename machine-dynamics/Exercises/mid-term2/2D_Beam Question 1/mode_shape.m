
function [FiiNn,Freq]=Mode_shape(Kc,Mc,Disp,gdof,cdof,nnum2gdof,Nmuoto)


%% Solution of eigenvalue problem (Normal modes for supported beam)
[FiiN d]=eig(Kc,Mc);

% Sorting amtrices according the frequencies
[Freq, idx]=sort(sqrt(diag(d))/(2*pi));
for i=1:length(idx)
    Fiiapu(:,i)=FiiN(:,idx(i));
end
FiiN=Fiiapu;

%%


% gFiiN=zeros(size(FiiN,1),gdof);
% a=size(gFiiN)
% b=size(FiiN)
%% Replacing rows
% zz=0;
% for i=1:size(Disp,1)
%     dofs=nnum2gdof(Disp(i,1),Disp(i,2));
%     for j=dofs:gdof
% %              j
%         if j==dofs
%             gFiiN(:,j)=0;
%             zz=zz+1;
%         else
%             if j-zz>size(FiiN,1)
%                 break
%             end
%             
%             gFiiN(:,j)=FiiN(:,j-zz);
%             
%         end
%      end
% end

%% Replacing columns
% ggFiiN=zeros(gdof,gdof);
% zz=0;
% for i=1:size(Disp,1)
%     dofs=nnum2gdof(Disp(i,1),Disp(i,2));
% 
% for j=dofs:gdof
% 
%     if j==dofs
%         ggFiiN(j,:)=0;
%         zz=zz+1;
%     else
%         
%         if j-zz>size(FiiN,1)
%            break
%         end
%         ggFiiN(j,:)=gFiiN(j-zz,:);
% 
%     end
% 
% end
% end


%%
ggFiiN=zeros(gdof,size(FiiN,1));
zz=0;
for i=1:size(Disp,1)
    dofs=nnum2gdof(Disp(i,1),Disp(i,2));

for j=dofs:gdof

    if j==dofs
        ggFiiN(j,:)=0;
        zz=zz+1;
    else
        
        if j-zz>size(FiiN,1)
           break
        end
        ggFiiN(j,:)=FiiN(j-zz,:);

    end

end
end
%%
% % Selceting number of modes (number of Nmuoto lowest eigen modes)
% FiiNn=ggFiiN;
FiiNn=ggFiiN;




    
    
