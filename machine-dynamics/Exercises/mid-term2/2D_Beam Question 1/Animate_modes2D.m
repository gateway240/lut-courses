function []=Animate_modes2D(Node,Elem,nnum2gdof,FiiXX,FreqXX,plot_title,nstart,nend,scf,Kb)
% Animate_modes2D function used for animating eigenmodes
% function []=Animate_modes2D(Node,Elem,nnum2gdof,FiiXX,FreqXX,plot_title,nstart,nend,scf)
%
% Node    = node matrix
% Elem    = element matrix
% nnum2gdof = node numbers and corresponding global degrees of freedom
% FiiXX   = modes to be animated
% FreqXX  = frequencies of modes
% plot_title = figure title (mode number and frequency are automatically added)
% nstart  = number of first mode to be animated
% nstart  = number of last mode to be animated
% scf     = scaling factro for modes
% 
% Written by Jussi Sopanen, LTKK/KoA, 2002
% ======================================================================================


% Step 5: Animating eigenmodes

% going through all modes
for mm=nstart:nend
   
    
    for ii=1:size(Node,1)
        
        nnum=Node(ii,1); %node number
        %global dof correspond node number
        UXindx=nnum2gdof(Node(ii,1),1);
        UYindx=nnum2gdof(Node(ii,1),2);
        ROTZindx=nnum2gdof(Node(ii,1),3);
        
        %Getting nodal deformations from deformation vector
        %          [Node   UX       UY         ROTZ] 
        Deform(ii,:)=[nnum FiiXX(UXindx,mm) FiiXX(UYindx,mm)];
    end
    
    %Calculating axis limits for plotting----------------------------------
    % disp_scale_factor=100;
    NodesDef_1=Node;
    NodesDef_2=Node;
    for ii=1:size(Node,1)
        for jj=2:3
            NodesDef_1(ii,jj)=Node(ii,jj)+scf*cos(0*pi)*Deform(ii,jj);
            NodesDef_2(ii,jj)=Node(ii,jj)+scf*cos(1*pi)*Deform(ii,jj);
        end
    end
    %calculating limits
    xmin=min(min([NodesDef_1(:,2) NodesDef_2(:,2)]));
    ymin=min(min([NodesDef_1(:,3) NodesDef_2(:,3)]));
    xmax=max(max([NodesDef_1(:,2) NodesDef_2(:,2)]));
    ymax=max(max([NodesDef_1(:,3) NodesDef_2(:,3)]));
    xx=xmax-xmin;
    yy=ymax-ymin;
    
    aos=0.10; %axis offset 10 %    
    
    %Animating mode--------------------------------------------------------
    for bb=0:0.1:2 %scaling factor of mode
        %Step 6: plotting deformed shape
        % disp_scale_factor=100;
        NodesDef=Node;
        for ii=1:size(Node,1)
            for jj=2:3
                NodesDef(ii,jj)=Node(ii,jj)+scf*cos(bb*pi)*Deform(ii,jj);
            end
        end
        
        %Step 6.1: Drawing the model
        
        str2=[plot_title ' ' int2str(mm) ', frequency = ' num2str(real(FreqXX(mm))) ' Hz'];
        
        %prep_modelplot(fig,model_title,Node,Elem,Disp,Force,xxplot ,pnum, ltype)
        %     xxplot= [E N FM D] pnum=[E N F M]
        %prep_modelplot(3,'Deformed Structure',Node,Elem,Disp,Force,[1 1 0 0],[0 1 1 1],{'--b',2});
        Disp=[];
        Force=[];
        prep_modelplot(3,str2,NodesDef,Elem,Disp,Force,[1 1 0 0 0],[0 1 1 1 1],{'-b',3},Kb);
        
         %axis settings for figure
        if abs(ymin-yy*aos)<0.01 && abs(ymax+yy*aos)<0.01
            axis(real([xmin-xx*aos xmax+xx*aos -0.2 0.2]))
        else
            axis(real([xmin-xx*aos xmax+xx*aos ymin-yy*aos ymax+yy*aos]))
        end
        
        %axis settings for figure
        %axis(real([xmin-xx*aos xmax+xx*aos ymin-yy*aos ymax+yy*aos]))
        
        pause(0.05)
        hold off
    end
    disp('Press any key to continue')
    pause
end
