function [Kg,Mg,Kc,Mc,FR,FRc,nnum2gdof]=Matrices_Beam2D(Node,Elem,Real,Mat,Disp,Force,lumpm,Kb)
%========================================================================
%[Kg,Mg,Kc,Mc,FR,nnum2gdof]=Matrices_Beam2D(Node,Elem,Real,Mat,Disp,Force,lumpm)
%This function calculates 2D Timoshenko beam element matrices
%
% OUTPUT:
% -------
% Kg     = global stiffness matrix of system
% Mg     = global mass matrix of system
% Kc     = global stiffness matrix of system including constraints
% Mc     = global stiffness matrix of system including constraints
% FR     = vector of generalized forces
% FRc    = vector of generalized forces including constraints
% nnum2gdof= node numbers and corresponding global degrees of freedom
%            nnum2gdof(node,:)=[UX,  UY, ROTZ]    
%                - example how to retrieve global dof
%                  node 10, dof UX -->  gdof=nnum2gdof(10,1)            
% INPUT:
% ------
% Node    = node matrix         [ID, X, Y]
% Elem    = element matrix      [ID, I, J, Mat, Real]
% Real    = Real Constant       [ID, A, I, B, H, shearz]
% Mat     = material properties [ID, E, nuxy, rho]
% Disp    = constraints         [Node, Dof, Value] (Dof: 1=UX, 2=UY, 3=ROTZ)
% Force   = nodal forces        [Node, Dof, Value]
% lumpm   = type of mass matrix, 0=consistent, 1=lumped
% 
% Written by Jussi Sopanen, LTKK/KoA, 2002
% ========================================================================

%Modifications-------------------------------------------------------------
% 28.01.2002 Added calculation of Mc matrix / Lis�tty Mc laskenta, nollataan rivit ja sarakkeet
%--------------------------------------------------------------------------

%Calculating number of degrees of freedom of model
doftmp=zeros( max(max(Elem(:,2:3))),1 ); %empty vector, size corresponding number of dofs

% Calculating the number of nodes used in Elem matrix
% Going through all node number between min and max
for aa=min(min(Elem(:,2:3))):max(max(Elem(:,2:3)))
    for bb=2:3
        for cc=1:size(Elem,1)
            %Adding 1 to table, if node number corresponds I or J node of the element
            if aa==Elem(cc,bb)
                doftmp(aa)=1;
            end
        end    
    end
end
%Number of active dofs
DOFs=sum(doftmp)*3;
str=['Number of dofs = ' int2str(DOFs)];
disp(str)
clear doftmp aa bb cc     

%Sorting Node matrix for numbering global dofs
[Y,indx]=sort(Node(:,1)); %Y is node number and indx is the index in Node matrix

%Going through all elements and forming system matrices
for ii=1:size(Elem,1)
    
    %Calculating connection between global dofs and I and J nodes
    for jj=1:size(Node,1)
        %Getting row of I node from Node matrix
        if Elem(ii,2)==Node(jj,1) %
           Iindx=jj; 
           
           %staring index of global dofs of node I
           % Y includes node number sorted from greatest to smallest
           % -if node number of I node is smallest then global dof Igdof is 1
           % -if node number of I node is 2nd smallest then global dof Igdof is 4
           % -etc.
           for nn=1:size(Node,1)
               if Node(Iindx,1)==Y(nn)
                   Igdof=(nn-1)*3+1;
                   %Node number and corresponding global dofs
                   %     nnum2gdof(node,:)=[UX,  UY, ROTZ]    
                   %     -example how to retrieve global dof: 
                   %        node 10, dof UX -->  gdof=nnum2gdof(10,1)
                   nnum2gdof(Y(nn),:)=[(nn-1)*3+1 (nn-1)*3+2 (nn-1)*3+3  ];
               end
           end
       end
        %Getting row of J node from Node matrix
        if Elem(ii,3)==Node(jj,1)
           Jindx=jj; 
        
           %staring index of global dofs of node J
           % Y includes node number sorted from greatest to smalles
           % -if node number of J node is smallest then global dof Jgdof is 1
           %- if node number of J node is 2nd smallest then global dof Jgdof is 4
           % -etc.
           for nn=1:size(Node,1)
               if Node(Jindx,1)==Y(nn)
                   Jgdof=(nn-1)*3+1;
                   %Node number and corresponding global dofs
                   %     nnum2gdof(node,:)=[UX,  UY, ROTZ]         
                   nnum2gdof(Y(nn),:)=[(nn-1)*3+1 (nn-1)*3+2 (nn-1)*3+3  ];
               end
           end    
       end
    end
        
    % Element length
    L(ii)=sqrt((Node(Jindx,2)-Node(Iindx,2) )^2+ (Node(Jindx,3)-Node(Iindx,3))^2);
    % Elementin kulma z-akselin ymp�ri
    theta= atan2( (Node(Jindx,3)-Node(Iindx,3)), (Node(Jindx,2)-Node(Iindx,2) ) );
    
    % Element properties
    A=Real(Elem(ii,5),2); % cross section area
    I(ii)=Real(Elem(ii,5),3); % second moment of area
    shear=Real(Elem(ii,5),5); % shear factor
    
    %material properties
    E=Mat(Elem(ii,4),2);
    nuxy=Mat(Elem(ii,4),3);
    rho=Mat(Elem(ii,4),4);
      
    %2D beam element stiffnesss matrix
    %function [K2]=K2D(A,E,I,L,nuxy,shearz,Igdof,Jgdof,DOFs, theta);
    [ki]=K2D(A,E,I(ii),L(ii),nuxy,shear,Igdof,Jgdof,DOFs, theta);

    %Summing element stiffness matrices
    if ii==1
        Kg=ki;
    else
        Kg=Kg+ki;
    end
    
    %2D beam element mass matrix
    % function [M2]=Mass2D(A,E,rho,I,L,nuxy,shearz,lumpm,Igdof,Jgdof,DOFs, theta);
    [mi]=Mass2D(A,E,rho,I(ii),L(ii),nuxy,shear,lumpm,Igdof,Jgdof,DOFs, theta);
    
    %Summing element mass matrices
    if ii==1
        Mg=mi;
    else
        Mg=Mg+mi;
    end
    
end

% Add bearing stiffness
% gdof_bearings = []; % vector for bearing dof numbers
for ii=1:size(Kb,1)
    gdof_bearings=nnum2gdof(Kb(ii,1),Kb(ii,2)); %Getting global dof
%     gdof_bearings = [gdof_bearings; gdof];
    Kg(gdof_bearings,gdof_bearings)=Kg(gdof_bearings,gdof_bearings)+Kb(ii,3);
end

%Handling constraints------------------------------------------------------

%Constraints
%----------Node,  Dof, Value
% esim. Disp=[5,    1,   0
%             5,    2,   0
%             5,    3,   0];

%Compiling constrained stiffness matrix
Kc=Kg;
% for ii=1:size(Disp,1)
%     gdof=nnum2gdof(Disp(ii,1),Disp(ii,2)); %Getting global dof
%     Kc(:,gdof)=zeros(DOFs,1); %adding zeros to column
%     Kc(gdof,:)=zeros(1,DOFs); %adding zeros to row
%     Kc(gdof,gdof)=1; %replacing diagonal term to 1
% end

gdof_constrained = []; % vector for constrained dof numbers
for ii=1:size(Disp,1)
    gdof=nnum2gdof(Disp(ii,1),Disp(ii,2)); %Getting global dof
    gdof_constrained = [gdof_constrained; gdof];
end
Kc(:,gdof_constrained)=[]; %removing columns
Kc(gdof_constrained,:)=[]; %removing rows

%Compiling constrained mass matrix
Mc=Mg;
% for ii=1:size(Disp,1)
%     gdof=nnum2gdof(Disp(ii,1),Disp(ii,2)); %Getting global dof
%     Mc(:,gdof)=zeros(DOFs,1); %adding zeros to column
%     Mc(gdof,:)=zeros(1,DOFs); %adding zeros to row
%     Mc(gdof,gdof)=0; %replacing diagonal term to 0 Note the difference
%     when comparing to stiffness matrix
% end
Mc(:,gdof_constrained)=[]; %removing columns
Mc(gdof_constrained,:)=[]; %removing rows

%Forces
%-----Node, Dof, Value
% e.g. Force=[10,  1,   -1000];

%Force vector--------------------------------------------------------------
FR=zeros(DOFs,1);
for ii=1:size(Force,1)
    gdof=nnum2gdof(Force(ii,1),Force(ii,2)); %Getting global dof
    FR(gdof)=Force(ii,3); %mapping global force vector
end

% Constrained force vector
FRc = FR;
FRc(gdof_constrained) = [];

