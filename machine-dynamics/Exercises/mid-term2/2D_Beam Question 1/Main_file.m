clear all
clear
clc
close all
% BEAM2d analysis script 27.01.2002 by Jussi Sopanen
% 
% Modifications in 19.10.2015 made by Eerik Sikanen:
% - all comments translated to English
% - a static analysiis it opens function added

% Modifications in 04.11.2016 made by Behnam ghalamchi:
% Normal mode shape function has added
%====================================================================

for i=1:20
    disp(' ')
end


%% Step 1: Reading INPUT data
INDATA_rock_drill

%% Step 2: Drawing the model
prep_modelplot(1,model_title,Node,Elem,Disp,Force, [1 1 1 1 1], [1 1 1 1 1],{'-b',3},Kb);

%% Step 3: Calculating matrices
[Kg,Mg,Kc,Mc,FR,FRc,nnum2gdof]=Matrices_Beam2D(Node,Elem,Real,Mat,Disp,Force,lumpm,Kb);

%% Step 4: mode shape and Frequencies
[Fii,Freq]=mode_shape(Kc,Mc,Disp,gdof,cdof,nnum2gdof,Nmuoto);
Freq_rads = Freq * 2*pi

%% Step 5: Animating modes
% Animate_modes2D(Node,Elem,nnum2gdof, Fii, Freq, 'Normal mode.:',1,4,0.4,Kb)

%% Step 6: Static analysis
% deformationScale = 50;
%  static_analysis(model_title,Node,Elem,Disp,Force,Kc,FRc,nnum2gdof,deformationScale,Kb)
