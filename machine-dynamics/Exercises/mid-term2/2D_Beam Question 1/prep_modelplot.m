function []=prep_modelplot(fig,model_title,Node,Elem,Disp,Force,xxplot,pnum,ltype,Kb)
%===================================================================
% []=prep_modelplot(fig,model_title,Node,Elem,Disp,Force,pnum)
% This function plot geometry of finite element model
%
% PARAMETERS:
% -----------------
% fig     = number of figure
% model_title= model name
% Node    = node matrix    [ID, X, Y]
% Elem    = element matrix [ID, I, J, Mat, Real]
% Disp    = constraints    [Node, Dof, Value] (Dof: 1=UX, 2=UY, 3=ROTZ)
% Force   = node forces    [Node, Dof, Value]
% xxplot  = settings for drawing components [E N FM D] (0=off,1=on)
% pnum    = settings for component numbers  [E, N, F, M ] (0=off,1=on)
% ltype   = cell array, includes line type and line width used for plotting
%           elements e.g. {'-b',3}
% 
% Written by Jussi Sopanen, LTKK/KoA, 2002
% ===================================================================


%-Modifications:--------------------------------------------------------
%  28.1.2002
%   - automatic scaling for figure axis (paos=max(model_size)*0.05;)
%   - added parameter for element line type ltype {'-b',3}
%   - automatic scaling for force and moment vector
%   - added parameter xxplot
%-------------------------------------------------------------------

% Ignoring all warnings, intercepting Imaginary parts warning 
warning('off')

figure(fig)

set(gcf,'Units','normalized')		% Units normalized (always)
set(gcf,'Position',[0.1 0.1 0.8 0.8])			% Position set to given
set(gcf,'Color',[1 1 1])			% Background color white (always)

set(gcf,'PaperPositionMode','manual')
set(gcf,'PaperPosition',[1, 1, 5, 3.5,])

%Calculating model dimensions for plotting---------------------------------
model_size=[max(Node(:,2))-min(Node(:,2))  max(Node(:,3))-min(Node(:,3)) ];
%plot axis offset 
paos=max(model_size)*0.10; % xx precents of maximum size
limits=real([min(Node(:,2))-paos max(Node(:,2))+paos min(Node(:,3))-paos max(Node(:,3))+paos]);
%node number offset (position of node number=(xnode+nnos,ynode+nnos)
nnos=max(model_size)*0.05; 

%force numbers
fvos=max(model_size)*0.05; %Force value off-set
mvos=max(model_size)*0.05; %moment value off-set

%calculating maximum forces and moments
maxF=1;maxM=1;
for kk=1:size(Force,1)
    if Force(kk,2) < 3 
        if abs(Force(kk,3)) > maxF
            maxF=abs(Force(kk,3));
        end
    else
        if abs(Force(kk,3)) > maxM
            maxM=abs(Force(kk,3));
        end
    end
end

%scaling factor for force vector N-->m
f_scale=max(model_size)/maxF*0.30;  %Max. force is 30% of max size
torq_scale=max(model_size)/maxM*0.10; %scaling factor for moment vector Nm-->r

%Drawing elements----------------------------------------------------------
if xxplot(1)==1
    
    for ii=1:size(Elem,1)
        
        for jj=1:size(Node,1)
            %Getting row of I node from Node matrix
            if Elem(ii,2)==Node(jj,1)
                Iindx=jj; 
            end
            %Getting row of J node from Node matrix
            if Elem(ii,3)==Node(jj,1)
                Jindx=jj; 
            end
        end
        
        %Drawing element as a line
        plot( [Node(Iindx,2) Node(Jindx,2)], [Node(Iindx,3) Node(Jindx,3)],...
            ltype{1},'linewidth',ltype{2})
        hold on
        
        %Element numbers
        if pnum(1)==1
            text('color',[0 0 1])
            text(mean([Node(Iindx,2) Node(Jindx,2)]+nnos),...
                mean([Node(Iindx,3) Node(Jindx,3)]+nnos),...
                int2str(Elem(ii,1) ),'color',[0 0 1])
        end
    end
end

%Drawing nodes and node numbers-------------------------------------------- 
if xxplot(2)==1
    for ii=1:size(Node,1)
        plot( Node(ii,2), Node(ii,3), 'ok','linewidth',2)
        %node numbers
        if pnum(2)==1
            text(Node(ii,2)+nnos,Node(ii,3)+nnos,int2str(Node(ii,1) ),'color',[0 0 0])
        end
    end
end


%-Forces-------------------------------------------------------------------
% Force   = nodal forces       [Node, Dof, Value]
if xxplot(3)==1
    %going through Force matrix
    for ii=1:size(Force,1)
        
        for jj=1:size(Node,1)
            %Getting row of Force node from Node matrix
            if Force(ii,1)==Node(jj,1) %
                Findx=jj; 
            end
        end
        
        %PLOTTING FORCES
        if Force(ii,2)==1 %FX*****************************
            xinc=f_scale*Force(ii,3); %increment of force in x-direction
            yinc=0;
        elseif Force(ii,2)==2 %FY************************
            xinc=0;
            yinc=f_scale*Force(ii,3); %increment of force in y-direction
        end
        
        %plotting forces
        if Force(ii,2)==1 | Force(ii,2)==2 
            %coordinates of the force
            x1=Node(Findx,2);
            x2=Node(Findx,2)+xinc;
            %
            y1=Node(Findx,3);
            y2=Node(Findx,3)+yinc;
            
            plot([x1 x2], [y1 y2], 'r','linewidth',2)
            
            %adding a tringle to the end of vector
            if x1 > x2 %voima -x
                plot(x2, y2, '<r','linewidth',2)
            elseif x1 < x2 %voima +x
                plot(x2, y2, '>r','linewidth',2)
            elseif y1 > y2 %voima -y
                plot(x2, y2, 'vr','linewidth',2)
            elseif y1 < y2 %voima +y
                plot(x2, y2, '^r','linewidth',2)
            end
            
            %plotting numbers
            if pnum(3)==1 %numbers of forces
                text(x2+fvos,y2+fvos,num2str(abs(Force(ii,3))), 'Color', [1 0 0] );
            end
        end
        
        
        %PLOTTING MOMENTS
        if Force(ii,2)==3 %MZ************************
            xinc=0;
            yinc=0;
            rr=torq_scale*Force(ii,3);
            alfa=(0:pi/10:pi)*sign(Force(ii,3));
            xt=rr*cos(alfa)+Node(Findx,2);
            yt=rr*sin(alfa)+Node(Findx,3);
            plot(xt,yt,'r','linewidth',2)
            
            %direction of moment presented using a triangle
            if Force(ii,3) > 0 %moment +mz
                plot(xt(11), yt(11), 'vr','linewidth',2)
            elseif Force(ii,3) < 0 %moment -mz
                plot(xt(11), yt(11), 'vr','linewidth',2)
            end
            %numbers of moments
            if pnum(4)==1
                text(xt(11)+mvos,yt(11)+mvos,num2str(Force(ii,3)), 'Color', [1 0 0] )
            end
        end
    end
end

%CONSTRAINTS******************************************************
%going through Disp matrix [Node, Dof, Value] (Dof: 1=UX, 2=UY, 3=ROTZ)
if xxplot(4)==1
    for ii=1:size(Disp,1)
        
        for jj=1:size(Node,1)
            %Getting row of Disp node from Node matrix
            if Disp(ii,1)==Node(jj,1) %
                Dindx=jj; 
            end
        end
        
        %coordianted for plotting triangle
        
        a=0.05; %side length of triangle or square
        %coordinates of node
        x1=Node(Dindx,2);
        y1=Node(Dindx,3);
        
        if Disp(ii,2)==1 %UX triangle pointing left
            alfa1=150*pi/180;
            alfa2=-150*pi/180;
            x2=x1+a*cos(alfa1);
            y2=y1+a*sin(alfa1);
            x3=x1+a*cos(alfa2);
            y3=y1+a*sin(alfa2);
            plot([x1 x2 x3 x1], [y1 y2 y3 y1],'g','linewidth',2)
        elseif Disp(ii,2)==2 %UY triangle pointing upwards
            alfa1=-60*pi/180;
            alfa2=-120*pi/180;
            x2=x1+a*cos(alfa1);
            y2=y1+a*sin(alfa1);
            x3=x1+a*cos(alfa2);
            y3=y1+a*sin(alfa2);
            plot([x1 x2 x3 x1], [y1 y2 y3 y1],'g','linewidth',2)
        elseif Disp(ii,2)==3 %ROTZ square
            plot([x1-a/2 x1+a/2 x1+a/2 x1-a/2 x1-a/2], [y1-a/2 y1-a/2 y1+a/2 y1+a/2 y1-a/2],'g','linewidth',2)
        end
    end
end

%Drawing bearing-------------------------------------------- 
if xxplot(5)==1
    for ii=1:size(Kb,1)
        plot( Node(Kb(ii),2), Node(Kb(ii),3), 'or','linewidth',10)
        %node numbers

    end
end
% general settings for figure
% paos=0.1; %plot axis offset
axis(limits)
axis equal
xlabel('X')
ylabel('Y')
title(model_title,'fontsize',14)

% Turning warnings on
warning('on')

