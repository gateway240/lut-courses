% matlab code
% Torsional vibration
% Machine Dynamic
% Exam Question 3
clc
clear
close
%% Input Data

teeth1 = 40;
teeth2 = 20;

n = teeth2/teeth1;

E=210e9; % Pa
G=80e9;

v =0.3;
rho = 7850; %kg/m^3

d1 = 100e-3;%m
d2 = 150e-3;%m

l1 = 0.8;%m
l2 = 1.0;%m

J1 = 9000;%kgm^2 - flywheel
J_e = 1000;%kgm^2 - engine
J_g = 250;%kgm^2 - gear1
J2 = J_e+J_g; %since rigid connection combining
J3 = 150;%kgm^2 - gear2
J4 = 2000;%kgm^2 - propeller


k1 = (pi * G)/(32 * (l1/(d1^4)));
k2 = (pi * G)/(32 * (l2/(d2^4)));

%% calculation

% computation of the system mass matrix
J=[J1  0  0;
    0  J2+ n^2*J3 0;
    0  0  n^2*J4;
    ]
% computation of the system stiffness matrix
K=[k1   -k1                 0;
   -k1    k1 + n^2*k2   -n^2*k2;
   0    -n^2*k2        n^2*k2]


%% dynamic analysis
[Fii d]=eig(K,J);
Freq_rad=sort(sqrt(diag(d))) %rad/s
Freq=Freq_rad/(2*pi); %Hz
%% plot
figure
for i=1:3
subplot(3,1,i)
%Normalized mode shape
Fii(:,i)=Fii(:,i)/Fii(1,i);
plot(Fii(:,(i)),'--ro');
title (['Mode' num2str(i) '--->' num2str(real(Freq(i))) '(Hz)'])
end 