%% Problem 1.2
% Machine Dynamics
% Author: Suraj Jaiswal
% Use % mark for comments, ending lines with ";" sign doesn't print the line

clear all% clear all variables
clc % clear command window

%% Input data
m=10; % kg
k=4000; % N/m
% initial conditions
x_0=0.1; % m
v_0=-0.2; % m/s

%% Calculation
% Undamped natural frequency
omega_n=sqrt(k/m); % rad/s

A=v_0/omega_n;
B=x_0;

%% response at certain time
% what is the response at certain time t? Equations from the lecture notes
t=0; % observed time point [second]
% displacement
x_t=A*sin(omega_n*t)+B*cos(omega_n*t);
% velocity
xdot_t=omega_n*A*cos(omega_n*t)-omega_n*B*sin(omega_n*t);
% acceleration
xdotdot_t=-omega_n^2*A*sin(omega_n*t)-omega_n^2*B*cos(omega_n*t);

%% response curve as a function of time
% define time vector for a certain time period
t=0:0.01:1; % starting time : increment : end time
% once t is defined as vector, the results are vectors having equal length
% displacement
x_t=A*sin(omega_n*t)+B*cos(omega_n*t);
% velocity
xdot_t=omega_n*A*cos(omega_n*t)-omega_n*B*sin(omega_n*t);
% acceleration
xdotdot_t=-omega_n^2*A*sin(omega_n*t)-omega_n^2*B*cos(omega_n*t);

%% plotting
% let's plot the response curves
figure
plot(t,x_t)
title('Displacement')
xlabel('Time [s]')
ylabel('Position [m]')
grid on
figure
plot(t,xdot_t)
title('Velocity')
xlabel('Time [s]')
ylabel('Velocity [m/s]')
grid on
figure
plot(t,xdotdot_t)
title('Acceleration')
xlabel('Time [s]')
ylabel('Acceleration [m/s^{2}]')
grid on
