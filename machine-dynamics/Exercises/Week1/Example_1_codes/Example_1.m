% Example: 1
% Author: Suraj Jaiswal
% Date: 01.09.2019

clear all; clc; close all

% Assign values for input variables
J0 = 10; % Mass moment of inertia torsional damper disk (kg.m^2)
c_theta = 300; % Damping coefficient (N.m.s)
d0 = 0.04; % Diamater of steel shaft (m)
L = 1; % Length of shaft (m)
G = 80*10^9; % Material property of steel shaft (Pa)
X = deg2rad(2); % Forced vibrational amplitude of the disk (radian)
M0 = 1*10^3; % (N.m)

% Define and calculate variables
Ip = (pi*(d0)^4)/32; % Polar moment of inertia of teh shaft
k_theta = (G*Ip)/L; % Stiffness of the shaft
omega_n = sqrt(k_theta/J0); % Natural frequency
zeta = (c_theta)/(2*sqrt(k_theta*J0)); % Damping ratio

% Find frequency ratio, r
g = @(r) (-X*k_theta)/(M0) + (1)/(sqrt((1-r^2)^2+(2*zeta*r)^2));
int = [0 10]; % interval
r0 = fzero(g,int); % Root of the function g between 0 and 10

% Calculate exitation frequency
omega = r0*omega_n

% Calculate transmissibility
TR = sqrt ((1+(2*zeta*r0)^2)/((1-(r0)^2)^2+(2*zeta*r0)^2));

% Torque transmitted to support
M_A = TR*M0