% INPUT FILE FOR ROTOR-BEARING DYNAMICS CODE (RoBeDyn)

%-Model name--------------------------------------------%
Inp.model_title='Testrotor';



% material properties
E=1.7e11;
nuxy=0.30;
rho=7400;
ks=6*(1+nuxy)/(7+6*nuxy); %shear correction factor 
%-material--------------------------------------------%
%---- ID, E, nuxy, rho
Inp.Mat=[1, E, nuxy, rho
          ];
      
% Rotor parameters
D1=41e-3;
D2=51e-3;
D3=71e-3;
D4=171e-3;

%-Real Constant-----------------------------------------% 
%--        ID,   A     ,     Izz,   Iyy      ,H, B,theta,istrn,    Ixx  , shearz,sheary
Inp.Real=[ 1 ,pi*D1^2/4,pi*D1^4/64,pi*D1^4/64,D1,D1, 0,    0  ,pi*D1^4/32,  ks,   ks  % shaft1
           2 ,pi*D2^2/4,pi*D2^4/64,pi*D2^4/64,D2,D2, 0,    0  ,pi*D2^4/32,  ks,   ks  % shaft2
           3 ,pi*D3^2/4,pi*D3^4/64,pi*D3^4/64,D3,D3, 0,    0  ,pi*D3^4/32,  ks,   ks  % shaft3
           4 ,pi*D4^2/4,pi*D4^4/64,pi*D4^4/64,D4,D4, 0,    0  ,pi*D4^4/32,  ks,   ks  % shaft4
             ];

s1=90e-3;
s2=s1+115e-3;
s3=s2+105e-3;
s4=s3+370e-3;
s5=s4+105e-3;
s6=s5+115e-3;
s7=s6+90e-3;

%keypoints [X, Y, Z]
k1=[0,0,0]';
k2=[s1,0,0]';
k3=[s2,0,0]';
k4=[s3,0,0]';
k5=[s4,0,0]';
k6=[s5,0,0]';
k7=[s6,0,0]';
k8=[s7,0,0]';


%------------------------------------------------------------
% generate Nodes and elements between generation of keypoint
% [Node,Elem,MaxNodeNro,MaxElemNro]=
%                     Lmesh(startp,endp, Nelem,  MatID,RealID,   nodestart,elemstart,start_node)
[Node1,Elem1,MxN,MxE]=Lmesh(  k1  , k2    ,2   ,    1  ,  1   ,      1     ,   1     ,    1); % first
[Node2,Elem2,MxN,MxE]=Lmesh(  k2  , k3    ,2   ,    1  ,  2  ,      MxN   ,   MxE   ,    0 ); % second
[Node3,Elem3,MxN,MxE]=Lmesh(  k3  , k4    ,2   ,    1  ,  3  ,      MxN   ,   MxE   ,    0 ); % third
[Node4,Elem4,MxN,MxE]=Lmesh(  k4  , k5    ,5   ,    1  ,  4  ,      MxN   ,   MxE   ,    0 ); % middle thing
[Node5,Elem5,MxN,MxE]=Lmesh(  k5  , k6    ,2   ,    1  ,  3  ,      MxN   ,   MxE   ,    0 ); % fourth
[Node6,Elem6,MxN,MxE]=Lmesh(  k6  , k7    ,2   ,    1  ,  2  ,      MxN   ,   MxE   ,    0 ); % fifth
[Node7,Elem7,MxN,MxE]=Lmesh(  k7  , k8    ,2   ,    1  ,  1  ,      MxN   ,   MxE   ,    0 ); % sixth



%-Nodes-------------------------------------------------%
%-----ID,   X,   Y,   Z
Inp.Node=[Node1; Node2; Node3; Node4; Node5; Node6; Node7;]; % collected node matrix
for ii=1:2; eval(strcat('clear Node', num2str(ii))); end % delete unwanted variables

%-Elements----------------------------------------------%   
%------ID, I,  J, Mat, Real   
Inp.Elem=[Elem1; Elem2; Elem3; Elem4; Elem5; Elem6; Elem7;]; % collected element matrix
for ii=1:2; eval(strcat('clear Elem', num2str(ii))); end % delete unwanted variables

%-constraints--------------------------------------------%
%-----Node, Dof, Value
% axial displacement and torsion are restricted
for k=1:size(Inp.Node,1)
    if k==1
        Inp.Disp=[Inp.Node(k,1), 1, 0
                  Inp.Node(k,1), 4, 0
                  ];
    else
        Inp.Disp=[Inp.Disp
                  Inp.Node(k,1), 1, 0
                  Inp.Node(k,1), 4, 0
                  ];
    end
end
% Inp.Disp=[];  


%-PointMass Elements-------------------------------------%
% Pointmasses defined in the global coordinate system
% MassPoints=  [ID, node,    mass,    Jxx,  Jyy,  Jzz, h , L    ,    dia, dia_in];
Inp.MassPoints=[ 1,   18  , 7.5  ,  0.030,0.016,0.016, 0 , 40e-3, 300e-3, D1% fan 
                 ];    
%              1,   18  , 2.6  ,  0.007,0.006,0.006, 0 , 40e-3, 300e-3, D1% fan 
%-SpringDamper ------------------------------------------%                
%   SpringDamper=[ID, Inode, Jnode, Type, Dir,  Value];
Inp.SpringDamper=[ ];
% Type = 1 --> spring
% Type = 2 --> damper
% Dir 1=X, 2=Y, 3=Z
% If the node J is 0, then the spring is attached to ground


% Unbalance masses-------------------------------------------
% -- Node, value (kg*m), angle
Inp.UB=[18, 0.001*200e-3, 0
];


%-lumped mass, if lumpm = 1 ------------------------------%
Inp.lumpm=0;

% modal damping ratios
%                      Mode#, damping
% Inp.ModalDamping=[ 1,    10*1e-3   %1. bending
%                    2,    10*1e-3 
%                    3,    1.5e-3  %2. bending
%                    4,    1.5e-3
%                    5,    2e-3   %3. bending
%                    6,    2e-3];
Inp.ModalDamping=[];


% Bearing parameters ------------------------------------%
if 1
     
    % bearing matrices input-------------------------------------%
    jj=1;
    Inp.Bearing(jj).type='Bearing Matrix'; %String that describes the bearing
    Inp.Bearing(jj).Inode=4;     % The node where the bearing is attached 
    Inp.Bearing(jj).Jnode=0;     % Support node where the bearing is attached (for ground 0)
    
    % Stiffness matrix (directions in global coordinate system, x=axial dir. yz=radial)
    %     Kb=[kxx  kxy  kxz
    %         kyx  kyy  kyz
    %         kzx  kzy  kzz];
    Inp.Bearing(jj).Kb=[0    0       0
                         0    5e8       0
                         0      0     5e8];
    % Damping matrix (directions in global coordinate system, x=axial dir. yz=radial)
    %     Cb=[cxx  cxy  cxz
    %         cyx  cyy  cyz
    %         czx  czy  czz];
    Inp.Bearing(jj).Cb=0.00*1e-5*Inp.Bearing(jj).Kb;
                        
    % a second bearing
    Inp.Bearing(2)=Inp.Bearing(1);
    Inp.Bearing(jj).Inode=15;     % The node where the bearing is attached 
    Inp.Bearing(jj).Jnode=0;     % Support node where the bearing is attached (for ground 0)
                         
    %  end of Bearing matrix input-------------------------------%
else
    Inp.Bearing=[];
end   








%----------------------%

% Add pointmasses elements to element matrix
for jj=1:size(Inp.MassPoints,1)
    
    ElemMP(jj,1)=MxE+jj; % the maximum element number + 1
    ElemMP(jj,2)=Inp.MassPoints(jj,2); %The node number
    ElemMP(jj,3:5)=0; % J node and other parameters of zeros
    
end
% Updating 
if exist('ElemMP')
    Inp.Elem=[Inp.Elem
        ElemMP];
end


      
%-Force------------------------------------------------%
%-----Node, Dof, Value
% Force=[];
Inp.Force=[];

%-Cleanup------------------------------------------------------%
% Delete the temporary variables
save ModelInp.mat Inp
clear all
load ModelInp.mat 
%---------------------------------------------------------------%


