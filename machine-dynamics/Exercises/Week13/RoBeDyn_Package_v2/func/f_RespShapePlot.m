function f_RespShapePlot(Inp,Request,U_vector,MaxAmp,newfigure,sf)
% Roottorin pakkov�r�htelyn muodon tulostus
% INPUT:
% Roottorin taipuman tulostus (vaatii lasketun vasteen) +++++++++++++++
% Request.RespPlot.Spin_speed=3400*2*pi/60;
%
% Written by Jussi Sopanen LTY/IMVE 2004

if nargin==4
    newfigure=1;
end
% etsit��n nopeuden indeksi
[V I]=find(Request.RespPlot.Spin_speed==Request.UBResp.Spin_vec);
FiiR=U_vector(:,:,I);

if nargin<5
    % default arvot
    % lasketaan muodon skaalauskerroin (skaalaa um --> m)
    ModDim=max(max(Inp.Node(:,2:4)))-min(min(Inp.Node(:,2:4)));
    if ModDim/max(MaxAmp(:,I)) > 10 && ModDim/max(MaxAmp(:,I)) < 100
        ScaleFactor=10;
    elseif ModDim/max(MaxAmp(:,I)) > 100 && ModDim/max(MaxAmp(:,I)) < 1000
        ScaleFactor=100;
    elseif ModDim/max(MaxAmp(:,I)) > 1000 && ModDim/max(MaxAmp(:,I)) < 10000
        ScaleFactor=1000;
    elseif ModDim/max(MaxAmp(:,I)) > 10000 && ModDim/max(MaxAmp(:,I)) < 100000
        ScaleFactor=10000;
    else
        ScaleFactor=100000;
    end
else
    ScaleFactor=sf; %k�ytet��n annettua arvoa
end

% Calculation of Rotor Matrices------------------------------------------
% These matrices are constant with respect to rotation speed
[Matrx]=f_Rotor_Matrix(Inp);
% Contents of structure Matrx:
% Matrx.Kc=Kc; % Stiffness Matrix
% Matrx.Cc=Cc; % Damping matrix
% Matrx.Gc=Gc; % Gyroscopic matrix (must be multiplied with Spin Speed)
% Matrx.Mc=Mc; % Mass matrix
% Matrx.FR=FR; % Vector of external applied forces
% Matrx.F2=F2; % Unbalance vector (see documentation)
% Matrx.F3=F3; % Unbalance vector (see documentation)
% Matrx.nnum2gdof=nnum2gdof; % Indexing matrix of unconstrained dofs
% Matrx.nnum2cgdof=nnum2cgdof; % Indexing matrix of constrained dofs

% kopioidaan indeksointimatriisi
nnum2cgdof=Matrx.nnum2cgdof;

% Muotojen tulostus--------------------------------------------------------

Node=Inp.Node;
Elem=Inp.Elem;

if newfigure==1
    figure
    set(gcf,'Units','normalized')		% Units normalized (always)
    set(gcf,'Position',[0.1 0.1 0.8 0.8])			% Position set to given
    set(gcf,'Color',[1 1 1])			% Background color white (always)
end
% mm=Request.RespPlot.ModeNro; % Tulostettavan muodon numero

% t=(0:2*pi/20:1.8*pi); % aika parametri, t�ysi kierros
% sf=5000; %scale factor
% muodon tulostus eli piirto
for ii=1:size(Node,1)
    %nnum=Node(ii,1); %solmunumero
    %solmunumeroa vastaava globaali DOF    
    UXindx=nnum2cgdof(Node(ii,1),1);
    UYindx=nnum2cgdof(Node(ii,1),2);
    UZindx=nnum2cgdof(Node(ii,1),3);
    
    % Noden x-siirtym�
    if ~UXindx==0 % jos ei rajoitettu gdof
        u1(ii,:)=Node(ii,2)*ones(1,size(FiiR,2))+...
                 0*ScaleFactor*FiiR(UXindx,:); %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    else
        u1(ii,:)=Node(ii,2)*ones(1,size(FiiR,2));
    end
    % Noden y-siirtym�
    if ~UYindx==0 % jos ei rajoitettu gdof
        v1(ii,:)=Node(ii,3)*ones(1,size(FiiR,2))+...
                 ScaleFactor*FiiR(UYindx,:);    
    else
        v1(ii,:)=Node(ii,3)*ones(1,size(FiiR,2));
    end
    % Noden z-siirtym�
    if ~UZindx==0 % jos ei rajoitettu gdof
        w1(ii,:)=Node(ii,4)*ones(1,size(FiiR,2))+...
                 ScaleFactor*FiiR(UZindx,:);        
    else
        w1(ii,:)=Node(ii,4)*ones(1,size(FiiR,2));
    end

end

% Transpoosit siirtym�matriiseista plot3 k�sky� varten
u1=u1'; v1=v1'; w1=w1';

% Tulostetaan ympyr�t
H=plot3(u1, v1, w1,'b','linewidth',1);
h_leg(1)=H(1); %tallennetaan handle legendiin
t_leg{1}='Whirl (\Omegat=-1.8\pi...0)';
hold on
% pallot solmuihin
mu=size(u1,1); 
h_leg(2)=plot3(u1(mu,:), v1(mu,:), w1(mu,:),'ok','markersize',6,'linewidth',2);
t_leg{2}='Node';

% Tulostetaan palkkielementit viivoina 
t=0;
for ii=1:size(Elem,1)
    
    if ~Elem(ii,3) == 0  % Jos ei ole massaelementti
        
        for jj=1:size(Node,1)
            %Haetaan I noden rivi Node matriisista
            if Elem(ii,2)==Node(jj,1)
                Iindx=jj; 
            end
            %Haetaan I noden rivi Node matriisista
            if Elem(ii,3)==Node(jj,1)
                Jindx=jj; 
            end
        end
        
        %solmunumeroa vastaava globaali DOF    
        UXindxI=nnum2cgdof(Node(Iindx,1),1);
        UYindxI=nnum2cgdof(Node(Iindx,1),2);
        UZindxI=nnum2cgdof(Node(Iindx,1),3);
        UXindxJ=nnum2cgdof(Node(Jindx,1),1);
        UYindxJ=nnum2cgdof(Node(Jindx,1),2);
        UZindxJ=nnum2cgdof(Node(Jindx,1),3);        
        
        % Noden x-siirtym�
        if ~UXindxI==0 % jos ei rajoitettu gdof
            uI=Node(Iindx,2)+ 0*ScaleFactor*FiiR(UXindxI,mu) ; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        else
            uI=Node(Iindx,2);
        end
        if ~UXindxJ==0 % jos ei rajoitettu gdof
            uJ=Node(Jindx,2)+ 0*ScaleFactor*FiiR(UXindxJ,mu) ;  %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        else
            uJ=Node(Jindx,2);
        end
        
        % Noden y-siirtym�
        if ~UYindxI==0 % jos ei rajoitettu gdof
            vI=Node(Iindx,3)+ScaleFactor*FiiR(UYindxI,mu) ; 
        else
            vI=Node(Iindx,3);
        end
        if ~UYindxJ==0 % jos ei rajoitettu gdof
            vJ=Node(Jindx,3)+ScaleFactor*FiiR(UYindxJ,mu) ; 
        else
            vJ=Node(Jindx,3);
        end
        % Noden z-siirtym�
        if ~UZindxI==0 % jos ei rajoitettu gdof
            wI=Node(Iindx,4)+ScaleFactor*FiiR(UZindxI,mu) ; 
        else
            wI=Node(Iindx,4);
        end
        if ~UZindxJ==0 % jos ei rajoitettu gdof
            wJ=Node(Jindx,4)+ScaleFactor*FiiR(UZindxJ,mu) ; 
        else
            wJ=Node(Jindx,4);
        end
        
        %Elementti viivana
        plot3( [Node(Iindx,2) Node(Jindx,2)], [Node(Iindx,3) Node(Jindx,3)],...
            [Node(Iindx,4) Node(Jindx,4)],'-k','linewidth',1) % keskiviva
        h_leg(3)=plot3( [uI uJ], [vI vJ], [wI wJ ],'-k','linewidth',2); % viiva solmusta solmuun
        t_leg{3}='Centerline';
        if ii==1
            plot3( [Node(Iindx,2) uI ], [Node(Iindx,3) vI], [Node(Iindx,4) wI],'-k','linewidth',2)
        else
            plot3( [Node(Jindx,2) uJ ], [Node(Jindx,3) vJ], [Node(Jindx,4) wJ],'-k','linewidth',2)
        end
        
    else % jos on massaelementti
        
        for jj=1:size(Node,1)
            %Haetaan I noden rivi Node matriisista
            if Elem(ii,2)==Node(jj,1)
                Iindx=jj; 
            end
        end
        
        %solmunumeroa vastaava globaali DOF    
        UXindxI=nnum2cgdof(Node(Iindx,1),1);
        UYindxI=nnum2cgdof(Node(Iindx,1),2);
        UZindxI=nnum2cgdof(Node(Iindx,1),3);
        
        % Noden x-siirtym�
        if ~UXindxI==0 % jos ei rajoitettu gdof
            uI=Node(Iindx,2)+  0*ScaleFactor*FiiR(UXindxI,mu);  %�!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        else
            uI=Node(Iindx,2);
        end
        
        % Noden y-siirtym�
        if ~UYindxI==0 % jos ei rajoitettu gdof
            vI=Node(Iindx,3)+ScaleFactor*FiiR(UYindxI,mu) ; 
        else
            vI=Node(Iindx,3);
        end
        
        % Noden z-siirtym�
        if ~UZindxI==0 % jos ei rajoitettu gdof
            wI=Node(Iindx,4)+ScaleFactor*FiiR(UZindxI,mu) ; 
        else
            wI=Node(Iindx,4);
        end
        
        %Massaelementttipisteen�
        h_leg(4)=plot3( [uI ], [vI], [wI ],'or','linewidth',3,'markersize',10);
        t_leg{4}='MassPoint';
    end

end

NumLeg=size(h_leg,2)+1; %tarkistetaan onko legendin arvoja 3 vai 4

% Laakerin plottaus
for kkk=1:size(Inp.Bearing,2)

    for jj=1:size(Node,1)
        %Haetaan I noden rivi Node matriisista
        if Inp.Bearing(kkk).Inode==Node(jj,1)
            Iindx=jj;
        end
    end

    %solmunumeroa vastaava globaali DOF
    UXindxI=nnum2cgdof(Node(Iindx,1),1);
    UYindxI=nnum2cgdof(Node(Iindx,1),2);
    UZindxI=nnum2cgdof(Node(Iindx,1),3);

    % Noden x-siirtym�
    if ~UXindxI==0 % jos ei rajoitettu gdof
        uI=Node(Iindx,2)+  0*ScaleFactor*FiiR(UXindxI,mu);  %�!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    else
        uI=Node(Iindx,2);
    end

    % Noden y-siirtym�
    if ~UYindxI==0 % jos ei rajoitettu gdof
        vI=Node(Iindx,3)+ScaleFactor*FiiR(UYindxI,mu) ;
    else
        vI=Node(Iindx,3);
    end

    % Noden z-siirtym�
    if ~UZindxI==0 % jos ei rajoitettu gdof
        wI=Node(Iindx,4)+ScaleFactor*FiiR(UZindxI,mu) ;
    else
        wI=Node(Iindx,4);
    end


    %Piirret��n laakeri vihre�n� neli�n�
    h_leg(NumLeg)=plot3( [uI ], [vI], [wI ],'sg','linewidth',3,'markersize',12);
    t_leg{NumLeg}='Bearing';
end


% Kuvan otsikon asetus
str0=['Rotor Deflection @ ' num2str(Request.RespPlot.Spin_speed*60/(2*pi)) ' rpm , Maximum Amplitude =' num2str(max(MaxAmp(:,I))*1e6,3) ' \mum' ];
str2={ Inp.model_title; str0};
title(str2,'Fontsize',14)
xlabel('X','fontsize',13)
ylabel('Y','fontsize',13)
zlabel('Z','fontsize',13)

% legend
legend(h_leg,t_leg,'Location', 'SouthEastOutside');




% kuvan asetukset
view(3);  axis vis3d; axis equal; axis off;
view3d rot;


% XYZ-koordinaatiston piirto
%f_draw_caxis(origloc, sz)
f_draw_caxis;
