function [uv]=skew(u)
% [uv]=skew(u)
%
% funktio laskee vinosymmetrisen matriisin uv vektorista u
%   u  = 3x1 tai 1x3 vektori
%   uv = 3x3 matriisi
%
uv=[  0   -u(3)  u(2)
     u(3)   0   -u(1)
    -u(2)  u(1)   0  ];
%
% Written by Asko Rouvinen LTKK/IMVe 2002