clear all;close all;clc
format long g
% Known Initial Values
m1=9; % Mass of the machine, kg
m2=3; % Mass of the dynamical damper, kg
e=0.14; % Unbalance, kgm
omega=1200/60*(2*pi); % Nominal rotation speed, rad/s
rpm_max=3000; % Maximum rpm
Z1=0.05; % Modal damping ratio
Z2=0.1; % Modal damping ratio

% k1 and k2 can be defined using the equation for natural frequency
k1=omega^2*m1;
k2=omega^2*m2;

% EOM
% Mass matrix, 2 DOF (x1,x2), diagonal matrix
M=[m1 0
   0 m2];

% Stiffness matrix
K=[17 -1
   -1 1];

% The damping matrix is created from the modal damping factors
% Solving the eigenvalue problem
[FiiN,d]=eig(K,M); %FiiN=eigenvectors, d=eigenvalues

% Re-arrange the matrix according to the natural frequencies (lowest first)
[omega_n, idx]=sort(sqrt(diag(d)))
FiiN=FiiN(:,idx);
%Mr=FiiN'*M*FiiN % Modal mass matrix --> identity matrix

% Build the damping matrix C (diagonal)
Ctmp(1,1)=2*Z1*omega_n(1);
Ctmp(2,2)=2*Z2*omega_n(2);

% Full damping matrix from the modal damping matrix
C=inv(FiiN')*Ctmp*inv(FiiN)

% Range of studied excitation frequencies
omega_f=0:pi/8:rpm_max/60*2*pi;

% SOLVING THE RESPONSE TO UNBALANCE--------------------------------------
for i=1:length(omega_f) % Loop over the specified speed range
    
    % Unbalance force is proportional to rotational speed square
    F=e*omega_f(i)^2; 
    
    % Solution for the equation 
    AA=[K-omega_f(i)^2*M -omega_f(i)*C
        omega_f(i)*C K-omega_f(i)^2*M];

    % External force vector
    FF=[F 0 0 0]';
    
    %Solving the coefficient vector ab=[a b]'
    %AA*ab=FF -> ab=AA^-1*FF
    ab(i,:)=AA^(-1)*FF;
    
end

% Vibration amplitudes Xi=sqrt(a_i^2+b_i^2)
X(:,:)=sqrt(ab(:,1:2).^2+ab(:,3:4).^2);

% Amplitudes and respective indices for the machine, meter
[AmplitudesForMass1,respectiveindices1] = findpeaks(X(:,1));
[AmplitudesForMass2,respectiveindices2] = findpeaks(X(:,2));
AmplitudesForMass1
AmplitudesForMass2

% Critical speeds for the machine, rpm
CriticalSpeedsForMass1 = omega_f(respectiveindices1)*60/(2*pi)
CriticalSpeedsForMass2 = omega_f(respectiveindices2)*60/(2*pi)

% Steady State Response Plotting
figure
plot(omega_f*60/(2*pi),X(:,1),'k','LineWidth',1.5)
hold on
grid on
plot(omega_f*60/(2*pi),X(:,2),'r','LineWidth',1.5)
str1 = {sprintf('%.4f',AmplitudesForMass1(1:1))
        sprintf('%.4f',AmplitudesForMass1(2:2))};
str2 = {sprintf('%.4f',AmplitudesForMass2(1:1))
        sprintf('%.4f',AmplitudesForMass2(2:2))};
text(omega_f(respectiveindices1)*60/(2*pi),AmplitudesForMass1,str1);
text(omega_f(respectiveindices2)*60/(2*pi),AmplitudesForMass2,str2);
title('Harmonic Response')
xlabel('Excitation frequency (rpm)')
ylabel('Displacement (m)')
legend('m_{1}','m_{2}','Location','NorthEast')
