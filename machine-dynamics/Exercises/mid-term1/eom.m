
%% Text below must be saved into in file eom.m
% time integration part 
function xdot=eom(t,y0,M,C,K)

% if t<5
%     omega_f=12/5*t; % Linearly increasing frequncy 0...12 rad/s
% else
    omega_f=12.279;
% end
% Define harmonic excitation force to M2, amplitude 1 N, frequency omega_f
F=[0 1*sin(omega_f*t) 0]';


xdot(1:3,1)=y0(4:6);
xdot(4:6,1)=inv(M)*(F-C*y0(4:6)-K*y0(1:3));
end
