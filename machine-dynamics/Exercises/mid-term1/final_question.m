%Machine Dynamics, Moodle Exam 
% matlab code for Resonant-vibratory mixer analysis
clear all

% System taken from US patent 7,188,993, Fig. 11

% Parameters
M1=5; %intermediate mass (kg)
M2=1.17*M1;  %oscillator mass, Preferred ratios given in standard
M3=0.6*M1 + 2;  %Payload mass, Preferred ratios given in standard

K1=100; %Stiffness N/m
K2=4.6*K1; %Preferred ratio given in standard
K3=3.9*K1; %Preferred ratio given in standard
K4=1.3*K1 * 1.5; %Preferred ratio given in standard

M=[M1 0 0
    0 M2 0
    0 0 M3]
K=[K1+K2+K3 -K2 -K3
    -K2     K2   0
    -K3     0    K3+K4]

%modal damping values for modes 1 and 3
zeta1=0.005;
zeta3=0.01;

% Excitation force: F(t)=F*sin(omega_f*t)

% Natural frequencies and modes 
[Fii,omega_square]=eig(K,M);
omega=sqrt(omega_square); % rad/s
Freq=omega/(2*pi); % 1/s (Hz)
Fii % eigmodes
omega
%% Add proportional damping

% alfa and beta solved from equations 
beta=(2*zeta1*omega(1,1)-2*zeta3*omega(3,3))/(omega(1,1)^2-omega(3,3)^2);
alfa=2*zeta1*omega(1,1)-(beta*omega(1,1)^2);

% Proportional Damping matrix 
C=alfa*M+beta*K;

% Test that the damping matrix is correctly formed,
% diagonal elements should be the defined dampign ratios
% zetas=(Fii'*C*Fii)./(2.*omega) % OK


% Damping ratio for mode 2:
% zeta2=alfa/(2*omega(2,2))+beta*omega(2,2)/2

%% Direct solution of harmonic responses 

omega_f=0:0.01:25; % frequency range
F=[0 1 0 0 0 0]'; %Force vector F=[Fs;Fc];

for ii=1:size(omega_f,2)
    
    %Form coefficient matrix
    BB=[K-omega_f(ii)^2*M -omega_f(ii)*C
        omega_f(ii)*C      K-omega_f(ii)^2*M];
    
    % solve unknown coefficient vectors
    ab(:,ii)=BB\F;
    
end

% Separate a and b vectors from ab
a=ab(1:3,:);
b=ab(4:6,:);

% Amplitude and phase
X=sqrt(a.^2+b.^2);
fii=atan2(b,a);

% plotting the results
figure
subplot(2,1,1)
semilogy(omega_f,X(1,:),'r') %amplitude of M1 (payload)
hold on
semilogy(omega_f,X(2,:),'b') %amplitude of M2
semilogy(omega_f,X(3,:),'g') %amplitude of M3 
ylabel('Amplitude')
legend('M1','M2','M3')
grid on
subplot(2,1,2)
plot(omega_f,fii(1,:),'r') %phase of M1 (payload)
hold on
plot(omega_f,fii(2,:),'b') %phase of M2
plot(omega_f,fii(3,:),'g') %phase of M3 
ylabel('Phase (rad)')
legend('M1','M2','M3')
grid on

%% Numerical time integration

% simulation time
t=0:0.01:60;

% initial conditions
x0=[0 0 0];
xdot0=[0 0 0];

% initial conditions
y0=[x0 xdot0]';

% simulation options and time integration with MATLAB ode45 time integrator
err=1e-4;
options = odeset('RelTol',err,'MaxStep',1e-3,'Stats','on');
[t,y]=ode45(@eom,t,y0,options,M,C,K);

% plotting the results
figure
plot(t,y(:,1),'r') %amplitude of m1 (payload)
hold on
plot(t,y(:,2),'b') %amplitude of m2
plot(t,y(:,3),'g') %amplitude of m3 
ylabel('Displacement')
legend('m1','m2','m3')
grid on




