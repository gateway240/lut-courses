clear all;close all;clc

%MODAL ANALYSIS OF 1 DOF SYSTEM

%Known initial values
m1=92; %Mass of the machine, kg
e=0.40; %Unbalance, kgm
zeta=0.018; %Modal daming ratio
omega_f=1500/60*(2*pi); %Nominal rotation speed, rad/s

%Resonance occurs at operation speed, so the stiffness k1 can be solved.
% Spring k1
k1=omega_f^2*m1;

% EOM
%Mass matrix, 1 dof (x1), diagonal matrix
M=[m1];
%Stiffness matrix
K=[k1];
% harmonic force: F*cos(w_f*t)
F=[e*omega_f^2]';

% Procedure of modal analysis
L=chol(M); %(M^(1/2))
invL=inv(L); %(M^-(1/2))
% Mtilde=I (M^-(1/2)*M*M^-(1/2)=I)
Ktilde=invL*K*invL; % mass normalized stiffness matrix
[P,lambda]=eig(Ktilde);
lambda;
P;
omega_n=sqrt(diag(lambda));
Ver=P'*P; % this should be identity matrix if everything has been done right
S=invL*P; % S = matrix of mode shapes vs. P = matrix of eigenvectors
f=S'*F; % modal forces

% solution
X1=f(1)/sqrt((omega_n(1)^2-omega_f^2)^2+(2*zeta*omega_n(1)*omega_f)^2);
Phi1=atan2((omega_n(1)^2-omega_f^2),(2*zeta*omega_n(1)*omega_f));
t=0:0.001:0.2;

for i=1:length(t)
r(i,1)=X1*cos(omega_f*t(i)-Phi1);
x(i,1:1)=S*r(i,1:1)';
end


x(1,1) %Amplitude of the machine at the nominal speed, meter

figure
subplot(2,1,1)
plot(t,r(:,1),'--k')
title('Modal Response')
hold on
grid on
subplot(2,1,2)
plot(t,x(:,1),'k')
title('Physical Response')
hold on
grid on
xlabel('Time (s)')
ylabel('Amplitude (m)')