clear all;close all;clc

%MODAL ANALYSIS OF 2 DOF SYSTEM

%Known initial values
m1=93; %Mass of the machine, kg
m2=9.3; %Mass of the additional block, kg
k2=243*1000; %Stiffness of spring k2, N/m
e=0.30; %Unbalance, kgm
zeta=0.017; %Modal daming ratio
omega_f=1500/60*(2*pi); %Nominal rotation speed, rad/s

%Resonance occurs at operation speed, so the stiffness k1 can be solved.
% Spring k1
k1=omega_f^2*m1;

% EOM
%Mass matrix, 2 dof (x1, x2), diagonal matrix
M=[m1 0
    0 m2];
%Stiffness matrix
K=[k1+k2   -k2
    -k2     k2];
%Harmonic force: F*cos(w_f*t)
F=[e*omega_f^2]';

%Procedure of modal analysis
L=chol(M); %(M^(1/2))
invL=inv(L); %(M^-(1/2))
%Mtilde=I (M^-(1/2)*M*M^-(1/2)=I)
Ktilde=invL*K*invL; %mass normalized stiffness matrix
[P,lambda]=eig(Ktilde);
lambda;
P;
omega_n=sqrt(diag(lambda));
Ver=P'*P %this should be identity matrix if everything has been done right
S=invL*P; % S = matrix of mode shapes vs. P = matrix of eigenvectors
f=S'*F; % modal forces
%Diagonal M and K matrices --> 2 SDOF systems!!
% note: M is identity matrix and K is mass normalized modal matrices

% solution
X1=f(1)/sqrt((omega_n(1)^2-omega_f^2)^2+(2*zeta*omega_n(1)*omega_f)^2);
Phi1=atan2((omega_n(1)^2-omega_f^2),(2*zeta*omega_n(1)*omega_f));

X2=f(2)/sqrt((omega_n(2)^2-omega_f^2)^2+(2*zeta*omega_n(2)*omega_f)^2);
Phi2=atan2((omega_n(2)^2-omega_f^2),(2*zeta*omega_n(2)*omega_f));

t=0:0.001:0.2;

for i=1:length(t)
r(i,1)=X1*cos(omega_f*t(i)-Phi1);
r(i,2)=X2*cos(omega_f*t(i)-Phi2);
x(i,1:2)=S*r(i,1:2)';
end

max(x(:,1)) %Amplitude of the machine (mass, m1) at nominal speed, meter
max(x(:,2)) %Amplitude of additional mass, m2, at nominal speed, meter

figure
subplot(2,1,1)
plot(t,r(:,1),'--k')
title('Modal Response')
hold on
grid on
plot(t,r(:,2),'--r')

subplot(2,1,2)
plot(t,x(:,1),'k')
title('Physical Response')
hold on
grid on
plot(t,x(:,2),'r')
legend('m_{1}','m_{2}')
xlabel('Time (s)')
ylabel('Amplitude (m)')