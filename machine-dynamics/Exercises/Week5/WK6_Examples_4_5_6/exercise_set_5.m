%% Machine Dynamics - Ex set 5
% Problem 1
% Janne Heikkinen, 3.10.2017
clear all;close all;clc;
%Known initial values
m1=19;
m2=10;
k1=50;
k2=80;
Z1=0.05;
Z2=0.01;
%% EOM
%Mass matrix, 2 dof (x1,x2), diagonal matrix
M=[m1 0
0 m2];
%Stiffness matrix
K=[k1+k2 -k2
-k2 k2];

% harmonic force: F*cos(w_f*t)
F=[2 0]';
w_f=2;
% Procedure of modal analysis
L=chol(M); %(M^(1/2))
invL=inv(L); %(M^-(1/2))
% Mtilde=I (M^-(1/2)*M*M^-(1/2)=I)
Ktilde=invL*K*invL'; % mass normalized stiffness matrix
[P,lambda]=eig(Ktilde)

omega_n=sqrt(diag(lambda));
Ver=P'*P; % this should be identity matrix if everything has been done right
S=invL*P; % S = matrix of mode shapes vs. P = matrix of eigenvectors
% modal forces
f=S'*F;
% Diagonal M and K matrices --> 2 SDOF systems!!
% note: M is identity matrix and K is mass normalized modal matrices
% solution
X1=f(1)/sqrt((omega_n(1)^2-w_f^2)^2+(2*Z1*omega_n(1)*w_f)^2);
Phi1=atan2((omega_n(1)^2-w_f^2),(2*Z1*omega_n(1)*w_f)); 
% NOTE! ‘atan2’ command requires denominator and nominator (in this order) in round brackets separated
% with comma. ‘atan2’ takes into account all four quadrants in the unit circle
% whereas ‘atan’ is using only 1st and 4th quarters
X2=f(2)/sqrt((omega_n(2)^2-w_f^2)^2+(2*Z2*omega_n(2)*w_f)^2);
Phi2=atan2((omega_n(2)^2-w_f^2),(2*Z2*omega_n(2)*w_f));
t=0:0.01:5;
for i=1:length(t)
r(i,1)=X1*cos(w_f*t(i)-Phi1);
r(i,2)=X2*cos(w_f*t(i)-Phi2);
x(i,1:2)=S*r(i,1:2)';
end
figure
subplot(2,1,1)
plot(t,r(:,1),'--k')
title('modal response')
hold on
grid on
plot(t,r(:,2),'--r')
subplot(2,1,2)
plot(t,x(:,1),'k')
title('physical response')
hold on
grid on
plot(t,x(:,2),'r')
legend('m_{1}','m_{2}')
