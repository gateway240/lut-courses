% Example 4: Damped Modal Solution 

clear all; close all; clc


% Known initial values
% Mass matrix
M=[9 0
   0 0];
% Stiffness matrix
K = [27     -3
     -3      3];
% Modal damping ratios
xi=[0.05 0.1]';
X0 = [1 0]' ; % Initial conditions for displacement
X0dot = [0 0]' ; % Initial conditions for velocity


% Procedure of modal analysis
L=chol(M); %(M^(1/2))
invL=inv(L); %(M^-(1/2))
% Mtilde=I (M^-(1/2)*M*M^-(1/2)=I)
Ktilde=invL*K*invL'; % mass normalized stiffness matrix
% Symmetric eigenvalue problem
[P,lambda]=eig(Ktilde);

% Remove semicolon ; to print values on Matlab command window
lambda;
P;

% Undamped and damped natural frequency
for k=1:length(X0)
    omega_n(k)=sqrt(lambda(k,k)); % natural frequency = sqrt(eigenvalue)
    omega_d(k)=sqrt(1-xi(k)^2)*omega_n(k);
end
% Remove semicolon ; to print values on Matlab command window
omega_n;
omega_d;



% Spectral matrix S and its inverse
S=invL*P; % S = matrix of mode shapes vs. P = matrix of eigenvectors
invS=P'*L;

% Modal initial conditions
r0=inv(S)*X0;
r0dot=inv(S)*X0dot;

% Integration constants A and B in damped solution
for k=1:length(X0)
    A(k) = (r0dot(k)+xi(k)*omega_n(k)*r0(k))/omega_n(k);
    B(k) = r0(k);
end
% Remove semicolon ; to print values on Matlab command window
A;
B;

% Solve modal and physical coordinates
t=0:0.01:60; %time vector
%
for i=1:length(t)
    for k=1:length(X0)
        % solve modal coordinates
        r(i,k)=exp(-xi(k)*omega_n(k)*t(i)) * (A(k)*sin(omega_d(k)*t(i))+B(k)*cos(omega_d(k)*t(i)));  
    end
    % Transform modal coordinates to physical coordinates
    x(i,1:length(X0))=S*r(i,1:length(X0))';
end

% Plot the results
figure(1)
for k=1:length(X0)
plot(t,x(:,k),'LineWidth',1.5)
hold all
grid on
legend_labels{k} = sprintf(['x' num2str(k) '(m' num2str(k) ')']);
title('PHYSICAL COORDINATES')
xlabel('Time (s)')
ylabel('Amplitude (m)')
end
legend(legend_labels{:})

figure(2)
for k=1:length(X0)
plot(t,r(:,k),'LineWidth',1.5)
hold all
grid on
legend_labels{k} = sprintf(['r' num2str(k) '(m' num2str(k) ')']);
title('MODAL COORDINATES')
xlabel('Time (s)')
ylabel('Amplitude (m)')
end
legend(legend_labels{:})
