clear all; close all; clc

% Known initial values
m1 = 1; %(in kg)
m2 = 4; %(in kg)
k1 = 2; %(in N/m)
X0 = [1 0]' ; % Initial conditions for displacement
X0dot = [0 0]' ; % Initial conditions for velocity

% EOM
% Mass matrix
M=[m1 0
   0 m2];

% Stiffness matrix
K = [ k1  -k1
     -k1   k1];

% Procedure of modal analysis
L=chol(M); %(M^(1/2))
invL=inv(L); %(M^-(1/2))
% Mtilde=I (M^-(1/2)*M*M^-(1/2)=I)
Ktilde=invL*K*invL; % mass normalized stiffness matrix
% Symmetric eigenvalue problem
[P,lambda]=eig(Ktilde);
lambda;
P;

% Testing the subresult
Ver=P'*P; % this should be identity matrix if everything has been done right
         % and vectors are orthogonal
% Spectral matrix S and its inverse
S=invL*P; % S = matrix of mode shapes vs. P = matrix of eigenvectors
invS=P'*L;

% Modal initial conditions
r0=inv(S)*X0;
r0dot=inv(S)*X0dot;

% Undamped Situation
% Modal coordinates
for k=1:length(X0)
omega_n(k)=sqrt(lambda(k,k)); % SQRT(K/M) - undamped natural freq.
A(k) = X0(k);
B(k) = X0dot(k)/omega_n(k);
end

t=0:0.01:10;

for i=1:length(t)
    for k=1:length(X0)
        if omega_n(k) == 0
            r(i,k)=r0(k) + (r0dot(k)*t(i)); % solution for a special case of rigid body mode
        else
            r(i,k)=r0(k)*cos(omega_n(k)*t(i))+r0dot(k)/omega_n(k)*sin(omega_n(k)*t(i));
        end
    end
x(i,1:length(X0))=S*r(i,1:length(X0))';
end

% Plot the results
figure(1)
for k=1:length(X0)
plot(t,x(:,k),'LineWidth',1.5)
hold all
grid on
legend_labels{k} = sprintf(['x' num2str(k) '(m' num2str(k) ')']);
title('REAL COORDINATES')
xlabel('Time (s)')
ylabel('Amplitude (m)')
end
legend(legend_labels{:})

figure(2)
for k=1:length(X0)
plot(t,r(:,k),'LineWidth',1.5)
hold all
grid on
legend_labels{k} = sprintf(['r' num2str(k) '(m' num2str(k) ')']);
title('MODAL COORDINATES')
xlabel('Time (s)')
ylabel('Amplitude (m)')
end
legend(legend_labels{:})
