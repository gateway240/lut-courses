function xdot=timeint(t,x,M,K,C)

if t < 1
    f=1;
else
    f=0;
end

F=[f];

% SEE LECTURE NOTES (lecture 6) page 24/32
% define xdot(1)=x(2)
xdot(1)=x(2);
% xdot(2)...
xdot(2)=inv(M)*(F-C*x(2)-K*x(1));

% time integration needs to return a column vector -> transpose of xdot
xdot=xdot';

end