clear all;close all;clc

% Known Initial Values
m1=57; % Mass of the machine, kg
e=0.08; % Unbalance, kgm
zeta=0.013; % Modal daming ratio
omega=1200/60*(2*pi); % Nominal rotation speed, rad/s
rpm_max=3000; % Maximum rpm

% k1 can be defined using the equation for natural frequency
k1=omega^2*m1;

% EOM
% Mass matrix, 1 DOF system
M=m1;
% Stiffness matrix
K=k1;

% The damping matrix is created from the modal damping factors
% Solving the eigenvalue problem
[FiiN,d]=eig(K,M); %FiiN=eigenvectors, d=eigenvalues

% Re-arrange the matrix according to the natural frequencies (lowest first)
[omega_n, idx]=sort(sqrt(diag(d)));
FiiN=FiiN(:,idx);

% The damping is a bit silly to define like this, but it works
% Modal damping
Ctmp(1,1)=2*zeta*omega_n(1);
% Damping from the modal damping
C=inv(FiiN')*Ctmp*inv(FiiN);

% Range of studied excitation frequencies
omega_f=0:pi/8:rpm_max/60*2*pi;

% SOLVING THE RESPONSE TO UNBALANCE--------------------------------------
for i=1:length(omega_f) % Loop over the specified speed range
    
    % Unbalance force is proportional to rotational speed square
    F=e*omega_f(i)^2; 
    
    %Solution for the equation 
    AA=[K-omega_f(i)^2*M -omega_f(i)*C
        omega_f(i)*C K-omega_f(i)^2*M];

    %External force vector
    FF=[F 0]';
    
    %Solving the coefficient vector ab=[a b]'
    %AA*ab=FF -> ab=AA^-1*FF
    ab(i,:)=AA^(-1)*FF;
    
end

% Vibration amplitudes Xi=sqrt(a_i^2+b_i^2)
X(:,:)=sqrt(ab(:,1).^2+ab(:,2).^2);

% Index of the operational speed (1200 rpm) on the curve
index=find(omega_f == omega); 
% Amplitude of the machine at the operational speed, meter
Desired_Amplitude=X(index)

% Steady State Response Plotting
figure
plot(omega_f*60/(2*pi),X(:,1),'k','LineWidth',1.5)
title('Harmonic Response')
grid on
xlabel('Excitation frequency (rpm)')
ylabel('Displacement (m)')
legend('m_{1}','Location','NorthEast')
