%% Exercise 6.2
clear all
close all
clc

m1=1;
k1=15;
c1=1;

M=[m1];
K=[k1];
C=[c1];

x0=[0];
xdot0=[1];

% vector of initial conditions
y0=[x0 xdot0]';

% time vector
t=0:0.01:10;

% integration paramaters
err=1e-4;
options = odeset('RelTol',err,'MaxStep',1e-3,'Stats','on');

% The following line throws some stuff in another function that solves the
% system responses using numerical time integration.
% ode45 refers to a specific numerical integration method.
% We need to define what variables, options and parameters are used in eom.
% Left side of the equaition defines what we get from the calculation
[t,y]=ode45(@timeint,t,y0,options,M,K,C);

figure
plot(t,y(:,1),'r')
hold on
grid on
legend('m_{1}')
title('numerical integration - displacement')

figure
plot(t,y(:,2),'r')
hold on
grid on
legend('m_{1}')
title('numerical integration - velocity')
