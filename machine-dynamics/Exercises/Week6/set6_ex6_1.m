%% Machine Dynamics - Ex set 6
% Ex 6.1
% Janne Heikkinen, 10.10.2017

% clear all
% close all
% clc

% Known initial values
m1=19;
m2=10;
k1=50;
k2=80;
Z1=0.05;
Z2=0.01;
    
%% EOM
% Mass matrix, 2 dof (x1,x2), diagonal matrix
M=[m1 0
   0 m2];

% Stiffness matrix
K=[k1+k2 -k2
    -k2 k2];

% Modal damping coefficients
xi=[Z1; Z2]; %modal damping factor for the mode 1 and mode 2

% The damping matrix is created from the modal damping factors
% Solving the eigenvalue problem
[FiiN,d]=eig(K,M); %FiiN=eigenvectors, d=eigenvalues

% Re-arrange the matrix according to the natural frequencies
[omega_n, idx]=sort(sqrt(diag(d)));
FiiN=FiiN(:,idx);
%Mr=FiiN'*M*FiiN %Modal mass matrix --> identity matrix

% Build the MODAL damping matrix C (diagonal matrix) - Lecture notes # 5
Ctmp(1,1)=2*Z1*omega_n(1);
Ctmp(2,2)=2*Z2*omega_n(2);

% Full damping matrix from the modal damping matrix - Lecture notes # 5
C=(FiiN')^(-1)*Ctmp*FiiN^(-1);

% range of studied excitation frequencies
omega_f=0:0.01:6;

% SOLVING THE RESPONSE TO UNBALANCE-----------------------------------------------
for i=1:length(omega_f) %loop over the specified speed range
    
    F=2; % see lectures why only amplitude taken
    
    %Solution for the equation 
    %M*x''+C*x'+Kx=F_s*sin(w_f*t)+F_c*sin(w_f*t) (see lecture notes)
    AA=[K-omega_f(i)^2*M -omega_f(i)*C
        omega_f(i)*C  K-omega_f(i)^2*M];

    %External force vector [Fsin1,Fsin2,Fcos1,Fcos2]
    FF=[0 0 F 0]';
    
    %Solving the coefficient vector ab=[a b]'
    %AA*ab=FF -> ab=AA^-1*FF
    ab(i,:)=AA^(-1)*FF;
    
end

% Vibration amplitudes Xi=sqrt(a_i^2+b_i^2)
X(:,:)=sqrt(ab(:,1:2).^2+ab(:,3:4).^2);

% Steady State Response plotting
figure
plot(omega_f,X(:,1),'k','LineWidth',2)
hold on
grid on
plot(omega_f,X(:,2),'r','LineWidth',2)
hold off
ylim([0 1])
title('Harmonic response', 'FontSize', [12], 'FontName','Times New Roman');
xlabel('Excitation frequency [rad/s]','FontSize', [12], 'FontName','Times New Roman');
ylabel('Displacement [m]', 'FontSize', [12], 'FontName','Times New Roman');
legend('m_{1}','m_{2}','Location','NorthWest');
set(legend, 'FontSize', [12], 'FontName','Times New Roman');
