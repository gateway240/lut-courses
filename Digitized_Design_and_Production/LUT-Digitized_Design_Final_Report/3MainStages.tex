\section{Research}
\label{ref_mainstages}

 \subsection{Existing Solution Analysis}
 
 The group began by analyzing the provided example swing arm design using finite element and static analysis in Solidworks. These studies were designed to determine the response and conditions of the existing swing arm to ensure that the new solution would respond equally or better than the existing one. Additionally the group performed topology analysis on portions of the example swing arm to determine opportunities for weight reduction in the model. 
 
 \subsection{Preliminary Design}
 
 Using the information obtained from the previous phase, the group began designing a new swing arm. The design began by removing portions of the provided example swing arm that are complex and heavy. The arching upper portion of the initial design is cumbersome and complex and a good candidate for redesign. The first design focused on improving this portion. Firstly the components constructing this body were removed from the model. Then new components were crafted that would have similar weight and load distribution in a lighter and slimmer profile. Then a tubular cross bar structure was designed to link the two edge portions and provide torsional rigidity. The overall first iteration design is shown from two angles in figures \ref{fig_swingarm_overall_v1_right} and \ref{fig_swingarm_overall_v1_left}.
 
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/v1/Overall.png}
    \caption{First Design Iteration Overall Swingarm (Right Side)}
    \label{fig_swingarm_overall_v1_right}
 \end{figure}
 
  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/v1/Overall_2.png}
    \caption{First Design Iteration Overall Swingarm (Left Side)}
    \label{fig_swingarm_overall_v1_left}
 \end{figure}
 
 Figures \ref{fig_swingarm_v1_left} and \ref{fig_swingarm_v1_right} show the details of the redesigned support arms. The cylindrical supports reduce weight while maintaining support of the lower swing arm.
 
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/v1/SideArmLeft.png}
    \caption{First Design Left Side}
    \label{fig_swingarm_v1_left}
 \end{figure}
 
  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/v1/SideArmRight.png}
    \caption{First Design Right Side}
    \label{fig_swingarm_v1_right}
 \end{figure}
 
 Figure \ref{fig_swingarm_v1_suspension} shows the initial design of the suspension connection bracket to the frame. The curved profile is designed to tighten the suspension and reduce the energy lost to the rear suspension while maintaining rider comfort. In this case since it is a super bike the performance and speed of the motorcycle is more important than rider comfort.  
 
 
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/v1/Suspension_Connection.png}
    \caption{First Design Suspension Connection}
    \label{fig_swingarm_v1_suspension}
 \end{figure}
 
 Finite element and static analysis was performed on this design to determine the load characteristics and it performed better than the initial design. This improvement is a good step but the majority of the swing arm still relied on tubular square beam and cross bar which will be evaluated and optimized in the final design.
 
 \subsection{Final Design}
 
Using the knowledge obtained from the previous sections, the iteration of the final design would strive to create a uni-body swing arm with minimal additional components and reduced weight. The design would need to connect to the frame of the motorcycle and the rear axle. To achieve this a cross-helix design was created. This allows for the design to be cast from aluminum which would dramatically reduce the cost, manufacturing time, and complexity compared to AM techniques. 

Following the creation of the cross-helix design, the mount bracket for the connecting the suspension to the frame was added and a new suspension connection was designed. Using digitized topology analysis produces a model that is unworkable and recreating the design manually creates a model that is not possible to manufacture with conventional techniques. Because of this intuitive topology analysis was performed and diamond and triangular cutouts were added to reduce weight while maintaining strength across the spans from the rear axle to the center cross member. Additionally, the holes to connect to the frame and suspension component were measured, placed, and cut. Once the design was assembled in Solidworks, finite element and static analysis was conducted to ensure that the new swing arm has equivalent load characteristics to the initial design. Additionally the weight of the new swing arm was calculated with Solidworks and compared to that of the original design.

Figure \ref{fig_swingarm_final_overall} displays an overall view of the finalized swing arm design connected to the frame and rear axle. Figure \ref{fig_swingarm_final_assembly} shows an isolated view of the swing arm assembly.

 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/final/Overall.png}
    \caption{Final Design Overall}
    \label{fig_swingarm_final_overall}
 \end{figure}
 
 \begin{figure}[Hs]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/final/assembly.png}
    \caption{Final Design Assembly}
    \label{fig_swingarm_final_assembly}
 \end{figure}
 
 The uni-body swing arm will be cast from aluminum alloy 6061. Figures \ref{fig_swingarm_final_cast_front} and \ref{fig_swingarm_final_cast_back} show the primary swing arm after it is cast. 
  
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/final/Cast1.png}
    \caption{Final Design Cast Front}
    \label{fig_swingarm_final_cast_front}
 \end{figure}
 
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/final/Cast2.png}
    \caption{Final Design Cast Back}
    \label{fig_swingarm_final_cast_back}
 \end{figure}
 
Figures \ref{fig_swingarm_final_milled_front} and \ref{fig_swingarm_final_milled_back} show what the design will look like following milling. Holes will be drilled for the suspension connection and frame attachment. Connection to the rear wheel bracket will be drilled out and then drilled for the fastener Side triangular profiles will be milled to reduce weight without compromising strength. The cutouts reduce the swing arm weight by approximately 1kg.

  
 \begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/final/Mill1.png}
    \caption{Final Design Milled Front}
    \label{fig_swingarm_final_milled_front}
 \end{figure}
 
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/final/Mill2.png}
    \caption{Final Design Milled Back}
    \label{fig_swingarm_final_milled_back}
 \end{figure}
 
Two 25-mm x 30-mm aluminium square bars will be heated and bent to the shape shown in figure \ref{fig_swingarm_final_cross_brace_top} to form the cross braces. The two bars will be cut, positioned together and welded to form the dual arch structure. The cross-bar assembly is then welded to the primary swing arm component. The cross bars will use Al 3003 instead of AI 6061 because AI 6061 is very difficult to bend. AI 3003 has medium strength but much better cold workability and a high elongation making it suitable for this part.  The frame connection displayed in red in figure \ref{fig_swingarm_final_cross_brace_bottom} component will be milled from a 15 mm thick aluminium block. Bolts and bearings will be used to connect the swing arm and suspension to the frame.

 
 \begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/final/cross_brace_top.png}
    \caption{Final Design Cross Brace Top}
    \label{fig_swingarm_final_cross_brace_top}
 \end{figure}
 
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/final/cross_brace_bottom.jpg}
    \caption{Final Design Cross Brace Bottom}
    \label{fig_swingarm_final_cross_brace_bottom}
 \end{figure}

 
