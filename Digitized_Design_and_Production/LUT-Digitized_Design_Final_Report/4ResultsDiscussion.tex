\section{Results and Discussion}
\label{ref_res&disc}

\subsection{Preliminary Static and Finite Element Analysis}
The first step of the entire design process was to analyse and identify the best material to use for the swing arm. After some research, a shortlist of four aluminum alloys. The next stage of the process was to analyse the existing design of the swing arm by applying all four materials individually and looking at the behavior of the materials with the help of topological and load analysis. The purpose of performing topology and load analysis on the existing swing arm was to identify the design limitations and drawbacks that we could learn from and improve going forward. 

Experimenting with topological analysis of certain portions of the existing assembly yields interesting findings. Using 300 N load with grounding of the part provided the reductions shown in figure \ref{fig_topology}. The topology study proved problematic because the algorithm removed portions of the part that cannot be removed because they are constrained to other parts. The interesting portion though is in the middle with the recessed bracket. This indicates that this portion could be redesigned with a lattice structure to improve strength and reduce weight as shown is figure \ref{fig_topology}. 
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/Topo_Test1.png}
    \caption{Topology Analysis}
    \label{fig_topology}
 \end{figure}

The swing arm was then fixed at either ends to avoid any variations in load analysis. This also helps in getting more realistic results as figure \ref{fig_load_fix} shows. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/Loadfix_1.png}
    \caption{Fixed positions}
    \label{fig_load_fix}
 \end{figure}

The first materials applied were two steel alloys, \textbf{AISI 1000/1015} and \textbf{AISI 4340} which are the most commonly used steel alloys. The results were as follows:

1. \textbf{AISI 1000:}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/AISI_1000.png}
    \caption{Static Load Analysis - AISI 1000/1015}
    \label{fig_AISI1000}
 \end{figure}

The above figure shows the comparison between four physical parameters: Static Nodal Stress, Static strain, Static displacement, and the load application. 
The figure shows that the model is deformed extensively on the right most edge. This defines the critical fracture point where if there is more load applied, the model will fracture at this point first.
\begin{itemize}
    \item The yield strength of this model is around \textbf{325MPa.}
    \item The deformation scale is around\textbf{ 4,274} with the static displacement range being between \textbf{5x10-3mm} and \textbf{5.9x10-3mm}.
\end{itemize}

2. \textbf{AISI 4340:}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/AISI_4340.png}
    \caption{Static Load Analysis - AISI 4340}
    \label{fig_AISI4340}
 \end{figure}

\begin{itemize}
    \item The yield strength of the model is close to \textbf{470MPa} and compared to the other steel alloy, it is much stronger and durable. Also, as discussed earlier, AISI 4340 is one of the most abundant metal alloys in use.
    \item The deformation scale is slightly lesser than AISI 1015 which is a good sign as the material indicates more hardness. The deflection is in the range of \textbf{3.5x$\mathbf{10^-3}$ to 4x$\mathbf{10^-3}$mm}
\end{itemize}

These were the two steel alloys being considered for the design and fabrication of our Swing-Arm.
We also considered Aluminium alloys which are lighter and stronger than steel, but more expensive as a result. But their load analysis results were promising.

3. \textbf{Al 7075:}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/AL-7075.png}
    \caption{Static Load Analysis - Al 7075}
    \label{fig_Al7075}
 \end{figure}

From the figure, it is clear that there is immediate visual improvement in the use of Aluminium alloys over steel. The yield strength is significantly higher than steel alloys at \textbf{505 Mpa} under load condition. The deformation scale is much lesser as Aluminium is stronger than steel and hence, undergoes far less deformation. The displacement is between \textbf{1.2x10-2mm and 1.5x10-2mm.}

4. \textbf{Al 6061:}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/Al_6061.png}
    \caption{Static Load Analysis - Al 6061}
    \label{fig_Al6061}
 \end{figure}

Al 6061 is the most used Aluminium alloy, it is more durable than steel alloys and lighter. Although its properties do not match that of Al7075, it is cheaper and comparatively easier to obtain. 
The yield strength is less than that of steel but acceptable at \textbf{275Mpa} under static load. The displacement is also an interesting parameter as it shows that the material under such loading of 3000N, has sufficient toughness and tensile strength.

Because of this we chose Al 6061 as the material for the swing arm.

\subsection{First Design Vertical Load Test}

 Vertical load testing was conducted for each prototype as shown in figure \ref{fig_swingarm_v1_left_vertical_load} and \ref{fig_swingarm_v1_right_vertical_load}. These findings show that the initial design is capable of withstanding the loading condition without any critical stress points or excessive deformation. 
 
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/v1/VerticalLoad1_v1.png}
    \caption{First Design Left Side Vertical Load }
    \label{fig_swingarm_v1_left_vertical_load}
 \end{figure}
 
    \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/v1/VerticalLoad2_v1.png}
    \caption{First Design Right Side Vertical Load}
    \label{fig_swingarm_v1_right_vertical_load}
 \end{figure}
 
 \subsection{Final Design Vertical Load Test}
 
 The main swing arm component of the final design was subject to the vertical loading condition analysis in SolidWorks. The results reveal some stress points but overall show that the prototype is able to withstand the loading condition. Further optimization can be performed to alleviate the stress points but they are not of a magnitude that should be cause for concern. 
 
 \begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/MainStages/final/vertical_load.png}
    \caption{Final Design Vertical Load}
    \label{fig_swingarm_final_vertical_load}
 \end{figure}


\subsection{Topology and Weight Reduction}

An important aspect of improving the swing arm design was to reduce the overall weight. Table \ref{tab:results_weights} shows the weight of the new swing arm compared to the original design. This weight reduction improves the bikes performance and handling. As mentioned in the introduction, digitized topology analysis presents a variety of challenges and problems. Because of this the group used proven triangular lattice structures to reduce weight in the cross bars of the swing arm. This combined with the slimmer uni-body design and use of aluminum resulted in an over 60\% weight reduction compared to the original design. 

\begin{table}[h!]
\centering
\begin{tabular}{||c c ||} 
 \hline
 Swing Arm & Weight (Kg) \\ [0.5ex] 
 \hline\hline
 Original Swing Arm & 13.7 \\
 New Swing Arm & 5.3 \\ [1ex] 
 \hline
\end{tabular}
\caption{Swing Arm Weight Comparisons}
\label{tab:results_weights}
\end{table}


\subsection{Further Work}
\label{future}
The work we have done so far has been beneficial in reducing considerable amount of weight as mentioned above. Our aim is to further decrease the weight especially since the superbike in question is electrically powered. Electric motorbikes and electrical vehicles in general are much heavier than their IC engine counterparts. This weight primarily gets added by the electric components such as the battery(s), the motor and other components that keep the bike in an optimum condition. \\
\\
Hence, we need to optimize the strength to weight ratio further and one of the improvements can be in further reduction of weight of the Swing Arm. This can be done by evenly distributing the load applied in all directions (vertical, lateral and torsional) by creating a pattern of openings in critical load points.\\
\\
Further analysis and modeling can be conducted on the suspension attachment point and assembly to reduce energy lost to the suspension as well as fatigue analysis by creating a cyclic stress-strain relationship that can help us determine new design solutions to optimize the Swing Arm in every possible load condition.






