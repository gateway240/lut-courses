close all
clear
clc
 
%% Axial Loading Wind Speed 12 Shear Exponent 0.2

muRx = 2.95e5;        % Mean axial load capacity of the bearing estimated from chart
sigmaRx = 75e2;       % Standard Deviation approximated based on axial load mean value

xRx = ((-5*sigmaRx + muRx): 100 : (5*sigmaRx + muRx));
fRx = exp(-0.5*((xRx - muRx) / sigmaRx).^ 2) / (sigmaRx*sqrt(2*pi));


muS1 = 2.65e5;           % Mean f(S). Approximated from chart.
sigmaS1 = (0.15e5)/4;    % Standard Deviation f(S). Estimated by conversion from Box and Whisker plot. (range / 4)

x1 = ((-5*sigmaS1 + muS1): 100 : (5*sigmaS1 + muS1));
fSx12 = exp(-0.5*((x1 - muS1) / sigmaS1).^ 2) / (sigmaS1*sqrt(2*pi));

figure('name','Axial with Wind Speed 12 m/s')
    hold on
    fill(x1, fSx12, 'r', 'FaceAlpha', 0.6);
    fill(xRx, fRx, 'b', 'FaceAlpha', 0.6);
    legend fSx12 fRx;
    grid on
%     title('Axial Load with Wind Speed of 12 m/s')
    saveas(gcf,'axial-load-12.png')
    hold off

%% Axial Loading Wind Speed 16 Shear Exponent 0.2

muS2 = 1.95e5;           % Mean f(S). Approximated from chart.
sigmaS2 = (0.075e5)/4;    % Standard Deviation f(S). Estimated by conversion from Box and Whisker plot. (range / 4)

x2 = ((-5*sigmaS2 + muS2): 100 : (5*sigmaS2 + muS2));
fSx16 = exp(-0.5*((x2 - muS2) / sigmaS2).^ 2) / (sigmaS2*sqrt(2*pi));

figure('name','Axial with Wind Speed 16 m/s')
    hold on
    fill(x2, fSx16, 'r', 'FaceAlpha', 0.6);
    fill(xRx, fRx, 'b', 'FaceAlpha', 0.6);
    legend fSx16 fRx;
    grid on
%     title('Axial Load with Wind Speed of 16 m/s')
    saveas(gcf,'axial-load-16.png')
    hold off

%% Radial Loading Wind Speed 12 Shear Exponent 0.2

muRr = 4.7e5;        % Mean axial load capacity of the bearing estimated at 7500 kN
sigmaRr = 1500;       % Standard Deviation approximated based on 1% of axial load mean value

xRr = ((-5*sigmaRr + muRr): 100 : (5*sigmaRr + muRr));
fRr = exp(-0.5*((xRr - muRr) / sigmaRr).^ 2) / (sigmaRr*sqrt(2*pi));

muS3 = 4.6e5;            % Mean f(S). Approximated from chart.
sigmaS3 = (0.075e5)/4;    % Standard Deviation f(S). Estimated by conversion from Box and Whisker plot. (range / 4)

x3 = ((-5*sigmaS3 + muS3): 100 : (5*sigmaS3 + muS3));
fSr12 = exp(-0.5*((x3 - muS3) / sigmaS3).^ 2) / (sigmaS3*sqrt(2*pi));

figure('name','Radial with Wind Speed 12 m/s')
    hold on
    fill(x3, fSr12, 'r', 'FaceAlpha', 0.6);
    fill(xRr, fRr, 'b', 'FaceAlpha', 0.6);
    legend fSr12 fRr;
    grid on
%     title('Radial Load with Wind Speed of 12 m/s')
    saveas(gcf,'radial-load-12.png')
    hold off

%% Radial Loading Wind Speed 16 Shear Exponent 0.2 

muS4 = 4.4e5;            % Mean f(S). Approximated from chart.
sigmaS4 = (0.075e5)/4;    % Standard Deviation f(S). Estimated by conversion from Box and Whisker plot. (range / 4)

x4 = ((-5*sigmaS4 + muS4): 100 : (5*sigmaS4 + muS4));
fSr16 = exp(-0.5*((x4 - muS4) / sigmaS4).^ 2) / (sigmaS4*sqrt(2*pi));

figure('name','Radial with Wind Speed 16 m/s')
    hold on
    fill(x4, fSr16, 'r', 'FaceAlpha', 0.6);
    fill(xRr, fRr, 'b', 'FaceAlpha', 0.6);
    legend fSr16 fRr;
    grid on
%     title('Radial Load with Wind Speed of 16 m/s')
    saveas(gcf,'radial-load-16.png')
    hold off
