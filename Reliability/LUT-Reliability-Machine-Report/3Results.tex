\section{Results}
\label{ref_results}

\subsection{Fault Tree }

The researchers have identified the underlying top failure event for the system as not producing electricity or low efficiency. The failure rate is presented in the unit error per year and turbine. Possible points of failure for the whole system are accounted in the following paragraphs.

\subsubsection{General Failure Analysis}

\textbf{Blades Failure} is the most common wind turbine failure (3800 incidents each year) and can lead to costly repairs and revenue lost from being shut down. The probability of blade failure is 1.5\% per turbine. The failure modes of the blade can include: bending stress, fatigue, fracture, buckling, deformation, extreme loads. 
The following factors can also can contribute to the blade failure: lightning strikes, material and power regulator failure, poor design.

\textbf{Sensor Failure} has a failure rate of 0.3 \%. Many different sensors can fail on a wind turbine so its failure modes include: Wind vane, Accelerometers, Temperature sensors, Displacement sensors

\textbf{Nacelle Drive Train Failure} has a failure rate of 0.05 \%.

\textbf{Generator Failure} has a failure rate of 0.1 \%. This failure causes loss of power production, which causes down time and loss of revenue for the wind farm operator. Failure modes can include: overload (overspeed) and overheat, misalignment, fatigue, jammed bearing and its seizure. 
Other possible reasons for the failure can be: design faults, improper installation, lubricant contamination, inadequate electrical insulation.

\textbf{Gearbox Failure} has a failure rate of 0.1 \%. Around 1200 failures occur annually, 96\% of which are caused by bearings and gears failures. The gearbox contributes around 13 \% to the cost of the overall wind turbine which makes it an expensive component to replace.Its failure modes include: internal gear teeth failure (fatigue loads, improper material, loss of lubricating oil). 

\textbf{Main Shaft Failure} modes include: fracture causing fatigue loads and material properties below specifications. 

\textbf{Main Bearing Failure} modes include: Lubrication (unsuitable, Aged, Insufficient) accounts for 55\% and Solid contamination 20\%.

\textbf{Hub Assembly Failure} has a failure rate of 0.2 \%. Its failure modes include: structure failure, bolt failure that cause excessive design loads, stress corrosion.

\textbf{Yaw System Failure} modes include: increased bearing friction (lack of lubrication) causing galled surface. 

\textbf{Lubrication Failure} modes include: loss of oil, overheating. It can damage many components and cause leakage, ambient temperature above or below design conditions, excessive friction losses. 

\subsubsection{Generator Failure Analysis}

There are four root causes of failure for the generator and their failure modes are:
\begin{itemize}[leftmargin=1cm]
    \item \textbf{Design issues}: Inadequate electrical insulation, loose components, transient shaft voltages, rotor lead failures.
    \item \textbf{Operations issues}: improper installation, irregularities of voltage, improper grounding, overspeed conditions.
    \item \textbf{Maintenance practices}: ignoring alignment, cooling system failures that lead to heat related failure, bearing failure, rotor lead failures. 
    \item \textbf{Environmental conditions}: leading wind, thermal cycling, moisture, contamination, electrical storms
\end{itemize}

The following list includes the most probable components to fail (from the highest rate to the lowest):

\begin{enumerate}
    \item Bearings 
    \item Stator
    \item Collector rings
    \item Rotor
    \item Cooling system
    \item Rotor leads
    \item Others
\end{enumerate}

\subsubsection{Fault Tree Diagrams}

% TODO: add probability to fault treess

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/fault-tree-general.png}
    \caption{General Level Fault Tree for Wind Turbine}
    \label{fig_fault-tree-general}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/fault-tree-specific.png}
    \caption{Detailed Fault Tree for Wind Turbine Generator}
    \label{fig_fault-tree-specific}
\end{figure}


\subsubsection{System Risk Analysis}

The undesired top event studied is the loss of electricity or the low efficiency of the wind turbine. Additionally, the system component that was selected is the generator which is a primary part of the nacelle. The risks of the entire system that affect (or) increase the top event probability are:

\begin{itemize}[leftmargin=1cm]
    \item \textbf{Blade Failure}: This failure is, by far, the most common failure that occurs in a wind turbine and directly affects the electricity production and efficiency of the wind turbine, as the rotation of the blades generates the kinetic energy required to generate electricity.
    \item \textbf{Generator Failure}: This component is what makes the electricity from the mechanical energy converted from the wind’s kinetic energy.
    \item \textbf{Main Shaft}: The shaft is the main metallic component that conveys the energy from the blades to the rest of the system. If this fails, there cannot be any electricity generated. 
\end{itemize}

The system component (generator) is itself a significant factor in increasing the probability of the top event. However, some parts of the generator affect the top event more than others including: 

\begin{itemize}[leftmargin=1cm]
    \item \textbf{Stator and Rotor}: The stator contains all the coils of wire which will have voltage induced in them as the magnets pass over them. The rotor is the moving part which generates electricity along with the stator. Hence, they directly affect the production of electricity.
    \item \textbf{Electronics}: Generator is one of the primary parts of the electrical system of the wind turbine. Electrical issues can lead to mechanical damages such as overheating in the generator or vice versa mechanical issues can lead to electrical issues like short circuit.
    \item \textbf{Bearings}: The bearings are attached to the armature of the generator which influence the rotation of the rotor.
\end{itemize}

The generator is also prone to other types of system failures such as blade failures (chain reaction). Additionally, operational and maintenance issues affect the long-term functioning of the generator and if not taken care of, the efficiency of the wind turbine will be affected.

\subsection{Tribology Study}

\subsubsection{Friction}

Friction is the main opposing force in the wind turbine process and forms the biggest loss of mechanical performance in the entirety of the wind turbine including the generator. Friction is also linked to wear in a way that it’s said that friction is the principal cause of wear. It’s clear that by minimizing the amount of friction between moving parts, huge improvements in performance can be made along with greatly improved reliability. This is why bearings are used in between of mechanical parts in relative motion. Bearing is a machine element whose main purpose is to minimize friction in the desired direction of motion. Bearings however are not perfect and produce friction within them causing a loss in performance and reliability. The potential savings from improved bearings has been of great interest in multiple fields of technology including wind turbines. Improvements in bearing design have been made possible by understanding the different phenomena of tribology happening in bearings.

Studying our topic, the generator, it can be noticed that bearings are used to support the armature of the generator. According to information found in literature, the bearings of the generator are responsible for about 70\% of failures in the generator of a wind turbine. Failures of the generator also seem to be much more common in wind turbines (p=0.1) than in other industries (p=0.0315-0.0707). \cite{DHANOLA2020104885} When comparing this information to the fact that generator failure downtimes are among the highest in the whole system, it becomes clear that the bearings are a critical part in the construction and therefore worth investigating.

\subsubsection{Lubrication}

When it comes to lubricating bearings in a wind turbine, lubricants must have a wide temperature range and anti-corrosion and anti-false brinelling properties. Bearings inside a wind turbine generator must withstand challenging conditions. It’s difficult and costly to exchange these bearings. To achieve a finely tuned system, it’s vital to use the right components. One of those components is the right grease for the bearing and application conditions. Considering the working conditions for a wind turbine generator, lubrication grease must have followed properties:
\begin{itemize}[leftmargin=1cm]
    \item Suitable for applications with medium and high-speed ball (and roller) bearings operating at medium and high temperature.
    \item Extremely long life at high temperatures
    \item Excellent corrosion protection
    \item High thermal and mechanical stability
    \item Good start-up performance at low temperatures
    \item Low noise properties
\end{itemize}
Based on these properties some high-quality greases can be suggested by different companies. SKF developed high-quality greases LGEP 2, LGWM 1 and LGWM 2 for the proper lubrication of main shaft bearings. Among them, LGHP 2 is suggested for wind turbine generator application. SKF LGHP 2 is a premium quality mineral oil-based grease, using a modern Polyurea (di-urea) thickener. %The main properties of these grease are:
% TODO: Add table

\subsubsection{Wear}

Mechanical wear itself is a damaging and gradual process of material removal or deformation caused either by mechanical (erosion) or chemical (corrosion) reasons. There are four main wear phenomena, that can affect both simultaneously and consecutively when chains of different wear types are formed: adhesive, abrasive, fatigue and tribochemical wear. 
Wear can have large economic and functional relevance in terms of the wind turbines, as it causes operating surfaces and machine parts to degrade, leading to material failure or loss of functionality. The generator is a component that specifically can be exposed to wear as it is an assembly of mechanical parts in relative motion, sliding and rolling. The generator's components that suffer from wear are slip rings (ring and brush), pulleys and bearings.
Almost all failures are the result of long service life, but other possible reasons may be:
\begin{itemize}[leftmargin=1cm]
    \item Poor quality of manufacturing of components
    \item Violation of operating rules or work outside the limits of normal modes
    \item External factors (liquids, temperatures, chemicals, dirt)
\end{itemize}

One of the main threats to bearings in wind turbine generators is stray currents that can cause micro-pitting (that refers to fatigue failure and tribochemical wear) and other types of surface damage if current passes through the bearing. The bearing failure might lead to a catastrophic failure of the generator itself. However, modern generators are equipped with sealed (maintenance-free) bearings that simply need to be replaced after a certain period. A particular solution might be protecting the bearings by using different degrees of insulation. For example, a ceramic surface coating might resist small currents. For more serious situations, hybrid bearings with ceramic rollers can be used, ensuring no conductivity and very long lifetimes. Bearings are also frequently exposed to abrasive wear. A typical example is a three-body wear type caused by dirt particles that get inside a bearing and get sandwiched between races and rolling elements.

 % TODO: add image 
The slip rings are key elements of the generator that also experience mechanical wear. Operating at high speeds (1250 RPM), they ensure the rotor coils-cables connections. These contacts are typically a rotating ring and a stationary brush. The rings transfer power to and from the rotor, while the brushes conduct current between stationary wires and moving parts. These two components might experience tribochemical wear due to electrical underloading or overloading, as well as while running in corrosive acid gas environments or in a space with a lack of oxygen. In addition to this, adhesive wear can be caused by the sliding action of the slip rings.

The generator pulley is mounted on the rotor shaft and is designed to drive the generator. In addition to its main function, the pulley also protects the generator. During the operation of the shaft, vibrations are formed, which can adversely affect the operation of the generator. The pulley acts as a vibration damper and thus protects the generator from adverse influences. Due to constant rotation, it can also be exposed to abrasive wear.


\subsubsection{Risks of the generator fault tree caused by tribology }
The ANSI/AGMA/AWEA 6006-A03 standard \cite{Musial2007ImprovingWT}, provides specifications for wind turbine gearbox design and contains maintenance requirements to provide an expected gearbox failure lifetime of 20 years. Contrary to this standard, numerous investigations \cite{AmericanGear-Ansi} have shown real-world gearbox repair and replacement rates occur at significantly smaller time intervals than the promised 20 years lifespan. This additional unexpected repair and maintenance cost contributes to the increase in the cost of wind energy and slows the needed adoption of wind technology.

The \textbf{Nacelle} (Gearbox and Bearings) is affected by tribological effects in some of the following ways:
\begin{itemize}[leftmargin=2cm]
    \item \textbf{Water contamination}: this can degrade the performance of the gearbox lubricant by causing the lubricant to foam or making it unable to create a contact film. Water can also rust internal components that are unprotected. This is especially prevalent in wet operating conditions
    \item \textbf{Micro-pitting for gear teeth}: When the elastohydrodynamic (EHD) lubrication film (oil film) is the same order of magnitude as the surface roughness, the surface topography interacts and forms micro pits. This can be caused by extreme load or temperature, water contamination in the oil, or low oil viscosity which is quite likely in extreme wind turbine environments. 
    \item \textbf{Rolling fatigue for roller bearing}: crack propagation failure caused by the near-surface alternating stress field. Correctly selected, mounted, and maintained bearings usually fail due to this.
\end{itemize}

The \textbf{Blades} are affected by tribological effects in some of the following ways:
\begin{itemize}[leftmargin=2cm]
    \item \textbf{False Brinelling}: vibrations in the blades start this process in the pitch bearings. False brinelling is bearing damage caused by fretting and can but does not have to include corrosion 
    \item \textbf{Leading edge erosion}: a wear phenomenon of wind turbine blades which results in mass loss and surface damages exposed to raindrops in this case 
\end{itemize}

The \textbf{Hub Assembly} is affected by tribological effects in some of the following ways:
\begin{itemize}[leftmargin=2cm]
    \item \textbf{Corrosion}: can occur from inadequate material protection in harsh environments (ex. On-shore saltwater environments)
    \item \textbf{Chattering + seizing}: caused by improper or insufficient lubrication during the lifetime of the component
\end{itemize}	

The \textbf{Yaw System} is affected by tribological effects in some of the following ways:
\begin{itemize}[leftmargin=2cm]
    \item \textbf{Chattering}: again caused by improper or insufficient lubrication, corrosion, or wear of the yaw system’s bearings. Chattering could cause excess stress to the generator shaft and generator bearing(s), thus causing decreased efficiency of electricity generation or damage to the generator resulting in no electricity generation.
    \item \textbf{Seizing}: caused by extreme wear, corrosion, or lack of lubrication of the yaw system’s bearings, and could result in diminished or zero electricity production based on turbine orientation with respect to wind direction.
\end{itemize}	

\subsection{Distributions}
Common wind turbine designs include power transmission from the rotor to the generator through the system composing the main shaft friction connection, multiplying gearbox, and a flexible coupling. All these components affect the load bearing capacity and failure rate.    

In the \textbf{Gearbox} \cite{drivetrain-windturbine} torque from the rotor shaft is transmitted through the friction joint to the slow speed shaft, planetary gears, and outer ring gear as part of the housing. Increasing the number of planetary gears provides a higher power to mass ratio which improves efficiency and decreases wear.

In the \textbf{Generator} \cite{drivetrain-windturbine} \cite{main-bearing-loading} failure of the high-speed shaft bearing is common. \blockquote{The full failure mechanisms are not fully understood. There is a relatively low predicted durability of cylindrical roller bearings at the side of the pinion (“upwind” side) and acceptable durability of radial bearings at the gearbox output (“downwind” side). Additionally the failure rate of the main bearing is high as most bearings do not reach the design life of roughly 20 years and some fail in less then 6 years.}

Failure data shows that \textbf{Generator Bearing} \cite{calculating-loads-doi:10.1177/0309524X20914022} failure is the main cause of overall generator failure. Researchers believe this is caused by increased bearing loads. These increased bearing loads are determined by the magnitude of misalignment \cite{calculating-loads-doi:10.1177/0309524X20914022} as well as by the type of connecting coupling installed between the gearbox and generator shaft. Additionally, Flexible Connecting Couplings (FCCs) are a principal source of failure and contribute significantly to the load bearing capacity of the generator.

Tables \ref{tab:axial-load-cap} and \ref{tab:axial-load-varying-wind-speed} are derived from manufacturers data and empirical evidence from scientific literature which are used to generate the distribution plots.

\input{tables/axial-load-cappacity}
\input{tables/axial-load-wind-speed}

\begin{figure}[H] \label{ fig-radial} 
  \begin{minipage}[b]{0.5\linewidth}
    \includegraphics[width=1\linewidth]{Images/radial-load-12.png} 
    \caption{Radial Load [Wind 12 m/s]} 
    \label{fig:radial-load-12}
  \end{minipage} 
  \begin{minipage}[b]{0.5\linewidth}
    \includegraphics[width=1\linewidth]{Images/radial-load-16.png} 
    \caption{Radial Load [Wind 16 m/s]} 
    \label{fig:radial-load-16}
  \end{minipage} 
\end{figure}
\begin{figure}[H] \label{ fig-axial} 
  \begin{minipage}[b]{0.5\linewidth}
    \includegraphics[width=1\linewidth]{Images/axial-load-12.png} 
    \caption{Axial Load [Wind 12 m/s]} 
    \label{fig:axial-load-12}
  \end{minipage} 
  \begin{minipage}[b]{0.5\linewidth}
    \includegraphics[width=1\linewidth]{Images/axial-load-16.png} 
    \caption{Axial Load [Wind 16 m/s]} 
    \label{fig:axial-load-16}
  \end{minipage} 
\end{figure}

% TODO: talk about distributions

Reliability-based product design requires specific knowledge of what failure characteristics to study to make the make the machine more reliable. The characteristics can be used to determine:
\begin{enumerate}
    \item How failures can happen
    \item How frequently failures occur and their types
    \item Probability factors affecting failures.
\end{enumerate}
A number of factors contribute to calculating these values. \textbf{Reliability Given Time} is primarily used to determine the probability or percentage of reliability of a product or component, in this case, the generator, for a given period. The generator, being a typical electrical component, should have a Weibull characteristic life of between 6-12 years. \textbf{Probability of Failure Given Time} is the exact opposite of Reliability Given Time which uses the values from the former to calculate the failure percentage. Now, this characteristic is not necessarily needed if we have the Reliability Percentage and vice versa. \textbf{Mean time to failure (MTTF)} is the average time which the component is expected to function until failure. This characteristic is crucial to determine the reliability aspect of the component. It is calculated by \cite{wind-turbine-reliability}:
\begin{equation}
    \mbox{MTBF} = \mbox{Operational Time } / \mbox{ No. Of failures}
\end{equation}
\textbf{Failure Rate} and MTBF go hand in hand as they are calculated with respect to each other. It is defined as the number of failures that occur in a given period. The failure characteristics of individual pieces of electrical equipment, (i.e. components) can be partially described by the following basic reliability statistics: failures per year per component (failures per unit year) \cite{wind-turbine-reliability}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/failure-rate-1.png}
    \caption{Wind Turbine Failures per Year \cite{wind-turbine-reliability}}
    \label{fig_wind_turbine_failures_1}
\end{figure}
Figure \ref{fig_wind_turbine_failures_1} shows the failure rates of each subsystem and in our case, the generator failure rate shown as 20\%, which means the reliability rate is about 80\% for a given time. However, a weak subsystem of the wind turbine affects the reliability of the whole system. Hence, it is imperative to study the reliability factor for each subsystem to avoid a cascade of failures. \textbf{Warranty time} is the estimated time of successful functioning of the component before potential wear or failure. This characteristic cannot be accurately studied as it is just an estimation based on trials. 
Of all the characteristics listed above, the most suitable characteristic directly affecting the reliability aspect of the generator is the failure rate as well as the MTBF as they go hand in hand. Also, these characteristics can be used to describe the reliability aspects of the generator, these studies are based on theoretical assumptions and do not factor in variations and environmental fluctuations. Real world scenarios might not be the same as simulations done in optimal conditions as there are little to no variations affecting the reliability calculations.

In reliability-based design, we attempt to make predictions about the product's life by fitting a statistical distribution to life data. The distribution with the parameters for the data set can be used to estimate important product's life features such as reliability or probability of failure at a specific time, the failure rate. Once the distribution parameters for a particular data set are calculated, a variety of plots can be built, and different kinds of probability levels can be calculated. The plot of the failure probability over time describes the probability of a machine part failing by a particular time. The failure probability can also be named "unreliability". The plot of the reliability over time describes the probability of a successful operation of a machine part by a specific time. The plot of the probability density function describes the probability of a machine variable for a random range. This range can be tolerance or deviation. The reason for using a range for the variable is that the probability of an exact number is equal to zero (for example the probability for having stress exactly equal to 100MPa is zero, it may be 99.999). In fact, most of the other plots are based on PDF. The plot of the failure rate over time shows the number of failures per unit of time that is expected to occur for a machine part by a particular time.

Although all these plots can show the reliability aspects of the studied machine part, the plot that might be especially suitable for the application of the wind turbine drivetrain (and the generator particularly) is the failure rate over time plot. A great addition to the failure rate over time plot would be a chart with potential failures modes, from which it could be seen how each failure mode contributes to the failure rate of the system.
The reliability data, including failure rate and downtime (the time during which a turbine does not produce power output due to a failure), are critical for the design, operation, maintenance, and performance assessment of the wind turbine. The failure statistics can be calculated either by failure rates per turbine (or a specific component of the generator) per year or the number of failures in a certain time interval.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/failure-box-plot.png}
    \caption{Failures rates of subassembly components \cite{reliability-data-review}}
    \label{fig_turbine_failure_box_plot}
\end{figure}

The box plot in figure \ref{fig_turbine_failure_box_plot} represents the failures rates of subassembly components.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/failure-rate-2.png}
    \caption{Wind Turbine Component Failure Rate \cite{RePEc:eee:rensus:v:23:y:2013:i:c:p:463-472}}
    \label{fig_wind_turbine_failures_2}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/bathtub-plot.png}
    \caption{Bathtub Curve \cite{RePEc:eee:rensus:v:23:y:2013:i:c:p:463-472}}
    \label{fig_bathtub}
\end{figure}

There is an assumption that the failure rate of wind turbine assemblies follows the “bathtub curve” shape as depicted in figure \ref{fig_bathtub}, meaning that there is a constant failure rate during the useful life period. It was done by counting the failure occurrence and extracting reliability results in terms of failure rates.

\subsection{Failure Mode Matrix}
Table \ref{tab:derived_failure_modes} depicts certain components' failure modes and corresponding material properties that are needed to be ensured in order to minimize the failure probability. Each component of the table is presented with its sub-elements and their particular failures, as all constituting elements might experience different risks. 
When having the potential failures derived, appropriate material properties can be considered for each element. This simple approach allows quick identification of what specific element and its material's property should be enhanced. 

\input{tables/failure-modes/component-failure-modes}
\input{tables/failure-modes/qualitative-fmeca}

Another tool carried out in terms of this section is a failure modes effects and criticality analysis (FMECA) matrix, with implemented both qualitative and quantitative approaches. This is a useful tool especially for the maintenance and facility managing teams. 

The qualitative FMECA table's axes represent the likelihood and consequences. Based on the color-coding, the priority is highlighted. The red cells show the aspects to be paid with major attention as there is a minimized reliability caused by a specific failure mode. The green-colored cells demonstrate the scenarios with less significance and likelihood. The area in between colored in yellow and orange shows moderate levels of risk. 

With regards to the quantitative matrix, the representation is the same, however, it is based on numbers. The consequences of this are shown in table \ref{tab:quantitative_fmeca} which shows the downtime of the wind turbine and the expenses that will follow up. 

\input{tables/failure-modes/quantitative-fmeca}

Another table from which specific mechanical properties responsible for the type of failure can be identified is a failure mode analysis related to the material properties in table \ref{tab:failure_mode}. It correlates the likelihood of the construction failures and corresponding properties to attention should be paid in order to prevent this or that collapse. 
With the presence of details about what wrong is going on during the operation of the mechanical component, the team can focus on a specific property of that component. Either repairing or replacing the failing component can be done considering costs, or in some cases, the team can go ahead with the component until the final breakdown happens if the part does not play a significant role, meaning that if it does not affect the power output.

\input{tables/failure-modes/material-property-failure-modes}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/four-field/four-field-roto.png}
    \caption{Qualitative four-field analysis of generator rotor reliability}
    \label{fig_four-field-rotor}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/four-field/four-field-stator.png}
    \caption{Qualitative four-field analysis of generator stator with windings reliability}
    \label{fig_four-field-stator}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/four-field/four-field-internal-bearing.png}
    \caption{Qualitative four-field analysis of generator internal bearing reliability}
    \label{fig_four-field-bearing}
\end{figure}

\input{tables/integrated-failure-mode} % TODO: fix this table per Kush's suggestion

\subsection{SWOT analysis of different reliability engineering tools used from usability/sustainability perspectives}

\subsubsection{Fault tree and general failure analysis}

\begin{figure}[H]
    \centering
    \input{tables/swot/swot-fault-tree}
    \caption{SWOT Analysis of Fault Tree and General Failure Analysis}
    \label{fig_swot_fault_tree}
\end{figure}

A fault tree is good in situations where chains of events need to be analyzed. Fault tree analysis shows clearly the different paths to a failure from an unwanted top event. Together with general failure analysis they provide a good base for reliability assessment. Fault tree system is standardized and when combined with probabilities from a reliable source and standardized failure mode it gives good amount of objective information.

\subsubsection{Tribology study}

\begin{figure}[H]
    \centering
    \input{tables/swot/swot-tribology}
    \caption{SWOT Analysis of Tribology Study}
    \label{fig_swot_tribology}
\end{figure} 

A tribology study is useful when the root cause of failures in parts in contact with one another needs to be studied. A tribology study also allows the design of more efficient machine elements by making optimization of contact parts possible. Through tribology study, for example, materials can be selected optimally. However, tribology study can lead to over reliability and overly expensive material choices if no compromise is made.

\subsubsection{Distributions}

\begin{figure}[H]
    \centering
    \input{tables/swot/swot-distribution}
    \caption{SWOT Analysis of Distribution Analysis}
    \label{fig_swot_distribution}
\end{figure}

Reliability distributions allows optimizing reliability and lifetime. However, using distributions require historical data, approximations and average values so the final product of the analysis can be inaccurate.

\subsubsection{Failure Mode Matrix}
\begin{figure}[H]
    \centering
    \input{tables/swot/swot-failure-matrix}
    \caption{SWOT Analysis of Failure Mode Matrix}
    \label{fig_swot_failure_matrix}
\end{figure}

Failure mode matrices can be helpful when multiple perspectives on system failures is wanted. Through conducting different analyses, a good general picture of failure possibilities can be formed. However, methods of failure mode matrix analysis are very subjective.
