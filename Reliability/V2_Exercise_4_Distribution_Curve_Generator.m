close all
clear
clc
 
%% Axial Loading Wind Speed 12 Shear Exponent 0.2

muR = 7.5e6;        % Mean load capacity of the bearing estimated at 7500 kN
sigmaR = 75e3;      % Standard Deviation approximated based on 1% of mean value

xR = ((-5*sigmaR + muR): 0.01 : (5*sigmaR + muR));
fR = exp(-0.5*((xR - muR) / sigmaR).^ 2) / (sigmaR*sqrt(2*pi));


muS1 = 2.65e5;           % Mean f(S). Approximated from chart.
sigmaS1 = (0.15e5)/4;    % Standard Deviation f(S). Estimated by conversion from Box and Whisker plot. (range / 4)

x1 = ((-5*sigmaS1 + muS1): 10 : (5*sigmaS1 + muS1));
fSx12 = exp(-0.5*((x1 - muS1) / sigmaS1).^ 2) / (sigmaS1*sqrt(2*pi));

hold on
fill(x1, fSx12, 'r', 'FaceAlpha', 0.6);
fill(xR, fR, 'b', 'FaceAlpha', 0.6);
legend fSx12 fR;
grid on
xlabel('x')
hold off

%% Axial Loading Wind Speed 16 Shear Exponent 0.2

muS2 = 1.95e5;           % Mean f(S). Approximated from chart.
sigmaS2 = (0.075e5)/4;    % Standard Deviation f(S). Estimated by conversion from Box and Whisker plot. (range / 4)

x2 = ((-5*sigmaS2 + muS2): 0.01 : (5*sigmaS2 + muS2));
fSx16 = exp(-0.5*((x2 - muS2) / sigmaS2).^ 2) / (sigmaS2*sqrt(2*pi));

hold on
fill(x2, fSx16, 'r', 'FaceAlpha', 0.6);
fill(xR, fR, 'b', 'FaceAlpha', 0.6);
legend fSx16 fR;
grid on
xlabel('x')
hold off

%% Radial Loading Wind Speed 12 Shear Exponent 0.2

muS3 = 4.6e5;            % Mean f(S). Approximated from chart.
sigmaS3 = (0.075e5)/4;    % Standard Deviation f(S). Estimated by conversion from Box and Whisker plot. (range / 4)

x3 = ((-5*sigmaS3 + muS3): 0.01 : (5*sigmaS3 + muS3));
fSr12 = exp(-0.5*((x3 - muS3) / sigmaS3).^ 2) / (sigmaS3*sqrt(2*pi));

hold on
fill(x3, fSr12, 'r', 'FaceAlpha', 0.6);
fill(xR, fR, 'b', 'FaceAlpha', 0.6);
legend fSr12 fR;
grid on
xlabel('x')
hold off

%% Radial Loading Wind Speed 16 Shear Exponent 0.2

muS4 = 4.4e5;            % Mean f(S). Approximated from chart.
sigmaS4 = (0.075e5)/4;    % Standard Deviation f(S). Estimated by conversion from Box and Whisker plot. (range / 4)

x4 = ((-5*sigmaS4 + muS4): 0.01 : (5*sigmaS4 + muS4));
fSr16 = exp(-0.5*((x4 - muS4) / sigmaS4).^ 2) / (sigmaS4*sqrt(2*pi));

hold on
fill(x4, fSr16, 'r', 'FaceAlpha', 0.6);
fill(xR, fR, 'b', 'FaceAlpha', 0.6);
legend fSr16 fR;
grid on
xlabel('x')
hold off
