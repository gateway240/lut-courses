\section{Introduction}
\label{ref_intro}
In recent years, the packaging market has evolved to employ new technologies. Products once manually wrapped in foil for protection by workers are now packed in cardboard boxes with robotics. Packaging is now used together with the term processing as it is integrated into the supply chain processes.

As stated by the International Federation of Robotics (IFR), sales of robots are increasing year-by-year, with a 16\% increase in 2016 compared to the previous year. The IFR estimates that over 3 million industrial robots will be at work in 2020, representing an average annual growth of 14\% between 2017 and 2020 \citep{IFRpaper}. By the same time the packaging market is growing rapidly in size. Having a setback because of the COVID pandemic of 6\% in 2020 compared to the previous year the market still increased from \$843.8 billion in 2015 to \$859.9 billion in 2020, with expectations to reach a total market value of \$1.13 trillion in 2030 \citep{SmithersRep}. One major trend for this market identified by \cite{McKinseyRep} is the growth of the e-commerce sector. In particular that includes the implementation of fully automated warehouses with AI-adaptable robotic systems.

As no overview of the different robotic systems in the context of packaging exist in the literature, this paper provides a brief summary of robotic systems researched in the literature. Therefore, a methodology is provided to rate the contribution of the each publication in the different stages of the packaging line. A case study on Amazon's packaging system is given to demonstrate how these technologies are already in use in one of the biggest retailers on the market.

In this section the basics of robotics in packaging are described, providing the theoretical background, together with a description the historical development, and explaining possible future trends. In Section \ref{ref_methods} a measuring system is developed to categorize different achievements in robotic packaging. Section \ref{ref_mainstages} then sorts the references into the three different stages of packaging and presents a survey of their presence in literature. The case study applies the results to a real-life example. Afterwards the results are presented in a table and discussed in Section \ref{ref_res&disc}. The paper is summarized in Section \ref{ref_conclusion}.

\subsection{Basics of Robotics and Packaging}
\label{ref_basics}

When studying packaging technology it is important to understand what the purpose of packaging is. 
\cite{Bottani2011} states that product packaging serves the following critical functions: 
\blockquote{
\begin{enumerate}[label=\roman*]
    \item protecting food, which involves mechanical and thermal protection during transportation, chemical and physical protection from external agents and avoidance of dispersion;
    \item increasing product shelf life, by contributing to the preservation of the organoleptic and nutritional properties of product;
    \item delivering information to the final customers, concerning nutrition properties and conservation of product.
\end{enumerate}

Packaging is thus essential to preserve the nutritional properties of food products, and to reduce the use of additives during the production process. 
}

While these three properties define the central tenants and critical functions of packaging there are also other considerations. Packaging must be designed to be aesthetically pleasing and recognizable if the company desires to sell it to consumers. Manufacturers must also consider the different levels of packaging (primary, secondary, and tertiary) to improve efficiency and maximize the amount of product that can fit on a single pallet or other shipping method.  

Now that the reader has an understanding of the purpose of packaging and considerations to account for when creating it, the authors will provide a primer on basic robotics. The 4 most popular types of commercial robots available for industrial automation. According to \citet[][p.4]{leganani2012} these include: 
\blockquote{
\begin{enumerate}[label=(\alph*)]
\item Cartesian robots (or gantry robots)
\item Cylindrical/polar, and SCARA robots
\item Parallel kinematics manipulators (e.g. "delta robot")
\item Anthropomorphic robots
\end{enumerate}
Each kinematic structure determines a corresponding shape of the working space. Manipulators of type a), b), and d) are called "serial manipulators" because they are realized connecting in series several links each one actuated by a revolute or a prismatic joint. Each joint is actuated by a motor; the term axis usually denotes a joint and its actuator (linear or revolute motor). The first three joints (main axes) determine the motion of the arm and so the XYZ position of the gripper whose orientation can be modified by 1, 2 or 3 "secondary axes" located in the wrist. Usually industrial manipulators have between 4 and 6 degrees of freedom (DOF).
}

Certain robots and certain geometries are much more suited for certain packaging applications than others. For example a Cartesian gantry robot is very well suited for pick and place operations whereas an anthropomorphic robot would be very poorly suited for this task. Additionally some robots require much more complex programming, control, and sensors. A 6 axis polar robot will require much more complex setup, maintenance, and positioning then a 3 axis gantry robot.

With an understanding of the basics of robotics and packaging the reader is now ready to explore the history and milestones that have lead to the creation of the modern packaging line, and a brief outlook of new trends emerging in the packaging market. 

\subsection{Milestones in Packaging}
\label{ref_history}

\citet{jones2016routledge} provides a concise history on the early history of packing:
\blockquote{
For as long humans have been hunting, gathering, and trading packages have been used for collecting, storing, and shipping. Early basket-weaving technology developed to craft packages made from grasses and other natural fibers, especially for dry and harvested food. For longer distance shipping of liquids like wine and oil, sturdy shipping containers were developed.
Clay amphorae, wooden casks, wooden crates and shipping sacks are some examples of how the humans have transported food and beverage for centuries.  }

It was not until the late 1800s that packaging moved beyond simple bulk shipping containers to household-sized packs designed to appeal directly to consumers \citep{jones2016routledge}. That significant change in packaging rose the value of the package itself as never before, opening wide possibilities for marketing and logistical system. In 1861  Another remarkable innovation in the sector was the mechanisation of the process for making tinplate cans, glass bottles, and paperboard cartons by Edwin Norton, Michael Owens and Robert Gair during the period 1880-1910. Package converting was an early application of mass production, and Norton's can-making line was credited with inspiring Henry Ford's assembly line process \citep{Twede2012}.

During the Twentieth Century the packaging line involved workers and machines, but with the emergence of new materials some parts of the process started to be semi automated.

Nowadays, for each process of the packaging line - manufacture, cleaning, filling, sealing, labelling, wrapping and palletizing of packaging - there is a dedicated machinery, and in some cases a fully automated process.

\subsection{Trends in Robotics and Packaging [Shivam]}
\label{ref_trends}

Rising number of manufacturing industries around the globe is one of the major factors boosting the growth of the global packaging robot market. Rapid urbanization along with increasing disposable income is anticipated to increase the demand for digital labeling and packaging. In addition, numerous industries such as healthcare, sports, and FMCG are anticipated to foster the market growth. These robots are getting smarter owing to technological advancements in sensing and software; thus, this results in high demand for these packaging robots. In addition, they offer ease of use owing to integrated controls and intuitive human-machine interfaces.

On the basis of application, the global packaging robot market is segmented as picking, packing, case packing, tray packing, filling, and others. The others segment is further sub-segmented as palletizing, case palletizing, bag palletizing, and de-palletizing. Based on the gripper type, the global market is segmented as the claw, clamp, vacuum, and others. On the basis of end-use industries, the global packaging robot market is further segmented as food and beverages, pharmaceuticals, consumer products, tracking and logistics, industrial packaging, chemicals, electronics devices, and others.

Asia Pacific is anticipated to witness high growth in the global packaging robot market mainly owing to rapid industrialization across various countries such as China, India, and South Korea. China is expected to witness considerable growth in terms of revenue in the region. Japan is also anticipated to exhibit high growth potential. In addition, the U.S. is anticipated to dominate the market in the near future. Owing to the presence of a large number of diversified industries, Europe is also expected to show high demand.

in the future, The global packaging robot market is expected to draw growth on the back of several domestic and multinational organizations operating in different industries around the world that adopt robotics as an indispensable slice of their business strategies. On a worldwide platform, the growth of the global packaging robot market could be catapulted with mounting concerns over overall equipment effectiveness (OEE) and safety and advancement in various manufacturing sectors.The global packaging robot market is forecasted to reach a valuation of \$5.03 billion by 2024 while rising at a CAGR of more than 14.0\% between 2016 and 2024. In 2016, the global packaging robot market had acquired a \$1.67 billion.

Some of the major players dominating the global packaging robot market are Fanuc, Adept Technology, KUKA Robotics, and Yaskawa Electric America. Other key players influencing the global market are Motoman, Intelligent Actuator, Denso Robotics, Panasonic, A-B-C Packaging Machine, AFAST Robotics, BluePrint Automation, Bosch Rexroth, Okura USA, Fuji Yusoki Kogyo, Yamaha Robotic, and Epson Robots.