\section{Robotics in the Main Stages of Packaging}
\label{ref_mainstages}

\subsection{Robotics in Primary Packaging}
\label{ref_primary}

    \subsubsection{Needs of Primary Packaging}
    As introduced in Section \ref{ref_history}, the primary packaging is the fundamental layer in contact with the product that has to arrive to the consumer. At the beginning, primary packaging was used to display and protect goods, without a precise focus on what the customer wanted. During the Industrial Revolution customers moved more and more to the cities, living in relatively cramped surroundings without the storage capability of the farms, thus the need to supply pre-packing items in measured quantities emerged, ensuring the buyer some assurance to the quality and the quantity of the goods. Eventually this pre-packaging was moved a stage further away from the consumer to the point of production rather than sale which is the packaging system we know today, at the point of production rather than sale. This move brought a shift from bulk to consumers packs, which had to survive the journey from the factory to the shop and then home. This journey today may span countries and even continents and will include storage stages on route. It additionally gave producers the opportunity to develop their own style in promoting their products to consumers \citep{EMBLEM20123}.
    
    This short historical introduction demonstrates how important and complex the primary packaging stage has become in all consumer goods, but especially in perishable ones. The book \citet{PAINE1990} expresses the main needs of primary packaging in these categories:
    \begin{itemize}
        \item \textbf{Containment} - the product has to stay safely inside of the package from the end of the packaging line to the customer;
        \item \textbf{Protection and preservation} - the package has to resist to mechanical damage and deterioration by different climates it faces from the factory to the customer;
        \item \textbf{Communication} -  legal requirements of labelling, instructions about the handling and storage and marketing purposes;
        \item \textbf{Machinability} - need for a good performance in filling and closing operations;
        \item \textbf{Convenience and use} -  primary packaging must provide convenience at all stages from the packaging line, through warehousing to distribution, as well as satisfying the needs of the user of the product;
    \end{itemize}
    \subsubsection{New Possibilities with Robotics}
    As discussed in Section \ref{ref_measur} robots are bringing new possibilities to the packaging industry. Even if the first applications were in the tertiary packaging, nowadays the primary packaging is impacted by industrial robotics. “The bakery/snack environment is a challenge in that there is often flour, and in some cases oil, to deal with. This means the machines must be easy to clean, and be food-safe when handling primary goods. Tooling has to be hygienic, as well as gentle on the product,” says Craig Souser, president, JLS Automation, York, PA in \citep{PakagingRobotics}. These handling capabilities were one of the obstacles in the first applications of robots in food primary packaging.
    
    Industrial robots are used in a wide spectrum of applications. The main applications are still today however, welding (25\%) and assembly (33\%) applications. Food processing, for example is still a minor application area for robots, accounting for only about 5\%, stated in 2008 \citep{Mahalik2008}.
    Despite that, some reasons are pushing the packaging research and development in this way: 
    \begin{itemize}
        \item [(i)] robots are becoming easier to handle
        \item [(ii)] robotization improves productivity
        \item [(iii)] scarcity of labor and health issues
        \item [(iv)] lean manufacturing
    \end{itemize}
    The food processing industry is showing increased interest in robotics due to the issues such as hygiene, labor shortages and cost \citep{mahalik2009}.
    As demonstrated above, in primary packaging the end-effector plays a vital role in terms of hygiene and food shape preservation. Let's see in the following examples all the benefits and the parameters involved in the application of industrial robots in the first stage of the packaging line.
    \subsubsection{Applications}
    One of the benefits mentioned previously is hygiene. For instance, in the meat industry, automation and robotics can improve the microbial condition of processed meat by removing staff from the production and primary packaging process \citep{clausenv2002}. Furthermore, reducing the numbers of staff in the plant decrease the costs of preserving hygiene \citep{PURNELL2013304}.
    
    In 2014, Denso Robotics announced a washable six-axis robot with a sterilisation-resistant surface that is suitable for use in the pharmaceutical, food, electronics and medical industries. 
    The robot VS H2O2/UV has a rounded shape with no external screws, thus avoiding build-ups of residue or dirt. This surface is made to resist hydrogen peroxide or ultraviolet light sterilisation techniques \citep{drives&control2014}.
    Another robot able to withstand acidic and alkaline food industry cleaner, used in daily wash-down operations, is the Fanuc M-430iA. This Robot is optimized for high-speed, sustained picking and packing, even in primary food handling. Typical products are food, beverages, candy, medical supplies, cosmetics and office supplies \citep{FanucM430iA}.
    
    Another application of washable robots is meat processing and packaging. At Georgia Tech's Research Institute a washable robot has been developed to package the raw meat coming from the conveyor. In just one second "the robot picks up the product, places it aesthetically on the tray and allows visual inspection" research engineer Jonathan Holmes stated \citep{johnteresko2008}.
    
    Hygiene is not always only for the product to be packaged itself, but also for the operators. Some of the driving influences encouraging the development of robotics and automation in food production stem from human contact with the product and, more importantly, the removal of people from some hazardous work conditions \citep{k.khodabandehloo1993}. For meat processing and packaging for example, keeping low temperatures along the whole supply chain can improve the shelf life of the product, but can be dangerous and difficult to maintain the focus and the precision for the operators. In some cases other factors can lead to an hazardous packaging environment. This is the case of pharmaceutical packaging. About 8 million U.S. healthcare workers are potentially exposed to hazardous drugs, including pharmacy and nursing personnel, physicians, operating room personnel, environmental services workers, workers in research laboratories, veterinary care workers, and shipping and receiving personnel \citep{NIOSH2020}. At Mission Hospital in Asheville, North Carolina, the St\"aubli XT-60L UL robot fills bags and syringes with drugs through the RIVA compounding system. 8 million doses have been filled by the RIVA automated systems \citep{tanyam.anandan2019}. 
    
    The Talon\textsuperscript{\textregistered} Pick and Place System integrates hygienic and accurate loading with high flexibility \citep{Talon}. Flexibility, as mentioned in \ref{ref_measur} is another enormous benefit that industrial robotics are bringing to the primary packaging. The Talon\textsuperscript{\textregistered} Pick and Place System is the perfect example of the flexibility of the modern Delta Robots. Just changing the vacuum gripper the numbers of products that can be packaged is impressive. Steak filets, raw poultry, cheese rounds, burger patties, pork chops, tortilla stacks, seafood, sliced bread, cookies are only some of the possibilities in the food primary packaging. One of the tasks accomplished by this robot is the assembly and packaging of sandwich with speed and hygiene. One more example of flexibility is The Human Touch Robotics Ltd company. They developed universal adaptive gripping solution that can handle objects of different type, size, shape, stiffness and weight \citep{thehumantouchroboticsltd}. For example, their grippers are able to package different types of sushi in one box.
    Flexibility of robots can drastically help firms to reduce their individual products' costs. Flexible or modular designs of robots ensure fast and easy integration into existing product line \citep{lowcostRobots}. In the following text from \citet{abb2016} two examples of how robots' flexibility has made primary packaging more efficient and profitable are analyzed: 
    
    \blockquote{
    Food and beverage industry has seen competition increase, labor costs rise and margins drop during the last decades. At Interbanket’s facility, a Dutch bakery since the 1960s, the necessity to increase the production capacity was necessary to keep the company alive. The major challenge was the large amount of cookies it needed to package. On the one line that was slated for robotic automation it makes 19 different kinds of products, packed 30 different ways into a tray, including flat, at an angle of 45 degrees and at an angle of 90 degrees. Sales Director Ron Haaring said: “We needed incredible flexibility to transform the line quickly between batches of cookies, and the only way it could be built was with robots.” In the early 1980s Interbanket had a daily production output of 1,500 kg of dough, and in 2016, thanks to the possibilities offered by the robots in their primary packaging, it went through more than 3,500 kg of dough each day, using fewer people in the same amount of time.}
    
    \blockquote{    
    Martin Paul, head of the packaging department at Staedtler. “I realized immediately that we could make the packaging of our products significantly more efficient using this robot.” From September 2015, with the capability of ABB FlexPicker system, using integrated camera to recognise the products position and suction grippers, they significantly increased the speed of pick and place of pens and erasers in the blister packs. Now they are able to package 145 different products with the same system. This mainly thanks to the short changeover times the robots offers. As mentioned in \ref{ref_measur}, changeover and training times played a big role in the flexibility of Staedtler packaging system. “Especially during the peak season, the time when school starts, we require extreme flexibility to be able to respond quickly to possible shortages,” Paul says. “Thanks to the short changeover times of the FlexPicker system, this is no issue at all.”}
    
    In Figure \ref{fig_primary} some of the robots used for Primary Packaging, mendioned in the text, are represented.
    \begin{figure}[h]
        \centering
        \includegraphics[width=0.9\textwidth]{Images/MainStages/PrimaryRobots.jpeg}
        \caption{Primary Packaging Robots}
        
        \label{fig_primary}
    \end{figure}
 
\newpage
    
\subsection{Robotics in Secondary Packaging [Shivam]}
\subsubsection{introduction}
\label{ref_secondary}
There is one old industry pharse that states " secondary packaging is the last thing on your mind, but the first thing your customer sees". so it is intend to protect not only the product, but also the primary packaging of the product.secondary packaging is additional to the primary packaging and that is used for protection and collation of individual units during storage, transport and distribution. secondary packaging is the packaging that holds togather the individual units of a good.it is designed not so much to hold the good that is the job is primary packaging,so much as a means to deliver mass quantities of the good to the point of sale or end user.secondary packaging may be removed from the item without changing the qualities or attributes of the good.secondary packaging serves the role of facilitating the transportation of a good from the manufacture to the end user.however, it is frequently not seen by the ultimate recipient of the product.secondary packaging should be tough enough to protect the item,and yet easy enough to open that employess engaged in restoring can acess product without damaging it. 

 \includegraphics[width=8cm,height=6cm]{Images/secondary.jpg}


\subsubsection{needs of secondary packaging}
\label{ref_secondary}
The role of secondary packaging goes beyond building an attractive final package.it protects products moving through the supply chain and also preserves the appearance and integrity of the primary package. in addition to its provocative functions, secondary packaging plays an increasingly visible role in branding and display strategies.secondary packaging protects product but it also provides opportunities to inform customers, shared brand massaging and stand up for the environment.secondary packaging serves multiple functions for purchaser and their online or retail customers.nowadays globally numbers goods are transported daily and these goods needs to be handled and transported safely hence secondary packaging plays an important roe in safeguarding the primary product and hence it is important for safe transportation of products.  

A study conducted at Clemson University, sponsored by a reusable packaging company, Rehrig Pacific Company, investigated if a unique shipping shell/crate design with on‐message and product branding (direct colour print onto the shell) increased brand awareness, attention and purchase intent when placed directly onto the retail shelf. Reusable packaging for 2 L bottles of carbonated soft drink packaged in proprietary reusable shells with a multicolour logo of the major US beverage company was chosen for the study. The branded proprietary reusable shell was evaluated against a standard reusable shell in the industry used by the same company. Testing was conducted using state‐of‐the‐art eye tracking hardware and CUshop™, a full immersion consumer retail experience laboratory. Data captured from 89 study participants indicated a strong preference for the new branded reusable shell. Statistical analysis demonstrated a 47\% increase in eye fixations on the branded display and an increase in fixation duration, a measure of how long consumers looked at the display, of 34\%\big(p < 0.05)More significantly, an increase in the visual saliency of the primary package, the 2 L bottle, was illustrated by an increase in fixation duration of 46\%(p < 0.05). and an increase of the number of fixations of 54\%(p < 0.01) when displayed in the branded reusable shell. Thus, results indicate that in‐store marketing incorporating a unique secondary packaging may lift brand awareness and increase purchase intent.

\subsubsection{application of robots in secondary packaging}
\label{ref_secondary}
Advances in the areas of collaborative operation, machine safety, simplified programming, space-saving designs, machine vision, gripper designs and sanitation allow robots to work in areas that were once dominated by manual processes,” Dean Elkins, segment leader, material handling, Yaskawa America Inc., Motoman Robotics Division, Miamisburg, OH.

According to robot industry statistics, deployment of robotics in North America in the food and consumer goods segment reported record setting growth of a 47.6\% increase in the number of units shipped from 2017-2018 (total 2,946).

While all processing/packaging applications charted significant increase (processing, primary packaging, secondary packaging and transport packaging) secondary packaging charted the largest and fastest growth.The study, “Robotics 2019: Innovation 2 Implementation,” reveals that five years ago, 48\% of respondents reported using robots for secondary packaging. In 2018, that number jumped to 70\%.Because of recent advances in robotic gripper and control systems, high-speed pick and place is now a viable option for automating product packaging, according to Carl Vause, CEO, Soft Robotics Inc., Bedford, MA. “One common area we see is primary packaging from a conveyor into a flow wrapping system, and then secondary packaging of product into cartons. These solutions can run at high speeds across multiple SKU lines without product damage.” as mention above secondary packaging plays huge role in distribution, display and branding so it is very important that secondary packaging should be attractive and good..Humans have long been used for packaging work but nowadays to reduces the human efforts and for better efficiency and accuracy robots are used for the secondary packaging.

\includegraphics[width=10cm,height=8cm]{Images/pharmaceutic-filter-assembly-machine.jpg}

This custom pharmaceutic filter assembly machine is a packaging machine that allows temporary packaging of pharmaceutical filters between two production steps. A stack of filters is automatically constituted by a 6-axis robot at the output of the upstream machine. A cardboard equipped with a plastic bag is brought into the robotic cell on a motor-driven roller conveyor. The sack is pressed on the sides of the cardboard box by a technician and the box is oriented to allow insertion of the preformed stack.The assembly is made to limit the grounding of the robotic cell and allow easy cleaning of the machine between two production batches.

\includegraphics[width=8cm,height=6cm]{Images/robots-packing.jpg}

A seamless and clean transition with minimal human contact from process and picking to primary packaging is a pre-requisite. High speed 4 axis delta robots and the more traditional small 6 axis robots combine to ensure desired throughput and high quality.

Higher payload delta and 4 or 6 axis robots then complete the packing process by  placing the primary packed product into the secondary packaging which can be cases, trays, crates or shelf ready packaging.
Should throughput permit the packing robot can even stack the cases if required hence enabling further savings on cost and floor space.

\subsection{Robotics in Tertiary Packaging}
\label{ref_tertiary}
 As explained in the Introduction of this Section, palletizing is the last step in packaging before a product leaves the plant and the first step (de-palletizing) when a product enters the factory. Because palletizing and de-palletizing use the same robotics (excluding the software algorithms), the following sections will use the term palletizing to refer both processes interchangeably. The term palletizing itself describes "grabbing, handling, stacking and unstacking tasks." \citep{zi2017}. The goods are stacked onto sheets or pallets of different sizes. The sizes of the pallets follow the guidelines of the \cite{ISO6780} or special measurements.
 
 Robotics in tertiary packaging were first discussed theoretically in the literature in the 1980's. One of the first papers discussing the integration of robots in palletizing is \cite{hall1989}. He describes the achievements of the University of Concinnati in developing a robot that can stack a pallet with objects of mixed size and weight. The robot determines where to place the parcels on the pallet by applying a heuristic. To handle the package two inputs are provided to the robot: The packages position and orientation. The robot must recognize these parameters and plan a path to move it from its source to its destination on the pallet. At the time, the recognition of position and orientation was challenging as visual localization was still in research.
 
 Since then packaging has taken large steps in increasing its capabilities. History goes along with the development of three different types of palletizing: Manual, mechanized and robotic \citep{abdou1992}. In manual palletizing workers stack the pallets by hand. This monotonous work was identified to lead to severe back injuries and musculoskeletal disorders \citep{dempsey1999}. At the same time it leads to a high psychological workload. Because of invoked limitations for strains put on workers, reorganisation is required. That involves the decrease of package weights, special training for the workers or the increase of the size of the work force \citep{elfeituri2012}. Because of the rising cost of the work force one possible substitution for stacking packages manually is robotics, which are decreasing in cost \citep{zi2017}. In case of stacking true mixed pallet loads - mix-sized cases on each layer - robots overcome the limitations of conventional/mechanical palletizers, that can only stack packages of the same size per layer and are limited to certain package sizes \citep{columbia2009}.
 
 The need to substitute manual palletizing is reflected by the development of the market. Between 2012 and 2016  63 large full automated warehouses were build relying on robot technologies alone \citep{buck2017}.
 
 According to Alan Spreckley, an expert for palletizing robotics at ABB, robotics meet the new requirements of the market. He states that the trend to single person households and the reaction of customers on special offers lead to a scenario where palletizing patterns need to be changed quickly and efficient. Virtual commissioning and the real-time offline programming ensures that quickly changing demands for palletizing strategies can be implemented. \citep{PE2019Alan} 
 \\
 \\
 \textbf{Palletizing robots in scientific literature}
 
 As previously mentioned, palletizing robots have been researched since the beginning of the 1980's. Because research discussed here should still have a high impact on today's developments only work after 2000 is listed.
 
 In \citeyear{lambiase2003} \citeauthor{lambiase2003} criticise the price of electrically and hydraulically powered solutions, making it close to impossible for small and medium entrepreneurs to acquire them. Therefore, a palletizer is developed relying on pneumatics alone to unlock the possibility for cheaper pricing. The palletizer has the form of a portal robot and is able to lift weights up to 50kg to a maximum height of 2m. For the proposed robot, the main characteristics are then analysed according to the "Taguchi method". They conclude that the most difficult challenge lies in the control of the vertical axis. Still, the proposed accuracy and repeatability are within limits for the proposed industrial application.
 
 In a technical report of their work \cite{pires2003} explain their effort to develop and test a de-palletizing robot for non-flat ceramics. The robot, an ABB Foundry Plus type, must overcome several difficulties arising from the objects' dimensions, the needed low cycle times and regular changes of the product. For the application a user interface is developed, which makes it possible to adjust in case of non-fully stacked pallets. As the robot only determines the position of an object by infrared it can not distinguish between different models by itself. Therefore, a training mode enables the introduction of new models. This way an average cycle time of 12s per piece (5pcs/min) is achieved.
 
 \cite{pires2004} report a case study using an ABB IRB 4400 for palletizing purposes in a automobile side-window production. The robot picks up a window pain, and transfers it to a pallet. Optical sensors analyse the pallets height, angel and size before the transfer to ensure no damages to the glass occurs during the process. Additional to training and changing models in \citeauthor{pires2003} previous work online monitoring and adjusting is supported. Cycle times of 9s/pc are possible.
 
 In proceedings of the International Conference on Automation and Logistics \cite{zhang2008} present their work on a 4-DOF robot. The robot has to transfer cases from two conveyors to two pallets. Because the arrangement of the objects and the paths taken by the end-effector play a major role, the positions related to the robot and the motions palletizing are optimized. The optimization is conducted by simplifying a "3-D path planning problem to a one parameter planning problem".
 
 \cite{bloss2010} in a case study applies a hybrid system including a robotic and an automatized palletizer in a candy factory, where manual stacking was the standard. The hybrid approach is taken because of the needs for (1) a high output and (2) high flexibility of the production. A manual lane still remains for low volume orders and unidentified packages. For the robotic part, three Motoman EPL 160s with four degrees of freedom are used. For each product, the stacking pattern is predetermined and the robots are programmed to handle a limited number of products. The products are transferred by a vacuum gripper to a slip-sheet. If a sheet is stacked up, a conveyor takes it away and the robot lays a new slip-sheet in place. That way each of the robots can palletize 25pcs/min.
 
 In proceedings of the International Symposium on Robotics \cite{wurll2016} lists the biggest competitors on the market for palletizing: Witron, Dematic, SSI Schäfer, Vanderlande, Syleps, Symbotic (Axium), TGW, KUKA/Swisslog. Robots of these manufacturers have nearly the same performance. Yet, three new technologies can be identified: 
 \begin{itemize}[noitemsep]
     \item [1] \textbf{Down-Stacking} - where the pallet is lowered during the process to minimize the path of the case.
     \item [2] \textbf{A robotless solution for truly mixed palletizing} - instead of an articulated robot the cases are shoved onto the pallet.
     \item [3] \textbf{Visual detection of the package} - at the end of the conveyor the positioning becomes obsolete as the robot adjusts to the position of the object 
 \end{itemize}
 The paper also presents two Figures (Figure \ref{fig_pallet1} and Figure \ref{fig_pallet2}) that show the performance of the company's palletizers derived from a market research from Swisslog in 2015, ending by presenting two case studies of Coca Cola and dm, that already apply these robotic palletizing systems.
 \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{Images/MainStages/ComparisonPalletizers_1.png}
    \caption{Comparison of robotic palletizers by price and performance \citep{wurll2016}}
    \label{fig_pallet1}
 \end{figure}
 \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{Images/MainStages/ComparisonPalletizers_2.png}
    \caption{Comparison of robotic palletizers by Performance, Price, Price-Performance-Ratio, Quality, No. of References, Suitable for Arctic Environment and Functionality \citep{wurll2016}}
    \label{fig_pallet2}
 \end{figure}
 
 In a letter \cite{krug2016} present a study based on the autonomous picking and palletizing (APPLE) robot. This new approach is essentially a grasping device mounted on a fork lift. This combination enables picking and palletizing at the same time. They try to assess the possibility to use this new robot for substituting commissioning. Moving around in an environment with humans also requires safety measures regarding the recognition of workers.
 
 \cite{zi2017} present a new 6-degrees of freedom cable actuated serial-parallel robot for palletizing. Opposing the previously presented robots, the robot moves by actuating cables. The kinematics, stability and workspace envelope of the robot are studied. The workspace varies heavily depending on the workload. However, they conclude that with a sufficient weight the robot could fulfil requirements for palletizing.
 
 \cite{khan2018} places the palletizing robots, together with robots for other tasks, in the context of the food and beverage industry. A broad overview of robots in the sector is presented. Still, the robots face challenges typical for the food industry that must be considered when applying them, like food safety and hygiene. At the same time the authors present identified trends and opportunities of the sector.
 
 With a report about their work \cite{cristoiu2018} provide insight into the beverage industry. For the purpose of stacking up bottles a new end-effector is designed which is mounted at the arm of an ABB IRB 660. They point out that the conveyor providing the bottles plays an important role in the handling, designing it as well and integrating it into their simulations.
 
 In \citeyear{martini2019} \citeauthor{martini2019} supply an interesting contribution to the mechanical optimization. Developing a balancing system and installing it in a robot. The results show a significant reduction in energy consumption.
 
 The newest contribution to this field is from \citep{lamon2020}. Instead of completely replacing manual picking a collaborative approach is taken. An autonomous ground vehicle with a manipulator and several sensors is studied to stack cases autonomously or in cooperation with human assistance. The robot calculates the optimal placement of the packages, taking physical and psychological workload from the worker. In a preliminary case study the researchers demonstrated the robot shows a positive influence on ergonomics and an increase in the productivity.
 
 As we can see since the 2000 tertiary packaging has had several different appearances in the literature. The mentions reach from overviews of robotics, over analysing of existing robotic systems to the development of completely new solutions for packaging.
 \pagebreak
\subsection{Case Study: Amazon}
\label{ref_casestudy}

Amazon Inc. is incorporating robotics into packaging at some of it's distribution warehouses. As a pilot program, machines are being installed in select warehouses improve packaging efficiency and speed. Each item to be packed is scanned and the machine creates and packs the item in a custom cardboard package that fits it exactly. While this presents an exciting opportunity for innovation and efficiency improvements, opponents are concerned with the potential job losses that could occur from implementation automation and machines of this nature. As \cite{AmazonRobotsReuters} states, Amazon tries to address this problem:

\blockquote{“We are piloting this new technology with the goal of increasing safety, speeding up delivery times and adding efficiency across our network,” an Amazon spokeswoman said in a statement. “We expect the efficiency savings will be re-invested in new services for customers, where new jobs will continue to be created.”}
While this may be the case that workers can be trained with new skills and moved to other portions of the company it is important to consider the impacts automation and robotics like this has on the workforce and what society can do to address it.https://www.overleaf.com/project/5fae434b85122d827be9b81f

Per \cite{AmazonRobotsReuters} Amazon claims that a fully robotic future is far off but this statement only portents the inevitable. The future is moving toward more automation and robotics and eventually it should be possible to automate the entire supply chain. According to \cite{AmazonRobotsReuters} the machines that Amazon has installed are four to five times faster than an equivalent human operator and can process 600 to 700 boxes per hour. These machines require only a person to load appropriate orders, replace machine supplies, and fix the occasional jam in the machine.

One of the most challenging aspects of robotizing packaging is picking up fragile goods of differing sizes accurately without crushing or breaking them. Many companies and laboratories are spending a significant amount in research and development trying to solve this problem. Amazon is experimenting with picking solutions for delicate goods but none have been ready for large scale adoption yet. Additionally as more machines are added, more and more people will be required to service and maintain them which will lead to a a larger need for skilled jobs in robotics and automation. 

While Amazon is leading the field in the usage of robotics in packaging as demonstrated by this case study it is also important to examine the challenges and drawbacks of implementing robotic packaging.The robots that Amazon has installed in this case per \cite{AmazonRobotsExtremeTech} will remove an estimated 24 jobs per warehouse. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{Images/MainStages/Amazon-Warehouses.jpg}
    \caption{Growth Of Amazon Warehouses in the U.S. \citep{AmazonRobotsExtremeTech}}
    \label{fig_amazon1}
 \end{figure}
 
Figure \ref{fig_amazon1} shows the near exponential growth of the number of Amazon warehouses since the 2000s. Given the current number of warehouses, implementing this system in every warehouse could cause a loss of nearly 4,200 jobs. Amazon relies heavily on subsidies from local and state governments for creating job opportunities in different states and these kinds of job losses can hurt their favorable standing with many of these states and organizations. 

The shift to robotized packaging and automation in other fields indicates the world is at another critical crossroads of transformation. Similar to the industrial revolution when many jobs and people moved to factories, society is at a point where manual labor and blue collar work will begin to decline and be replaced by a new wave of skilled and technical labor jobs and a fundamental economic shift. Many proposals exist for how to handle the job loss caused by automation which is a topic for further research and discussion.

Amazon is constantly striving to increase automation in their factories to improve speed, efficiency, and worker safety. As they move toward the fully automated factory of the future it is important to consider the people left in the wake of this shift. Amazon has tried a variety of worker training efforts to train existing workers for different skill sets including servicing robots, programming, and other more skilled tasks but these programs have had limited success. This case study shows that robotized packaging can bring many benefits including faster packing, improved worker safety, reduced waste, and higher efficiency but can also cause job loss which needs to be proactively solved to ensure future societal prosperity. 